//
//  ZBYShieldingGestureViewController.m
//  OCBase2021
//
//  Created by 周保勇 on 2021/4/26.
//

#import "ZBYShieldingGestureViewController.h"
#import "ZBYCollectionView.h"
#import "ZBYShieldingGestureCollectionViewCell.h"

@interface ZBYShieldingGestureViewController () <UICollectionViewDelegate, UICollectionViewDataSource>

@end

@implementation ZBYShieldingGestureViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UICollectionViewFlowLayout * layout = [UICollectionViewFlowLayout new];
    layout.itemSize = ZBYSize.screenSize;
    layout.minimumLineSpacing = 0;
    layout.minimumInteritemSpacing = 0;
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    
    ZBYCollectionView * collectionView = [[ZBYCollectionView alloc] initWithFrame:self.view.bounds collectionViewLayout:layout];
    collectionView.pagingEnabled = YES;
    collectionView.delaysContentTouches = NO;
    collectionView.backgroundColor = [UIColor whiteColor];
    [collectionView registerClass:ZBYShieldingGestureCollectionViewCell.class forCellWithReuseIdentifier:ZBYShieldingGestureCollectionViewCell.description];
    collectionView.delegate = self;
    collectionView.dataSource = self;
    if (@available(iOS 11, *)) {
        collectionView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }
    else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    [self.view addSubview:collectionView];
}

// MARK: - UICollectionViewDelegate, UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 10;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ZBYShieldingGestureCollectionViewCell * cell = [ZBYShieldingGestureCollectionViewCell cellWithCollectionView:collectionView indexPath:indexPath];
    cell.backgroundColor = [UIColor random];
    return cell;
}

@end
