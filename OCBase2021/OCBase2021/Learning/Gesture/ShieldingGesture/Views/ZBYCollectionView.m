//
//  ZBYCollectionView.m
//  OCBase2021
//
//  Created by 周保勇 on 2021/4/27.
//

#import "ZBYCollectionView.h"
#import "ZBYShieldingScrollView.h"

@implementation ZBYCollectionView

/// 方法1：在UIScrollView上的子视图上禁止滚动事件
//- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
//    UIView * hitView = [super hitTest:point withEvent:event];
//    if ([hitView isKindOfClass:ZBYShieldingScrollView.class]) {
//        self.scrollEnabled = NO;
//    }
//    else {
//        self.scrollEnabled = YES;
//    }
//    return hitView;
//}

/// 方法2：下面两个方法与上面的一个方法达到的效果相同
- (BOOL)touchesShouldBegin:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event inContentView:(UIView *)view {
    if ([view isKindOfClass:ZBYShieldingScrollView.class]) {
        self.scrollEnabled = NO;
    }
    else {
        self.scrollEnabled = YES;
    }
    return [super touchesShouldBegin:touches withEvent:event inContentView:view];
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    self.scrollEnabled = YES;
}

@end
