//
//  ZBYShieldingGestureCollectionViewCell.m
//  OCBase2021
//
//  Created by 周保勇 on 2021/4/26.
//

#import "ZBYShieldingGestureCollectionViewCell.h"
#import "ZBYShieldingScrollView.h"

@interface ZBYShieldingGestureCollectionViewCell () <UIGestureRecognizerDelegate> {
    UILabel * _shieldingTapView;
    ZBYShieldingScrollView * _shieldingScrollView;
    UITapGestureRecognizer * _tapGesture;
}

@end

@implementation ZBYShieldingGestureCollectionViewCell

+ (instancetype)cellWithCollectionView:(UICollectionView *)collectionView indexPath:(NSIndexPath *)indexPath {
    static NSString * identifier;
    identifier = NSStringFromClass([self class]);
    ZBYShieldingGestureCollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    return cell;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initSubviews];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setModel:(NSObject *)model {
    _model = model;
    
}

- (void)initSubviews {
    _tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureEvent:)];
    _tapGesture.delegate = self;
    [self.contentView addGestureRecognizer:_tapGesture];
    
    _shieldingTapView = [[UILabel alloc] init];
    _shieldingTapView.userInteractionEnabled = YES;
    _shieldingTapView.numberOfLines = 0;
    _shieldingTapView.text = @"子视图屏蔽父视图手势事件";
    _shieldingTapView.backgroundColor = [UIColor random];
    [self.contentView addSubview:_shieldingTapView];
    [_shieldingTapView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(ZBYSize.navigationBarHeight);
        make.left.mas_equalTo(0);
        make.width.mas_equalTo(ZBYSize.screenWidth);
        make.height.mas_equalTo(100);
    }];
    
    _shieldingScrollView = [[ZBYShieldingScrollView alloc] init];
    _shieldingScrollView.userInteractionEnabled = YES;
    _shieldingScrollView.numberOfLines = 0;
    _shieldingScrollView.text = @"在UIScrollView子视图上禁止滚动事件";
    _shieldingScrollView.backgroundColor = [UIColor random];
    [self.contentView addSubview:_shieldingScrollView];
    [_shieldingScrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_shieldingTapView.mas_bottom).offset(20);
        make.left.mas_equalTo(0);
        make.width.mas_equalTo(ZBYSize.screenWidth);
        make.height.mas_equalTo(100);
    }];
}

- (void)tapGestureEvent:(UIGestureRecognizer *)sender {
    NSLog(@"tapGestureEvent-%f", _shieldingTapView.y);
    [ZBYMBProgressTool showText:@"tapGestureEvent" animated:YES];
}

// MARK: - UIGestureRecognizerDelegate


/// 子视图屏蔽父视图的手势
/// @param gestureRecognizer 手势
/// @param touch 点击所在的视图
/// @return ture 响应手势，false 不响应手势
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    if ([gestureRecognizer isKindOfClass:UIPanGestureRecognizer.class]) {   // 拖动手势禁止
        return NO;
    }
    if (gestureRecognizer == _tapGesture) { // 点击事件
        BOOL isDescendantShieldingTapView = [touch.view isDescendantOfView:_shieldingTapView];
        return !isDescendantShieldingTapView;
    }
    else {
        return YES;
    }
}

@end
