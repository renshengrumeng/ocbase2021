//
//  UIScrollView+GestureConflict.h
//  OCBase2021
//
//  Created by 周保勇 on 2021/4/27.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIScrollView (GestureConflict)

@end

NS_ASSUME_NONNULL_END
