//
//  ZBYLeftBackCollectionViewCell.m
//  OCBase2021
//
//  Created by 周保勇 on 2021/4/27.
//

#import "ZBYLeftBackCollectionViewCell.h"

@interface ZBYLeftBackCollectionViewCell () {
    UILabel * _textLabel;
    UISlider * _sliderView;
}

@end

@implementation ZBYLeftBackCollectionViewCell

+ (instancetype)cellWithCollectionView:(UICollectionView *)collectionView indexPath:(NSIndexPath *)indexPath {
    static NSString * identifier;
    identifier = NSStringFromClass([self class]);
    ZBYLeftBackCollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    return cell;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initSubviews];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setModel:(NSObject *)model {
    _model = model;
    
}

- (void)initSubviews {
    _textLabel = [[UILabel alloc] init];
    _textLabel.numberOfLines = 0;
    _textLabel.text = @"全屏侧滑返回\nUIScorllView\nUISlider\n相互之间的手势冲突";
    _textLabel.backgroundColor = [UIColor random];
    [self.contentView addSubview:_textLabel];
    [_textLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(ZBYSize.navigationBarHeight);
        make.left.mas_equalTo(0);
        make.width.mas_equalTo(ZBYSize.screenWidth);
        make.height.mas_equalTo(100);
    }];
    
    _sliderView = [UISlider new];
    _sliderView.maximumValue = 1;
    _sliderView.minimumValue = 0;
    [self.contentView addSubview:_sliderView];
    [_sliderView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(_textLabel.mas_bottom).offset(20);
        make.left.mas_equalTo(20);
        make.width.mas_equalTo(ZBYSize.screenWidth - 40);
        make.height.mas_equalTo(100);
    }];
}

@end
