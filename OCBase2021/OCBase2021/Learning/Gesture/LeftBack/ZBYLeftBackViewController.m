//
//  ZBYLeftBackViewController.m
//  OCBase2021
//
//  Created by 周保勇 on 2021/4/27.
//

#import "ZBYLeftBackViewController.h"
#import "ZBYLeftBackCollectionViewCell.h"

@interface ZBYLeftBackViewController () <UICollectionViewDelegate, UICollectionViewDataSource>

@end

@implementation ZBYLeftBackViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UICollectionViewFlowLayout * layout = [UICollectionViewFlowLayout new];
    layout.itemSize = ZBYSize.screenSize;
    layout.minimumLineSpacing = 0;
    layout.minimumInteritemSpacing = 0;
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    
    UICollectionView * collectionView = [[UICollectionView alloc] initWithFrame:self.view.bounds collectionViewLayout:layout];
    collectionView.pagingEnabled = YES;
    collectionView.delaysContentTouches = NO;
    collectionView.backgroundColor = [UIColor whiteColor];
    [collectionView registerClass:ZBYLeftBackCollectionViewCell.class forCellWithReuseIdentifier:ZBYLeftBackCollectionViewCell.description];
    collectionView.delegate = self;
    collectionView.dataSource = self;
    if (@available(iOS 11, *)) {
        collectionView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    }
    else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    [self.view addSubview:collectionView];
}

// MARK: - UICollectionViewDelegate, UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 4;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ZBYLeftBackCollectionViewCell * cell = [ZBYLeftBackCollectionViewCell cellWithCollectionView:collectionView indexPath:indexPath];
    cell.backgroundColor = [UIColor random];
    return cell;
}

@end
