//
//  ZBYGestureViewController.m
//  OCBase2021
//
//  Created by 周保勇 on 2021/4/26.
//

#import "ZBYGestureViewController.h"
#import "ZBYShieldingGestureViewController.h"
#import "ZBYLeftBackViewController.h"

@interface ZBYGestureViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, copy) NSArray * models;

@end

@implementation ZBYGestureViewController

- (NSArray *)models {
    if (_models == nil) {
        _models = @[
        @{@"title" : @"触摸事件、手势基础", @"subTitle" : @"触摸相关的类和方法"},
        @{@"title" : @"子视图和父视图手势冲突", @"subTitle" : @"子视图屏蔽父视图手势事件和在UIScrollView子视图上禁止滚动事件"},
        @{@"title" : @"全屏侧滑冲突", @"subTitle" : @"全屏侧滑、UIScrollView和UISlider间的滑动冲突"},
        @{@"title" : @"谈谈响应链", @"subTitle" : @"OC中的事件响应机制"}];
    }
    return _models;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITableView * tableView = [[UITableView alloc] initWithFrame:CGRectZero style:(UITableViewStylePlain)];
    tableView.delegate = self;
    tableView.dataSource = self;
    [self.view addSubview:tableView];
    
    tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];
}

// MARK: - UITableViewDelegate, UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.models.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString * identifier = @"123456";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:identifier];
        cell.backgroundColor = [UIColor whiteColor];
        cell.textLabel.textColor = [UIColor blackColor];
        cell.detailTextLabel.textColor = [UIColor blackColor];
    }
    cell.textLabel.text = self.models[indexPath.row][@"title"];
    cell.detailTextLabel.text = self.models[indexPath.row][@"subTitle"];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSDictionary * info = self.models[indexPath.row];
    NSString * title = info[@"title"];
    if ([title containsString:@"触摸事件、手势基础"]) {
        [self touchEventBase];
        [self gestureBase];
    }
    if ([title containsString:@"子视图和父视图手势冲突"]) {
        ZBYShieldingGestureViewController * shieldingVC = [ZBYShieldingGestureViewController new];
        shieldingVC.title = title;
        [self.navigationController pushViewController:shieldingVC animated:true];
    }
    if ([title containsString:@"全屏侧滑冲突"]) {
        ZBYLeftBackViewController * shieldingVC = [ZBYLeftBackViewController new];
        shieldingVC.title = title;
        [self.navigationController pushViewController:shieldingVC animated:true];
    }
    if ([title containsString:@"谈谈响应链"]) {
        [self eventResponseChain];
    }
}

// MARK: - 触摸事件、手势基础

- (void)touchEventBase {
    /*
     iOS中对事件处理的类是UIResponder；
     与其相关的继承链条：
     NSObject ->UIResponder ->UIView -> ...
                            |
                            ->UIViewController -> ...
     相关事件：
     touchesBegan：触摸开始
     touchesMoved：触摸移动
     touchesEnded：触摸结束
     touchesCancelled：触摸取消
     */
}

- (void)gestureBase {
    /*
     UITapGestureRecognizer：单机手势
     UIPanGestureRecognizer：拖动手势
     UILongPressGestureRecognizer：长按手势
     UISwipeGestureRecognizer：轻扫手势
     UIPinchGestureRecognizer：捏合手势
     UIRotationGestureRecognizer：旋转手势
     */
    
    UITapGestureRecognizer * tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapEvent:)];
    tapGesture.numberOfTapsRequired = 1;    // 设置点击次数
    tapGesture.numberOfTouchesRequired = 1; // 设置手势数量
    [self.view addGestureRecognizer:tapGesture];
    
    UITapGestureRecognizer * doubleGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doubleEvent:)];
    doubleGesture.numberOfTapsRequired = 2;    // 设置点击次数
    doubleGesture.numberOfTouchesRequired = 1; // 设置手势数量
    [self.view addGestureRecognizer:doubleGesture];
    
    // 双击事件失败，再识别单机事件
    [tapGesture requireGestureRecognizerToFail:doubleGesture];
    
    UILongPressGestureRecognizer * longGesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longEvent:)];
    longGesture.numberOfTouchesRequired = 1; // 设置手势数量
    longGesture.minimumPressDuration = 2;   // 长按手势生效持续最小时间
    [self.view addGestureRecognizer:longGesture];
    
    // 拖拽手势
    UIPanGestureRecognizer * panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panEvent:)];
    [self.view addGestureRecognizer:panGesture];
    
    // 轻扫手势
    UISwipeGestureRecognizer * swipeGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(swipeEvent:)];
    swipeGesture.direction = UISwipeGestureRecognizerDirectionLeft; // 设置轻扫方向
    swipeGesture.numberOfTouchesRequired = 1; // 设置手势数量
    [self.view addGestureRecognizer:swipeGesture];
    
    // 捏合手势
    UIPinchGestureRecognizer * pinchGesture = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(pinchEvent:)];
    pinchGesture.scale = 1.0;   // 设置在屏幕坐标中相对于触摸点的比例
    [self.view addGestureRecognizer:pinchGesture];
    
    // 旋转手势
    UIRotationGestureRecognizer * rotGesture = [[UIRotationGestureRecognizer alloc] initWithTarget:self action:@selector(rotEvent:)];
    rotGesture.rotation = 0.0;
    [self.view addGestureRecognizer:rotGesture];
}

- (void)tapEvent:(UIGestureRecognizer *)sender {
    switch (sender.state) {
        case UIGestureRecognizerStateBegan:
            NSLog(@"Began");
            break;
        case UIGestureRecognizerStateChanged:
            NSLog(@"Changed");
            break;
        case UIGestureRecognizerStateEnded:
            NSLog(@"Ended");
            break;
        case UIGestureRecognizerStateCancelled:
            NSLog(@"Cancelled");
            break;
        case UIGestureRecognizerStateFailed:
            NSLog(@"Failed");
            break;
            
        default:
            NSLog(@"other");
            break;
    }
}

- (void)doubleEvent:(UIGestureRecognizer *)sender {
    NSLog(@"doubleEvent");
}

- (void)longEvent:(UIGestureRecognizer *)sender {
    NSLog(@"longEvent");
}

- (void)panEvent:(UIPanGestureRecognizer *)sender {
    // 声明一个静态的变量记录拖拽视图时视图当前的中心点位置
    static CGPoint currentCenter;
    switch (sender.state) {
        case UIGestureRecognizerStateBegan:
            currentCenter = self.view.center;
            break;
        case UIGestureRecognizerStateChanged: {
            // 获取拖拽手势在某一个视图拖拽的距离
//            CGPoint translation = [sender translationInView:self.view];
//            [sender setTranslation:CGPointZero inView:self.view];
            break;
        }
        case UIGestureRecognizerStateEnded: {
            
            break;
        }
            
        default:
            break;
    }
    NSLog(@"panEvent");
}

- (void)swipeEvent:(UIGestureRecognizer *)sender {
    NSLog(@"swipeEvent");
}

- (void)pinchEvent:(UIPinchGestureRecognizer *)sender {
    // 声明一个静态的变量记录捏合时缩放值
    static CGFloat lastScale;
    switch (sender.state) {
        case UIGestureRecognizerStateBegan:
            lastScale = sender.scale;
            break;
        case UIGestureRecognizerStateChanged: {
            lastScale = sender.scale;
            break;
        }
        case UIGestureRecognizerStateEnded: {
            lastScale = 1;
            break;
        }
            
        default:
            break;
    }
    NSLog(@"pinchEvent");
}

- (void)rotEvent:(UIRotationGestureRecognizer *)sender {
    // 声明一个静态的变量记录捏合时缩放值
    static CGFloat lastRotate;
    switch (sender.state) {
        case UIGestureRecognizerStateBegan:
            lastRotate = sender.rotation;
            break;
        case UIGestureRecognizerStateChanged: {
            lastRotate = sender.rotation;
            break;
        }
        case UIGestureRecognizerStateEnded: {
            lastRotate = 0;
            break;
        }
            
        default:
            break;
    }
    NSLog(@"rotEvent");
}

// MARK: - 谈谈响应链

- (void)eventResponseChain {
    /**
     连接地址：https://juejin.cn/post/6844903665724030990
     http://sindrilin.com/2018/08/27/response_chain.html
     
     iOS Touches事件处理知识总结
     https://www.jianshu.com/p/44a5b59e7e85
     
     Responder一点也不神秘————iOS用户响应者链完全剖析
     https://blog.csdn.net/mobanchengshuang/article/details/11858217
     
     当用户的手指在屏幕上的某一点按下时，屏幕接收到点击信号将点击位置转换成具体坐标，然后本次点击被包装成一个点击事件UIEvent。最终会存在某个视图响应本次事件进行处理，而为UIEvent查找响应视图的过程被称为响应链查找，在整个过程中有两个至关重要的类：UIResponder和UIView
     
     响应者
     响应者是可以处理事件的具体对象，一个响应者应当是UIResponder或其子类的实例对象。从设计上来看，UIResponder主要提供了三类接口：

     向上查询响应者的接口，体现在nextResponder这个唯一的接口
     用户操作的处理接口，包括touch、press和remote三类事件的处理
     是否具备处理action的能力，以及为其找到target的能力

     总体来说UIResponder为整个事件查找过程提供了处理能力
     
     视图
     视图是展示在界面上的可视元素，包括不限于文本、按钮、图片等可见样式。虽然UIResponder提供了让对象响应事件的处理能力，但拥有处理事件能力的responder是无法被用户观察到的，换句话说，用户也无法点击这些有处理能力的对象，因此UIView提供了一个可视化的载体，从接口上来看UIView提供了三方面的能力：

     视图树结构。虽然responder也存在相同的树状结构，但其必须依托于可视载体进行表达
     可视化内容。通过frame等属性决定视图在屏幕上的可视范围，提供了点击坐标和响应可视对象的关联能力
     内容布局重绘。视图渲染到屏幕上虽然很复杂，但按照不同的layout方式提供了不同阶段的重绘调起接口，使得子类具有很强的定制性

     视图树的结构如下，由于UIView是UIResponder的子类，可以通过nextResponder访问到父级视图，但由于responder并不全是具备可视载体的对象，通过nextResponder向上查找的方式可能会导致无法通过位置计算的方式查找响应者

     查找响应者
     讲了这么多，也该聊聊查找响应者的过程了。前面说了，responder决定了对象具有响应处理的能力，而UIView才是提供了可视载体和点击坐标关联的能力。换句话说，查找响应者实际上是查找点击坐标落点位置在其可视范围内且其具备处理事件能力的对象，按照官方话来说就是既要responde又要view的对象。因为需要先找到响应者，才能有进一步的处理，所以直接从后者的接口找起，两个api：

     /// 检测坐标点是否落在当前视图范围内
     - (BOOL)pointInside:(CGPoint)point withEvent:(nullable UIEvent *)event;
     /// 查找响应处理事件的最终视图
     - (nullable UIView *)hitTest:(CGPoint)point withEvent:(nullable UIEvent *)event;
     
     通过exchange掉第一个方法的实现，很轻易的就能得出响应者查找的顺序：
     - (BOOL)zby_pointInside: (CGPoint)point withEvent: (UIEvent *)event {
         BOOL res = [self zby_pointInside: point withEvent: event];
         if (res) {
             NSLog(@"[%@ can answer]", self.class);
         } else {
             NSLog(@"non answer in %@", self.class);
         }
         return res;
     }


     */
}

@end
