//
//  ZBYMATeacher.h
//  OCBase2021
//
//  Created by 周保勇 on 2021/5/14.
//

#import <Foundation/Foundation.h>
#import "ZBYMAPerson.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZBYMATeacher : ZBYMAPerson

@end

NS_ASSUME_NONNULL_END
