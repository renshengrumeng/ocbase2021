//
//  ZBYMemoryAllocationViewController.m
//  OCBase2021
//
//  Created by 周保勇 on 2021/5/14.
//

#import "ZBYMemoryAllocationViewController.h"
#import "ZBYMAPerson.h"
#import "ZBYMATeacher.h"

#import <objc/runtime.h>
#import <objc/message.h>
#import <objc/objc.h>
#import <mach-o/dyld.h>

@interface ZBYMemoryAllocationViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, copy) NSArray * models;
@property (nonatomic, strong) NSObject * objc;


@end

@implementation ZBYMemoryAllocationViewController

- (NSArray *)models {
    if (_models == nil) {
        _models = @[
        @{@"title" : @"OC中的对象", @"subTitle" : @"OC运行后存在的对象"},
        @{@"title" : @"OC实例对象申请内存和系统开辟内存", @"subTitle" : @"排序的改变和获取"},
        @{@"title" : @"OC对象内存结构", @"subTitle" : @"OC对象内存结构"},
        @{@"title" : @"NSObject的isa和superClass", @"subTitle" : @""},
        @{@"title" : @"class_ro_t和class_rw_t的区别", @"subTitle" : @"区别和联系"},
        @{@"title" : @"OC中weak的底层解析", @"subTitle" : @"weak修饰的对象的存储和释放过程"},
        @{@"title" : @"isa指针分析", @"subTitle" : @"isa指针每一位代表的意义"},
        @{@"title" : @"AutoreleasePool原理", @"subTitle" : @"自动释放池的实现原理"},
        @{@"title" : @"Tagged Pointer", @"subTitle" : @"标记指针详解"},
        @{@"title" : @"栈中内存结构考察", @"subTitle" : @"iOS 栈中OC对象内存结构考察"},
        @{@"title" : @"Category", @"subTitle" : @"深入理解Objective-C：Category"},
        @{@"title" : @"Mach-O结构基础", @"subTitle" : @"Mach-O的详细结构"},
        @{@"title" : @"Mach-O加载与调用", @"subTitle" : @"Mach-O如何在载道内存并进行调用"},
        @{@"title" : @"IPA构建流程", @"subTitle" : @"OC代码编译打包为执行程序流程"},
        @{@"title" : @"IPA装载流程", @"subTitle" : @"应用启动流程"},
        @{@"title" : @"dealloc和对象释放", @"subTitle" : @"对象释放的流程"}
         ];
    }
    return _models;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
    UITableView * tableView = [[UITableView alloc] initWithFrame:CGRectZero style:(UITableViewStylePlain)];
    tableView.delegate = self;
    tableView.dataSource = self;
    [self.view addSubview:tableView];
    
    tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];
}

- (void)dealloc {
    NSLog(@"%@-dealloc", self.class);
}

// MARK: - UITableViewDelegate, UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.models.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString * identifier = @"123456";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:identifier];
        cell.backgroundColor = [UIColor whiteColor];
        cell.textLabel.textColor = [UIColor blackColor];
        cell.detailTextLabel.textColor = [UIColor blackColor];
    }
    cell.textLabel.text = self.models[indexPath.row][@"title"];
    cell.detailTextLabel.text = self.models[indexPath.row][@"subTitle"];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSDictionary * info = self.models[indexPath.row];
    NSString * title = info[@"title"];
    if ([title containsString:@"OC中的对象"]) {
        [self runtimeObject];
    }
    if ([title containsString:@"OC对象内存结构"]) {
        [self objectAllocParsing];
    }
    if ([title containsString:@"OC实例对象申请内存和系统开辟内存"]) {
        [self instenseMemoryAndSystemMemory];
    }
    if ([title containsString:@"isa和superClass"]) {
        [self isaAndSuperClass];
    }
    if ([title containsString:@"class_ro_t和class_rw_t的区别"]) {
        [self instenseMemoryAndSystemMemory];
    }
    if ([title containsString:@"isa指针分析"]) {
        [self isaDetail];
    }
    if ([title containsString:@"OC中weak的底层解析"]) {
        [self weakInitAndDealloc];
    }
    if ([title containsString:@"AutoreleasePool原理"]) {
        [self autoreleasePoolDetail];
    }
    if ([title containsString:@"Tagged Pointer"]) {
        [self taggedPointerDetail];
    }
    if ([title containsString:@"栈中内存结构考察"]) {
        [self stackMemory];
    }
    if ([title containsString:@"Category"]) {
        [self deepCategory];
    }
    if ([title containsString:@"Mach-O结构基础"]) {
        [self machOBase];
    }
    if ([title containsString:@"IPA构建流程"]) {
        [self ipaConstructionProcess];
    }
    if ([title containsString:@"IPA装载流程"]) {
        [self ipaLoadProcess];
    }
    if ([title containsString:@"dealloc和对象释放"]) {
        [self releaseProcess];
    }
    
}

// MARK: - OC中的对象

- (void)runtimeObject {
    /** OC中对象的分类：
     1.实例对象：（instance object）类初始化后的对象，用于存储成员变量（ivar变量）的值：
        存储：isa指针、成员变量(ivar变量)的值
     2.类对象：（class object）runtime在项目启动时，自己创建的类的单例对象，用于存储对象方法、对象方法调用信息、属性信息等；
        存储：
        2.1 isa指针
        2.2 superclass指针
        2.3 类的属性信息(@property)，如成员变量的类型、名称、存取偏移量等
        2.4 类的对象方法信息，如方法的名称、执行代码指针等
        2.5 类的协议信息（protocol）
        2.6 类成员变量的值
     3.元类对象：（meta class object）runtime在项目启动时，自己创建的类的单例对象，用于存储类方法、类方法调用信息、类属性信息等；
     详细：https://www.cocoawithlove.com/2010/01/what-is-meta-class-in-objective-c.html
     存储：
        3.1 isa指针
        3.2 superclass指针
        3.3 类属性信息，如成员变量的类型、名称、存取偏移量等
        3.4 类方法信息，如方法的名称、执行代码指针等
     */
    // 实例对象
    ZBYMAPerson * instance = [ZBYMAPerson new];
    ZBYMAPerson * instance2 = [ZBYMAPerson new];
    ZBYMAPerson * instance3 = [ZBYMAPerson new];
    // 类对象
    Class class = object_getClass(instance);
    // 元类对象
    Class metaClass = object_getClass(class);
    NSString * className = NSStringFromClass(class);
    const char * cClassName = [className UTF8String];
    Class metaClass2 = objc_getMetaClass(cClassName);
    // 根元类对象
    Class rootMetaClass = object_getClass(metaClass);
    // 根根元类对象
    Class rootRootMetaClass = object_getClass(rootMetaClass);
    NSLog(@"实例对象:%p-%p-%p", instance, instance2, instance3);
    NSLog(@"类对象:%p-%p-%p", class, instance2.class, [instance3 class]);
    NSLog(@"类对象:%p-%p-%p", [class class], [instance2.class class], [instance3 class]);
    NSLog(@"元类对象:%p-%p", metaClass, metaClass2);
    NSLog(@"\n%p 实例对象\n%p 类\n%p 元类\n%p 根元类\n%p 根根元类", instance, class, metaClass, rootMetaClass, rootRootMetaClass);
}

// MARK: - NSObject alloc的过程

- (void)objectAllocParsing {
    /**
     OC isa 和 Class：https://halfrost.com/objc_runtime_isa_class/
     OC对象的内存结构及其建立过程：http://www.javashuo.com/article/p-ukjaozjd-by.html
     OC源码和其它三方源码解读更新：https://github.com/draveness/analyze/tree/master
     */
    /** OC类中类对象内存构成：
     
     // 定义的Class类对象的声明
     typedef struct objc_class *Class;
     
     struct objc_object {   // 早期的objc_object定义
        Class _Nonnull isa  OBJC_ISA_AVAILABILITY;  // isa指针
        ...
     };
     
     struct objc_object { // objc2版本arm64架构下的的objc_object定义
         private:
             isa_t isa;

         public:
         ...
     }
     
     // objc_class继承自objc_object
     struct objc_class : objc_object {
         // Class ISA;  // isa指针，继承自objc_object
         Class superclass;  // 父类指针
         cache_t cache;             // 调用方法的缓存列表
         class_data_bits_t bits;    // class_rw_t * plus custom rr/alloc flags
         ...
     }
     
     struct cache_t {
         struct bucket_t *_buckets; //   一个散列表，用来方法缓存，bucket_t类型，包含key以及方法实现IMP
         mask_t _mask;  //  分配用来缓存bucket的总数
         mask_t _occupied;  //  表明目前实际占用的缓存bucket的个数
    }
     
     Cache的作用主要是为了优化方法调用的性能。当对象receiver调用方法message时，首先根据对象receiver的isa指针查找到它对应的类，然后在类的methodLists中搜索方法，如果没有找到，就使用super_class指针到父类中的methodLists查找，一旦找到就调用方法。如果没有找到，有可能消息转发，也可能忽略它。但这样查找方式效率太低，因为往往一个类大概只有20%的方法经常被调用，占总调用次数的80%。所以使用Cache来缓存经常调用的方法，当调用方法时，优先在Cache查找，如果没有找到，再到methodLists查找。
     
    struct bucket_t {
         private:
         cache_key_t _key;
         IMP _imp;  // 函数指针，指向了一个方法的具体实现
     
         // 最新版本
         private:
             // IMP-first is better for arm64e ptrauth and no worse for arm64.
             // SEL-first is better for armv7* and i386 and x86_64.
         #if __arm64__
             explicit_atomic<uintptr_t> _imp;
             explicit_atomic<SEL> _sel;
         #else
             explicit_atomic<SEL> _sel;
             explicit_atomic<uintptr_t> _imp;
         #endif
     
         inline IMP imp(UNUSED_WITHOUT_PTRAUTH bucket_t *base, Class cls)   // 函数指针，指向了一个方法的具体实现
     }
     
     typedef id _Nullable (*IMP)(id _Nonnull, SEL _Nonnull, ...);   // IMP定义
     
     struct class_data_bits_t {
         friend objc_class;

         // Values are the FAST_ flags above.
         uintptr_t bits;
     
         class_rw_t* data;
         class_ro_t *safe_ro;
     }
     
     struct class_rw_t {
          uint32_t flags;
          uint32_t version;

          using ro_or_rw_ext_t = objc::PointerUnion<const class_ro_t, class_rw_ext_t, PTRAUTH_STR("class_ro_t"), PTRAUTH_STR("class_rw_ext_t")>;    // 访问ro，还是访问ext
          class_rw_ext_t *ext;          // 方便读写的ext
          const class_ro_t *ro;         // 只读信息：编译时确定的信息

          method_array_t methods;       // 方法列表
          property_array_t properties;  // 属性列表
          protocol_array_t protocols;   // 协议列表

          Class firstSubclass;
          Class nextSiblingClass;

          char *demangledName;
     }
     
     struct class_ro_t {
         uint32_t flags;
         uint32_t instanceStart;    // 运行时可能更新
         uint32_t instanceSize;     // 运行时可能更新
         #ifdef __LP64__
         uint32_t reserved;
         #endif
         
         const uint8_t * ivarLayout;    // 运行时可能更新
         
         const char * name;
         method_list_t * baseMethodList;    // 方法列表（类对象存放对象方法，元类对象存放类方法）
         protocol_list_t * baseProtocols;   // 协议列表
         const ivar_list_t * ivars;         // 成员变量列表
         
         const uint8_t * weakIvarLayout;    // 弱引用
         property_list_t *baseProperties;   // 属性列表
         ...
     }
     
     struct property_t {
         const char *name;  // 属性的名字
         const char *attributes;    // 属性的描述
     };
     
     struct method_t {
        // “big”结构体：这是存储选择器、类型和实现的三个指针的传统表示。
         struct big {
             SEL name;
             const char *types;
             MethodListIMP imp;
         };
     private:
         // “small”结构体：这将存储到名称、类型和实现的三个相对偏移量。
         struct small {
             // name字段要么指向一个选择器(在共享缓存中)，要么指向一个selref(在其他地方)。
             RelativePointer<const void *> name;
             RelativePointer<const char *> types;
             RelativePointer<IMP> imp;

             bool inSharedCache() const {   // 是否在共享缓存中
                 return (CONFIG_SHARED_CACHE_RELATIVE_DIRECT_SELECTORS &&
                         objc::inSharedCache((uintptr_t)this));
             }
         };
     public:
        SEL name;   // 函数名
        const char *types;  // 发放类型编码（返回值类型、参数类型）
        IMP imp;    // 函数指针，指向方法的实现，也就是指向实现该方法的代码区
     }
     
     struct ivar_t {
         int32_t *offset;   // 实例变量距离对象的首地址的偏移量：运行时可能更新
         const char *name;  // 实例变量的名字
         const char *type;  // 实例变量的类型
         // alignment is sometimes -1; use alignment() instead
         uint32_t alignment_raw;
         uint32_t size; // 实例变量的内存大小

         uint32_t alignment() const {
             if (alignment_raw == ~(uint32_t)0) return 1U << WORD_SHIFT;
             return 1 << alignment_raw;
         }
     };
     */
    ZBYMAPerson * p = [ZBYMAPerson new];
    NSLog(@"%p", p);
}

// MARK: - OC实例对象申请内存和系统开辟内存

typedef struct {
    char c;     // 1 (1 + 7)
    double d;   // 8 8
    int i;      // 4 4
    short s;    // 2 2  最后22，取最大成员所占字节为8，最接近22的8的倍数为24，所以最后该结构体占24
} Struct1;

typedef struct {
    double d;   // 8 8
    int i;      // 4 4
    char c;     // 1 1 + 1
    short s;    // 2 2 最后16，取最大成员所占字节为8，16是8的倍数，所以最后该结构体占16
} Struct2;

- (void)instenseMemoryAndSystemMemory {
    /** 内存对其原则
     第一条：第一个成员的首地址为0
     第二条：每个成员的首地址是自身大小的整数倍
            第二条补充：以4字节对齐为例，如果自身大小大于4字节，都以4字节整数倍为基准对齐。
            例如：以前的成员内存对齐到13字节，成员本身是4字节，将13补齐为4的倍数，即为16；从16的以后的4个字节为该成员的存储地址；
                 如果以前的成员内存对齐到12字节，成员本身是4字节，因12是4的倍数，不需要补齐；那么12以后的4个字节为该成员的存储地址。
     第三条：最后以结构总体对齐。
             第三条补充：取结构体中最大成员类型倍数；以8字节对齐为例，如果超过8字节，都以8字节整数倍为基准对齐。（其中这一条还有个名字叫：“补齐”，补齐的目的就是多个结构变量挨着摆放的时候也满足对齐的要求。）
            例如：结构体中最大成员所占字节数为8，对齐后结构体所占的字节数为28，需要补齐为8的倍数，即为32；最后该结构体所占内存字节数为32；
                 如果结构体中最大成员所占字节数为8，对齐后结构体所占的字节数为32，不需要补齐；最后该结构体所占内存字节数为32。
     */
    NSLog(@"%ld-%ld", sizeof(Struct1), sizeof(Struct2));
    
    ZBYMAPerson * p = [ZBYMAPerson new];
    // isa              // 8
    p.name = @"zby";    // 8
    p.age = 32;         // 4 (4 + 4)
    p.height = 171;     // 8
    p.hobby = @"男";     // 8    36, 8的倍数为40，占用内存总数为40
    
    ZBYMATeacher * t = [ZBYMATeacher new];
    // isa              // 8

    /**
     class_getInstanceSize：获取的是类的实例对象的成员变量（ivar）所占用的大小(即实例对象实际使用的内存空间)
     (p.class)输出的是40，
     (t.class)输出的是8，
     malloc_size：获得实例对象的指针指向的内存大小(即实例对象占用的内存空间)，OC对象的整体是以16的倍数进行对齐，及不满足16的倍数需要补足为16的倍数
     ((__bridge const void *)(p))输出的是48
     ((__bridge const void *)(t))输出的是16
     */
    NSLog(@"%lu-%lu-%lu", sizeof(p), class_getInstanceSize(p.class), malloc_size((__bridge const void *)(p)));
    NSLog(@"%lu-%lu-%lu", sizeof(t), class_getInstanceSize(t.class), malloc_size((__bridge const void *)(t)));
}

// MARK: - isa和superClass

- (void)isaAndSuperClass {
    /**
     https://www.jianshu.com/p/496af9592d27
     在早期的Runtime里面，isa指针直接指向class/meta-class对象的地址，isa就是一个普通的指针。
     
     苹果从ARM64位架构开始，对isa进行了优化，将其定义成一个共用体（union）结构，结合 位域 的概念以及 位运算 的方式来存储更多类相关信息。isa指针需要通过与一个叫ISA_MASK的值（掩码）进行二进制&运算，才能得到真实的class/meta-class对象的地址。接下来，就具体探究一下苹果究竟是怎么优化的。
     
     OC中的isa和superClass指针走位图：
     1.isa指针走位：实例对象的isa指针 -> 类对象 -> 元类对象 -> 根元类对象 -> 根元类对象
     2.superClass指针走位：
        2.1实例对象中没有该指针，只有类对象和元类对象中有；
        2.2类对象 -> 父类对象 -> ... -> 根类对象 -> nil
        2.3元类对象 -> 父元类对象 -> ... -> 根元类对象 -> 根类对象 -> nil
     
     OC中相关isa相关的方法
     
     1.class类方法和对象方法
     
     class类方法返回该类
     + (Class)class {
         return self;
     }
     
     class对象方法返回该类
     - (Class)class {
         return object_getClass(self);
     }
     
     传入参数obj是类的对象，则返回类；如果是该类，则返回该类的元类
     Class object_getClass(id obj)
     {
         if (obj) return obj->getIsa();
         else return Nil;
     }
     
     2.isKindOfClass类方法和对象方法：isKindOfClass底层实际是调用的objc_opt_isKindOfClass方法
     
     第一次比较是 获取类的元类 与 传入类对比，再次之后的对比是获取上次结果的父类 与 传入 类进行对比
     对比顺序：元类（isa） --> 根元类（父类） --> 根类（父类） --> nil（父类） 与 传入类的对比
     + (BOOL)isKindOfClass:(Class)cls {
         // 获取类的元类 vs 传入类
         // 根元类 vs 传入类
         // 根类 vs 传入类
         // 举例：LGPerson vs 元类 (根元类) (NSObject)
         for (Class tcls = self->ISA(); tcls; tcls = tcls->getSuperclass()) {
             if (tcls == cls) return YES;
         }
         return NO;
     }

     第一次是获取对象类 与 传入类对比，如果不相等，后续对比是继续获取上次 类的父类 与传入类进行对比
     对比顺序：对象的类 --> 父类 --> 根类 --> nil 与 传入类的对比
     - (BOOL)isKindOfClass:(Class)cls {
         for (Class tcls = [self class]; tcls; tcls = tcls->getSuperclass()) {
             if (tcls == cls) return YES;
         }
         return NO;
     }
     
     3.isMemberOfClass类方法和对象方法
     
     元类与传入的类对比
     + (BOOL)isMemberOfClass:(Class)cls {
         return self->ISA() == cls;
     }

     对象类与传入的类对比
     - (BOOL)isMemberOfClass:(Class)cls {
         return [self class] == cls;
     }
     
     总结：isMemberOfClass和isKindOfClass类方法是元类与传入类对比；isMemberOfClass只比较当前元类，而isKindOfClass会沿着元类的superClass指针向上进行逐级对比。
     isMemberOfClass和isKindOfClass对象方法是对象类与传入类对比；isMemberOfClass只比较当前对象类，而isKindOfClass会沿着类的superClass指针向上进行逐级对比。
     */
    BOOL res1 = [[NSObject class] isKindOfClass:[NSObject class]];
    BOOL res2 = [[NSObject class] isMemberOfClass:[NSObject class]];
    BOOL res3 = [[NSObject new] isKindOfClass:[NSObject class]];
    BOOL res4 = [[NSObject new] isMemberOfClass:[NSObject class]];
    
    NSLog(@"--%d-%d-%d-%d", res1, res2, res3, res4);
    
    Class persionClass = ZBYMAPerson.class;
    Class super1 = ZBYMAPerson.superclass;
    Class super2 = [super1 superclass];
    Class persionMetaClass = object_getClass(persionClass);
    Class metaSuper1 = [persionMetaClass superclass];
    Class metaSuper2 = [metaSuper1 superclass];
    Class metaSuper3 = [metaSuper2 superclass];
    NSLog(@"persion_superClass--%p-%p-%p", persionClass, super1, super2)
    NSLog(@"persion_meta_superClass--%p-%p-%p-%p", persionMetaClass, metaSuper1, metaSuper2, metaSuper3);
}

// MARK: - ro、rw和rwe

- (void)roAndRw {
    /**
     https://blog.csdn.net/shengpeng3344/article/details/105800310
     https://blog.csdn.net/s3657149/article/details/124657344
     
     class_ro_t存储了当前类在编译期就已经确定的属性（属性对应的成员变量）、方法以及遵循的协议，里面是没有分类的方法的。那些运行时添加的方法将会存储在运行时生成的class_rw_t中。ro即表示read only，是无法进行修改的。
     
     class_rw_t生成在运行时，在编译期间，class_ro_t结构体就已经确定，objc_class中的bits存放着该结构体的地址。在runtime运行之后，具体说来是在运行runtime的realizeClass方法时，会生成class_rw_t结构体，该结构体包含了class_ro_t，并且更新bits指针，换成class_rw_t结构体的地址。
     
     class_rw_t生成过程：它会先将class_ro_t的大部分内容引用，小部分内容拷贝过来；然后再将当前类的分类的这些属性、方法等拷贝到其中。所以可以说class_rw_t是class_ro_t的超集，当然实际访问类的方法、属性等也都是通过指针访问的class_rw_t中的内容
     
     一旦一个类被使用，运行时就会分配一个额外的内存，所以这个内存就变成了磁盘内存（及class_rw_t内存）。但在实践中，类的使用只占10%，没错。rw会导致内存浪费，所以Apple把rw动态加载的方法、Protocol方法、分类方法和动态添加的实例变量都放在class_rw_ext_t中。
     
     class_rw_ext_t缩写rwe，读写ext，在运行时存储类的方法，协议和实例变量等信息。可以减少内存的消耗。苹果在WWDC2020里面说过，只有大约10%左右的类需要动态修改。所以只有10%左右的类里面需要生成class_rw_ext_t这个结构体。这样的话，可以节约很大一部分内存。
     
     class_rw_ext_t生成条件：
     1. 用过runtime的api动态修改的时候；
     2. 有分类的时候，且分类和本类都为非懒加载类(实现了+ load方法)
     */
    
    /**
     编译时的方法等存在class_ro_t中，运行时的方法存在class_rw_ext_t中，class_rw_ext_t中也包含了class_ro_t中的方法，class_rw_t封装了methods、properties()、protocols方法来方便调用。
     
     类的变量存在class_ro_t中的ivars中。
     
     objc_class结构体中的bits成员变量存放着类的属性、方法、协议等信息。这一些信息，有的是在编译期间就确定的；另外一些则是在运行runtime的realizeClass的方法时添加上去的。
     
     在realizeClass方法中，并不是将class_ro_t中的属性、方法的list拷贝到class_rw_t中；而只是通过class_rw_t来操作class_ro_t中的各种list。class_rw_t中的list_array_tt 有指向 class_ro_t中的 entsize_list_tt 的指针。
     
     总结：程序加载时方法存在ro。当类第一次使用的时候，rw就会引用ro；如果动态修改，就会从ro拷贝到rwe；修改的时候也是去操作rwe：
     1. 编译期加载完成前：objc_class 的 bits 指向ro；
     2. 加载完成即运行时：objc_class 的 bits 指向rw；
     3. 当类有动态修改时，即有动态加载属性，方法，协议时：objc_class 的 bits 指向rw，rw会创建rwe，并通过rwe进行方法和属性的操作；
     4. 3完成后完成某一操作的指针指向链：isa -> bits -> rw -> rwe -> ro -> ro中的操作
     */
    
    /**
     Runtime源码 —— property和ivar
     https://www.jianshu.com/p/89ac27684693
     
     原本以为很简单的property和ivar，其实一点也不简单，特别是ivar，真的是花了很多时间。顺便把class_ro_t中几个之前没有分析的属性也一并理解了一下，还是很不错的。

     property在编译期会生成_propertyName的ivar，和相应的get/set属性
     ivars在编译期确定，但不完全确定，offset属性在运行时会修改
     对象的大小是由ivars决定的，当有继承体系时，父类的ivars永远放在子类之前
     class_ro_t的instanceStart和instanceSize会在运行时调整
     class_ro_t的ivarLayout和weakIvarLayout存放的是强ivar和弱ivar的存储规则
     */
}

// MARK: - weak

- (void)weakInitAndDealloc {
    /**
     其它文章：https://www.jianshu.com/p/7e6c0a006c6d
     weak如何修饰对象：
     1.__weak id weakPtr;
     2. NSObject *o = ...;
        __weak id weakPtr = o;
     3.@property (nonatomic, weak) id name;
     
     weak引用相关数据结构：
     static objc::ExplicitInit<StripedMap<SideTable>> SideTablesMap;
     
     static StripedMap<SideTable>& SideTables() {
         return SideTablesMap.get();
     }
     
     template<typename T>
     class StripedMap {
     #if TARGET_OS_IPHONE && !TARGET_OS_SIMULATOR // 如果是iphone设备Map的数量为8
         enum { StripeCount = 8 };
     #else
         enum { StripeCount = 64 };
     #endif

         struct PaddedT {
             T value alignas(CacheLineSize); // T value 64字节对齐
         };

         // 所有PaddedT struct 类型数据被存储在array数组中(这里的PaddedT 带入代码中即 SideTable)。
         PaddedT array[StripeCount];

         // index的hash算法，该方法以void *作为key 来获取void *对应在StripedMap 中的位置
         static unsigned int indexForPointer(const void *p) {
             uintptr_t addr = reinterpret_cast<uintptr_t>(p);
             return ((addr >> 4) ^ (addr >> 9)) % StripeCount;
         }

         // 苹果为array数组写了一些公共的存取数据的方法，主要是调用indexForPointer方法，使得外部传入的对象地址指针直接hash到对应的array节点,这里主要是对&符号重写，所以我们经常看到&SideTables()[obj]这种调用操作，实际上就是调用这个array[indexForPointer(p)].value从而获取SideTable
      public:
        ...
     };
     
     struct SideTable {  // 当实例对象的isa指针中存储的引用计数无法用位存储时就会创建Sidetable，使用Sidetable进行引用计数，同时objc_initWeak（有弱引用的时候）也会创建Sidetable
         spinlock_t slock;   // 自旋同步锁：底层是os_unfair_lock
         RefcountMap refcnts;    // ARC计数表
         weak_table_t weak_table;    // 弱引用表
     };
     
     全局弱引用表。将对象id存储为键，并将weak_entry_t结构存储为其值。
     struct weak_table_t {
         weak_entry_t *weak_entries;    // 弱引用数组，用来存储weak_entry_t对象，是一个链表结构
         size_t    num_entries; // 弱引用数组大小，如果到阈值会自动扩容
         uintptr_t mask;    // 进行哈希运算的mask，大小是num_entries-1
         uintptr_t max_hash_displacement;   // 最大冲突数，一般不会大于这个数
     };

     struct weak_entry_t {
         DisguisedPtr<objc_object> referent;
         union {
             struct {
                 weak_referrer_t *referrers;
                 uintptr_t        out_of_line_ness : 2;
                 uintptr_t        num_refs : PTR_MINUS_2;
                 uintptr_t        mask;
                 uintptr_t        max_hash_displacement;
             };
             struct {
                 // out_of_line_ness field is low bits of inline_referrers[1]
                 weak_referrer_t  inline_referrers[WEAK_INLINE_COUNT];
             };
         };
     }
     Runtime会将弱引用的信息（key为指向对象的指针，value是weak指针的变量的地址数组）封装到weak_entry_t结构体中，具体存储到DisguisedPtr类中。
     
     OC的Runtime会维护一个weak表（weak_table_t结构体），用于维护指向对象的所有weak指针，是weak_entry_t的结构体上的一层封装。
     weak_table_t类型的结构体，是一个哈希表，但是在这个表中的操作并不是线程安全的。
     于是Runtime对于weak_table_t上又进行了一层封装，也就是SideTable。SideTable这层封装对于weak引用机制的主要目的是解决线程安全的问题。
     
     weak修饰对象的存储和释放：
     当weak引用一个对象时，Runtime会调用objc_copyWeak -> objc_initWeak -> storeWeak 方法链 -> 全局散列表SideTables -> 如果有对应的SideTable，更新弱引用表，没有创建一个SideTable用来存储对象弱引用的信息
     
     具体过程：
     1.初始化：runtime会调用objc_initWeak函数，初始化一个新的weak指针指向的对象的地址；
     2.添加引用：objc_initWeak会调用storeWeak函数；
     3.storeWeak函数，调用weak_unregister_no_lock函数清除旧值，调用weak_register_no_lock函数添加新值；
     4.weak_register_no_lock函数中，创建weak_entry_t，为一个对象保存新的弱引用；
     5.当weak引用的对象释放时，该对象会由dealloc函数调用clearDeallocating函数；
     6.clearDeallocating中会调用weak_clear_no_lock函数对该对象所有的weak引用表进行移除；
     7.weak_clear_no_lock是根据weak引用对象的地址值为key，寻找所有weak引用的指针变量数组，并将其设置为nil。
     
     引申问题：
     1.clearDeallocating调用时机；
     2.dealloc的执行顺序；
     3.weak存储在哪里；
     4.SideTable知道么，什么结构，为什么有多个SideTable而不是一个SideTable里实现，哈希冲突；
     */
    
    /**
     // https://www.jianshu.com/p/751265a03324
     // weak_table_t是一个c语言的结构体，是runtime对于HashTable的具体实现
     struct weak_table_t {
         weak_entry_t *weak_entries;    // entry数组
         size_t    num_entries; // 目前保存的entry的数目
         uintptr_t mask;    // mask和max_hash_displacement计算内存偏移量
         uintptr_t max_hash_displacement;
     };
     
     // 该函数的目的是扩充weak_table_t的空间，扩充的条件是Table 3/4及以上的空间已经被使用。可以看出HashTable的初始化大小是64个weak_entry_t的空间，每次扩充后的空间都是当前空间的两倍，即2的N次方(N>=6)
     static void weak_grow_maybe(weak_table_t *weak_table)
     
     // weak_table_t的空间扩充和缩小，代码很容易看懂。首先根据new_size申请相应大小的内存，new_entries指针指向这块新申请的内存。设置weak_table的mask为new_size - 1。此处mask的作用是记录weak_table实际占用的内存边界，此外mask还有另一个重要的作用，之后会再次提及
     static void weak_compact_maybe(weak_table_t *weak_table)
     
     众所周知，HashTable可能会有hash碰撞，而weak_table_t使用了开放寻址法来处理碰撞。如果发生碰撞的话，将寻找相邻(如果已经到最尾端的话，则从头开始)的下一个空位。max_hash_displacement记录当前weak_table最大的偏移值，即hash函数计算的位置和实际存储位置的最大偏差。
     */
    NSObject * o = [NSObject new];
    __weak id weakPtr = o;
    NSLog(@"%@", weakPtr)
}

// MARK: - isa

- (void)isaDetail {
    /**
     union isa_t {
         isa_t() { }
         isa_t(uintptr_t value) : bits(value) { }

         uintptr_t bits;

     private:
         // Accessing the class requires custom ptrauth operations, so
         // force clients to go through setClass/getClass by making this
         // private.
         Class cls;

     public:
     #if defined(ISA_BITFIELD)
         struct {
             ISA_BITFIELD;  // defined in isa.h
         };
     #endif
     };
     */
    
    /**
     1.在64位前，isa就是一个普通的指针，存储这class对象、meta-class对象的内存地址，即Class cls；
     2.从64位后开始，对isa进行了优化，变成了一个联合体（union），共占有8个字节，64位；这其中的cls、bits、还有结构体共用一块地址空间；
     3.64位优化过的isa用不同的位域来存储不同的信息，不同位的信息可以通过位操作获取。
     */
    
    /**
     # if __arm64__ // arm64架构的isa
     // ARM64 simulators have a larger address space, so use the ARM64e
     // scheme even when simulators build for ARM64-not-e.
     #   if __has_feature(ptrauth_calls) || TARGET_OS_SIMULATOR // arm64架构电脑M芯片的isa
     #     define ISA_MASK        0x007ffffffffffff8ULL // 掩码
     #     define ISA_MAGIC_MASK  0x0000000000000001ULL // 掩码
     #     define ISA_MAGIC_VALUE 0x0000000000000001ULL
     #     define ISA_HAS_CXX_DTOR_BIT 0
     #     define ISA_BITFIELD  \
             uintptr_t nonpointer        : 1;   \
             uintptr_t has_assoc         : 1;   \
             uintptr_t weakly_referenced : 1;   \
             uintptr_t shiftcls_and_sig  : 52;  \
             uintptr_t has_sidetable_rc  : 1;   \
             uintptr_t extra_rc          : 8
     #     define RC_ONE   (1ULL<<56)
     #     define RC_HALF  (1ULL<<7)
     #   else   // arm64架构手机A芯片的isa
     #     define ISA_MASK        0x0000000ffffffff8ULL
     #     define ISA_MAGIC_MASK  0x000003f000000001ULL
     #     define ISA_MAGIC_VALUE 0x000001a000000001ULL
     #     define ISA_HAS_CXX_DTOR_BIT 1
     #     define ISA_BITFIELD  \
             uintptr_t nonpointer        : 1;   \
             uintptr_t has_assoc         : 1;   \
             uintptr_t has_cxx_dtor      : 1;   \
             uintptr_t shiftcls          : 33;  \
             uintptr_t magic             : 6;   \
             uintptr_t weakly_referenced : 1;   \
             uintptr_t unused            : 1;   \
             uintptr_t has_sidetable_rc  : 1;   \
             uintptr_t extra_rc          : 19
     #     define RC_ONE   (1ULL<<45)
     #     define RC_HALF  (1ULL<<18)
     #   endif
     
     # elif __x86_64__  // x86架构电脑intel芯片的isa
     #   define ISA_MASK        0x00007ffffffffff8ULL
     #   define ISA_MAGIC_MASK  0x001f800000000001ULL
     #   define ISA_MAGIC_VALUE 0x001d800000000001ULL
     #   define ISA_HAS_CXX_DTOR_BIT 1
     #   define ISA_BITFIELD    \
           uintptr_t nonpointer        : 1; \
           uintptr_t has_assoc         : 1; \
           uintptr_t has_cxx_dtor      : 1; \
           uintptr_t shiftcls          : 44;    \
           uintptr_t magic             : 6; \
           uintptr_t weakly_referenced : 1; \
           uintptr_t unused            : 1; \
           uintptr_t has_sidetable_rc  : 1; \
           uintptr_t extra_rc          : 8
     #   define RC_ONE   (1ULL<<56)
     #   define RC_HALF  (1ULL<<7)

     # else
     #   error unknown architecture for packed isa
     # endif

     // SUPPORT_PACKED_ISA
     #endif
     
     struct SideTable {  // 当实例对象的isa指针中存储的引用计数无法用位存储时就会创建Sidetable，使用Sidetable进行引用计数，同时objc_initWeak（有弱引用的时候）也会创建Sidetable
         spinlock_t slock;   // 自旋同步锁：底层是os_unfair_lock
         RefcountMap refcnts;    // ARC计数表：以对象的内存地址为key，value是对象的引用计数数
         weak_table_t weak_table;    // 弱引用的hash表：引用的对象地址作为键(key)，引用指针变量作为值(value)
     };
     */
    
    /**
     nonpointer：
        0：代表普通的指针，存储着Class、Meta-Class对象的内存地址
        1：代表开启了64位优化，使用不同位域存储了不同信息
     has_assoc：
        是否有设置过关联对象，如果没有，释放时会更快
     has_cxx_dtor：
        表示该对象是否有 C++ 或者 Objc 的析构器(OC中dealloc或者Swift中deinit函数)，如果没有，释放时会更快
     shiftcls：
        类指针信息。存储着Class、Meta-Class对象的内存地址 这里也解释了&ISA_MASK才能取到Class、Meta-Class对象的内存地址
     magic：
        用于在调试时分辨对象是否完成初始化
        判断对象是否初始化完成，在arm64中0x16是调试器判断当前对象是真的对象还是没有初始化的空间。
     weakly_referenced：
        是否有被弱引用指向过，如果没有，释放时会更快
     unused：
        对象是否正在释放内存
     has_sidetable_rc：
        0：引用计数存储在isa的extra_r所占用位域中
        1：引用计数过大无法存储在isa中，引用计数存储在一个叫SideTable的类的属性中
     extra_rc：
        当表示该对象的引用计数值，实际上是引用计数值减 1， 例如，如果对象的引用计数为 10，那么 extra_rc 为 9。如果引用计数大于 10， 则需要使用到下面的 has_sidetable_rc
     */
    [self testisa];
}

- (void)testisa {
    NSObject * obj = [[NSObject alloc] init];
    NSLog(@"1 isa_t = %p", *(void **)(__bridge void*)obj);
    /**
     64位苹果A芯片架构：
     isa 0x1a206aa3259
     转换为64位，高位不足补0：
     高位                        <-                          低位
     ... 0001 1010 0010 0000 0110 1010 1010 0011 0010 0101 1001
     有上述讲解，从低位到高位：
     第一位nonpointer位，1表示优化过的isa
     第二位has_assoc位，0表示该对象没有关联对象
     第三位has_cxx_dtor位，0表示该对象没有C++析构函数
     第4-36位shiftcls位，类对象和元类对象地址信息，可以通过位运算获得
     第37-42位magic位，
     第43位weakly_referenced位，0表示没有弱引用
     第44位unused位，0表示没有正在释放
     第45位has_sidetable_rc位，0表示没有用SideTable存储引用计数
     第46-64位has_sidetable_rc位，0表示新创建的对象，引用计数位 0 + 1 = 1
     */
    self.objc = obj;
    NSObject * tmpObj = obj;
    NSLog(@"2 isa_t = %p", *(void **)(__bridge void*)obj);
    __weak typeof(obj) weakRefObj = obj;
    NSLog(@"3 isa_t = %p", *(void **)(__bridge void*)obj);
    objc_setAssociatedObject(obj, "attachKey", @[@1, @2], OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    NSLog(@"4 isa_t = %p", *(void **)(__bridge void*)obj);
    /**
     64位苹果A芯片架构：
     转换为64位，高位不足补0：
        高位                        <-                             低位
     1  ... 0000 0001 1010 0010 0000 0110 1010 1010 0011 0010 0101 1001
     2  ... 0100 0001 1010 0010 0000 0110 1010 1010 0011 0010 0101 1001
     3  ... 0100 0101 1010 0010 0000 0110 1010 1010 0011 0010 0101 1001
     4  ... 0100 0101 1010 0010 0000 0110 1010 1010 0011 0010 0101 1011
     
     对比：
     1.第一位为1，都是64位nonpointer优化后的isa
     2.4中打印的第二位为1，表示有关联对象，其它为0没有关联对象
     3.第三位都为0，说明对象没有C++析构函数
     4.4-36位isa都相同，同一个实例对象指向的类地址码相同
     5.37-42位magic位debug调试位
     6.43位，1和2为0，3和4为1，因为1和2时对象没有弱引用，3和4对象被弱引用了
     7.44位都为0，表示对象没有正在释放操作
     8.45为为0，表示没有用SideTable存储引用计数
     9.46-64位，2、3和4打印的47位为1，表示引用计数为 2 + 1 = 3，1为0表示在1是为创建对象引用计数为 0 + 1 = 1
     */
    
}

// MARK: - AutoreleasePool

- (void)autoreleasePoolDetail {
    /**
     官方文档：https://developer.apple.com/library/archive/documentation/Cocoa/Conceptual/MemoryMgmt/Articles/mmAutoreleasePools.html#//apple_ref/doc/uid/20000047-CJBFBEDI
     大神Runloop文档：https://blog.ibireme.com/2015/05/18/runloop/
     其它文章：https://blog.csdn.net/shuiyue184480/article/details/123231806
     
     AutoreleasePool工作原理：
     
     1. App启动后，苹果在主线程 RunLoop 里注册了两个 Observer，其回调都是 _wrapRunLoopWithAutoreleasePoolHandler()，从程序启动到加载完成，主线程的RunLoop会处于休眠状态，等待用户交互来唤醒。
        第一个 Observer 监视的事件是 Entry(即将进入Loop)，其回调内会调用 _objc_autoreleasePoolPush() 创建自动释放池。其 order 是-2147483647，优先级最高，保证创建释放池发生在其他所有回调之前。
        第二个 Observer 监视了两个事件：
        BeforeWaiting(准备进入休眠) 时调用_objc_autoreleasePoolPop() 和 _objc_autoreleasePoolPush() 释放旧的池并创建新池；
        Exit(即将退出Loop) 时调用 _objc_autoreleasePoolPop() 来释放自动释放池。这个 Observer 的 order 是 2147483647，优先级最低，保证其释放池子发生在其他所有回调之后。

     2. 在主线程执行的代码，通常是写在诸如事件回调、Timer回调内的。这些回调会被 RunLoop 创建好的 AutoreleasePool 环绕着，所以不会出现内存泄漏，开发者也不必显示创建 Pool 了。
     
     AutoreleasePool的底层代码实现主要是两个方法一个类：
     _objc_autoreleasePoolPush()、_objc_autoreleasePoolPop()、AutoreleasePoolPage
     前两个方法底层都是调用AutoreleasePoolPage的push和pop方法，所以主要的其实是这个类。
     
     */
}

// MARK: - Tagged Pointer

- (void)taggedPointerDetail {
    /**
     链接地址：https://mp.weixin.qq.com/s?__biz=MzIyMTg0OTExOQ==&mid=2247486305&idx=2&sn=68856c90ba169d7af0034575cc63631a&chksm=e8373811df40b1077fec00830c9eb595a5b750a7b5120ab68ea711ace42d2ebd031c7212d2db&scene=27#wechat_redirect
     源码版本objc4-818.2
     在objc4源码中，我们经常会在函数中看到Tagged Pointer。Tagged Pointer究竟是何方神圣？

     从64位系统开始，iOS引入了Tagged Pointer技术，用于优化小对象（NSString，NSNumber，NSDate）的存储。

     Tagged Pointer主要为了解决两个问题：
     1.内存资源浪费，堆区需要额外的开辟空间：
     例如NSNumber对象，其值是一个整数。正常情况下，如果这个整数只是一个 NSInteger 的普通变量，那么它所占用的内存是与 CPU 的位数有关，在 32 位 CPU 下占 4 个字节，在 64 位 CPU 下是占 8 个字节的。而指针类型的大小通常也是与 CPU 位数相关，一个指针所占用的内存在 32 位 CPU 下为 4 个字节，在 64 位 CPU 下也是 8 个字节。
     所以一个普通的 iOS 程序，如果没有Tagged Pointer对象，从 32 位机器迁移到 64 位机器中后，虽然逻辑没有任何变化，但这种 NSNumber、NSDate 一类的对象所占用的内存会翻倍。
     
     2.访问效率，每次set/get都需要访问堆区，浪费时间，而且需要管理堆区对象的声明周期，降低效率为了改进上述提到的内存占用和效率问题，所以苹果提出了Tagged Pointer对象。对于某些占用内存很小的数据实例，不再单独开辟空间去存储，而是将实际的实例值存储在对象的指针中，同时对该指针进行标记，用于区分正常的指针指向！由于NSNumber、NSDate类的变量本身的值需要占用的内存大小常常不需要8个字节，拿整数来说，4个字节所能表示的有符号整数就可以达到20多亿（注：2^31=2147483648，另外1位作为符号位)，对于绝大多数情况都是可以处理的。所以苹果将一个对象的指针拆成两部分，一部分直接保存数据，另一部分作为特殊标记，表示这是一个特别的指针，不指向任何一个地址。
     
     Tagged Pointer内存管理：
     普通类内存管理：
     总共的空间 = 栈指针空间（isa普通类指针） + 堆中分配的空间
     指针变量指向堆分配的内存空间，需要动态分配内存，进行引用计数机制，控制对象堆内存的管理。
     Tagged Pointe内存管理：
     总空间 = 栈指针空间（Tagged Pointer）
     栈指针空间 = 对象的特殊标记(Tag) + 对象的值(Data)
     对象的特殊标记(Tag)为特殊标记，用于区分NSNumber、NSDate、NSString等对象类型
     Data为对象的值
     指针变量包含值，不用进行引用计数机制对内存管理，所以不需要retain，release操作。
     
     Tagged Pointer总结：
     1.Tagged Pointer指向的并不是一个类，它是适用于64位处理器的一个内存优化机制，专门用来存储小对象，当存储不下时，则转为对象，例如NSNumber, NSDate, NSString。
     2.指针的值不再是地址了，而是真正的值。所以，实际上它不再是一个对象了，它只是一个披着对象外衣的普通变量而已。所以，它的内存并不存储在堆中，也不需要malloc和free。
     3.在内存读取上有着3倍的效率，创建时比以前快106倍。
     
     Tagged Pointer原理和底层实现：
     
     Tagged Pointer 的数据混淆
     在现在的版本中，为了保证数据安全，苹果对 Tagged Pointer 做了数据混淆，开发者通过打印指针无法判断它是不是一个Tagged Pointer，更无法读取Tagged Pointer的存储数据。

     所以在分析Tagged Pointer之前，我们需要先关闭Tagged Pointer的数据混淆，以方便我们调试程序。通过设置环境变量OBJC_DISABLE_TAG_OBFUSCATION为YES。
     
     调用taggedPointerString
     
     打印结果表示当字符串的长度为10个以内时，字符串的类型都是NSTaggedPointerString类型，当超过10个时，字符串的类型才是__NSCFString
     
     分析打印结果：
     
     NSTaggedPointer标志位：
     objc-internal.h中:
     static inline bool
     _objc_isTaggedPointer(const void * _Nullable ptr)
     {
         return ((uintptr_t)ptr & _OBJC_TAG_MASK) == _OBJC_TAG_MASK;
     }
     
     上面这个方法我们看到，判断一个对象类型是否为NSTaggedPointerString类型实际上是讲对象的地址与_OBJC_TAG_MASK进行按位与操作，结果在跟_OBJC_TAG_MASK进行对比，我们在看下_OBJC_TAG_MASK的定义：
     #elif OBJC_MSB_TAGGED_POINTERS
     #   define _OBJC_TAG_MASK (1UL<<63)
     #else
     #   define _OBJC_TAG_MASK 1UL
     
     我们都知道一个对象地址为64位二进制，它表明如果64位数据中，最高位是1的话，则表明当前是一个tagged pointer类型。

     那么我们在看下上面打印出的地址，所有NSTaggedPointerString地址都是0xd开头，d转换为二进制1110，根据上面的结论，我们看到首位为1表示为NSTaggedPointerString类型。在这里得到验证。

     注意:TaggedPointer类型在iOS和MacOS中标志位是不同的iOS为最高位而MacOS为最低位
     
     判断对象类型：
     正常情况下一个对象的类型，是通过这个对象的ISA指针来判断的，那么对于NSTaggedPointer类型我们如何通过地址判断对应数据是什么类型的呢？
     
     objc4-723之前：
     在objc4-723之前，我们可以通过与判断TaggedPointer标志位一样根据地址来判断，而类型的标志位就是对象地址的61-63位；比如对象地址为0xa开头，那么转换成二进制位1010,那么去掉最高位标志位后，剩余为010,即10进制中的2。
     接着我们看下runtime源码objc-internal.h中有关于标志位的定义如下：
     #if __has_feature(objc_fixed_enum)  ||  __cplusplus >= 201103L
     enum objc_tag_index_t : uint16_t
     #else
     typedef uint16_t objc_tag_index_t;
     enum
     #endif
     {
         // 60-bit payloads
         OBJC_TAG_NSAtom            = 0,
         OBJC_TAG_1                 = 1,
         OBJC_TAG_NSString          = 2,
         OBJC_TAG_NSNumber          = 3,
         OBJC_TAG_NSIndexPath       = 4,
         OBJC_TAG_NSManagedObjectID = 5,
         OBJC_TAG_NSDate            = 6,

         // 60-bit reserved
         OBJC_TAG_RESERVED_7        = 7,

         // 52-bit payloads
         OBJC_TAG_Photos_1          = 8,
         OBJC_TAG_Photos_2          = 9,
         OBJC_TAG_Photos_3          = 10,
         OBJC_TAG_Photos_4          = 11,
         OBJC_TAG_XPC_1             = 12,
         OBJC_TAG_XPC_2             = 13,
         OBJC_TAG_XPC_3             = 14,
         OBJC_TAG_XPC_4             = 15,
         OBJC_TAG_NSColor           = 16,
         OBJC_TAG_UIColor           = 17,
         OBJC_TAG_CGColor           = 18,
         OBJC_TAG_NSIndexSet        = 19,
         OBJC_TAG_NSMethodSignature = 20,
         OBJC_TAG_UTTypeRecord      = 21,

         // When using the split tagged pointer representation
         // (OBJC_SPLIT_TAGGED_POINTERS), this is the first tag where
         // the tag and payload are unobfuscated. All tags from here to
         // OBJC_TAG_Last52BitPayload are unobfuscated. The shared cache
         // builder is able to construct these as long as the low bit is
         // not set (i.e. even-numbered tags).
         OBJC_TAG_FirstUnobfuscatedSplitTag = 136, // 128 + 8, first ext tag with high bit set

         OBJC_TAG_Constant_CFString = 136,

         OBJC_TAG_First60BitPayload = 0,
         OBJC_TAG_Last60BitPayload  = 6,
         OBJC_TAG_First52BitPayload = 8,
         OBJC_TAG_Last52BitPayload  = 263,

         OBJC_TAG_RESERVED_264      = 264
     };
     #if __has_feature(objc_fixed_enum)  &&  !defined(__cplusplus)
     typedef enum objc_tag_index_t objc_tag_index_t;
     #endif
     
     那么我们知道2表示的OBJC_TAG_NSString即字符串类型。因为目前已经无法验证这种情况了 所以我们不做其他类型验证。
     
     objc4-750之后
     
     static Class *
     classSlotForBasicTagIndex(objc_tag_index_t tag)
     {
     #if OBJC_SPLIT_TAGGED_POINTERS
         uintptr_t obfuscatedTag = _objc_basicTagToObfuscatedTag(tag);
         return &objc_tag_classes[obfuscatedTag];
     #else
         uintptr_t tagObfuscator = ((objc_debug_taggedpointer_obfuscator
                                     >> _OBJC_TAG_INDEX_SHIFT)
                                    & _OBJC_TAG_INDEX_MASK);
         uintptr_t obfuscatedTag = tag ^ tagObfuscator;

         // Array index in objc_tag_classes includes the tagged bit itself
     #   if SUPPORT_MSB_TAGGED_POINTERS
         return &objc_tag_classes[0x8 | obfuscatedTag];
     #   else
         return &objc_tag_classes[(obfuscatedTag << 1) | 1];
     #   endif
     #endif
     }
     
     classSlotForBasicTagIndex() 函数的主要功能就是根据指定索引 tag 从数组objc_tag_classes中获取类指针,而下标的计算方法发是根据外部传递的索引tag。比如字符串 tag = 2。当然这并不是简单的从数组中获取某条数据。
     
     
     */
    [self taggedPointerString];
}

- (void)taggedPointerNumber {
    NSNumber *number1 = @(0x1);
    NSNumber *number2 = @(0x20);
    NSNumber *number3 = @(0x3F);
    NSNumber *number4 = @(0xFFFFFFFFFFEFE);
    NSNumber *maxNum = @(MAXFLOAT);
    
    // 使用了Tagged Pointer，NSNumber对象的值直接存储在了指针上，不会在堆上申请内存。则使用一个NSNumber对象只需要指针的 8 个字节内存就够了，大大的节省了内存占用。
//    NSLog(@"%zd", malloc_size(number1);
    NSInteger number1Size = malloc_size((__bridge const void *)(number1));
    NSLog(@"占用堆内存=%zd", number1Size);
    NSLog(@"指针内存=%zd", sizeof(number1));
    // NSNumber普通对象，会在堆上申请内存
    NSInteger maxNumSize = malloc_size((__bridge const void *)(maxNum));
    NSLog(@"占用堆内存=%zd", maxNumSize);
    NSLog(@"指针内存=%zd", sizeof(maxNum));
    
    NSLog(@"%p %@ %@", number1, number1, number1.class);
    NSLog(@"%p %@ %@", number2, number2, number2.class);
    NSLog(@"%p %@ %@", number3, number3, number3.class);
    NSLog(@"%p %@ %@", number4, number4, number4.class);
    NSLog(@"%p %@ %@", maxNum, maxNum, maxNum.class);
}

- (void)taggedPointerString {
    NSMutableString *mutableStr = [NSMutableString string];
        NSString *immutable = nil;
        #define _OBJC_TAG_MASK (1UL<<63)
        char c = 'a';
        do {
            [mutableStr appendFormat:@"%c", c++];
            immutable = [mutableStr copy];
            NSLog(@"%p %@ %@", immutable, immutable, immutable.class);
        }while(((uintptr_t)immutable & _OBJC_TAG_MASK) == _OBJC_TAG_MASK);
}

- (void)getTaggedPointerClass {
#   define _OBJC_TAG_INDEX_SHIFT 0
#   define _OBJC_TAG_INDEX_MASK 0x7UL
    NSArray * objc_tag_classes = @[@"OBJC_TAG_NSAtom", @"OBJC_TAG_1", @"OBJC_TAG_NSString", @"OBJC_TAG_NSNumber", @"OBJC_TAG_NSIndexPath", @"OBJC_TAG_NSManagedObjectID", @"OBJC_TAG_NSDate"];
    uint16_t NSString_Tag = 2;
    uint16_t NSNumber_Tag = 3;
    uintptr_t objc_debug_taggedpointer_obfuscator = 0;
    // 3 = 0011
    // _OBJC_TAG_INDEX_MASK = 0x7 = 0111
            uintptr_t string_tagObfuscator = ((objc_debug_taggedpointer_obfuscator
                                               >> _OBJC_TAG_INDEX_SHIFT)
                                              & _OBJC_TAG_INDEX_MASK);

            uintptr_t number_tagObfuscator = ((objc_debug_taggedpointer_obfuscator
                                               >> _OBJC_TAG_INDEX_SHIFT)
                                              & _OBJC_TAG_INDEX_MASK);

    // 异或操作 相同返回0 不同返回1
    // 2 ^ 3 = 0010 ^ 0011 = 0001
    // 3^ 3 = 0011 ^ 0011 = 0000
    uintptr_t string_obfuscatedTag = NSString_Tag ^ string_tagObfuscator;
    uintptr_t number_obfuscatedTag = NSNumber_Tag ^ number_tagObfuscator;
    uintptr_t string_obfuscatedTag_index = 0x8 | string_obfuscatedTag;
    uintptr_t number_obfuscatedTag_index = 0x8 | number_obfuscatedTag;

    // 按位或
    // 1000 | 0001 = 1001 = 9
    // 1000 | 0000 = 1000 = 8
//            NSLog(@"%@", objc_tag_classes[0x8 | string_obfuscatedTag]);
//            NSLog(@"%@", objc_tag_classes[0x8 | number_obfuscatedTag]);
}

// MARK: - 栈中内存结构考察

- (void)stackMemory {
    /**
     链接地址：https://www.jianshu.com/p/fa20a8770c7d
     https://www.jianshu.com/p/d1d3fd53d0bd
     */
    id cls = [ZBYMAPerson class];
    void *obj = &cls;
    [(__bridge id)obj showName];
}

// MARK: - 深入理解Category

- (void)deepCategory {
    // https://tech.meituan.com/2015/03/03/diveintocategory.html
}

// MARK: - Mach-O基础

- (void)machOBase {
    /**
     Mach-O官方文档：https://developer.apple.com/library/archive/documentation/DeveloperTools/Conceptual/MachOTopics/0-Introduction/introduction.html
     Mach-O文件查看工具：https://github.com/gdbinit/MachOView
     Mach-O文件详细构成：https://github.com/aidansteele/osx-abi-macho-file-format-reference
     Mach-O的编译获取：
     Mach-O相关其它文章：
        https://www.jianshu.com/p/43ef7ddd0081
        抖音团队：https://mp.weixin.qq.com/s?__biz=MzI1MzYzMjE0MQ==&mid=2247486932&idx=1&sn=eb4d294e00375d506b93a00b535c6b05&chksm=e9d0c636dea74f20ec800af333d1ee94969b74a92f3f9a5a66a479380d1d9a4dbb8ffd4574ca&scene=21#wechat_redirect
     Mach-O无用代码检测：https://github.com/wuba/WBBlades
     1. 简介：
     Mach-O(mach object file format)是一种文件的格式，在MacOS/iOS操作系统上可执行的文件格式(MacOS/iOS应用程序二进制接口(ABI)中用于在磁盘上存储程序和库的标准文件格式)。 类似于windows上的PE格式 (Portable Executable), linux上的elf格式 (Executable and Linking Format)。例如：动态库、静态库和其它代码等。
     它由Header，Load commands，Raw segment data（Sections或Loader Info）组成。
     
     2. Header(头信息)：
     引入：#import <mach-o/dyld.h>
     文件：mach-o/loader.h
     指定文件的目标体系结构，如cup类型、文件类型等。详细结构见图：mach_o.png
     struct mach_header_64 {
       uint32_t magic;  //
       uint32_t cputype;    // cpu类型：arm64、m2、x86_64、 PPC64；PPC、x86_32(IA_32)是32位的头信息是结构体mach_header
       uint32_t cpusubtype;
       uint32_t filetype;   // Mach-O文件类型：具体类型见枚举HeaderFileType
       uint32_t ncmds;      // Load commands个数
       uint32_t sizeofcmds; // Load commands的占用内存大小
       uint32_t flags;
       uint32_t reserved;
     };
     
     2.1 Mach-O文件类型：
     enum HeaderFileType {
       // Constants for the "filetype" field in llvm::MachO::mach_header and
       // llvm::MachO::mach_header_64
       MH_OBJECT = 0x1u,
       MH_EXECUTE = 0x2u,
       MH_FVMLIB = 0x3u,
       MH_CORE = 0x4u,
       MH_PRELOAD = 0x5u,
       MH_DYLIB = 0x6u,
       MH_DYLINKER = 0x7u,
       MH_BUNDLE = 0x8u,
       MH_DYLIB_STUB = 0x9u,
       MH_DSYM = 0xAu,
       MH_KEXT_BUNDLE = 0xBu
     };
     
     MH_OBJECT：.o文件即目标文件、.a静态库文件(其实就是N个.o合并在一起)
     MH_EXECUTE：.app/xx可执行文件
     MH_DYLIB：.dylib和.framework/xx等动态库文件
     MH_DYLINKER：/usr/lib/xx动态链接编辑器
     MH_DSYM：.dSYM/Contents/Resources/DWARF/xx二进制符号表文件（常用于分析APP的崩溃信息）
     
     3. Load commands(加载命令信息，command简写cmd)：
     指定文件的逻辑结构和文件在虚拟内存中的布局；用于运行时，将程序代码加载到内存中的命令信息。
     存储 Mach-O 的布局信息，比如 Segment command 和 Data 中的 Segment/Section 是一一对应的。除了布局信息之外，还包含了依赖的动态库等启动 App 需要的信息。
     3.1 Load Commands有很多类型：
        LC_UUID：存储uuid信息的cmd
        LC_BUILD_VERSION：存储可运行操作系统版本的信息
        LC_SYMTAB：符号表加载命令信息
        LC_SEGMENT_64或LC_SEGMENT：分段加载命令，如果有Section，会根据Section结构体进行加载；其大小用cmdsize表示
     
     3.1.1 常见特殊命名的LC_SEGMENT_64：
        \__PAGEZERO：
        \__TEXT：读取文本字段的命令
        \__DATA：读取数据段的命令
        \__LINKEDIT：包含由链接编辑器创建和维护的所有结构的段。使用-seglinkedit选项创建，仅适用于MH_EXECUTE和FVMLIB文件类型
     
     3.2 Load Commands主要包含了有多个 Segment 段，每个 Segment 中又包含了多个 Section 段。每一部分都是系统执行指令，用于读取数据(Raw segment data)的指令。
     
     4. Raw segment data(原始段数据)：
     该部分包含了实际的代码和数据，Data 被分割成很多个 Segment，每个 Segment 又被划分成很多个 Section，分别存放不同类型的数据。
     
     标准的三个 Segment 是 TEXT，DATA，LINKEDIT，也支持自定义：
     TEXT：代码段，只读可执行，存储函数的二进制代码(\__text)，常量字符串(\__cstring)，Objective C 的类/方法名等信息
     DATA：数据段，读写，存储 Objective C 的字符串(\__cfstring)，以及运行时的元数据：class/protocol/method…
     LINKEDIT：启动 App 需要的信息，如 bind & rebase 的地址，代码签名，符号表…
     */
    UIButton * btn = [UIButton buttonWithType: UIButtonTypeSystem];
    ZBYMAPerson * person = [ZBYMAPerson new];
    person.name = @"周";
    // 代码获取MachO信息
    int count = _dyld_image_count();
    for(int i = 0; i < count; i++) {
        const struct mach_header *machHeader = _dyld_get_image_header(i);
        const char*  name = _dyld_get_image_name(i);
        NSLog(@"filetype is %d, name is %s", machHeader->filetype, name);
    }
}

// MARK: - OC代码编译和打包为执行程序即ipa构建

- (void)ipaConstructionProcess {
    /**
     https://mp.weixin.qq.com/s?__biz=MzI1MzYzMjE0MQ==&mid=2247486932&idx=1&sn=eb4d294e00375d506b93a00b535c6b05&chksm=e9d0c636dea74f20ec800af333d1ee94969b74a92f3f9a5a66a479380d1d9a4dbb8ffd4574ca&scene=21#wechat_redirect
     
     1. IPA 构建流程：
     既然要构建，那么必然会有一些地方去定义如何构建，对应 Xcode 中的两个配置项：
     __Build Phase__：以 Target 为维度定义了构建的流程。可以在 Build Phase 中插入脚本，来做一些定制化的构建，比如 CocoaPod 的拷贝资源就是通过脚本的方式完成的。
     __Build Settings__：配置编译和链接相关的参数。特别要提到的是 other link flags 和 other c flags，因为编译和链接的参数非常多，有些需要手动在这里配置。很多项目用的 CocoaPod 做的组件化，这时候编译选项在对应的.xcconfig 文件里。
     
     以单 Target 为例，我们来看下构建流程：见图ipa_construction_process.png
     
        1.1 源文件(.m/.c/.swift 等)是单独编译的，输出对应的目标文件(.o)
        1.2 目标文件和静态库/动态库一起，链接出最后的 Mach-O
        1.3 Mach-O 会被裁剪，去掉一些不必要的信息
        1.4 资源文件如 storyboard，asset 也会编译，编译后加载速度会变快
        1.5 Mach-O 和资源文件一起，打包出最后的.app
        1.6 对.app 签名，防篡改
     
     2. 编译：
     编译器可以分为两大部分：前端和后端，二者以 IR（中间代码）作为媒介。这样前后端分离，使得前后端可以独立的变化，互不影响。C 语言家族的前端是 clang，swift 的前端是 swiftc，二者的后端都是 llvm。
        2.1 前端负责预处理，词法语法分析，生成 IR
        2.2 后端基于 IR 做优化，生成机器码
     详情见图ipa_compile.png
     
     那么如何利用编译优化启动速度呢？
     __代码数量会影响启动速度，为了提升启动速度，我们可以把一些无用代码下掉__。那怎么统计哪些代码没有用到呢？可以利用 LLVM 插桩来实现。LLVM 的代码优化流程是一个一个 Pass，由于 LLVM 是开源的，我们可以添加一个自定义的 Pass，在函数的头部插入一些代码，这些代码会记录这个函数被调用了，然后把统计到的数据上传分析，就可以知道哪些代码是用不到的了 。
     Facebook 给 LLVM 提的 order_file[2]的 feature 就是实现了类似的插桩。
     
     3.链接：
     经过编译后，我们有很多个目标文件，接着这些目标文件会和静态库，动态库一起，链接出一个 Mach-O。链接的过程并不产生新的代码，只会做一些移动和补丁。
     详情见图ipa_link.png
     
     tbd 的全称是 text-based stub library，是因为链接的过程中只需要符号就可以了，所以 Xcode 6 开始，像 UIKit 等系统库就不提供完整的 Mach-O，而是提供一个只包含符号等信息的 tbd 文件。
     
     4.裁剪：
     编译完 Mach-O 之后会进行裁剪(strip)，是因为里面有些信息，如调试符号，是不需要带到线上去的。裁剪有多种级别，一般的配置如下：
        All Symbols，主二进制
        Non-Global Symbols，动态库
        Debugging Symbols，二方静态库
     __为什么二方库在出静态库的时候要选择 Debugging Symbols 呢？是因为像 order_file 等链接期间的优化是基于符号的，如果把符号裁剪掉，那么这些优化也就不会生效了。__
     
     5.签名 & 上传：
     裁剪完二进制后，会和编译好的资源文件一起打包成.app 文件，接着对这个文件进行签名。签名的作用是保证文件内容不多不少，没有被篡改过。接着会把包上传到 iTunes Connect，上传后会对\__TEXT段加密，加密会减弱 IPA 的压缩效果，增加包大小，也会降低启动速度（__iOS 13 优化了加密过程，不会对包大小和启动耗时有影响__）。
     */
}

// MARK: - ipa的装载即应用程序的启动

- (void)ipaLoadProcess {
    /**
     https://mp.weixin.qq.com/s?__biz=MzI1MzYzMjE0MQ==&mid=2247486932&idx=1&sn=eb4d294e00375d506b93a00b535c6b05&chksm=e9d0c636dea74f20ec800af333d1ee94969b74a92f3f9a5a66a479380d1d9a4dbb8ffd4574ca&scene=21#wechat_redirect
     */
}

// MARK: - dealloc流程

- (void)releaseProcess {
    /**
     https://gitkong.github.io/2019/10/24/ARC下-Dealloc还需要注意什么/
     
     dealloc：
     其实就是NSObject的一个方法，当对象的ARC计数为0，该对象被销毁的时候，系统就会回调这个方法，用来释放内存占用。
     
     那dealloc方法里面，我们要处理什么？
     移除通知、移除观察者、移除定时器或对于一些非Objective-C对象也需要手动清空，比如CoreFoundation中的对象。
     
     dealloc中需要注意的地方，容易犯错的地方：
     1. dealloc调用线程：
     很多人都认为是在主线程调用的，其实并不是，而是取决于最后在什么线程中release后触发，这个稍后结合第四个错误点来分析会比较清楚。
     
     Runtime 源码 objc-object.h 源码：
     
     ALWAYS_INLINE bool objc_object::rootRelease(bool performDealloc, bool handleUnderflow)

     ...

     if (performDealloc) {
         ((void(*)(objc_object *, SEL))objc_msgSend)(this, @selector(dealloc));
     }
     
     2. dealloc中置空操作
     常见的操作：
     - (void)dealloc {
         NSLog(@"BaseModel dealloc");
         self.baseName = nil;
     }
     这个表面看上去没什么问题，在《Effective Objective-C 2.0》一书中第7条提到：
     在对象内部尽量直接访问实例变量，而在初始化方法和dealloc方法中，总是应该直接通过实例变量来读写数据。
     
     除了文中所说的加快访问速度之外，但是如果用法不恰当的话，会出现不必要的崩溃问题。下面举个简单的例子分析一下：
     定义了一个ZBYMAPerson基类，基类中演示了使用self.name = nil
     __同时定义了一个子类ZBYMATeacher继承自ZBYMAPerson，子类中重写了name 的setter方法，并获取name进行其他操作__
     
     当ZBYMATeacher作为一个临时变量生成后赋值name，变量使用完后系统会自动回收，此时大家可以想想会发什么什么问题？

     不难想到，此时会出现崩溃现象，原因是[NSString stringWithString:baseName] 这里，name是nil，而这个方法是不允许传nil参数的，当然，这个业务处理上肯定需要一个判空操作，我们先来分析一下为什么会是nil
     
     __子类ZBYMATeacher被释放会调用子类的dealloc方法，然后会调用父类ZBYMAPerson的dealloc方法，此时父类中通过setter方法来赋值nil，而子类SubModel重写了，子类拿到nil来处理导致崩溃问题__
     
     究竟属性是否需要手动置空释放？实际上来说，是不需要手动释放的，因为dealloc中.cxx_destruct会处理。当然因为执行是有一定延迟性，为了节省资源，在确保属性没利用价值的时候可以手动清空，这个后文会分析dealloc的处理逻辑。
     
     3. dealloc中使用\__weak:
     举个简单例子来模拟实际复杂业务场景：
     在ZBYMATeacher中的dealloc调用[self performSelectorWhenDealloc]，切performSelectorWhenDealloc使用的weak，具体代码如ZBYMATeacher
     
     当ZBYMATeacher这个类被释放，调用dealloc的时候会出现崩溃，崩溃信息如下：Cannot form weak reference to instance (0x281022f70) of class ZBYMATeacher. It is possible that this object was over-released, or is in the process of deallocation.
     
     崩溃原因我们来分析一下：

     先了解一下\__weak 到底做了什么操作，通过clang 转换的代码是这样的 \__attribut\__((objc_ownership(weak))) typeof(self) weakSelf = self; 这样还是看不出问题，我们看回堆栈，堆栈崩在objc_initWeak 函数中，我们可以看看Runtime 源码 objc_initWeak 函数的定义是怎么样的：
     id
     objc_initWeak(id *location, id newObj)
     {
         if (!newObj) {
             *location = nil;
             return nil;
         }

         return storeWeak<DontHaveOld, DoHaveNew, DoCrashIfDeallocating>
             (location, (objc_object*)newObj);
     }
     
     可以留意到，内部调用了 storeWeak 函数，其中有个模板名称是DontCrashIfDeallocating 不难猜到，当调用到了 storeWeak 函数的时候，如果释放过程中存储，那就会crash，函数最终会调用register函数 id weak_register_no_lock(weak_table_t *weak_table, id referent_id, id *referrer_id, bool crashIfDeallocating)
     
     id
     weak_register_no_lock(weak_table_t *weak_table, id referent_id,
                           id *referrer_id, WeakRegisterDeallocatingOptions deallocatingOptions)
     {
        ...
             if (deallocating) {
                 if (deallocatingOptions == CrashIfDeallocating) {
                     _objc_fatal("Cannot form weak reference to instance (%p) of "
                                 "class %s. It is possible that this object was "
                                 "over-released, or is in the process of deallocation.",
                                 (void*)referent, object_getClassName((id)referent));
                 } else {
                     return nil;
                 }
             }
        ...
     }
     
     在这就找到了崩溃时打印出信息了。通过上面的分析，我们也知道，\__weak 其实就是会最终调用objc_initWeak 函数进行注册。抱着求学的态度，可以在clang 8.7 对objc_initWeak函数描述中找到答案：
     
     object is a valid pointer which has not been registered as a \__weak object. value is null or a pointer to a valid object. If value is a null pointer or the object to which it points has begun deallocation, object is zero-initialized. Otherwise, object is registered as a \__weak object pointing to value. Equivalent to the following code:
     
     4. dealloc中使用GCD：
     GCD相信大家平时用得不少，但在dealloc方法里面使用GCD大家有没有注意呢，先来举个简单例子，我们在主线程中创建一个定时器，然后类被释放的时候销毁定时器，相关代码如下：
     在ZBYMATeacher中定义两个方法，fireTimer设置定时器，invalidateTimer移除定时器
     
     补充说明一下，定时器的释放和创建必须在同一个线程，这个也是比较容易犯的错误点，官方描述如下：
     Stops the timer from ever firing again and requests its removal from its run loop.
     
     This method is the only way to remove a timer from an NSRunLoop object. The NSRunLoop object removes its strong reference to the timer, either just before the invalidate method returns or at some later point. If it was configured with target and user info objects, the receiver removes its strong references to those objects as well.
     
     简单解释一下，当前的定时器销毁只能从启动定时器的Runloop中移除，然后Runloop和线程是一一对应的，因此需要确保销毁和创建在同一个线程中处理，否则可能会出现释放不了的情况。

     说回正题，当定时器所在类被释放后，此时调用invalidateTimer 方法去销毁定时器的时候就会出现崩溃情况。

     崩溃报错：Thread 1: EXC_BAD_ACCESS (code=1, address=0x8)
     
     出现访问野指针问题了，原因其实不难想到程序代码默认是在主线程主队列中执行，而dealloc中异步执行主队列中释放定时器释放，GCD会强引用self，此时dealloc已经执行完成了，那么self其实已经被free释放掉了，此时销毁内部再调用self就会访问野指针。

     我们来继续分析一下，GCD为啥会强引用self，以及简单分析一下GCD的调用时机问题。
     
     强引用问题，我们可以通过Clang去查看一下底层源码实现，简单转换如下代码：
     ZBYMATeacher添加如下代码并Clang：
     - (void)dealloc {
         dispatch_async(dispatch_queue_create("Kong", 0), ^{
            [self test];
         });
     }
     
     转换后的代码量有点多，我们可以只抓重点来查看一下，具体实现细节本文就不过多分析。

     可以发现，dealloc 方法 转换成 static void _I_TestModel_dealloc(TestModel * self, SEL _cmd) 内部调用的dispatch 方法变化不大，主要是看Block的传递，可以留意到\__TestModel__dealloc_block_impl_0 这个结构体地址参数，看其代码实现可以发现，TestModel *const \__strong self; 就是这个\__strong 使得Block 会对self 进行强引用。顺带说一下，结构体中有个struct \__block_impl impl; 成员变量，而这个结构体内部有个FuncPtr 成员，Block的调用实际上就是通过FuncPtr 来实现的。
     
     以上就通过一个简单的例子，解释了dealloc 中使用GCD中出现的问题，实际上，GCD还会出现很多种搭配情况.
     原理是一样的，简单总结一下：__GCD任务底层通过链表管理，队列任务遵循FIFO模式，那么任务执行肯定就会有延迟性，同一时刻只能执行一个任务，只要dealloc任务执行先，那么此时block使用self就会访问野指针，因为dealloc内会有free操作。__
     */
    
    ZBYMATeacher * teacher = [ZBYMATeacher new];
    teacher.name = @"zhou";
    
    /**
     release源码调用流程：
     ARC自动管理，当引用计数为0时，会自动调用该实例对象的release方法
     release -> _objc_rootRelease(self) -> obj->rootRelease() -> rootRelease(true, RRVariant::Fast) -> ((void(*)(objc_object *, SEL))objc_msgSend)(this, @selector(dealloc))
     -> 调用dealloc函数
     
     dealloc源码调用流程：
     
     dealloc -> _objc_rootDealloc(self) -> obj->rootDealloc() -> object_dispose((id)this) ->
     objc_destructInstance(obj) -> (object_cxxDestruct(obj)、_object_remove_assocations(obj, true)、obj->clearDeallocating()) -> clearDeallocating_slow() -> (weak_clear_no_lock(&table.weak_table, (id)this)、table.refcnts.erase(this)) ->
     sidetable_present() -> free(this)
     
     主要的几个函数：
     object_dispose(id obj)：源码中释放实例对象内存的入口函数；
     objc_destructInstance(id obj)：处理C++析构函数和关联对象的相关内存；
     object_cxxDestruct(obj)：调用析构函数释放属性或者成员变量；调用流程：
     object_cxxDestruct -> object_cxxDestructFromClass(obj, obj->ISA()) -> lookupMethodInClassAndLoadCache(cls, SEL_cxx_destruct) -> SEL_cxx_destruct ->
     汇编命令调用objc_storeStrong(id *location, id obj) -> 这里进行释放操作
     clearDeallocating_slow()：处理弱引用表和ARC引用计数表相关内存。
     
     为什么dealloc 中使用GCD 会容易访问野指针？
     从上述源码调用过程，可以发现dealloc内部最终都会走free操作，而这个操作就会导致野指针访问问题
     
     为什么属性或者说成员变量会自动释放？
     从上述源码调用过程，可以发现属性或者成员变量通过objc_storeStrong方法进行释放。
     
     dealloc注意总结：
     1. dealloc中尽量直接访问实例变量来置空：例如用下划线的实例变量置空，而不是用self.name置空
     2. dealloc中切记不能使用\__weak self：使用weak self引用变量，或者设置weak类型的属性（如：delegate = self）；
     3. dealloc中切线程操作尽量避免使用GCD，可利用performSelector，确保线程操作先于dealloc完成。
     
     dealloc机制应用：
     从源码中可以知道，dealloc在对象置nil以及free之前，会进行关联对象释放，那么可以利用关联对象销毁监听dealloc完成，做一些自动释放操作，例如通知监听释放等，实际上网上也是有一些例子了。
     例如：KVOController库等
     */
}

@end
