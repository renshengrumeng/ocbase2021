//
//  ZBYMATeacher.m
//  OCBase2021
//
//  Created by 周保勇 on 2021/5/14.
//

#import "ZBYMATeacher.h"

@interface ZBYMATeacher ()

/** 定时器 */
@property (nonatomic, strong) NSTimer * timer;

@end

@implementation ZBYMATeacher

- (instancetype)init {
    self = [super init];
    if (self) {
//        [self fireTimer];
    }
    return self;
}

- (void)setName:(NSString *)name {
//    _name = [name copy];
    [super setName:name];
    NSLog(@"ZBYMATeacher setName:%@", [NSString stringWithString:name]);
}

- (void)performSelectorWhenDealloc {
    __weak typeof(self) weakSelf = self;
    // 模拟复杂的block结构，需要弱引用解除循环引用
    void (^block)(void) = ^ {
        [weakSelf test];
    };
    block();
//    self.delegate = some; // 设置代理，代理一般为weak，和上述block中用weakSelf类相同
}

- (void)test {
    NSLog(@"test");
}

- (void)dealloc {
    NSLog(@"%@-dealloc", self.class);
//    [self performSelectorWhenDealloc];    // 存储弱引用
//    [self invalidateTimer]; // 移除定时器
    
    // GCD强引用self分析
//    dispatch_async(dispatch_queue_create("Kong", 0), ^{
//        [self test];
//    });
}

// MARK: - private

- (void)fireTimer {
    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        if (!weakSelf.timer) {
            weakSelf.timer = [NSTimer scheduledTimerWithTimeInterval:1.0 repeats:YES block:^(NSTimer * _Nonnull timer) {
                NSLog(@"TestDeallocModel timer:%p", timer);
            }];
            [[NSRunLoop currentRunLoop] addTimer:weakSelf.timer forMode:NSRunLoopCommonModes];
        }
    });
}

- (void)invalidateTimer {
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self.timer) {
            NSLog(@"TestDeallocModel invalidateTimer:%p model:%p", self->_timer, self);
            [self.timer invalidate];
            self.timer = nil;
        }
    });
}

@end
