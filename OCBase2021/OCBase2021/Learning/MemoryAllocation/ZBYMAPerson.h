//
//  ZBYMAPerson.h
//  OCBase2021
//
//  Created by 周保勇 on 2021/5/14.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZBYMAPerson : NSObject

@property (nonatomic, copy) NSString * _Nullable name;
@property (nonatomic, assign) int age;
@property (nonatomic, assign) long height;
@property (nonatomic, copy) NSString * hobby;

//@property (nonatomic, assign) int sex;
//@property (nonatomic) char ch1;
//@property (nonatomic) ichar ch1;

- (void)showName;

@end

NS_ASSUME_NONNULL_END
