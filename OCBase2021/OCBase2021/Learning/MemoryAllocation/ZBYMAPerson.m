//
//  ZBYMAPerson.m
//  OCBase2021
//
//  Created by 周保勇 on 2021/5/14.
//

#import "ZBYMAPerson.h"

@implementation ZBYMAPerson

- (void)setName:(NSString *)name {
    _name = [name copy];
    NSLog(@"ZBYMAPerson setName:%@", name);
}

- (void)showName {
    NSLog(@"my name's %@", self.name);
}

- (void)dealloc {
    NSLog(@"%@-dealloc", self.class);
    self.name = nil;    // 崩溃：self.name会调用setName，如果setName中有不能为空的，就会导致崩溃
//    _name = nil;    // 实例变量不崩溃
}

@end
