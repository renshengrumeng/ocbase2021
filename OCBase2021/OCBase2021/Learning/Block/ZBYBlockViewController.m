//
//  ZBYBlockViewController.m
//  OCBase2021
//
//  Created by 周保勇 on 2021/4/25.
//

#import "ZBYBlockViewController.h"
#import "ZBYBlockPerson.h"
#import "ZBYBlockCPModel.h"

@interface ZBYBlockViewController () <UITableViewDelegate, UITableViewDataSource>

// 声明一个Block类型
typedef int (^MySomeBlock) (int);

@property (nonatomic, copy) NSArray * models;

/// 定义一个声明了Block类型的Block属性
@property (nonatomic, copy) MySomeBlock mySomeBlock;

/// 定义一个没有声明Block类型的Block属性
@property (nonatomic, copy) void (^myBlock)(int sendValue);
@property (nonatomic, copy) void (^myBlock2)(UIViewController *);
@property (nonatomic, copy) void (^myBlock3)(void);

@end

@implementation ZBYBlockViewController

- (NSArray *)models {
    if (_models == nil) {
        _models = @[
        @{@"title" : @"Block基础", @"subTitle" : @"Block的定义和调用"},
        @{@"title" : @"Block调用和函数指针调用", @"subTitle" : @"Block调用和函数指针调用"},
        @{@"title" : @"Block捕获变量的类型", @"subTitle" : @"5种变量类型的捕获和未捕获变量"},
        @{@"title" : @"Block的类型", @"subTitle" : @"6种Block和3种常用的Block"},
        @{@"title" : @"Block的Copy", @"subTitle" : @"Copy"},
        @{@"title" : @"Block底层解析和循环引用", @"subTitle" : @"Block的底层解析和循环引用"},
        @{@"title" : @"Block循环引用解决", @"subTitle" : @"打破循环引用的三种方法"},
        @{@"title" : @"Block的弱引用和多线程的问题", @"subTitle" : @"Block的弱引用和多线程的问题"},
        @{@"title" : @"Block的链式编程", @"subTitle" : @"iOS链式编程"},
        @{@"title" : @"Block与函数指针", @"subTitle" : @"区别"}
        ];
    }
    return _models;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
    UITableView * tableView = [[UITableView alloc] initWithFrame:CGRectZero style:(UITableViewStylePlain)];
    tableView.delegate = self;
    tableView.dataSource = self;
    [self.view addSubview:tableView];
    
    tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
//        make.edges.equalTo(self.view);
    }];
    
//    __block UIViewController * vc = self;
//    self.myBlock = ^(int sendValue) {
//        dispatch_after(2, dispatch_get_main_queue(), ^{
//            NSLog(@"%@", vc);
//            vc = nil;
//        });
//    };

    self.myBlock2 = ^(UIViewController *vc) {
        dispatch_after(2, dispatch_get_main_queue(), ^{
            NSLog(@"%@", vc);
        });
    };
    
//    __weak typeof(self) weakSelf = self;
//    self.myBlock3 = ^ {
//        dispatch_after(2, dispatch_get_main_queue(), ^{
//            NSLog(@"%@", weakSelf);
//        });
//    };
    
    
//    self.myBlock(2);
    self.myBlock2(self);
//    self.myBlock3();
}

// MARK: - UITableViewDelegate, UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.models.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString * identifier = @"123456";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:identifier];
        cell.backgroundColor = [UIColor whiteColor];
        cell.textLabel.textColor = [UIColor blackColor];
        cell.detailTextLabel.textColor = [UIColor blackColor];
    }
    cell.textLabel.text = self.models[indexPath.row][@"title"];
    cell.detailTextLabel.text = self.models[indexPath.row][@"subTitle"];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSDictionary * info = self.models[indexPath.row];
    NSString * title = info[@"title"];
    if ([title containsString:@"Block基础"]) {
        [self blockBase];
    }
    if ([title containsString:@"Block调用和函数指针调用"]) {
        [self blockAndFunctionPointCall];
    }
    if ([title containsString:@"Block捕获变量的类型"]) {
        [self blockCaptureVariable];
    }
    if ([title containsString:@"Block的类型"]) {
        [self blockClassType];
    }
    if ([title containsString:@"Block底层解析和循环引用"]) {
        [self blockReferenceCycle];
    }
    if ([title containsString:@"Block循环引用解决"]) {
        [self breakBlockReferenceCycle];
    }
    if ([title containsString:@"Block的弱引用和多线程的问题"]) {
        [self blockWeakReferenceInMultithreading];
    }
    if ([title containsString:@"Block的链式编程"]) {
        [self blockChainProgramming];
    }
    if ([title containsString:@"Block的Copy"]) {
        [self blockAutoCopy];
    }
}

// MARK: - Block基础

- (void)blockBase {
    /*
     Block其它链接：https://juejin.cn/post/7046299111095533604
     */
    // 基础定义和调用
    int (^myBlock)(int) = ^(int value){
        NSLog(@"%d", value);
        return 1;
    };
    myBlock(2);
    
    // 声明Block类型和调用
    self.mySomeBlock = ^int(int value) {
        return value + 1;
    };
    if (self.mySomeBlock) {
        NSLog(@"block返回值=%d", self.mySomeBlock(2))
    }
}

// MARK: - Block调用和函数指针调用

- (void)blockAndFunctionPointCall {
    // 函数指针调用
    void (*funcP)(int) = &funcName; // 获取函数的指针
    (*funcP)(10);   // 函数的指针调用函数
    funcName(11);   // 函数直接调用
    
    // block代码块相当于匿名函数，block名称相当于匿名函数的指针
    void (^block)(int) = ^(int a) {
        NSLog(@"block-%d\n", a);
    };
    block(10);  // block代码块调用
}

/// 函数
void funcName(int a) {
    NSLog(@"funcName-%d\n", a);
}
    
// MARK: - Block捕获变量的类型

// 全局变量
int globalValue = 0;
// 全局静态变量
static int staticGlobalValue = 0;
    
- (void)blockCaptureVariable {
    // 局部静态变量
    static int staticValue = 0;
    // 局部变量
    int autoValue = 0;
    // block变量
    __block int blockValue = 0;
    
    // 未捕获变量
    void (^noVlaueBlock)(void) = ^{
        NSLog(@"未捕获变量的Block\n");
    };
    // 捕获全局静态变量
    void (^staticGlobalValueBlock)(void) = ^{
        staticGlobalValue = 2;
        NSLog(@"捕获全局静态变量的Block\n");
    };
    // 捕获全局变量
    void (^globalValueBlock)(void) = ^{
        globalValue = 2;
        NSLog(@"捕获全局变量的Block\n");
    };
    /**
     当Block捕获静态全局变量、全局变量和未捕获外部的情况相同：
     struct __block_impl {
       void *isa;
       int Flags;
       int Reserved;
       void *FuncPtr;
     };

     struct __showBlockBase_block_impl_0 {
       struct __block_impl impl;
       struct __showBlockBase_block_desc_0* Desc;
       __showBlockBase_block_impl_0(void *fp, struct __showBlockBase_block_desc_0 *desc, int flags=0) {
         impl.isa = &_NSConcreteStackBlock;
         impl.Flags = flags;
         impl.FuncPtr = fp;
         Desc = desc;
       }
     };
     */
    
    // 捕获局部静态变量
    void (^staticValueBlock)(void) = ^{
        staticValue = 2;
        NSLog(@"捕获局部静态变量的Block\n");
    };
    /**
     当Block捕获静态变量时，Block在底层解释为相关类时会增加一个指针类型的属性变量，该指针类型的值类型与该静态变量的值类型相对应
     // 局部静态变量
     static int staticValue = 0;
     
     struct __showBlockBase_block_impl_3 {
         struct __block_impl impl;
         struct __showBlockBase_block_desc_3* Desc;
         int *staticValue;  // 捕获的外界局部静态变量
         __showBlockBase_block_impl_3(void *fp, struct __showBlockBase_block_desc_3 *desc, int *_staticValue, int flags=0) : staticValue(_staticValue) {
             impl.isa = &_NSConcreteStackBlock;
             impl.Flags = flags;
             impl.FuncPtr = fp;
             Desc = desc;
         }
     };
     */
    
    // 捕获局部普通变量（局部变量、自动变量）
    void (^autoValueBlock)(void) = ^{
        NSLog(@"%@", [NSString stringWithFormat:@"捕获局部普通变量的Block-%d\n", autoValue]);
    };
    /**
     当Block捕获自动变量时，Block在底层解释为相关类时会增加一个的属性变量与该自动变量相对应
     // 自动变量
     int value = 0;
     
     struct __showBlockBase_block_impl_4 {
         struct __block_impl impl;
         struct __showBlockBase_block_desc_4* Desc;
         int value;  // 捕获的外界自动变量
         __showBlockBase_block_impl_4(void *fp, struct __showBlockBase_block_desc_4 *desc, int _value, int flags=0) : value(_value) {
             impl.isa = &_NSConcreteStackBlock;
             impl.Flags = flags;
             impl.FuncPtr = fp;
             Desc = desc;
         }
     };
     */
    
    // 捕获局部普通变量
    void (^blockValueBlock)(void) = ^{
        blockValue = 2;
        NSLog(@"捕获局部普通变量的Block\n");
    };
    /**
     当Block捕获__block修饰的自动变量时，Block在底层解释为相关类时会增加一个指针类型的属性变量，且该属性变量的类型是该类型对应的一个类，类中的一个属性变量的值与该自动变量相对应；
     当该Block从栈copy到堆上时，栈上__block修饰的变量对应的类的实例中的__forwarding指针和对上的类实例的__forwarding都是指向堆中的变量
     // block变量
     __block int i = 0;

     struct __Block_byref_i_0 {
         void *__isa;
         __Block_byref_i_0 *__forwarding;
         int __flags;
         int __size;
         int i;
     };
     
     struct __showBlockBase_block_impl_5 {
         struct __block_impl impl;
         struct __showBlockBase_block_desc_5* Desc;
         __Block_byref_i_0 *i;  // 捕获的外界__block修饰的自动变量
         __showBlockBase_block_impl_5(void *fp, struct __showBlockBase_block_desc_5 *desc, __Block_byref_i_0 *_i, int flags=0) : i(_i->__forwarding) {
             impl.isa = &_NSConcreteStackBlock;
             impl.Flags = flags;
             impl.FuncPtr = fp;
             Desc = desc;
         }
     };
     */
    
    noVlaueBlock();
    staticGlobalValueBlock();
    globalValueBlock();
    staticValueBlock();
    autoValueBlock();
    blockValueBlock();
}

// MARK: - Block的类型

- (void)blockClassType {
    __block int value = 2;
    void (^blockValueBlock)(void) = ^{
        value = 2;
        NSLog(@"捕获局部普通变量的Block\n");
    };
    NSArray * superClassList = [[blockValueBlock class] superClassList];
    NSLog(@"%@", superClassList);
    NSLog(@"%@", [NSClassFromString(@"NSBlock") subClassList]);
    /**从上述打印中可以看到Block有NSBlock父类及其4个子类NSGlobalBlock、NSAutoBlock、NSMallocBlock、
     NSStackBlock
     */
    /** 查看地址：https://opensource.apple.com/tarballs/ -> 搜索libclosure -> 下载最新的版本 -> Block_private.h和Block.h文件
     libclosure是Block底层动态库
     // Block_private.h
     BLOCK_EXPORT void * _NSConcreteMallocBlock[32];
     BLOCK_EXPORT void * _NSConcreteAutoBlock[32];
     BLOCK_EXPORT void * _NSConcreteFinalizingBlock[32];
     BLOCK_EXPORT void * _NSConcreteWeakBlockVariable[32];
     // Block.h
     BLOCK_EXPORT void * _NSConcreteGlobalBlock[32];
     BLOCK_EXPORT void * _NSConcreteStackBlock[32];
     
     从底层动态库中看到NSBlock共有6个子类：其中NSConcreteFinalizingBlock和NSConcreteWeakBlockVariable在运行时不可见；
     _NSConcreteAutoBlock对应NSAutoBlock，系统调用，不对外使用；
     常用的为三个：NSGlobalBlock、NSMallocBloc、NSStackBlock
     */
    
    /**
     Block根据在内存分区的位置，可以分为三类：
        1.全局Block（NSGlobalBlock）即在静态全局区的Block；
        2.栈Block（NSStackBlock）即在栈上的Block；
        3.堆Block（NSMallocBlock）即在堆上的Block。
       当Block在捕获静态变量、静态全局变量、全局变量、未捕获外部变量时，为全局Block。
       当Block定义后被赋值给了某个变量或者某个类的属性时，在这个过程中会执行 Block_copy将原有的NSStakeBlock变成NSMallocBlock；
       当Block定义后没有赋值给某个变量，那它的类型就是NSStakeBlock。
     */
}

// MARK: - Block的Copy

- (void)blockAutoCopy {
    /**
     不同类型Block的copy：
     全局block，copy什么也不做；
     栈block，copy将栈中的block复制到堆上，类似深拷贝；
     堆block，copy后内存不变，类似浅拷贝
     
     大多数情况下编译器会对Block进行判断并自动Copy到堆上。
     Block的自动Copy的场景：
     1.Block赋值给通过strong或copy修饰的id或block类型的成员变量时；
     2.Cocoa框架的方法且方法名中含有usingBlock等时；
     3.GCD的API时；
     4.Block作为函数的返回值时；
     
     需要手动Copy Block的场景：
     1.Block作为函数的参数时
     */
}

// MARK: - Block的循环引用和底层解析

- (void)blockReferenceCycle {
    /** Block的copy
     1.Block的自动copy，何种情况下触发自动copy，什么情况下需要手动copy呢？
        1.1当Block是实例变量的属性或者当Block赋值给变量时，即Block有强引用时，会自动copy；
        1.2当Block作为函数的返回值时，会自动copy；
        1.3Cocoa框架的方法且方法名中含有usingBlock等的方法，会自动copy；
        1.4GCD中的接口的Block会自动copy；
        1.5如果方法中的某个参数是Block时，需要将Block进行copy时，需要手动调用[Block copy];
     2.三种常用的Block的copy发生了什么？
        2.1全局Block没有copy的操作，对其copy，相当于什么也不做；
        2.2栈Block通过copy操作，类似于深拷贝，将栈Block复制一份到堆区，堆区的Block就是堆Block；
        2.3堆Block的copy操作，类似于浅拷贝，copy的变量指针指向同一份的堆Block。
     3.Block的copy操作的底层解析，以捕获__block的Block为例(三层copy)
        3.1栈Block调用copy方法，会新建一个堆Block，堆Block会retain方法对其进行ARC内存计数；
        3.2retain -> objc_retainBlock -> _Block_copy
        3.3_Block_copy中会对堆Block进行初始化和其它设置（也叫Block的三层copy）：
            3.3.1 完成对栈Block内存的开辟等；
            3.3.2 对__block变量对应的实例的深复制，且将栈对应的实例的__forwarding指向__block变量；
            3.3.3 对堆Block的__block变量对应的实例的对应属性的赋值为__block变量的值。
     4 当这种Block从栈中Copy到堆中，需要进行三步，也叫Block的三层Copy：
        4.1 创建一个__Block_byref_i_0对象到堆中，该对象的存在一个属性，该属性为指针（__forwarding），指向__block修饰的变量的地址；
        4.2 创建__showBlockBase_block_impl_5对象，即Block调用对象，该对象存在一个属性，该属性为指针，指向__Block_byref_i_0对象的地址；
        4.3 栈Block即其它调用环境拷贝到堆上，新增一个堆Block。
     */
    
    int autoValue = 2;
    void (^noVlaueBlock)(void) = ^{
        NSLog(@"未捕获变量的Block\n");
    };
    void (^noVlaueBlock2)(void) = [noVlaueBlock copy];
    NSLog(@"%@-%@", noVlaueBlock, noVlaueBlock2);
    void (^autoValueBlock)(void) = ^{
        NSLog(@"%@", [NSString stringWithFormat:@"捕获局部普通变量的Block-%d\n", autoValue]);
    };
    void (^autoValueBlock2)(void) = [autoValueBlock copy];
    NSLog(@"%@-%@", autoValueBlock, autoValueBlock2);
//    NSLog(@"%@", [noVlaueBlock class]);
//    NSLog(@"%@", [noVlaueBlock copy]);
//    NSLog(@"%@", [autoValueBlock class]);
//    NSLog(@"%@", [^{
//        NSLog(@"%@", [NSString stringWithFormat:@"捕获局部普通变量的Block-%d\n", autoValue]);
//    } class]);
    
    MySomeBlock block = ^(int value){
        return value + 1;
    };
    MySomeBlock block2 = [block copy];
    MySomeBlock block3 = [block copy];
    NSLog(@"%p-%p-%p", block, block2, block3);
    
    /** Block的内存管理
     1.Block首次定义是在栈上，对栈上的Block进行copy操作时，会调用objc_retainBlock方法，在该方法中会调用
        _Block_copy将栈上的Block复制到堆上；
     2.栈上的Block在执行完毕后会立即释放，而堆上的Block通过ARC机制管理自己的内存。
     */
    /** Block的循环引用，为什么？
     1.栈上的Block在执行完毕后就会释放，一般不会导致循环引用；我们所说的循环引用是堆上的Block形成；
     2.堆Block循环引用的原因：对象间接或者直接持有Block，Block中间接或者直接访问（持有）该对象，就会造成循环引用，
        导致对象和Block内存无法释放。
     */
    self.myBlock = ^(int sendValue) {
        // 直接引用self
        NSLog(@"%@", self);
        NSLog(@"%@", self.description);
    };
    if (self.myBlock) {
        self.myBlock(2);
    }
}

// MARK: - Block循环引用解决
    
- (void)breakBlockReferenceCycle {
    /** 避免Block循环引用有三种方法：
     1.创建一个__weak修饰的指针变量，将造成循环引用的对象的赋值该指针变量，在Block中使用该指针变量，打破循环引用；
     2.在Block可执行完毕后将所持有对象设置为空，但是该方法如果该Block没有执行，依旧不能打破循环引用；
     3.将持有对象作为Block的参数传入。
     */
    // 方法1
    __weak typeof(self) weakSelf = self;
    self.myBlock3 = ^ {
        dispatch_after(2, dispatch_get_main_queue(), ^{
            NSLog(@"%@", weakSelf);
        });
    };
    self.myBlock3();
    
    // 方法2
    __block UIViewController * vc = self;
    self.myBlock = ^(int sendValue) {
        dispatch_after(2, dispatch_get_main_queue(), ^{
            NSLog(@"%@", vc);
            vc = nil;
        });
    };
    self.myBlock(2);
    
    // 方法3
    self.myBlock2 = ^(UIViewController *vc) {
        dispatch_after(2, dispatch_get_main_queue(), ^{
            NSLog(@"%@", vc);
        });
    };
    self.myBlock2(self);
}

// MARK: - Block的弱引用和多线程的问题

- (void)blockWeakReferenceInMultithreading {
    /** 问题引出
     在多线程中，在一个线程执行之前，Block的弱引用对象已经释放，那么在该线程中执行Block时，访问的弱引用对象为nil
     */
    ZBYBlockPerson * person = [[ZBYBlockPerson alloc] init];
    // 弱引用对象
    __weak typeof(ZBYBlockPerson) *weakPerson = person;
    person.name = @"zhou";
    person.block = ^(int age) {
        // 开辟一条线程：
        dispatch_after(3, dispatch_get_global_queue(0, 0), ^{
            // 访问弱引用的对象
            NSLog(@"name = %@", weakPerson.name);
        });
    };
    person.block(33);
    
    /** 问题解决
     在多线程中，在一个线程执行之前，用一个强引用指向Block外的弱引用，就可以解决，且在该线程代码块执行完毕后，释放该引用对象。
     */
    ZBYBlockPerson * person2 = [[ZBYBlockPerson alloc] init];
    // 弱引用对象
    __weak typeof(ZBYBlockPerson) *weakPerson2 = person2;
    person2.name = @"zhou";
    person2.block = ^(int age) {
        __strong typeof(ZBYBlockPerson) * strongPerson = weakPerson2;
        // 开辟一条线程
        dispatch_after(3, dispatch_get_global_queue(0, 0), ^{
            // 访问弱引用的对象
            NSLog(@"name = %@", strongPerson.name);
        });
    };
    person2.block(2);
}

// MARK: - Block的链式编程

- (void)blockChainProgramming {
    ZBYBlockCPModel * cpModel = [ZBYBlockCPModel new];
    cpModel.result = 1;
    CGFloat result = cpModel.add(3).minus(2).mutiply(4).divide(2).result;
    NSLog(@"%f", result);
}

// MARK: - Block和函数指针

- (void)blockAndFunctionPointer {
    /**
     相似点：
     1.函数指针和Block都可以实现回调的操作，声明上也很相似，实现上都可以看成是一个代码片段。
     2.函数指针类型和Block类型都可以作为变量和函数参数的类型。（typedef定义别名之后，这个别名就是一个类型）
     
     不同点：
     1.函数指针只能指向预先定义好的函数代码块（可以是其他文件里面定义，通过函数参数动态传入的），函数地址是在编译链接时就已经确定好的。
     2.Block本质是Objective-C对象，是NSObject的子类，可以接收消息。
     3.函数里面只能访问全局变量，而Block代码块不光能访问全局变量，还拥有当前栈内存和堆内存变量的可读性（当然通过__block访问指示符修饰的局部变量还可以在block代码块里面进行修改）。
     4.从内存的角度看，函数指针只不过是指向代码区的一段可执行代码，而block实际上是程序运行过程中在栈内存动态创建的对象，可以向其发送copy消息将block对象拷贝到堆内存，以延长其生命周期。 关于第2点可以作一个实验，在定义block之后打一个断点，Cmd+R运行后，可以在调试窗口看到，block确实是一个对象，拥有isa指针。 另外，采用block写法，gcc编译出来可执行文件体积更大，这应该还是跟block是对象有关。
     */
}

@end
