//
//  ZBYBlockCPModel.m
//  OCBase2021
//
//  Created by 周保勇 on 2021/5/21.
//

#import "ZBYBlockCPModel.h"

@implementation ZBYBlockCPModel

- (ZBYBlockCPModel *(^)(CGFloat value))add {
    return ^id(CGFloat value) {
        self.result += value;
        return self;
    };
}

- (ZBYBlockCPModel *(^)(CGFloat value))minus {
    return ^id(CGFloat value) {
        self.result -= value;
        return self;
    };
}

- (ZBYBlockCPModel *(^)(CGFloat value))mutiply {
    return ^id(CGFloat value) {
        self.result *= value;
        return self;
    };
}

- (ZBYBlockCPModel *(^)(CGFloat value))divide {
    return ^id(CGFloat value) {
        self.result /= value;
        return self;
    };
}

@end
