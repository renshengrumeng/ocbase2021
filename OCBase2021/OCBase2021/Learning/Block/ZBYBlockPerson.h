//
//  ZBYBlockPerson.h
//  OCBase2021
//
//  Created by 周保勇 on 2021/5/13.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZBYBlockPerson : NSObject

/// 名称
@property (nonatomic, copy) NSString * name;


typedef void (^Block) (int age);

/// block
@property (nonatomic, copy) Block block;

@end

NS_ASSUME_NONNULL_END
