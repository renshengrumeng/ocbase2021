//
//  ZBYBlockCPModel.h
//  OCBase2021
//
//  Created by 周保勇 on 2021/5/21.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@class ZBYBlockCPModel;
typedef ZBYBlockCPModel * _Nonnull (^ZBYBlockCPModelBlock)(CGFloat value);

//typedef <#returnType#> (^<#BlockName#>) (<#type#>);


@interface ZBYBlockCPModel : NSObject

/// 结果值
@property (nonatomic, assign) CGFloat result;

- (ZBYBlockCPModel *(^)(CGFloat value))add;

- (ZBYBlockCPModel *(^)(CGFloat value))minus;

- (ZBYBlockCPModel *(^)(CGFloat value))mutiply;

- (ZBYBlockCPModel *(^)(CGFloat value))divide;

@end

NS_ASSUME_NONNULL_END
