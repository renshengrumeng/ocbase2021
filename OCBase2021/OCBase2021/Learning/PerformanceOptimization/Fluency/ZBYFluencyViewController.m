//
//  ZBYFluencyViewController.m
//  OCBase2021
//
//  Created by anniekids good on 2024/4/8.
//

#import "ZBYFluencyViewController.h"

@interface ZBYFluencyViewController ()

@end

@implementation ZBYFluencyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)fluencyBase {
    /**
     https://blog.csdn.net/u014600626/article/details/122524652
     
     市面上的iOS卡顿分析方案有三种：监控FPS、监控RunLoop、ping主线程。

     前面2个都比较熟悉，第三个是最近才了解到的。
     
     方案一：监控FPS

     一般来说，我们约定60FPS即为流畅。那么反过来，如果App在运行期间出现了掉帧，即可认为出现了卡顿。

     监控FPS的方案几乎都是基于CADisplayLink实现的。简单介绍一下CADisplayLink：CADisplayLink是一个和屏幕刷新率保持一致的定时器，一但 CADisplayLink 以特定的模式注册到runloop之后，每当屏幕需要刷新的时候，runloop就会调用CADisplayLink绑定的target上的selector。
     可以通过向RunLoop中添加CADisplayLink，根据其回调来计算出当前画面的帧数。
     */
}

@end
