//
//  ZBYPerformanceOptimizationViewController.m
//  OCBase2021
//
//  Created by anniekids good on 2024/4/8.
//

#import "ZBYPerformanceOptimizationViewController.h"
#import "ZBYLessLaunchViewController.h"

@interface ZBYPerformanceOptimizationViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, copy) NSArray * models;

@end

@implementation ZBYPerformanceOptimizationViewController

- (NSArray *)models {
    if (_models == nil) {
        _models = @[
        @{@"title" : @"简介", @"subTitle" : @"性能优化简介"},
        @{@"title" : @"启动优化", @"subTitle" : @"启动优化"},
        @{@"title" : @"内存优化", @"subTitle" : @"内存优化"},
        @{@"title" : @"流畅度优化", @"subTitle" : @"流畅度优化"},
        @{@"title" : @"耗电量优化", @"subTitle" : @"耗电量优化"}];
    }
    return _models;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
    UITableView * tableView = [[UITableView alloc] initWithFrame:CGRectZero style:(UITableViewStylePlain)];
    tableView.delegate = self;
    tableView.dataSource = self;
    [self.view addSubview:tableView];
    
    tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    tableView.contentInset = UIEdgeInsetsMake(0, 0, (ZBYSize.tabBarHeight + ZBYSize.bottomSafeHeight), 0);
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(ZBYSize.navigationBarHeight);
        make.left.right.bottom.mas_equalTo(0);
    }];
}

- (void)optimizeDescription {
    /**
     https://blog.csdn.net/good7ob/article/details/131487023
     简介：
     在现代移动应用的开发中，性能优化是至关重要的一环。一个高性能的iOS应用不仅能够提供流畅的用户体验，还能够节省设备资源和延长电池寿命。本文将为你总结iOS性能优化的关键步骤、代码技巧和实际案例，帮助你打造出高效、流畅的iOS应用。
     
     工具：
     在进行性能优化之前，首先需要对应用进行性能分析，找出瓶颈和优化的重点。可以使用工具如Instruments、Xcode Profiler等进行性能监测和分析，查看CPU、内存、网络等方面的使用情况。
     
     优化方向：
     
     减少视图层次：
     视图层次过多会增加渲染和布局的负担，影响应用的性能。因此，我们应该尽量减少视图层次的嵌套，避免不必要的视图层次结构。
     
     自动布局和Frame布局：
     视图层次过多会增加渲染和布局的负担，影响应用的性能。因此，我们应该尽量减少视图层次的嵌套，避免不必要的视图层次结构。
     自动布局和Frame布局的优缺点：https://www.jianshu.com/p/165fea085eee
     
     图片优化：
     图像是iOS应用中常见的资源，优化图像加载和显示对于性能提升至关重要。可以通过以下几种方式来优化图像：
     压缩图像大小，减少内存占用。
     使用适合屏幕分辨率的图像，避免过大或过小的图像显示。
     使用适当的图像格式，如JPEG、PNG等。
     异步加载图像，避免阻塞主线程。
     用自带弧度或者阴影的图片，避免代码绘制。
     
     使用异步操作
     将耗时的操作移出主线程，使用异步操作可以避免阻塞用户界面，提高应用的响应性。可以使用GCD（Grand Central Dispatch）或Operation队列来管理异步任务。
     
     内存管理和资源释放
     内存管理是iOS性能优化中的关键一环。避免内存泄漏和过度使用内存，及时释放不再需要的资源是至关重要的。以下是一些内存管理的实践建议：
     及时释放不再使用的对象，特别是在循环引用的情况下。
     使用weak引用避免循环引用。
     
     数据存储：
     对于频繁的数据存储和访问，可以通过以下方式进行优化：
     使用合适的数据存储方案，如使用Core Data、Realm或SQLite等。
     使用异步读写操作，避免阻塞主线程。
     合理设计数据结构和索引，提高数据查询和访问的效率。
     
     网络请求和数据加载：
     网络请求和数据加载是移动应用中常见的性能瓶颈之一。以下是一些优化网络请求和数据加载的方法：
     使用HTTP缓存，减少重复请求。
     合理设置请求超时时间，避免长时间的等待。
     使用分页加载和懒加载，减少一次性加载大量数据。
     图片和资源的延迟加载，优化用户体验。
     
     合理使用动画和过渡效果：
     动画和过渡效果可以增加应用的交互性和吸引力，但过多或复杂的动画会消耗大量的CPU资源。因此，我们应该合理使用动画效果，避免过度绘制和动画链的嵌套。
     
     优化表格视图性能：
     让我们通过一个实际案例来演示性能优化的步骤和代码。

     假设我们的应用中有一个表格视图（UITableView），需要显示大量的数据。为了提高表格视图的性能，我们可以采取以下优化措施：

     使用dequeueReusableCell(withIdentifier:for:)方法复用单元格，减少创建和销毁单元格的开销。

     使用willDisplay(_:forRowAt:)方法预加载单元格的数据，避免滑动过程中的延迟加载。

     对于复杂的单元格，可以使用异步加载和缓存机制，提高单元格的渲染效率。

     合理设置表格视图的行高和分区头部/尾部视图，避免过度绘制和布局。

     通过以上优化措施，我们可以提高表格视图的滚动性能，使用户可以流畅地浏览和操作大量数据。
     
     结语：
     性能优化是一个持续的过程，应该定期进行性能测试和评估，找出应用中的瓶颈和性能问题，并针对性地进行优化。使用性能测试工具和监测工具，如Xcode Profiler、Instruments等，进行性能分析和优化。
     */
}

// MARK: - UITableViewDelegate, UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.models.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString * identifier = @"123456";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.backgroundColor = [UIColor whiteColor];
        cell.textLabel.textColor = [UIColor blackColor];
        cell.detailTextLabel.textColor = [UIColor blackColor];
    }
    cell.textLabel.text = self.models[indexPath.row][@"title"];
    cell.detailTextLabel.text = self.models[indexPath.row][@"subTitle"];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSDictionary * info = self.models[indexPath.row];
    NSString * title = info[@"title"];
    if ([title containsString:@"启动优化"]) {
        ZBYLessLaunchViewController * launchVC = [ZBYLessLaunchViewController new];
        launchVC.title = title;
        [self.navigationController pushViewController:launchVC animated:YES];
    }
}

@end
