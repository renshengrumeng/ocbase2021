//
//  ZBYLessLaunchViewController.m
//  OCBase2021
//
//  Created by 周保勇 on 2021/5/8.
//

#import "ZBYLessLaunchViewController.h"
#import <dlfcn.h>
#import <libkern/OSAtomicQueue.h>

@interface ZBYLessLaunchViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, copy) NSArray * models;

@end

@implementation ZBYLessLaunchViewController

- (NSArray *)models {
    if (_models == nil) {
        _models = @[
        @{@"title" : @"二进制排序启动优化原理", @"subTitle" : @"启动优化原理"},
        @{@"title" : @"代码文件编译顺序和函数符号排序", @"subTitle" : @"排序的改变和获取"},
        @{@"title" : @"llvm静态插桩", @"subTitle" : @"hook所有的方法（包含OC方法、Block代码块、C和C++方法）"}
        ];
    }
    return _models;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
    UITableView * tableView = [[UITableView alloc] initWithFrame:CGRectZero style:(UITableViewStylePlain)];
    tableView.delegate = self;
    tableView.dataSource = self;
    [self.view addSubview:tableView];
    
    tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];
}

// MARK: - UITableViewDelegate, UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.models.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString * identifier = @"123456";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:identifier];
        cell.backgroundColor = [UIColor whiteColor];
        cell.textLabel.textColor = [UIColor blackColor];
        cell.detailTextLabel.textColor = [UIColor blackColor];
    }
    cell.textLabel.text = self.models[indexPath.row][@"title"];
    cell.detailTextLabel.text = self.models[indexPath.row][@"subTitle"];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSDictionary * info = self.models[indexPath.row];
    NSString * title = info[@"title"];
    if ([title containsString:@"代码文件编译顺序和函数符号排序"]) {
        [self getMarchOSortFile];
    }
    if ([title containsString:@"llvm静态插桩"]) {
        [self hookAllMessage];
        NSString * filePath = [NSTemporaryDirectory() stringByAppendingPathComponent:@"symbol.order"];
        [self saveSymbolListWithFilePath: filePath];
        NSString * funcSymbolStr = [self getSymbolListWithFilePath:filePath];
        NSLog(@"排序文件：\n%@", funcSymbolStr);
    }

}

// MARK: - 二进制重排优化启动原理

- (void)machOFileLoadSort {
    // 抖音品质建设 - iOS启动优化《原理篇》 https://mp.weixin.qq.com/s?__biz=MzI1MzYzMjE0MQ==&mid=2247486932&idx=1&sn=eb4d294e00375d506b93a00b535c6b05&chksm=e9d0c636dea74f20ec800af333d1ee94969b74a92f3f9a5a66a479380d1d9a4dbb8ffd4574ca&scene=21#wechat_redirect
    // 抖音品质建设 - iOS启动优化《实战篇》 https://mp.weixin.qq.com/s?__biz=MzI1MzYzMjE0MQ==&mid=2247487757&idx=1&sn=a52c11f6a6f217bd0d3283de9b00c8bc&chksm=e9d0daefdea753f954cfcb15d5d0f90302a9f45ba06968377644ffe9e5757a69c5b0132d2c8b&scene=178&cur_album_id=1590407423234719749#rd
    // 字节跳动团队掘金博客：https://juejin.cn/user/1838039172387262
    // 美团外卖iOS App冷启动治理：https://tech.meituan.com/2018/12/06/waimai-ios-optimizing-startup.html
}

// MARK: - 获取二进制排序文件

- (void)getMarchOSortFile {
    /** 如何查看和修改文件的编译顺序呢？
     1.Build Phases -> Compile Sources中.m文件的顺序，就是.m对应的march-o的编译连接顺序；想要改变march-o文件的编译连接顺序，拖动其对应的.m为自己想要的顺序就行了
     2.项目中的函数符号表，默认是根据文件顺序进行连接编译的；比如如果AppDelegate在前，会将AppDelegate文件中的所有
     函数符号连接编译后，再进行后续文件的函数符号连接编译
     */
    
    /** 如何查找整个项目函数符号的编译连接顺序呢？
     步骤如下：
     1.修改设置：Build Settings -> Linking -> Write Link Map File -> YES
     2.Command B编译一下，在工程目录Products中选中“项目.app”，右键Show in Finder
     3.与Products同级的Intermediates.noindex文件夹下 -> 项目名称.build -> Debug-iphoneos -> 项目名称.build -> 项目名称-LinkMap-normal-arm64.txt
     4.项目名称-LinkMap-normal-arm64.txt文件说明：
        4.1文件中：# Object files:是代码文件的编译顺序；
        4.2# Sections:分为代码段（__TEXT，只读）和数据段（__DATA， 可读可写）
        4.3# Symbols:这里就是我们重点关注的，默认函数符号是按文件进行排列的，而不是按照函数调用的先后顺序进行排列的；
            其中Address为函数的执行内存地址，size为函数内存的大小，file是所在文件的编译顺序，name是函数名称（_下划线开头为C或者C++函数，-[或者+[为OC方法）
     */
    
    /** 如何按照项目的启动函数符号调用顺序进行重新排序呢？这就是二进制重排，函数符号默认是按照文件进行排列的，page fault或page in的优化原理见上抖音品质建设 - iOS启动优化《原理篇》
     1、获取下列用 llvm静态插桩 的方法获取的函数调用顺序的文件（Xcode -> Window -> Devices And Simulators -> 手机 -> 项目 -> ⚙️ -> Download Contaier -> 下载的文件右键显示包内容 -> 排序文件）
     2、将1中的排序文件放入工程项目中
     3、工程项目配置Link File：Build Settings -> Order File -> 2中文件的路径
     4、command B编译
     5、按照上述（如何查找整个项目函数符号的编译连接顺序呢？）步骤，寻找排序后的文件，查看是否重新排序
     */
    [ZBYMBProgressTool showText:@"文件及符号排序的改变和获取" animated:YES];
}



// MARK: - llvm静态插桩

- (void)hookAllMessage {
    // llvm静态插桩hook：https://clang.llvm.org/docs/SanitizerCoverage.html
    
    /** llvm静态插桩hook步骤
     1、在Build Settings -> Other C Flags / Other C++ Flags -> 添加：-fsanitize-coverage=trace-pc-guard
     2、添加__sanitizer_cov_trace_pc_guard_init和__sanitizer_cov_trace_pc_guard的函数
     3、用原子队列保存调用符号的节点：由于OC中方法、函数或者block代码块的调用有可能在其它线程中调用，所以要用原子队列保存，而不是用数组保存（课表数组不是线程安全的）
     4、在适当的时机取出符号的数组，即为函数符号表的调用顺序；取出原子队列的数据：__sanitizer_cov_trace_pc_guard的天坑，while循环会触发__sanitizer_cov_trace_pc_guard函数调用；在配置中改为：-fsanitize-coverage=trace-pc-guard改为-fsanitize-coverage=func,trace-pc-guard
     5、如果和Swift文件混编，需要在1步骤中添加设置：Build Settings -> Other swift Flags -> 添加：-sanitize-coverage=func和-sanitize=undefined
     */
    test();
    [ZBYLessLaunchSwiftClass showMessage];
}

/// 获取符号调用顺序文件
/// @param filePath <#filePath description#>
- (NSString *)getSymbolListWithFilePath:(NSString *)filePath {
    return [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:nil];
}

/// 保存符号调用顺序文件
- (void)saveSymbolListWithFilePath:(NSString *)filePath {
    // 取出原子队列的数据：__sanitizer_cov_trace_pc_guard的天坑，while循环会触发__sanitizer_cov_trace_pc_guard函数调用；在配置中改为：-fsanitize-coverage=trace-pc-guard改为-fsanitize-coverage=func,trace-pc-guard
    // 用数组存放，去重和排序
    NSMutableArray<NSString *> * funcSymbolList = [[NSMutableArray alloc] init];
    NSMutableArray<NSString *> * messageSymbolList = [[NSMutableArray alloc] init];
    while (YES) {   // while循环会触发__sanitizer_cov_trace_pc_guard函数，天坑
        SYNode * node = OSAtomicDequeue(&symbolList, offsetof(SYNode, next));
        if (node->next == NULL) {
            break;
        }
        Dl_info info;
        dladdr(node->pc, &info);
        NSString * messageSymbolStr = @(info.dli_sname);
        // 设置重排字符串
        BOOL isObjc = [messageSymbolStr hasPrefix:@"-["] || [messageSymbolStr hasPrefix:@"+["];
        NSString * symbolStr = isObjc ? messageSymbolStr : [@"_" stringByAppendingString:messageSymbolStr];
        [messageSymbolList addObject:symbolStr];
    }
    
    // 取反
    NSEnumerator * emt = [messageSymbolList reverseObjectEnumerator];
    // 去重
    NSString * funcName;
    while (funcName = [emt nextObject]) {
        if (![funcSymbolList containsObject:funcName]) {
            [funcSymbolList addObject:funcName];
        }
    }
    // 把自己去掉
    [funcSymbolList removeObject:[NSString stringWithFormat:@"%s", __FUNCTION__]];
    // 将数组变为字符串
    NSString * funcSymbolStr = [funcSymbolList componentsJoinedByString:@"\n"];
    
    // 存入文件
//    NSString * filePath = [NSTemporaryDirectory() stringByAppendingPathComponent:@"symbol.order"];
    NSDate * funcSymbolData = [funcSymbolStr dataUsingEncoding:NSUTF8StringEncoding];
    [[NSFileManager defaultManager] createFileAtPath:filePath contents:funcSymbolData attributes:nil];
}

+ (void)load {
    
}

+ (void)initialize {
    
}

// block代码块
void (^testBlock)(void) = ^(void) {
    
};

// c函数
void test() {
    testBlock();
}

void __sanitizer_cov_trace_pc_guard_init(uint32_t *start,
                                                    uint32_t *stop) {
    static uint64_t N;  // Counter for the guards.
    if (start == stop || *start) return;  // Initialize only once.
    printf("INIT: %p %p\n", start, stop);
    for (uint32_t *x = start; x < stop; x++)
    *x = ++N;  // Guards should start from 1.
}

// 原子队列保存调用函数列表
static OSQueueHead symbolList = OS_ATOMIC_QUEUE_INIT;
// 定义队列的节点
typedef struct {
    void *pc;
    void *next;
} SYNode;

void __sanitizer_cov_trace_pc_guard(uint32_t *guard) {
    // 由于OC中方法、函数或者block代码块的调用有可能在其它线程中调用，所以要用原子队列保存，而不是用数组保存（课表数组不是线程安全的）
//    if (!*guard) return;  // 屏蔽load方法调用
    void *PC = __builtin_return_address(0);
    // 通过函数地址拿去函数名称
    // 存入原子队列
    SYNode * node = malloc(sizeof(SYNode));
    *node = (SYNode){PC, NULL};
    // 第一个参数是原子队列头指针，第二个参数为存入节点，第三个参数为存入节点的相对队列头指针的偏移量
    OSAtomicEnqueue(&symbolList, node, offsetof(SYNode, next));
}

@end
