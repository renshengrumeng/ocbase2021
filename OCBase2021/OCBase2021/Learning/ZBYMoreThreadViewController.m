//
//  ZBYMoreThreadViewController.m
//  OCBase2021
//
//  Created by anniekids good on 2022/2/24.
//

#import "ZBYMoreThreadViewController.h"

@interface ZBYMoreThreadViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, copy) NSArray * models;

@end

@implementation ZBYMoreThreadViewController

- (NSArray *)models {
    if (_models == nil) {
        _models = @[
        @{@"title" : @"pthread", @"subTitle" : @"pthread"},
        @{@"title" : @"NSThread", @"subTitle" : @"NSThread"},
        @{@"title" : @"GCD", @"subTitle" : @"GCD"},
        @{@"title" : @"NSOperation", @"subTitle" : @"NSOperation"},
        @{@"title" : @"多线程中的锁", @"subTitle" : @"线程安全与锁"},
        @{@"title" : @"深入理解Thread线程和Queue队列", @"subTitle" : @"Block探索"}];
    }
    return _models;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITableView * tableView = [[UITableView alloc] initWithFrame:CGRectZero style:(UITableViewStylePlain)];
    tableView.delegate = self;
    tableView.dataSource = self;
    [self.view addSubview:tableView];
    
    tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];
}

// MARK: - UITableViewDelegate, UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.models.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString * identifier = @"123456";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.backgroundColor = [UIColor whiteColor];
        cell.textLabel.textColor = [UIColor blackColor];
        cell.detailTextLabel.textColor = [UIColor blackColor];
    }
    cell.textLabel.text = self.models[indexPath.row][@"title"];
    cell.detailTextLabel.text = self.models[indexPath.row][@"subTitle"];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

// MARK: - GCD
- (void)gcdBase{
    // 详情见：GCD
}

// MARK: - Lock
- (void)lockBase{
    // 详情见：多线程的锁
}

@end
