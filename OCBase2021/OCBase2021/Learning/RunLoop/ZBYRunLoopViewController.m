//
//  ZBYRunLoopViewController.m
//  OCBase2021
//
//  Created by anniekids good on 2022/2/22.
//

#import "ZBYRunLoopViewController.h"
#import "ZBYThreadAliveViewController.h"
#import <mach/message.h>

@interface ZBYRunLoopViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, copy) NSArray * models;

@end

@implementation ZBYRunLoopViewController

- (NSArray *)models {
    if (_models == nil) {
        _models = @[
        @{@"title" : @"RunLoop详解", @"subTitle" : @"RunLoop详解"},
        @{@"title" : @"Source0和Source1", @"subTitle" : @"区别"},
        @{@"title" : @"线程保活", @"subTitle" : @"线程保活"}];
    }
    return _models;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // 线程保活：https://www.jianshu.com/p/adf8bdd62487
    UITableView * tableView = [[UITableView alloc] initWithFrame:CGRectZero style:(UITableViewStylePlain)];
    tableView.delegate = self;
    tableView.dataSource = self;
    [self.view addSubview:tableView];
    
    tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];
}

// MARK: - UITableViewDelegate, UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.models.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString * identifier = @"123456";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.backgroundColor = [UIColor whiteColor];
        cell.textLabel.textColor = [UIColor blackColor];
        cell.detailTextLabel.textColor = [UIColor blackColor];
    }
    cell.textLabel.text = self.models[indexPath.row][@"title"];
    cell.detailTextLabel.text = self.models[indexPath.row][@"subTitle"];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSDictionary * info = self.models[indexPath.row];
    NSString * title = info[@"title"];
    if ([title containsString:@"RunLoop详解"]) {
        [self runLoopDetail];
    }
    if ([title containsString:@"线程保活"]) {
        [self threadLife];
    }
    if ([title containsString:@"Source0和Source1"]) {
        [self threadLife];
    }
    
}

// MARK: - RunLoop

- (void)runLoopDetail {
    /**
     官方文档：
     寻找步骤：https://developer.apple.com/ -> 滑动到底部选中Documentation -> 滑动到底部选中Documentation Archive
     -> 搜索框中搜索Threading -> 选中Threading Programming Guide -> 选中Run Loops
     最终地址：https://developer.apple.com/library/archive/documentation/Cocoa/Conceptual/Multithreading/RunLoopManagement/RunLoopManagement.html#//apple_ref/doc/uid/10000057i-CH16-SW1
     RunLoop源码资源地址：
        https://opensource.apple.com/tarballs/CF/
        https://github.com/apple-oss-distributions/CF/tags
     见其中的CFRunLoop.c
     
     其它比较不错的文章：
     ibireme大神    https://blog.ibireme.com/2015/05/18/runloop/
     https://www.jianshu.com/p/737da434fd67
     https://blog.csdn.net/hherima/article/details/51746125
     */
    
    /**
     Runloop概念:
     1. 字面意思运行循环，用于辅助线程运行的一种机制(如何管理事件/消息，如何让线程在没有处理消息时休眠以避免资源占用、在有消息到来时立刻被唤醒)；
     2. 本质是基于XNU内核的mach_port和mach_msg
     2. 一个线程只有一个Runloop，一个线程可以没有Runloop；
     3. 根Runloop可以嵌套；
     4. 主线程的Runloop系统自动启动，且常驻；
     5. 其他线程当获取该线程的Runloop时([NSRunLoop currentRunLoop])，该线程回自动创建一个Runloop；当该线程的Runloop中
     的操作运行完毕时，Runloop自动退出销毁，随后该线程销毁。详细见线程保活
     */
    
    /**
     Runloop的底层封装：
     
     */
    
    /**
     Runloop相关：
     自动释放池、延迟回调、触摸事件、屏幕刷新、
     */
    
    /**
     Runloop应用：
     滑动与图片的异步刷新：
     UIScrollView 在滑动时，图片不刷新，例如在：NSDefaultRunLoopMode 模式下刷新图片
     
     子线程常驻保活：
     详情见线程保活
     */
    
    
}

// MARK: - Source0和Source1

- (void)sourceDetail {
    /** Source有两个版本：Source0 和 Source1。
     把事件进行分类，大家会分成几类？分类的方式有很多，但一定会有下面这种：
     系统层事件、应用层事件、特殊事件。（这只是为了大家理解source1和source0举得一个不严谨的例子，大家不要在意细节）

     如果上面对事件分类的方式你理解，那就好办了，特殊事件我们不管，那么，source1基本就是系统事件，source0基本就是应用层事件。好，下面我们来说source0和source1.
     
     • Source1：
     基于mach_Port的,来自系统内核或者其他进程或线程的事件，可以主动唤醒休眠中的RunLoop（iOS里进程间通信开发过程中我们一般不主动使用）。mach_port大家就理解成进程间相互发送消息的一种机制就好, 比如屏幕点击, 网络数据的传输都会触发sourse1。
     • Source0：
     非基于Port的 处理事件，什么叫非基于Port的呢？就是说你这个消息不是其他进程或者内核直接发送给你的。一般是APP内部的事件, 比如hitTest:withEvent的处理, performSelectors的事件。
     
     
     简单举个例子：一个APP在前台静止着，此时，用户用手指点击了一下APP界面，那么过程就是下面这样的：
     我们触摸屏幕，先摸到硬件(屏幕)，屏幕表面的事件会被IOKit先包装成Event，通过mach_Port传给正在活跃的APP， Event先告诉source1（mach_port），source1唤醒RunLoop, 然后将事件Event分发给source0，然后由source0来处理。

     如果没有事件，也没有timer，则runloop就会睡眠；如果有，则runloop就会被唤醒，然后跑一圈。
     */
}

// MARK: - 线程保活

- (void)threadLife {
    ZBYThreadAliveViewController * aliveVC = [ZBYThreadAliveViewController new];
    aliveVC.title = @"线程保活";
    [self.navigationController pushViewController:aliveVC animated:YES];
}

@end
