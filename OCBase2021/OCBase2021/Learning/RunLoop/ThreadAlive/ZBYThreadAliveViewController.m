//
//  ZBYThreadAliveViewController.m
//  OCBase2021
//
//  Created by anniekids good on 2022/2/25.
//

#import "ZBYThreadAliveViewController.h"
#import "ZBYThreadAliveTool.h"

@interface ZBYThreadAliveViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, copy) NSArray * models;

@property (nonatomic, strong) ZBYThread * thread;

@property (nonatomic, strong) ZBYThreadAliveTool * threadTool;

/** 是否停止RunLoop */
@property (nonatomic, assign) Boolean isStoped;

@end

@implementation ZBYThreadAliveViewController

- (NSArray *)models {
    if (_models == nil) {
        _models = @[
        @{@"title" : @"线程创建和销毁基础", @"subTitle" : @"线程保活"},
        @{@"title" : @"RunLoop没有运行", @"subTitle" : @"线程保活"},
        @{@"title" : @"RunLoop添加port，run运行", @"subTitle" : @"线程保活"},
        @{@"title" : @"RunLoop添加port，runModel方法运行", @"subTitle" : @"线程保活"},
        @{@"title" : @"RunLoop添加port，runModel方法运行，强引用线程", @"subTitle" : @"线程保活"},
        @{@"title" : @"线程保活封装", @"subTitle" : @"线程保活"}];
    }
    return _models;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // 线程保活：https://www.jianshu.com/p/adf8bdd62487
    UITableView * tableView = [[UITableView alloc] initWithFrame:CGRectZero style:(UITableViewStylePlain)];
    tableView.delegate = self;
    tableView.dataSource = self;
    [self.view addSubview:tableView];
    
    tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];
}

// MARK: - UITableViewDelegate, UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.models.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString * identifier = @"123456";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.backgroundColor = [UIColor whiteColor];
        cell.textLabel.textColor = [UIColor blackColor];
        cell.detailTextLabel.textColor = [UIColor blackColor];
    }
    cell.textLabel.text = self.models[indexPath.row][@"title"];
    cell.detailTextLabel.text = self.models[indexPath.row][@"subTitle"];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSDictionary * info = self.models[indexPath.row];
    NSString * title = info[@"title"];
    if ([title containsString:@"线程创建和销毁基础"]) {
        [self threadBase];
    }
    if ([title containsString:@"RunLoop没有运行"]) {
        [self threadNoRun];
    }
    if ([title containsString:@"RunLoop添加port，run运行"]) {
        [self threadRun];
    }
    if ([title containsString:@"RunLoop添加port，runModel方法运行"]) {
        [self threadRunModel];
    }
    if ([title containsString:@"RunLoop添加port，runModel方法运行，强引用线程"]) {
        [self threadRunModelStrong];
    }
    if ([title containsString:@"线程保活封装"]) {
        [self threadAliveTool];
    }
}


- (void)doucumentDescription {
    /**
     链接地址：https://www.jianshu.com/p/a761f4a85a15
     线程：分主线程和其它线程（也叫子线程）；主线程中进行ui刷新和事件的处理等操作，且主线程存在RunLoop是一直存在线程中的，且在操作完成后不会退出RunLoop，而是处在闲置状态；
     而其它线程中的RunLoop是在第一次获取的时候创建并存在的，且当该线程的操作完成后，线程及其中的运行循环会自动销毁；
     线程的创建和销毁很耗性能，所以经常在子线程做事情最好使用线程保活，比如AFN2.X就使用RunLoop实现了线程保活。
     
     那么怎样才能让其它线程一直存活呢？
     其实很简单，让该子线程中RunLoop一直运行，不退出，该线程就会一直存活在程序中；怎样保持RunLoop一直运行不退出呢，将RunLoop里面添加Source\Timer\Observer，Port相关的是Source事件，运行起来就行了。
     
     知易行难，在子线程保活的过程中会遇到各种小问题，下面是我在线程保活和释放过程中遇到的问题。
     
     1.正常创建自线程和向子线程中添加操作，如：threadBase方法
     当线程中的操作完成后，线程和RunLoop会自动销毁。
     
     2.只添加Port等事件，不运行RunLoop，不会会和情况1相同
     
     3.添加Port事件并运行run方法，RunLoop一直运行，不会退出，导致线程不会销毁
     run方法的官方注释：
     this method effectively begins an infinite loop that processes data from the run loop’s input sources and timers.  // 这个方法有效地开始了一个无限循环，处理来自运行循环的输入源和计时器的数据。
     If you want the run loop to terminate, you shouldn't use this method. Instead, use one of the other run methods and also check other arbitrary conditions of your own, in a loop. A simple example would be:   // 如果您希望终止运行循环，则不应使用此方法。相反，可以使用其他运行方法之一，并在循环中检查您自己的其他任意条件。一个简单的例子是:
     BOOL shouldKeepRunning = YES; // global
     NSRunLoop *theRL = [NSRunLoop currentRunLoop];
     while (shouldKeepRunning && [theRL runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]]);
     where shouldKeepRunning is set to NO somewhere else in the program.
     
     4.添加Port事件并运行runMode方法，RunLoop一直运行，当标记改变RunLoop退出，线程销毁
     注意：需要用标记，打破while循环；如果用属性强引用引用线程，注意循环引用
     
     5.当外部强引用线程时，注意造成的循环引用；或者强引用线程的对象提前释放造成标记永远为NO，无法打破while，造成RunLoop无法退出
     
     6.完整封装见类：ZBYThreadTool两种封装
     6.1RunLoop运行Port保活线程
        需要添加标记，打破while循环，退出RunLoop
     6.2RunLoop运行source保活线程
        更偏向底层c语言，控制的更精准；可以控制执行完source后不退出当前loop，这样不用写while循环添加标记了。
     */
}

- (void)threadBase {
    ZBYThread * thread = [[ZBYThread alloc] initWithTarget:self selector:@selector(run) object:nil];
    [thread start];
}

- (void)threadNoRun {
    ZBYThread * thread = [[ZBYThread alloc] initWithTarget:self selector:@selector(run2) object:nil];
    [thread start];
}

- (void)threadRun {
    ZBYThread * thread = [[ZBYThread alloc] initWithTarget:self selector:@selector(run3) object:nil];
    [thread start];
    dispatch_after(1.5, dispatch_get_main_queue(), ^{   // 延迟在子线程中添加操作
        [self performSelector:@selector(doSomethingInAliveThread) onThread:thread withObject:nil waitUntilDone:NO];
    });
    dispatch_after(2.5, dispatch_get_main_queue(), ^{   // 退出线程，无法正常退出，因为NSRunLoop的run方法
        [self performSelector:@selector(quitRunLoop) onThread:thread withObject:nil waitUntilDone:NO];
    });
}

- (void)threadRunModel {
    ZBYThread * thread = [[ZBYThread alloc] initWithTarget:self selector:@selector(run4) object:nil];
    [thread start];
    dispatch_after(1.5, dispatch_get_main_queue(), ^{   // 延迟在子线程中添加操作
        [self performSelector:@selector(doSomethingInAliveThread) onThread:thread withObject:nil waitUntilDone:NO];
    });
    dispatch_after(2.5, dispatch_get_main_queue(), ^{   // 退出线程，可以正常退出，因为NSRunLoop的runModel方法
        [self performSelector:@selector(quitRunLoop) onThread:thread withObject:nil waitUntilDone:NO];
    });
}

- (void)threadRunModelStrong {
    ZBYThread * thread = [[ZBYThread alloc] initWithTarget:self selector:@selector(runForStrong) object:nil];
    [thread start];
    // 属性强引用线程，容易造成循环引用
    self.thread = thread;
    dispatch_after(1.5, dispatch_get_main_queue(), ^{   // 延迟在子线程中添加操作
        [self performSelector:@selector(doSomethingInAliveThread) onThread:thread withObject:nil waitUntilDone:NO];
    });
    dispatch_after(2.5, dispatch_get_main_queue(), ^{   // 退出线程，可以正常退出，因为NSRunLoop的runModel方法
        [self performSelector:@selector(quitRunLoop2) onThread:thread withObject:nil waitUntilDone:NO];
    });
}

-(void)threadAliveTool {
    ZBYThreadAliveTool * tool = [ZBYThreadAliveTool threadToolForPort];
//    ZBYThreadAliveTool * tool = [ZBYThreadAliveTool threadToolForC];
    [tool addExecuteTask:^{
        NSLog(@"--添加操作--%@",[NSThread currentThread]);
    }];
    dispatch_after(1.0, dispatch_get_main_queue(), ^{
        [tool stop];
    });
}

/// run方法：运行结束后线程被释放
- (void)run {
   @autoreleasepool {   // 防止大次循环内存暴涨
       for (int i = 0; i < 100; i++) {
          NSLog(@"--子线程操作%ld",(long)i);
       }
   }
    NSLog(@"--end--%@",[NSThread currentThread]);
}

- (void)run2 {
    // addPort可以添加
    [[NSRunLoop currentRunLoop] addPort:[NSMachPort port] forMode:NSRunLoopCommonModes];
//    [[NSRunLoop currentRunLoop] addTimer:<#(nonnull NSTimer *)#> forMode:<#(nonnull NSRunLoopMode)#>]
   @autoreleasepool {   // 防止大次循环内存暴涨
       for (int i = 0; i < 100; i++) {
          NSLog(@"--子线程操作%ld",(long)i);
       }
   }
    NSLog(@"--end--%@",[NSThread currentThread]);
}

- (void)run3 {
    @autoreleasepool {   // 防止大次循环内存暴涨
        for (int i = 0; i < 100; i++) {
            NSLog(@"--子线程操作%ld",(long)i);
        }
        // addPort可以添加
        [[NSRunLoop currentRunLoop] addPort:[NSMachPort port] forMode:NSRunLoopCommonModes];
        // NSRunLoop的run方法是无法停止的，它专门用于开启一个永不销毁的线程（NSRunLoop）
        [[NSRunLoop currentRunLoop] run];
        NSLog(@"--end--%@",[NSThread currentThread]);    // 当RunLoop停止时，会执行该代码
    }
}

- (void)run4 {
    self.isStoped = NO;
    @autoreleasepool {
        for (int i = 0; i < 100; i++) {
            NSLog(@"----子线程任务 %ld",(long)i);
        }
        NSLog(@"%@----子线程任务结束",[NSThread currentThread]);
        NSRunLoop *runLoop = [NSRunLoop currentRunLoop];
        // 往RunLoop里面添加Source\Timer\Observer，Port相关的事件
        // 添加了一个Source1，但是这个Source1也没啥事，所以线程在这里就休眠了，不会往下走，----end----一直不会打印
        [runLoop addPort:[NSMachPort port] forMode:NSRunLoopCommonModes];
        while (!self.isStoped) {
            // beforeDat:过期时间，传入distantFuture遥远的未来，就是永远不会过期
            [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
        }
    
        // NSRunLoop的run方法是无法停止的，它专门用于开启一个永不销毁的线程（NSRunLoop）
//        [runLoop run];
        NSLog(@"--end--%@",[NSThread currentThread]); // 当RunLoop停止时，会执行该代码
    }
}

- (void)runForStrong {
    __weak typeof(self) weakSelf = self;
    self.isStoped = NO;
    @autoreleasepool {
        for (int i = 0; i < 100; i++) {
            NSLog(@"----子线程任务 %ld",(long)i);
        }
        NSLog(@"%@----子线程任务结束",[NSThread currentThread]);
        NSRunLoop *runLoop = [NSRunLoop currentRunLoop];
        // 往RunLoop里面添加Source\Timer\Observer，Port相关的事件
        // 添加了一个Source1，但是这个Source1也没啥事，所以线程在这里就休眠了，不会往下走，----end----一直不会打印
        [runLoop addPort:[NSMachPort port] forMode:NSRunLoopCommonModes];
    
        // weakSelf判断是否存在，不存在会默认为NO，无法打破while循环
        // 要使用weakself，不然self强引用thread，thread强引用block，block强引用self，产生循环引用。
        while (weakSelf && !weakSelf.isStoped) {
            [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
        }
        NSLog(@"%s ----end----", __func__); // 当RunLoop停止时，会执行该代码
    }
}

- (void)doSomethingInAliveThread {
    // 任务可以正常执行，说明线程一直是活着的
    NSLog(@"%s", __func__);
}

- (void)quitRunLoop {
    self.isStoped = YES;
    CFRunLoopStop(CFRunLoopGetCurrent());
    [[NSThread currentThread] cancel];
}

/// 需要在quitRunLoop中，进行如下设置
- (void)quitRunLoop2 {
    self.isStoped = YES;
    // 停止RunLoop
    CFRunLoopStop(CFRunLoopGetCurrent());
    [self.thread cancel];
    // 解决循环引用问题
    self.thread = nil;
    NSLog(@"%s %@", __func__, [NSThread currentThread]);
}

@end
