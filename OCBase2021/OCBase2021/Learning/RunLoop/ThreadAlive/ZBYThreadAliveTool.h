//
//  ZBYThreadAliveTool.h
//  OCBase2021
//
//  Created by anniekids good on 2022/2/25.
//

#import <Foundation/Foundation.h>
#import "ZBYThread.h"

/// 任务的回调
typedef void (^ZBYThreadToolTask)(void);

NS_ASSUME_NONNULL_BEGIN

@interface ZBYThreadAliveTool : NSObject

@property (nonatomic, strong, readonly) ZBYThread * innerThread;

@property (nonatomic, assign, getter = isStopped) BOOL stopped;

/// 保活线程
+ (instancetype)threadToolForPort;

/// 保活线程
+ (instancetype)threadToolForC;

/// 在当前子线程添加一个执行任务
/// @param task 执行任务
- (void)addExecuteTask:(ZBYThreadToolTask)task;


/// 结束并销毁线程
- (void)stop;

@end

NS_ASSUME_NONNULL_END
