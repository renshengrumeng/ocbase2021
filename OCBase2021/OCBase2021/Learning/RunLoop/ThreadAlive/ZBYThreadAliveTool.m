//
//  ZBYThreadAliveTool.m
//  OCBase2021
//
//  Created by anniekids good on 2022/2/25.
//

#import "ZBYThreadAliveTool.h"

@interface ZBYThreadAliveTool ()

@property (strong, nonatomic, readwrite) ZBYThread * innerThread;

@end

@implementation ZBYThreadAliveTool

/// RunLoop运行Port保活线程
+ (instancetype)threadToolForPort {
    ZBYThreadAliveTool * tool = [ZBYThreadAliveTool new];
    tool.stopped = NO;
    __weak typeof(tool) weakTool = tool;
    tool.innerThread = [[ZBYThread alloc] initWithBlock:^{
        NSLog(@"begin----");
        [[NSRunLoop currentRunLoop] addPort:[[NSPort alloc] init] forMode:NSDefaultRunLoopMode];
        while (weakTool && !weakTool.isStopped) {
            [[NSRunLoop currentRunLoop] runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]];
        }
        NSLog(@"end----");
    }];
    // 自动开始线程
    [tool.innerThread start];
    return tool;
}

/// RunLoop运行source保活线程
/// C语言方式和OC方式达到的效果都是一样的，但是C语言方式控制的更精准，可以控制执行完source后不退出当前loop，
/// 这样就不用写while循环了。
+ (instancetype)threadToolForC {
    ZBYThreadAliveTool * tool = [ZBYThreadAliveTool new];
    tool.stopped = NO;
//    __weak typeof(tool) weakTool = tool;
    tool.innerThread = [[ZBYThread alloc] initWithBlock:^{
        NSLog(@"begin----");
        // 创建上下文（要初始化一下结构体，否则结构体里面有可能是垃圾数据）
        CFRunLoopSourceContext context = {0};
        // 创建source
        CFRunLoopSourceRef source = CFRunLoopSourceCreate(kCFAllocatorDefault, 0, &context);
        // 往Runloop中添加source
        CFRunLoopAddSource(CFRunLoopGetCurrent(), source, kCFRunLoopDefaultMode);
        // 销毁source
        CFRelease(source);
        // 启动
        //参数：模式，过时时间(1.0e10一个很大的值)，是否执行完source后就会退出当前loop
        CFRunLoopRunInMode(kCFRunLoopDefaultMode, 1.0e10, false);
        
        //如果使用的是C语言的方式就可以通过最后一个参数让执行完source之后不退出当前Loop，所以就可以不用stopped属性了
//            while (weakSelf && !weakSelf.isStopped) {
//                // 第3个参数：returnAfterSourceHandled，设置为true，代表执行完source后就会退出当前loop
//                CFRunLoopRunInMode(kCFRunLoopDefaultMode, 1.0e10, true);
//            }
        NSLog(@"end----");
    }];
    // 自动开始线程
    [tool.innerThread start];
    return tool;
}

- (void)addExecuteTask:(ZBYThreadToolTask)task {
    if (!self.innerThread || !task) return;
    [self performSelector:@selector(_executeTask:) onThread:self.innerThread withObject:task waitUntilDone:NO];
}

- (void)stop {
    if (!self.innerThread) return;
    [self performSelector:@selector(_stop) onThread:self.innerThread withObject:nil waitUntilDone:YES];
}

/// 对象释放，销毁其保活的线程
- (void)dealloc {
    [self stop];
}

// MARK: - private methods

- (void)_stop {
    self.stopped = YES;
    CFRunLoopStop(CFRunLoopGetCurrent());
    self.innerThread = nil;
}

- (void)_executeTask:(ZBYThreadToolTask)task {
    task();
}

@end
