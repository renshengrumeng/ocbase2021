//
//  ZBYLockViewController.m
//  OCBase2021
//
//  Created by 周保勇 on 2021/4/12.
//

#import "ZBYLockViewController.h"
#import <os/lock.h>

@interface ZBYLockViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, copy) NSArray * models;

@property (nonatomic, strong) NSMutableArray * mArray;

/** 产品总数 */
@property (nonatomic, assign) NSInteger productCount;
@property (nonatomic, assign) NSInteger productMaxCount;
@property (nonatomic, assign) BOOL beginProductConsump;
/** 条件锁 */
@property (nonatomic, strong) NSCondition * lock;

/** 自旋锁属性 */
@property (atomic, assign) NSInteger count;

@end

@implementation ZBYLockViewController

- (NSArray *)models {
    if (_models == nil) {
        _models = @[
        @{@"title" : @"锁的概念", @"subTitle" : @"锁的底层理解"},
        @{@"title" : @"@synchronized的不足", @"subTitle" : @"加锁对象被释放后，加锁失效问题，NSLock的引入"},
        @{@"title" : @"NSLock互斥锁不足", @"subTitle" : @"NSRecursiveLock引入"},
        @{@"title" : @"NSRecursiveLock死锁", @"subTitle" : @"NSRecursiveLock和synchronized"},
        @{@"title" : @"生产者-消费者问题 ", @"subTitle" : @"条件锁NSCondition的引入"},
        @{@"title" : @"更灵活的条件锁", @"subTitle" : @"NSConditionLock"},
        @{@"title" : @"atomic自旋锁", @"subTitle" : @"自旋锁"},
        @{@"title" : @"OSSpinLock自旋锁", @"subTitle" : @"自旋锁"}
        ];
    }
    return _models;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
    UITableView * tableView = [[UITableView alloc] initWithFrame:CGRectZero style:(UITableViewStylePlain)];
    tableView.delegate = self;
    tableView.dataSource = self;
    [self.view addSubview:tableView];
    
    tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];

    self.productCount = 0;
    self.productMaxCount = 50;
    self.beginProductConsump = NO;
    self.lock = [[NSCondition alloc] init];
    
    self.count = 100;
}

// MARK: - UITableViewDelegate, UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.models.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString * identifier = @"123456";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:identifier];
        cell.backgroundColor = [UIColor whiteColor];
        cell.textLabel.textColor = [UIColor blackColor];
        cell.detailTextLabel.textColor = [UIColor blackColor];
    }
    cell.textLabel.text = self.models[indexPath.row][@"title"];
    cell.detailTextLabel.text = self.models[indexPath.row][@"subTitle"];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.row) {
        case 0:
            [self synchronizedBase];
            break;
        case 1:
            [self synchronizedQuestion];
            break;
        case 2:
            [self lockQuestion];
            break;
        case 3:
            [self recursiveDeadLock];
            break;
        case 4:
            [self productionConsumptionInit];
            break;
        case 5:
            [self conditionLock];
            break;
        case 6:
            [self atomicLock];
            break;
        case 7:
            [self pinLock];
            break;
        default:
            break;
    }
}

- (void)lockBase {
    /*
     不再安全的 OSSpinLock https://blog.ibireme.com/2016/01/16/spinlock_is_unsafe_in_ios/
     OC中锁：分为互斥锁、条件锁和自旋锁
     互斥锁：用于保证在任何时刻，都有且只有一个线程访问对象；当获取锁操作失败时，线程会进入睡眠，等待锁释放时被唤醒；类型分为递归互斥锁和非递归互斥锁，其底层都是对Posix Thread中线程同步的mutex函数的封装。
     递归互斥锁： @synchronized和NSRecursiveLock
     非递归互斥锁：NSLock
     条件锁：就是条件变量，当进程的某些资源要求不满足时就进入休眠，也就是锁住了。当资源被分配到了，条件锁打开，进程继续运行。NSCondition和NSConditionLock
     
     自旋锁：它是为实现保护共享资源而提出的一种锁机制。
     自旋锁与互斥锁比较类似，它们都是为了解决对某项资源的互斥使用。
     无论是互斥锁，还是自旋锁，在任何时刻，最多只能有一个保持者，也就说，在任何时刻最多只能有一个执行单元获得锁。
     OC中的自旋锁：atomic，OSSpinLock（iOS10已废弃，用os_unfair_lock替代）
     调度机制上略有不同:
     互斥锁:如果资源已经被占用，资源申请者只能进入睡眠状态。
     自旋锁:不会引起调用者睡眠，如果自旋锁已经被别的执行单元保持，调用者就一直循环在那里看是否该自旋锁的保持者已经释放了锁，"自旋"一词就是因此而得名。
     */
    
    /*NSCondition 的对象实际上作为⼀个锁和⼀个线程检查器：
     锁主要为了当检测条件时保护数据源，执⾏条件引发的任务；
     线程检查器主要是根据条件决定是否继续运⾏线程，即线程是否被阻塞
     */
    NSCondition * lock = [NSCondition new];
    // ⼀般⽤于多线程同时访问、修改同⼀个数据源，保证在同⼀时间内数据源只被访问、修改⼀次，其他线程的命令需要在lock 外等待，只到unlock ，才可访问
    [lock lock];
    [lock unlock];  // 与lock配合使用
    [lock wait];    // 让当前线程处于等待状态
    [lock signal];  // CPU发信号告诉线程不⽤在等待，可以继续执⾏
    
    // ⼀旦⼀个线程获得锁，其他线程⼀定等待；是对NSCondition的条件封装
    NSConditionLock * conditionLock = [[NSConditionLock alloc] initWithCondition:2];
    /*
     表示 xxx 期待获得锁，如果没有其他线程获得锁（不需要判断内部的
     condition) 那它能执⾏此⾏以下代码，如果已经有其他线程获得锁（可能是条件锁，或者⽆条件
     锁），则等待，直⾄其他线程解锁
     */
    [conditionLock lock];
    
    /*
     表示如果没有其他线程获得该锁，但是该锁内部的
     condition不等于A条件，它依然不能获得锁，仍然等待。如果内部的condition等于A条件，并
     且没有其他线程获得该锁，则进⼊代码区，同时设置它获得该锁，其他任何线程都将等待它代码
     的完成，直⾄它解锁。
     */
    [conditionLock lockWhenCondition:2];
    
    /*
     表示释放锁，同时把内部的condition设置为A条件
     */
    [conditionLock unlockWithCondition:2];
}

/// @synchronized本质和原理
- (void)synchronizedBase {
    /*
     @synchronized加锁底层流程：
     -> objc_sync_try_enter 进入加锁流程
     -> 判断加锁的对象是否为nil（为nil，调用objc_sync_nil，什么也不做；相当于不加锁）
     -> 调用id2data方法将加锁对象和加锁过程封装和处理；
     为SyncData结构体
     typedef struct alignas(CacheLineSize) SyncData {
         struct SyncData* nextData; // 递归的节点
         DisguisedPtr<objc_object> object;  // 加锁的对象
         int32_t threadCount;  // number of THREADS using this block
         recursive_mutex_t mutex;   // 递归锁
     } SyncData;
     
     id2data方法中，如果没有加锁，加锁并缓存到一个全局StripedMap哈希表中；如果有缓存从StripedMap取出SyncList中的锁和数据
     struct SyncList {
         SyncData *data;    // 数据
         spinlock_t lock;   // 锁

         constexpr SyncList() : data(nil), lock(fork_unsafe_lock) { }
     };
     
     -> 加锁处理data->mutex.lock();
     -> 线程处理完毕后objc_sync_exit对锁进行释放处理
     
     @synchronized和NSRecursiveLock的不同：synchronized加锁和objc_sync_nil联合使用，可以防止递归锁死锁；而NSRecursiveLock会导致死锁；但是NSRecursiveLock的执行速度和效率比synchronized高
     
     */
    @synchronized (self) {
        for (int i = 0; i < 2000; i++) {
            self.mArray = [NSMutableArray array];
        }
    }
    
    /*
     NSLock：非递归互斥锁，是对底层互斥锁（pthread_mutex）的面相对象的封装；
     在递归调用时，会出现死锁现象
     */
    
    /*
     NSRecursiveLock：递归互斥锁的一种；这种锁与 @synchronized相比，容易造成死锁；
     */
    
    /*
     总结：普通线程安全使用NSLock
     递归调用的线程安全使用NSRecursiveLock
     如果循环递归调用，并且有多线程的情况下，请考虑死锁的情况，酌情使用NSRecursiveLock和 @synchronized
     */
}

/// @synchronized递归锁的不足，和NSLock非递归锁的引入
- (void)synchronizedQuestion {
    NSLock * lock = [NSLock new];
    for (int i = 0; i < 2000; i++) {
//        dispatch_async(dispatch_get_global_queue(0, 0), ^{
//            // 多条线程对属性进行赋值时，当有多条线程同时进行时，会对旧的值进行多次release操作，造成崩溃
//            self.mArray = [NSMutableArray array];
//        });
        
//        dispatch_async(dispatch_get_global_queue(0, 0), ^{
//            // 为何对self.mArray添加了递归互斥锁，还会导致崩溃呢？
//            /*
//             根据上述 @synchronized加锁的底层解析，可以知道，当synchronized加锁的对象为空时，相当于没有加锁，在这时对其多线程操作，就会出现上面代码出现的情况
//             */
//            @synchronized (self.mArray) {
//                self.mArray = [NSMutableArray array];
//            }
//        });
        
        // 用非递归互斥锁，就可以解决上述问题
        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            [lock lock];
            self.mArray = [NSMutableArray array];
            [lock unlock];
        });
    }
}

/// NSLock非递归互斥锁的不足，和NSRecursiveLock递归互斥锁的引入
- (void)lockQuestion {
    // NSLock在递归调用的不足；递归调用中，
//    NSLock * lock = [NSLock new];
    NSRecursiveLock * lock = [NSRecursiveLock new];
    dispatch_async(dispatch_get_global_queue(0, 0), ^{  // 单个线程的递归调用
        static void (^testMethod)(int);
        testMethod = ^(int value) {
            // 如果是NSLock锁，在锁住的代码中递归调用，会造成上次还未开锁，递归调用下一次已经加锁，造成锁的相互等待解锁而造成的死锁
            // 如果是NSRecursiveLock锁，则不会出现上述死锁情况
            [lock lock];
            if (value > 0) {
                NSLog(@"%d", value);
                testMethod(value - 1);
            }
            [lock unlock];
        };
        testMethod(10); // 如果是NSLock锁，输出：10后，死锁；如果是NSRecursiveLock锁，可以正常输出10～1
    });
}

/// NSRecursiveLock递归死锁
- (void)recursiveDeadLock {
    NSRecursiveLock * lock = [NSRecursiveLock new];
    for (int i = 0; i < 100; i++) { // 多线程的递归调用
        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            static void (^testMethod)(int);
            testMethod = ^(int value) {
                // 多线程同时执行加锁后，不同线程之间等待解锁操作，才能进行加锁代码的执行，造成相互等待的死锁
                // 例如：线程2和线程3同时进行了加锁操作；线程2要执行加锁操作，等待线程3的解锁操作；而线程3要执行加锁操作，等待线程2的解锁操作
//                [lock lock];
//                NSLog(@"lock-%@", [NSThread currentThread]);
//                if (value > 0) {
//                    NSLog(@"%d", value);
//                    testMethod(value - 1);
//                }
//                NSLog(@"unlock-%@", [NSThread currentThread]);
//
//                // 这里我们for循环的同时，异步的去递归调用，这时会有很多线程在相互等待操作!lock没有找到合适的出口
//                [lock unlock];
                
                // synchronized加锁时，如果已经加锁，是从缓存中去取的，不会重复的去创建!
                @synchronized (self) {
                    if (value > 0) {
                        NSLog(@"%d", value);
                        testMethod(value - 1);
                    }
                }
            };
            testMethod(10); // 如果是NSLock锁，输出：10后，死锁；如果是NSRecursiveLock锁，可以正常输出10～1
        });
    }
}

/// 生产者-消费者问题：条件锁
- (void)productionConsumptionInit {
    /*
     现实生活中的生产和消费，一个生产线生产产品，一个消费线消费产品，两条线同时进行；在产品到达库存最大值时停止生产，等待消费；在产品库存为零时，停止消费，等待生产。
     
     有一组生产者的线程和一组消费者的线程共享一个初始为空，大小为n的缓冲区，如果缓冲区未满，那么生产者就可以把生产的放进缓冲区里，如果缓冲区不空，那么消费者就可以从缓冲区取出来消费；
     问题分析：当
     关系分析：生产者和消费者对缓冲区的互斥访问是互斥关系，同时生产者和消费者又是一个协作的关系，之后生产者生产了之后，消费者才可以消费，他们是同步关系。
     */
    
    self.beginProductConsump = !self.beginProductConsump;
    // 生产
    dispatch_queue_t globalQueue = dispatch_get_global_queue(0, 0);
    dispatch_queue_t productQueue = dispatch_queue_create("productQueue", NULL);
    dispatch_async(globalQueue, ^{
        while (self.beginProductConsump) {
            [self.lock lock];
            if (self.productCount >= self.productMaxCount) {
                NSLog(@"库存满了 现有 count %zd", self.productCount);
                [self.lock wait];
            }
            else {
                self.productCount += 1;
                NSLog(@"生产一个 现有 count %zd", self.productCount);
                [self.lock signal];
            }
            [self.lock unlock];
        }
    });
    
    // 消费
    dispatch_queue_t consumpQueue = dispatch_queue_create("productQueue", NULL);
    dispatch_async(globalQueue, ^{
        while (self.beginProductConsump) {
            [self.lock lock];
            if (self.productCount <= 0) {
                NSLog(@"库存没了 现有 count %zd", self.productCount);
                [self.lock wait];
            }
            else {
                self.productCount -= 1;
                NSLog(@"消费一个 现有 count %zd", self.productCount);
                [self.lock signal];
            }
            [self.lock unlock];
        }
    });
}

/// NSConditionLock更灵活的条件锁
- (void)conditionLock {
    // 创建条件为2的锁
    NSConditionLock * cLock = [[NSConditionLock alloc] initWithCondition:2];
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        /*
         线程 1 调⽤[NSConditionLock lockWhenCondition:]，此时此刻因为不满⾜当前条件（条件为2），所
         以会进⼊ waiting 状态，当前进⼊到 waiting 时，会释放当前的互斥锁。
         */
        [cLock lockWhenCondition:1];
        NSLog(@"线程 1 - %@", [NSThread currentThread]);
        [cLock unlockWithCondition:0];
    });
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        /*
         接下来线程 2 执⾏[NSConditionLock lockWhenCondition:]，因为满⾜条件值，所以线程
         2 会打印，打印完成后会调⽤[NSConditionLock unlockWithCondition:],这个时候讲
         value 设置为 1，并发送 boradcast, 此时线程 1 接收到当前的信号，唤醒执⾏并打印。
         */
        [cLock lockWhenCondition:2];
        NSLog(@"线程 2 - %@", [NSThread currentThread]);
        [cLock unlockWithCondition:1];
    });
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        [cLock lock];   // 如果有锁等待，无锁执行一下代码
        NSLog(@"线程 3 - %@", [NSThread currentThread]);
        [cLock unlock];
    });
    
    // 综上：2 在 1 前；3 执行的顺序不确定，可以在2前，可以在2和1之间，也可在1之后
}

/// atomic自旋锁
- (void)atomicLock {
    /**
     OC中的自旋锁：是属性的atomic关键字，即属性是原子属性的，线程安全的
     
     可能存在两个问题:
     试图递归地获得自旋锁必然会引起死锁：递归程序的持有实例在第二个实例循环，以试图获得相同自旋锁时，不会释放此自旋锁。在递归程序中使用自旋锁应遵守下列策略：递归程序决不能在持有自旋锁时调用它自己，也决不能在递归调用时试图获得相同的自旋锁。此外如果一个进程已经将资源锁定，那么，即使其它申请这个资源的进程不停地疯狂“自旋”，也无法获得资源，从而进入死循环。
     过多占用cpu资源。如果不加限制，由于申请者一直在循环等待，因此自旋锁在锁定的时候，如果不成功，不会睡眠，会持续的尝试，单cpu的时候自旋锁会让其它process动不了。 因此，一般自旋锁实现会有一个参数限定最多持续尝试次数。 超出后, 自旋锁放弃当前time slice。 等下一次机会。
     */
    
    /**
     atomic属性底层原理：
     setter方法：
     objc_setProperty_atomic -> reallySetProperty -> 判断参数atomic为true -> 加锁 ->
     新旧值替换 -> 解锁 -> 释放旧值
     getter方法：
     objc_getProperty -> 判断参数atomic为true -> 加锁 -> 取值 -> 解锁 -> 为了提高性能，(安全地)在自旋锁之外自动释放
     */
    
    /**
     总结一下：atomic本质上是在底层给setter/getter添加了一把锁，来保证线程安全！！！那么就意味着代码在进入
     getter/setter时，线程是安全的。但是出了getter/setter时，多线程安全就只能靠程序员自己保证了！！！
     例如：下边代码
     */
    //异步任务 A
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        for (int i = 0; i < 20000; i ++) {
            self.count += 1;
            NSLog(@"异步任务 A:%@------%ld",NSThread.currentThread, (long)self.count);
        }
    });
    
    //异步任务 B
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        for (int i = 0; i < 20000; i ++) {
            self.count += 1;
            NSLog(@"异步任务 B:%@------%ld",NSThread.currentThread, (long)self.count);
        }
    });
}

/// OSSpinLock自旋锁
- (void)pinLock {
    /**
     新版 iOS 中，系统维护了 5 个不同的线程优先级/QoS: background，utility，default，user-initiated，user-interactive。高优先级线程始终会在低优先级线程前执行，一个线程不会受到比它更低优先级线程的干扰。这种线程调度算法会产生潜在的优先级反转问题，从而破坏了 spin lock。
     
     具体来说，如果一个低优先级的线程获得锁并访问共享资源，这时一个高优先级的线程也尝试获得这个锁，它会处于 spin lock 的忙等状态从而占用大量 CPU。此时低优先级线程无法与高优先级线程争夺 CPU 时间，从而导致任务迟迟完不成、无法释放 lock。这并不只是理论上的问题，libobjc 已经遇到了很多次这个问题了，于是苹果的工程师停用了 OSSpinLock。

     苹果工程师 Greg Parker 提到，对于这个问题，一种解决方案是用 truly unbounded backoff 算法，这能避免 livelock 问题，但如果系统负载高时，它仍有可能将高优先级的线程阻塞数十秒之久；另一种方案是使用 handoff lock 算法，这也是 libobjc 目前正在使用的。锁的持有者会把线程 ID 保存到锁内部，锁的等待者会临时贡献出它的优先级来避免优先级反转的问题。理论上这种模式会在比较复杂的多锁条件下产生问题，但实践上目前还一切都好。

     libobjc 里用的是 Mach 内核的 thread_switch() 然后传递了一个 mach thread port 来避免优先级反转，另外它还用了一个私有的参数选项，所以开发者无法自己实现这个锁。另一方面，由于二进制兼容问题，OSSpinLock 也不能有改动。

     最终的结论就是，除非开发者能保证访问锁的线程全部都处于同一优先级，否则 iOS 系统中所有类型的自旋锁都不能再使用了。
     
     在 iOS 10/macOS 10.12 发布时，苹果提供了新的 os_unfair_lock 作为 OSSpinLock 的替代，并且将 OSSpinLock 标记为了 Deprecated。
     
     os_unfair_lock是在iOS10之后为了替代自旋锁OSSpinLock而诞生的，主要是通过线程休眠的方式来继续加锁，而不是一个“忙等”的锁。猜测是为了解决自旋锁的优先级反转的问题。
     */
}

@end
