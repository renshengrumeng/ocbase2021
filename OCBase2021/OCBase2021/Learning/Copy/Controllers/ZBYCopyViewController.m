//
//  ZBYCopyViewController.m
//  OCBase2021
//
//  Created by 周保勇 on 2021/5/27.
//

#import "ZBYCopyViewController.h"
#import "ZBYCopyPerson.h"
#import <objc/runtime.h>

@interface ZBYCopyViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, copy) NSArray * models;

@end

@implementation ZBYCopyViewController

- (NSArray *)models {
    if (_models == nil) {
        _models = @[
        @{@"title" : @"简介", @"subTitle" : @"概念"},
        @{@"title" : @"非集合不可变类", @"subTitle" : @"以NSString为例"},
        @{@"title" : @"非集合可变类", @"subTitle" : @"以NSMutableString为例"},
        @{@"title" : @"集合不可变类", @"subTitle" : @"以NSArray为例"},
        @{@"title" : @"集合可变类", @"subTitle" : @"以NSMutableArray为例"},
        @{@"title" : @"自定义类的和NSCoping协议", @"subTitle" : @"以NSMutableArray为例"},
        @{@"title" : @"字符串的Copy", @"subTitle" : @"NSString属性的copy和strong"}
        ];
    }
    return _models;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
    UITableView * tableView = [[UITableView alloc] initWithFrame:CGRectZero style:(UITableViewStylePlain)];
    tableView.delegate = self;
    tableView.dataSource = self;
    [self.view addSubview:tableView];
    
    tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];
}

// MARK: - UITableViewDelegate, UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.models.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString * identifier = @"123456";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:identifier];
        cell.backgroundColor = [UIColor whiteColor];
        cell.textLabel.textColor = [UIColor blackColor];
        cell.detailTextLabel.textColor = [UIColor blackColor];
    }
    cell.textLabel.text = self.models[indexPath.row][@"title"];
    cell.detailTextLabel.text = self.models[indexPath.row][@"subTitle"];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSDictionary * info = self.models[indexPath.row];
    NSString * title = info[@"title"];
    if ([title isEqualToString:@"简介"]) {
        [self copyBaseDescription];
    }
    if ([title isEqualToString:@"非集合不可变类"]) {
        [self stringCopy];
    }
    if ([title isEqualToString:@"非集合可变类"]) {
        [self mutableStringCopy];
    }
    if ([title isEqualToString:@"集合不可变类"]) {
        [self containerCopy];
    }
    if ([title isEqualToString:@"集合可变类"]) {
        [self mutableContainerCopy];
    }
    if ([title isEqualToString:@"自定义类的和NSCoping协议"]) {
        [self customClassCopy];
    }
    if ([title isEqualToString:@"字符串的Copy"]) {
        [self stringPropertyCopy];
    }
    
}

// MARK: - 简介

- (void)copyBaseDescription {
    /**
     浅拷贝：
     1.指针拷贝，没有开辟新的内存；
     2.生成一个新的指针变量指向原有对象的地址；
     3.原有对象引用计数+1。
     深拷贝：
     1.从堆中开辟一份新的内存给新的对象，并将原有对象的信息赋值给新的对象；
     2.生成一个新的指针变量指向新的对象；
     3.原有对象引用计数不会增加，新的对象引用计数为1。
     完全拷贝：
     1.是对多层对象包含或者集合中存储对象而言的；
     2.被拷贝对象的每一层都进行了深拷贝；
     3.例如集合中的对象，对象中的对象等。
     */
}

// MARK: - 非集合不可变类

- (void)stringCopy {
    NSString * a = @"zby-haha-heihei-123456";
    NSString * a1 = [a copy];
    NSString * a2 = [a mutableCopy];
    NSLog(@"%p-%p-%p", a, a1, a2);
    NSLog(@"%@-%@-%@", a.class, a1.class, a2.class);
    
//    NSObject * o = [[NSObject alloc] init];
//    NSObject * o1 = [o copy];
//    NSObject * o2 = [o mutableCopy];
//    NSLog(@"%p-%p-%p", o, o1, o2);
//    NSLog(@"%@-%@-%@", o.class, o1.class, o2.class);
    /**
     结论：
     1.对非集合不可变对象的copy为浅拷贝，拷贝的指针指向原有对象地址
     2.对非集合不可变对象的mutableCopy为深拷贝，复制的对象为可变对象
     */
}

// MARK: - 非集合可变类

- (void)mutableStringCopy {
    NSMutableString * a = [[NSMutableString alloc] initWithFormat:@"zby-haha-heihei-123456"];
    NSMutableString * a1 = [a copy];
    NSMutableString * a2 = [a mutableCopy];
    NSLog(@"%p-%p-%p", a, a1, a2);
    NSLog(@"%@-%@-%@", a.class, a1.class, a2.class);
    
    NSMutableString * b = [NSMutableString stringWithString:@"非集合可变类"];
    NSMutableString * b1 = [a copy];
    NSMutableString * b2 = [a mutableCopy];
    NSLog(@"%p-%p-%p", b, b1, b2);
    NSLog(@"%@-%@-%@", b.class, b1.class, b2.class);
    /**
     结论：
     1.对非集合可变对象的copy为深拷贝，复制的对象为可变对象
     2.对非集合可变对象的mutableCopy为深拷贝，复制的对象为可变对象
     */
}

// MARK: - 集合不可变类

- (void)containerCopy {
    NSArray * a = @[@"集合不可变类", @"集合可变类"];
    NSArray * a1 = [a copy];
    NSArray * a2 = [a mutableCopy];
    NSLog(@"%p-%p-%p", a, a1, a2);
    NSLog(@"%@-%@-%@", a.class, a1.class, a2.class);
    
    NSDictionary * dic = @{@"key" : @"value"};
    NSDictionary * dic1 = [dic copy];
    NSDictionary * dic2 = [dic mutableCopy];
    NSLog(@"%p-%p-%p", dic, dic1, dic2);
    NSLog(@"%@-%@-%@", dic.class, dic1.class, dic2.class);
    
    NSSet * set = [NSSet setWithObjects:@"set集合", @12, nil];
    NSSet * set1 = [set copy];
    NSSet * set2 = [set mutableCopy];
    NSLog(@"%p-%p-%p", set, set1, set2);
    NSLog(@"%@-%@-%@", set.class, set1.class, set2.class);
    
    /**
     结论：
     1.对集合不可变对象的copy为浅拷贝，拷贝的指针指向原有对象地址
     2.对集合不可变对象的mutableCopy为深拷贝，复制的对象为可变对象
     */
    NSLog(@"%p-%p-%p", a[0], a1[0], a2[0]);
}

// MARK: - 集合可变类

- (void)mutableContainerCopy {
    NSMutableArray * a = [NSMutableArray arrayWithArray:@[@"集合不可变类", @"集合可变类"]];
    NSMutableArray * a1 = [a copy];
    NSMutableArray * a2 = [a mutableCopy];
    NSLog(@"%p-%p-%p", a, a1, a2);
    NSLog(@"%@-%@-%@", a.class, a1.class, a2.class);
    
    NSMutableDictionary * dic = [[NSMutableDictionary alloc] initWithDictionary:@{@"key" : @"value"}];
    NSMutableDictionary * dic1 = [dic copy];
    NSMutableDictionary * dic2 = [dic mutableCopy];
    NSLog(@"%p-%p-%p", dic, dic1, dic2);
    NSLog(@"%@-%@-%@", dic.class, dic1.class, dic2.class);
    
    NSMutableSet * set = [NSMutableSet setWithObjects:@"set集合", @12, nil];
    NSMutableSet * set1 = [set copy];
    NSMutableSet * set2 = [set mutableCopy];
    NSLog(@"%p-%p-%p", set, set1, set2);
    NSLog(@"%@-%@-%@", set.class, set1.class, set2.class);
    /**
     结论：
     1.对集合可变对象的copy为深拷贝，复制的对象为不可变对象
     2.对集合可变对象的mutableCopy为深拷贝，复制的对象为可变对象
     */
    NSLog(@"%p-%p-%p", a[0], a1[0], a2[0]);
    /**
     从上述输出中可以看出集合类可变对象mutableCopy和copy都返回一个新的集合，但集合内的元素仍然是浅拷贝
     */
    
    /**
    总结：
    1.不可变对象copy的都是浅拷贝，拷贝的指针指向原有对象地址；
    2.可变对象copy都是深拷贝，可变集合copy后的复制对象为不可变集合，非集合可变类copy后的复制对象为可变类；
    3.可变和不可变对象的mutableCopy的都是深拷贝，且复制的对象为可变对象。
    */
}

// MARK: - 自定义类的和NSCoping协议

- (void)customClassCopy {
    /**
     在OC中不是所有的类都支持拷贝，只有遵循NSCopying才支持copy，只有遵循NSMutableCopying才支持mutableCopy。
     
     NSObject有一个实列方法叫做-(id)copy。默认的copy方法调用[self copyWithZone:nil];对于采纳了NSCopying协议的子类，需要实现这个方法，否则会引发异常。
     
     NSMutableCopying只有在MRC模式下使用。
     */
    ZBYCopyPerson * p = [ZBYCopyPerson new];
    p.name = @"器不可变类";
    p.age = 23;
    ZBYCopyPerson * p1 = [p copy];
    NSLog(@"%p-%p", p, p1);
    /**
     结论：
     自定义类通过重写copyWithZone方法实现了深拷贝，通过copy方法（该方法默认调用copyWithZone方法）复制得到p1,从结果可以看出：深复制对象和和原对象的地址是不一样的.
     */
    NSArray * pList = @[p];
    NSArray * pCopyList = [NSArray arrayWithArray:pList];
    NSArray * pMCopyList = [[NSArray alloc] initWithArray:pList copyItems:YES];
    NSLog(@"%p-%p-%p", pList, pCopyList, pMCopyList);
    NSLog(@"%p-%p-%p", pList.firstObject, pCopyList.firstObject, pMCopyList.firstObject);
}

// MARK: - 属性字符串的拷贝

- (void)stringPropertyCopy {
    /**
     https://www.zhihu.com/question/20102376/answer/2479549830
     声明一个字符串类型的属性，一般使用copy关键字，那么能够使用assign和strong吗？为什么？
     
     举例：ZBYCopyPerson中声明四个属性用于说明四种字符串的属性
     */
    ZBYCopyPerson * persion = [[ZBYCopyPerson alloc] init];
    NSString * testStr = @"Hello world! I want somesting";
    NSLog(@"test = %@, p = %p", testStr, testStr);
    persion.stringCopy = testStr;
    persion.stringStrong = testStr;
    testStr = [testStr stringByAppendingString:@"Add some string."];
    NSLog(@"test = %@, p = %p", testStr, testStr);
    NSLog(@"stringCopy = %@, p = %p", persion.stringCopy, persion.stringCopy);
    NSLog(@"stringStrong= %@, p = %p", persion.stringStrong, persion.stringStrong);
    /**
     通过打印日志可以看出来，stringCopy和stringStrong指向的地址是testStr修改之前的地址，此时copy和strong只发生了浅copy(浅copy是指：堆中的地址没有发生改变，并没有copy一个新的地址来承载字符串内容，而是仅仅copy一个指针指向堆中的内容所在地址)，testStr修改后，testStr指向了新的地址，内容也发生改变，但是copy，和strong修饰的stringCopy和stringStrong属性指向的还是原来的地址。
     */
    
   ZBYCopyPerson * persion2 = [[ZBYCopyPerson alloc] init];
    NSMutableString * testMStr = [NSMutableString stringWithString:@"NSMutableString test pre copy"];
    NSLog(@"testMStr = %@, p = %p", testMStr, testMStr);
    persion2.stringCopy = testMStr;
    persion2.stringStrong = testMStr;
    [testMStr appendString:@" add some string."];
    NSLog(@"testMStr = %@, p = %p", testMStr, testMStr);
    NSLog(@"stringCopy = %@, p = %p", persion2.stringCopy, persion2.stringCopy);
    NSLog(@"stringStrong = %@, p = %p", persion2.stringStrong, persion2.stringStrong);
    /**
     此时由于testMStr是一个可变字符串类型，可以修改其内容，所以在堆中的位置不会变，这个时候copy修饰的stringCopy属性，会是一个深拷贝操作(在堆中会直接new一个内存块存储拷贝内容)所以我们看到stringCopy的地址发生了变化，但是strong修饰的stringStrong属性地址没有发生变化（浅拷贝），还是指向了testMStr地址所在，这时候当我们修改testMStr内容的时候，copy修饰的stringCopy值没有发生变化，但是strong修饰的stringStrong内容跟着变化了！
     */
    
    /**
     总结：
     1. NSString和NSMutableString类型的属性，不能用assign，有可能崩溃，assign修饰的属性在超过作用域时会释放；
     2. 声明NSString类型的属性可以用strong，大部分情况下和copy的效果相同；当声明一个属性为NSString是希望它不会变化，但是这里用strong修饰的话，就会出现上述值被修改的情况发生，所以声明NSString类型的属性使用copy，而非strong；
     3. 声明NSMutableString类型的属性不可以用copy，因为使用copy后，NSMutableString运行时会当作NSString用，如果运行时调用NSMutableString特有的方法，程序会崩溃。
     */
}

@end
