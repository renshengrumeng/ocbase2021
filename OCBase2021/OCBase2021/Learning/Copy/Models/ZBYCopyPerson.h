//
//  ZBYCopyPerson.h
//  OCBase2021
//
//  Created by 周保勇 on 2021/5/27.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZBYCopyPerson : NSObject <NSCopying>

/// 名字
@property (nonatomic, copy) NSString * name;

/// 年龄
@property (nonatomic, assign) NSInteger age;

@property (nonatomic, copy) NSString * stringCopy;
@property (nonatomic, strong) NSString * stringStrong;
@property (nonatomic, copy) NSMutableString * mutableStringCopy;
@property (nonatomic, strong) NSMutableString * mutableStringStrong;

@end

NS_ASSUME_NONNULL_END
