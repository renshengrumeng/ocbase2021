//
//  ZBYCopyPerson.m
//  OCBase2021
//
//  Created by 周保勇 on 2021/5/27.
//

#import "ZBYCopyPerson.h"

@implementation ZBYCopyPerson

- (nonnull id)copyWithZone:(nullable NSZone *)zone {
    ZBYCopyPerson * p = [[ZBYCopyPerson allocWithZone:zone] init];
    p.name = self.name;
    p.age = self.age;
    return p;
}

//- (nonnull id)mutableCopyWithZone:(nullable NSZone *)zone {
//    ZBYCopyPerson * p = [[ZBYCopyPerson mutableCopyWithZone:zone] init];
//    p.name = self.name;
//    p.age = self.age;
//    return p;
//}

@end
