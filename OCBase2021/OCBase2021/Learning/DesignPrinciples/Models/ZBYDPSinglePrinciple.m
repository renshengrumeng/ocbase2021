//
//  ZBYDPSinglePrinciple.m
//  OCBase2021
//
//  Created by 周保勇 on 2021/5/25.
//

#import "ZBYDPSinglePrinciple.h"

@implementation ZBYDPSinglePrinciple

@end

@implementation ZBYDPSinglePrincipleWaitPayOrder

- (void)cancelOrder {
    NSLog(@"只有待支付的订单可以取消订单");
}

@end

@implementation ZBYDPSinglePrincipleWaitGoodsOrder

- (void)showDeliveryInformation {
    NSLog(@"只有待收货的订单可以显示快递信息");
}

@end

@implementation ZBYDPSinglePrincipleReceivedGoodsOrder

- (void)returnGoods {
    NSLog(@"只有已收货的订单可以退货");
}

@end

@implementation ZBYDPSinglePrincipleOrderList


@end
