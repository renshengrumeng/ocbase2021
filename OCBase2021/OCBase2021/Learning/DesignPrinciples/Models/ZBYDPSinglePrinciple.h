//
//  ZBYDPSinglePrinciple.h
//  OCBase2021
//
//  Created by 周保勇 on 2021/5/25.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZBYDPSinglePrinciple : NSObject

@end


/// 待支付订单
@interface ZBYDPSinglePrincipleWaitPayOrder : NSObject

/// 只有待支付的订单可以取消订单
- (void)cancelOrder;

@end

/// 待收货订单
@interface ZBYDPSinglePrincipleWaitGoodsOrder : NSObject

/// 只有待收货的订单可以显示快递信息
- (void)showDeliveryInformation;

@end

/// 已收货订单
@interface ZBYDPSinglePrincipleReceivedGoodsOrder : NSObject

/// 只有已收货的订单可以退货
- (void)returnGoods;

@end

@interface ZBYDPSinglePrincipleOrderList : NSObject

/// 待支付订单列表
@property (nonatomic, copy) NSArray<ZBYDPSinglePrincipleWaitPayOrder *> * waitPayOrderList;

/// 待收货订单列表
@property (nonatomic, copy) NSArray<ZBYDPSinglePrincipleWaitGoodsOrder *> * waitGoodsOrderList;

/// 已收货订单列表
@property (nonatomic, copy) NSArray<ZBYDPSinglePrincipleReceivedGoodsOrder *> * receivedGoodsOrderList;

@end

NS_ASSUME_NONNULL_END
