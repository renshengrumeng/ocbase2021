//
//  ZBYDPOpenClosedPrinciple.h
//  OCBase2021
//
//  Created by 周保勇 on 2021/5/25.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZBYDPOpenClosedPrinciple : NSObject

@end

@interface ZBYDPSharedInfoModel : NSObject

/// 分享的类型
@property (nonatomic, assign) NSInteger type;

/// 分享的信息
@property (nonatomic, copy) NSDictionary<NSString *, id> * info;

@end

// MARK: - 未使用开放封闭原则

@interface ZBYDPSharedManager : NSObject

- (void)shareWithInfo: (ZBYDPSharedInfoModel *)info;

@end

// MARK: - 使用开放封闭原则的扩展

@protocol ZBYDPOpenCloseSharedDelegate <NSObject>

- (void)handle:(ZBYDPSharedInfoModel *)info;

@end

@interface ZBYDPOpenCloseSharedQQModel : NSObject<ZBYDPOpenCloseSharedDelegate>

- (void)handle:(ZBYDPSharedInfoModel *)info;

@end

@interface ZBYDPOpenCloseSharedWechatModel : NSObject<ZBYDPOpenCloseSharedDelegate>

- (void)handle:(ZBYDPSharedInfoModel *)info;

@end

@interface ZBYDPOpenCloseSharedManager : NSObject

/// 分享类型对象
@property (nonatomic, strong) id<ZBYDPOpenCloseSharedDelegate> shareInstence;

/// 分享类型对象信息
@property (nonatomic, copy) NSDictionary<NSString *, id> * info;

- (void)shareWithInfo:(ZBYDPSharedInfoModel *)info;

@end


NS_ASSUME_NONNULL_END
