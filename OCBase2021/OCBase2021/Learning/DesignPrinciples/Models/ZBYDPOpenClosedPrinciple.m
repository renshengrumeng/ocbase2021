//
//  ZBYDPOpenClosedPrinciple.m
//  OCBase2021
//
//  Created by 周保勇 on 2021/5/25.
//

#import "ZBYDPOpenClosedPrinciple.h"

@implementation ZBYDPOpenClosedPrinciple

@end

@implementation ZBYDPSharedInfoModel

@end

@implementation ZBYDPSharedManager

- (void)shareWithInfo:(ZBYDPSharedInfoModel *)info {
    switch (info.type) {
        case 1: // qq分享
            NSLog(@"qq分享");
            break;
        case 2: // 微信分享
            NSLog(@"微信分享");
            break;
            
        default:
            break;
    }
}

@end

@implementation ZBYDPOpenCloseSharedQQModel

- (void)handle:(ZBYDPSharedInfoModel *)info {
    NSLog(@"qq分享");
}

@end

@implementation ZBYDPOpenCloseSharedWechatModel

- (void)handle:(ZBYDPSharedInfoModel *)info {
    NSLog(@"微信分享");
}

@end

@implementation ZBYDPOpenCloseSharedManager

- (void)shareWithInfo:(ZBYDPSharedInfoModel *)info {
    if (self.shareInstence && [self.shareInstence performSelector:@selector(handle:)]) {
        [self.shareInstence handle:info];
    }
}

@end
