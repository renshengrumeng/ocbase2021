//
//  ZBYDesignPrinciplesViewController.m
//  OCBase2021
//
//  Created by 周保勇 on 2021/5/24.
//

#import "ZBYDesignPrinciplesViewController.h"
#import "ZBYDPSinglePrinciple.h"
#import "ZBYDPOpenClosedPrinciple.h"
#import "NSObject+Runtime.h"

@interface ZBYDesignPrinciplesViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, copy) NSArray * models;

@end

@implementation ZBYDesignPrinciplesViewController

- (NSArray *)models {
    if (_models == nil) {
        _models = @[
        @{@"title" : @"简介", @"subTitle" : @"简介"},
        @{@"title" : @"单一职责原则", @"subTitle" : @"Single Responsibility Principle"},
        @{@"title" : @"开放封闭原则", @"subTitle" : @"Open Closed Principle"},
        @{@"title" : @"里式替换原则", @"subTitle" : @"Liskov Substitution Principle"},
        @{@"title" : @"接口隔离原则", @"subTitle" : @"Interface Segregation Principle"},
        @{@"title" : @"依赖倒置原则", @"subTitle" : @"Dependence Inversion Principle"},
        @{@"title" : @"迪米特法则也叫最少知道法则", @"subTitle" : @"Law of Demeter"}
        ];
    }
    return _models;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
    UITableView * tableView = [[UITableView alloc] initWithFrame:CGRectZero style:(UITableViewStylePlain)];
    tableView.delegate = self;
    tableView.dataSource = self;
    [self.view addSubview:tableView];
    
    tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];
}

// MARK: - UITableViewDelegate, UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.models.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString * identifier = @"123456";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:identifier];
        cell.backgroundColor = [UIColor whiteColor];
        cell.textLabel.textColor = [UIColor blackColor];
        cell.detailTextLabel.textColor = [UIColor blackColor];
    }
    cell.textLabel.text = self.models[indexPath.row][@"title"];
    cell.detailTextLabel.text = self.models[indexPath.row][@"subTitle"];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSDictionary * info = self.models[indexPath.row];
    NSString * title = info[@"title"];
    if ([title containsString:@"简介"]) {
        [self baseDescription];
    }
    if ([title containsString:@"单一职责原则"]) {
        [self singleResponsibilityPrinciple];
    }
    if ([title containsString:@"开放封闭原则"]) {
        [self openClosedPrinciple];
    }
    if ([title containsString:@"里式替换原则"]) {
        [self liskovSubstitutionPrinciple];
    }
    if ([title containsString:@"接口隔离原则"]) {
        [self interfaceSegregationPrinciple];
    }
    if ([title containsString:@"依赖倒置原则"]) {
        [self dependenceInversionPrinciple];
    }
    if ([title containsString:@"迪米特法则"]) {
        [self lawOfDemeter];
    }

}

// MARK: - 简介

- (void)baseDescription {
    /**
     在进行软件系统设计时所要遵循的一些经验准则，应用该准则的目的通常是为了避免某些经常出现的设计缺陷。
     这是百度百科的定义，但是在有经验的设计者会发现，其设计原则并不是单一存在和使用的，并且如果对设计原则进行死搬硬套，不但解决不了问题，还会出现新的问题；所在在使用过程中我们要根据业务的需求和相互之间的关系，选择适合的。（开篇说了一些废话）
     
     将六大原则的英文首字母拼在一起就是SOLID（稳定的），所以也称之为SOLID原则。
     
     只有满足了这六大原则，才能设计出稳定的软件架构，但它们只是原则，有时还是需要学会灵活应变，千万不要生搬硬套，否则只会把简单问题复杂化，切记！
     */
}

// MARK: - 单一职责原则（Single Responsibility Principle）

- (void)singleResponsibilityPrinciple {
    /**
     一个类只承担一个职责；不同的类具备不同的职责，各司其职。做系统设计是，如果发现有一个类拥有了两种职责，那么就要问一个问题：可以将这个类分成两个类吗？如果真的有必要，那就分开，千万不要让一个类干的事情太多。
     
     优点：
     1)降低类的复杂度，即类和类的耦合，一个类只负责一项职责。
     2)提高类的可读性，可维护性和可拓展性
     3)降低变更引起的风险。
     
     例子：我们设计一个订单列表，列表分为待分享、待收货、已收货等列表，那我们是写一个类，使用if判断是哪个类型，然后请求相应的数据，还是写多个类，分别执行各自的功能呢。很多人会觉的写一个类比较省事，但是过多的判断条件，各种职责冗余到一个类中真的好吗，如果待分享列表需要加一些特殊的功能呢，待收货也需要加一些功能呢，那这个类是不是变得条件判断异常的多。所以还是写成多个类，实现各自的逻辑比较好。其实另外我们写列表的Cell，也是一个道理，分成几种类型的Cell去写，而不是一个Cell实现几种类型。
     */
    
    ZBYDPSinglePrincipleOrderList * orderList = [ZBYDPSinglePrincipleOrderList new];
    orderList.waitPayOrderList = @[[ZBYDPSinglePrincipleWaitPayOrder new]];
    orderList.waitGoodsOrderList = @[[ZBYDPSinglePrincipleWaitGoodsOrder new]];
    orderList.receivedGoodsOrderList = @[[ZBYDPSinglePrincipleReceivedGoodsOrder new]];
    NSLog(@"订单列表类：%p", orderList);
}

// MARK: - 开放封闭原则（Open Closed Principle）

- (void)openClosedPrinciple {
    /**
     类、模块、函数，可以去扩展，但不要去修改。如果要修改代码，尽量用继承或组合的方式来扩展类的功能，而不是直接修改类的代码。当然，如果能保证对整个架构不会产生任何影响，那就没必要搞的那么复杂，直接改这个类吧。
     
     例子：我们设计分享功能的时候，会用到不同的分享方式，我们可以选择在分享的时候使用判断分享条件然后使用不同的分享方式，然而这种设计真的好吗？如果我们添加了一个分享方法或者删除了一个分享方法是不是要改动share方法的逻辑，那每一次的调整都要改动share方法的逻辑是不是不合理了？依据开闭原则具体做法应该是设计扩展分享方式来实现不同的分享。
     */
    UIButton * button1 = [UIButton buttonWithType:(UIButtonTypeContactAdd)];
    UIButton * button2 = [UIButton buttonWithType:(UIButtonTypeInfoLight)];
    NSLog(@"%@-%@", button1.class, button2.class);
    NSNumber * integerN = [NSNumber numberWithInteger:1];
    NSNumber * floatN = [NSNumber numberWithFloat:2.0];
    NSLog(@"%@-%@", integerN.class, floatN.class);
    NSLog(@"%@", integerN.class);
    NSLog(@"%@", floatN.class);
    
}

// MARK: - 里式替换原则（Liskov Substitution Principle）

- (void)liskovSubstitutionPrinciple {
    /**
     父类可以被子类无缝替换，且原有的功能不受影响；在使用继承时，遵循里氏替换原则，在子类中尽量不要重写和重载父类的方法。
     
     继承包含这样一层含义：父类中凡是已经实现好的方法（相对抽象方法而言），实际上是在设定一系列的规范和契约，虽然它不强制要求所有的子类必须遵循这些契约，但是如果子类对这些非抽象方法任意修改，就会对整个继承体系造成破坏。而里氏替换原则就是表达了这一层含义。
     
     继承作为面向对象三大特性之一，在给程序设计带来巨大遍历的同时，也带来了弊端。比如使用继承会给程序带来侵入性，程序的可移植性降低，增加对象间的耦合性，如果一个类被其他的类所继承，则当这个类需要修改时，必须考虑到所有的子类，并且父类修改后，所有涉及到子类的功能都有可能产生故障。
     
     优点：增加程序的健壮性，即使增加了子类，原有的子类还可以继续运行，互不影响。
     */
}

// MARK: - 接口隔离原则（Interface Segregation Principle）

- (void)interfaceSegregationPrinciple {
    /**
     模块不应该依赖它不需要的接口；一个类对另一个类的依赖应该建立在最小的接口上。
     
     一个类实现的接口中，包含了它不需要的方法。将接口拆分成更小和更具体的接口，有助于解耦，从而更容易重构、更改。
     
     优点：提高程序的灵活度，提高内聚，减少对外交互，使得最小的接口做最多的事情。
     */
}

// MARK: - 依赖倒置原则（Dependence Inversion Principle）

- (void)dependenceInversionPrinciple {
    /**
     高层模块不应该依赖低层模块，二者都应该依赖其抽象；抽象不应该依赖细节，细节应该依赖抽象。
     
     类A直接依赖类B，如果要将类A改为依赖类C，则必须通过修改类A的代码来达成。此时，类A一般是高层模块，负责复杂的业务逻辑，类B和类C是低层模块，负责基本的原子操作；修改A会给程序带来风险。
     将类A修改未依赖接口I，类B和类C各自实现接口I，类A通过接口I间接与类B或类C发生联系，则会大大降低修改类A的记几率。
     
     依赖倒置原则基于这样一个事实：相对于细节的多变性，抽象的东西要稳定的多。以抽象为基础搭建的架构比以细节为基础的架构要稳定的多。在java中，抽象指的是接口或抽象类，细节就是具体的实现类，使用接口或抽象类的目的是制定好规范，而不涉及任何具体的操作，把展现细节的任务交给他们的实现类去完成。
     依赖倒置的中心思想是面向接口编程。
     
     优点：可以减少需求变化带来的工作量，做并行开发更加友好。
     */
}

// MARK: - 迪米特法则（Law of Demeter）

- (void)lawOfDemeter {
    /**
     一个对象应该对其他对象保持最少的了解。
     
     类与类关系越密切，耦合度越大。
     
     迪米特法则又叫最少知道原则，即一个类对自己依赖的类知道的越少越好。也就是说，对于被依赖的类不管多么复杂，都尽量将逻辑封装在类的内部。对外除了提供的public 方法，不对外泄露任何信息。
     迪米特法则还有个更简单的定义：只与直接的朋友通信。
     什么是直接的朋友：每个对象都会与其他对象由耦合关系，只要两个对象之间有耦合关系，我们就说这两个对象之间是朋友关系。耦合的方式很多，依赖，关联，组合，聚合等。其中，我们称出现成员变量，方法参数，方法返回值中的类为直接的朋友，而出现在局部变量中的类不是直接的朋友。也就是说，陌生的类最好不要以局部变量的形式出现在类的内部。
     
     优点：低耦合，高内聚。
     */
}

@end
