//
//  NSObject+ZBYKVC.h
//  OCBase2021
//
//  Created by 周保勇 on 2021/5/10.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSObject (ZBYKVC)

/// 自定义KVC的setter
/// @param value <#value description#>
/// @param key <#key description#>
- (void)zby_setValue:(nullable id)value forKey:(NSString *)key;


/// 自定义KVC的getter
/// @param key <#key description#>
- (nullable id)zby_valueForKey:(NSString *)key;

@end

NS_ASSUME_NONNULL_END
