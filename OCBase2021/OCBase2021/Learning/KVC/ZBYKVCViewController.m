//
//  ZBYKVCViewController.m
//  OCBase2021
//
//  Created by 周保勇 on 2021/4/8.
//

#import "ZBYKVCViewController.h"
#import "ZBYKVCPerson.h"
#import "NSObject+ZBYKVC.h"

@interface ZBYKVCViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, copy) NSArray * models;

@end

@implementation ZBYKVCViewController

- (NSArray *)models {
    if (_models == nil) {
        _models = @[
        @{@"title" : @"KVC底层解析", @"subTitle" : @"KVC底层解析"},
        @{@"title" : @"KVC的基本使用", @"subTitle" : @"KVC设置和获取各种类型属性"},
        @{@"title" : @"KVC的setter", @"subTitle" : @"setter寻找顺序"},
        @{@"title" : @"KVC的getter", @"subTitle" : @"getter寻找顺序"},
        @{@"title" : @"集合的setter和getter", @"subTitle" : @"集合的setter和getter"},
        @{@"title" : @"自定义KVC", @"subTitle" : @"自定义KVC"},
        @{@"title" : @"KVC注意的点", @"subTitle" : @"KVC注意的点"}
        ];
    }
    return _models;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
    UITableView * tableView = [[UITableView alloc] initWithFrame:CGRectZero style:(UITableViewStylePlain)];
    tableView.delegate = self;
    tableView.dataSource = self;
    [self.view addSubview:tableView];
    
    tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];
}

// MARK: - UITableViewDelegate, UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.models.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString * identifier = @"123456";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:identifier];
        cell.backgroundColor = [UIColor whiteColor];
        cell.textLabel.textColor = [UIColor blackColor];
        cell.detailTextLabel.textColor = [UIColor blackColor];
    }
    cell.textLabel.text = self.models[indexPath.row][@"title"];
    cell.detailTextLabel.text = self.models[indexPath.row][@"subTitle"];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSDictionary * info = self.models[indexPath.row];
    NSString * title = info[@"title"];
    if ([title containsString:@"KVC底层解析"]) {
        [self underlyingParsing];
    }
    if ([title containsString:@"KVC的基本使用"]) {
        [self setOrGetProperty];
    }
    if ([title containsString:@"KVC的setter"]) {
        [self setterFindSort];
    }
    if ([title containsString:@"KVC的getter"]) {
        [self getterFindSort];
    }
    if ([title containsString:@"自定义KVC"]) {
        [self customKVCMethod];
    }
    if ([title containsString:@"KVC注意的点"]) {
        [self kvcMoreTip];
    }
}

/// 底层解析和文档
- (void)underlyingParsing {
    /** 文档步骤：https://developer.apple.com -> 滑倒底部（Documentation）Read API documentation -> 滑倒底部(Documentation Archive) -> 搜索Key-Value Coding KVC文档：https://developer.apple.com/library/archive/documentation/Cocoa/Conceptual/KeyValueCoding/index.html#//apple_ref/doc/uid/10000107i
     KVC：键值编码机制，是OC中对对象属性进行间接访问的一种机制，底层是通过几个通用的方法对对象的属性进行访问。
     */
    ZBYKVCPerson * person = [ZBYKVCPerson new];
    person.name = @"zhou";
    person.age = 34;
    person->_myName = @"bao";
    NSString * getName = person.name;
    NSString * getMyName = person->_myName;
    NSLog(@"%@,%@", getName, getMyName);
    
    /** setting的底层调用步骤：以setName为例
     1.调用属性对应的setter方法，如setName
     2.汇编调用对应属性标识符的通用方法，如objc_setProperty_nonatomic_copy
     3.调用reallySetProperty
     4.reallySetProperty中对新值的复制和设置、对旧值释放、以及自旋锁加锁的设置等
     */
    
    /** 对setting底层调用的疑问
     1.为什么底层不是调用的对应的setName方法呢？
     2.为什么用objc_setProperty_nonatomic_copy等通用方法调用？
     解释：在程序运行时，程序中会有成千上万的属性，如果都生成对应的setter和getter，那么就会有成千上万的方法；使用通用原则，使得setter和getter底层调用对应的通用方法，让代码更精简，效率更高
     */
    
    /** getter的底层调用步骤：以setName为例
     1.调用属性对应的getter方法，如name
     2.汇编调方法objc_getProperty
     3.objc_getProperty获取处理
     */
    
}

- (void)setOrGetProperty {
    ZBYKVCPerson * person = [ZBYKVCPerson new];
    person.name = @"zhou";
    // 1.KVC：引用数据类型的存取
    [person setValue:@"yong" forKey:@"name"];
    NSString * personName = [person valueForKey:@"name"];
    NSLog(@"%@", personName);
    // 2.KVC：集合类型(可变和不可变)
    NSArray * array = [person valueForKey:@"array"];
    array = @[@"100", @"2", @"5"];
    [person setValue:array forKey:@"array"];
    NSLog(@"%@", [person valueForKey:@"array"]);
    // 3.KVC：集合操作符
    
    // 4.KVC：访问非对象属性(基本数据类型、结构体和自定义结构体)
    NSInteger personAge = (NSInteger)[person valueForKey:@"age"];
    [person setValue:@33 forKey:@"age"];
    personAge = (NSInteger)[person valueForKey:@"age"];
    NSLog(@"%@", [person valueForKey:@"age"]);
    
    CGRect rect = CGRectMake(1, 2, 3, 4);
    NSValue * rectValue = [NSValue valueWithCGRect:rect];
    [person setValue:rectValue forKey:@"rect"];
    CGRect personRect = [(NSValue *)[person valueForKey:@"rect"] CGRectValue];
    NSLog(@"rect-x=%f", personRect.origin.x);
    
    ThreeFloats threeFloats = {1.0, 2.0, 3.0};
    NSValue * threeFloatsValue = [NSValue valueWithBytes:&threeFloats objCType:@encode(ThreeFloats)];
    [person setValue:threeFloatsValue forKey:@"threeFloats"];
    NSValue * floatsValue = [person valueForKey:@"threeFloats"];
    NSLog(@"自定义结构体=%@", floatsValue);
    ThreeFloats th;
    [floatsValue getValue:&th];
    NSLog(@"自定义结构体=%f-%f-%f", th.x, th.y, th.z);
    // 5.KVC：层层访问
    person.son = [ZBYKVCSon new];
    [person setValue:@"son-name" forKeyPath:@"son.name"];
    NSLog(@"sonName=%@", person.son.name);
}

- (void)setterFindSort {
    ZBYKVCPerson * person = [ZBYKVCPerson new];
    [person setValue:@"yong" forKey:@"firstName"];
//    NSLog(@"%@ - %@ - %@ - %@", person->_firstName, person->_isFirstName, person->firstName, person->isfirstName);
    NSLog(@"%@", person->isFirstName);
    /** KVC setter调用优先级：以person的firstName为例
     1.先找setter方法
     2.accessInstanceVariablesDirectly为YES，可以继续找对应的下划线或者is开头的变量；为NO直接第三步
     3.如果没有报错：setValue:forUndefinedKey:
     */
    /** KVC setter顺序及优先级：
     setter方法：setFirstName、_setFirstName
     属性值：_firstName、_isFirstName、firstName、isFirstName    
     */
}

- (void)getterFindSort {
    ZBYKVCPerson * person = [ZBYKVCPerson new];
//    person->_firstName = @"_firstName";
//    person->_isFirstName = @"_isFirstName";
//    person->firstName = @"firstName";
    person->isFirstName = @"isFirstName";
    NSLog(@"%@", [person valueForKey:@"firstName"]);
    /** KVC getter调用优先级：以person的firstName为例
     1.先找getter方法
     2.accessInstanceVariablesDirectly为YES，可以继续找对应的下划线或者is开头的变量；为NO直接第三步
     3.如果没有报错：valueForUndefinedKey:
     */
    /** KVC getter顺序及优先级：
     getter方法：getFirstName、firstName、isFirstName、_firstName
     属性值：_firstName、_isFirstName、firstName、isFirstName
     */
}

- (void)customKVCMethod {
    /** 如何自定义KVC呢？
     1.创建分类重写setValue:forKey:和valueForKey:方法
     2.根据setValue:forKey:和valueForKey:方法的优先级对其进行操作
     3.异常抛出处理
     */
    
    ZBYKVCPerson * person = [ZBYKVCPerson new];
//    [person zby_setValue:@"yong" forKey:@"firstName"];
//    NSLog(@"%@ - %@ - %@ - %@", person->_firstName, person->_isFirstName, person->firstName, person->isFirstName);
//    NSLog(@"%@", person->isFirstName);
    
//    person->_firstName = @"_firstName";
//    person->_isFirstName = @"_isFirstName";
//    person->firstName = @"firstName";
    person->isFirstName = @"isFirstName";
    NSLog(@"%@", [person zby_valueForKey:@"firstName"]);
}

- (void)kvcMoreTip {
    ZBYKVCPerson * person = [ZBYKVCPerson new];
    // 1.KVC的自动类型转换：基本数据类型、结构体和自定义结构体
    [person setValue:@"20" forKey:@"age"];  // 自动将字符串20转换为NSNumber20
//    [person setValue:@"{1, 2, 3, 4}" forKey:@"rect"];   // 无法自动转换
//    [person setValue:@"{1, 2, 3}" forKey:@"threeFloats"];   // 无法自动转化
    // 2.设置空值：重写setNilValueForKey方法
    [person setValue:nil forKey:@"age"]; // NSNumber设置nil，走重写setNilValueForKey方法
    [person setValue:nil forKey:@"rect"]; // NSValue设置nil，走重写setNilValueForKey方法
    [person setValue:nil forKey:@"threeFloats"]; // NSValue设置nil，走重写setNilValueForKey方法
    [person setValue:nil forKey:@"name"]; // NSObject设置nil，不走重写setNilValueForKey方法
    // 3.setter找不到key：重写setValue:forUndefinedKey:防止设置不存在的key崩溃
    [person setValue:nil forKey:@"subject"];
    // 4.getter找不到key：重写valueForUndefinedKey:防止获取不存在的key崩溃
    [person valueForKey:@"subject"];
    // 5.键值验证
    NSError * error;
    NSString * name = @"zhou_bao";
    if (![person validateValue:&name forKey:@"name" error:&error]) {
        NSLog(@"%@", error.localizedDescription);
    }
    else {
        NSLog(@"%@", [person valueForKey:@"name"]);
    }
}

@end
