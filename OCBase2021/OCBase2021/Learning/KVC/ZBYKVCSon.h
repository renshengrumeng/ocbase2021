//
//  ZBYKVCSon.h
//  OCBase2021
//
//  Created by 周保勇 on 2021/5/10.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZBYKVCSon : NSObject

/** 名字 */
@property (nonatomic, strong) NSString * name;

@end

NS_ASSUME_NONNULL_END
