//
//  ZBYKVCPerson.m
//  OCBase2021
//
//  Created by 周保勇 on 2021/5/9.
//

#import "ZBYKVCPerson.h"

@interface ZBYKVCPerson ()

@end

@implementation ZBYKVCPerson

- (void)setNilValueForKey:(NSString *)key {
    NSLog(@"%@ 的值为空", key);
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key {
    NSLog(@"%@ 不存在", key);
}

- (id)valueForUndefinedKey:(NSString *)key {
    NSLog(@"%@ 不存在", key);
    return nil;
}

+ (BOOL)accessInstanceVariablesDirectly {
    return YES;
}

- (void)setName:(NSString *)name {
    _name = [name copy];
}

//- (void)setFirstName:(NSString *)firstName {
//    NSLog(@"%s - %@", __func__, firstName);
//}

//- (void)_setFirstName:(NSString *)firstName {
//    NSLog(@"%s - %@", __func__, firstName);
//}

//- (NSString *)getFirstName {
//    return NSStringFromSelector(_cmd);
//}

//- (NSString *)firstName {
//    return NSStringFromSelector(_cmd);
//}

//- (NSString *)isFirstName {
//    return NSStringFromSelector(_cmd);
//}

//- (NSString *)_firstName {
//    return NSStringFromSelector(_cmd);
//}

// MARK: - 键值验证 可以用RunTime实现，一般不怎么用

- (BOOL)validateValue:(inout id  _Nullable __autoreleasing *)ioValue forKey:(NSString *)inKey error:(out NSError *__autoreleasing  _Nullable *)outError {
    if ([inKey isEqualToString:@"name"]) {
        [self setValue:[NSString stringWithFormat:@"可以修改一下：%@", *ioValue] forKey:inKey];
        return YES;
    }
    *outError = [[NSError alloc] initWithDomain:[NSString stringWithFormat:@"%@ 不是 %@ 的属性", inKey, self] code:10088 userInfo:nil];
    return NO;
}

@end
