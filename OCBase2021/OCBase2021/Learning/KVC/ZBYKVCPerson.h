//
//  ZBYKVCPerson.h
//  OCBase2021
//
//  Created by 周保勇 on 2021/5/9.
//

#import <Foundation/Foundation.h>
#import "ZBYKVCSon.h"

//@class ZBYKVCSon;

typedef struct {
    float x, y, z;
} ThreeFloats;

NS_ASSUME_NONNULL_BEGIN

@interface ZBYKVCPerson : NSObject {
@public
    NSString * _myName;
    
    // setter or getter
//    NSString * _firstName;
//    NSString * _isFirstName;
//    NSString * firstName;
    NSString * isFirstName;
    
@private
    
}

/** 不可变数组 */
@property (nonatomic, copy) NSArray * array;

/** 可变数组 */
@property (nonatomic, strong) NSMutableArray * mArray;

/** 结构体 */
@property (nonatomic, assign) CGRect rect;

/** 自定义结构体 */
@property (nonatomic, assign) ThreeFloats threeFloats;

/** 名字 */
@property (nonatomic, strong) NSString * name;

/** 年龄 */
@property (nonatomic, assign) NSInteger age;

/// 儿子
@property (nonatomic, strong) ZBYKVCSon * son;

@end

NS_ASSUME_NONNULL_END
