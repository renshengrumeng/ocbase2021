//
//  NSObject+ZBYKVC.m
//  OCBase2021
//
//  Created by 周保勇 on 2021/5/10.
//

#import "NSObject+ZBYKVC.h"
#import <objc/runtime.h>

@implementation NSObject (ZBYKVC)

- (void)zby_setValue:(nullable id)value forKey:(NSString *)key {
    // 1.寻找setter方法和处理
    if (key == nil || key.length == 0) {
        return;
    }
//    NSString * keyCapital = key.capitalizedString;
    NSString * keyCapital = [key stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:[[key substringToIndex:1] capitalizedString]];
    NSString * setKey = [NSString stringWithFormat:@"set%@:", keyCapital];
    NSString * _setKey = [NSString stringWithFormat:@"_set%@:", keyCapital];
    if ([self zby_performSelectorWithMethodName:setKey withObject:value]) {
        NSLog(@"-------%@-------", setKey);
        return;
    }
    if ([self zby_performSelectorWithMethodName:_setKey withObject:value]) {
        NSLog(@"-------%@-------", _setKey);
        return;
    }
    // 2.判断accessInstanceVariablesDirectly，为YES继续；为NO抛出异常
    if (![self.class accessInstanceVariablesDirectly]) {
        @throw [NSException exceptionWithName:@"ZBYKVC-unknowKey" reason:[NSString stringWithFormat:@"[%@ setValue:forUndefinedKey:]:this calss is not key value coding", self] userInfo:nil];
    }
    // 3.寻找属性，找到设置；没找到，抛出异常
    // _firstName、_isFirstName、firstName、isFirstName
    NSString * _keyIvar = [NSString stringWithFormat:@"_%@", key];
    NSString * _isKeyIvar = [NSString stringWithFormat:@"_is%@", keyCapital];
    NSString * keyIvar = key;
    NSString * isKeyIvar = [NSString stringWithFormat:@"is%@", keyCapital];
    NSArray * ivarList = [self getIvarNameList];
    if ([ivarList containsObject:_keyIvar]) {
        // 获取相应的ivar
        Ivar ivar = class_getInstanceVariable(self.class, _keyIvar.UTF8String);
        object_setIvar(self, ivar, value);
        return;
    }
    if ([ivarList containsObject:_isKeyIvar]) {
        // 获取相应的ivar
        Ivar ivar = class_getInstanceVariable(self.class, _isKeyIvar.UTF8String);
        object_setIvar(self, ivar, value);
        return;
    }
    if ([ivarList containsObject:keyIvar]) {
        // 获取相应的ivar
        Ivar ivar = class_getInstanceVariable(self.class, keyIvar.UTF8String);
        object_setIvar(self, ivar, value);
        return;
    }
    if ([ivarList containsObject:isKeyIvar]) {
        // 获取相应的ivar
        Ivar ivar = class_getInstanceVariable(self.class, isKeyIvar.UTF8String);
        object_setIvar(self, ivar, value);
        return;
    }
    
    @throw [NSException exceptionWithName:@"ZBYKVC-unknowKey" reason:[NSString stringWithFormat:@"[%@ setValue:forUndefinedKey:]:this calss is not key value coding", self] userInfo:nil];
}


- (nullable id)zby_valueForKey:(NSString *)key {
    // 1.寻找getter方法和处理
    if (key == nil || key.length == 0) {
        return nil;
    }
    // getter方法：getFirstName、firstName、isFirstName、_firstName
//    NSString * keyCapital = key.capitalizedString;
    NSString * keyCapital = [key stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:[[key substringToIndex:1] capitalizedString]];
    NSString * getKeyValue = [NSString stringWithFormat:@"get%@", keyCapital];
    NSString * keyValue = key;
    NSString * isKeyValue = [NSString stringWithFormat:@"is%@", keyCapital];
    NSString * _keyValue = [NSString stringWithFormat:@"_%@", key];
    NSString * countOfKeyValue = [NSString stringWithFormat:@"countOf%@", keyCapital];
    NSString * objectInKeyValue = [NSString stringWithFormat:@"objectIn%@", keyCapital];
    
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
    if ([self respondsToSelector:NSSelectorFromString(getKeyValue)]) {
        return [self performSelector:NSSelectorFromString(getKeyValue)];
    }
    if ([self respondsToSelector:NSSelectorFromString(keyValue)]) {
        return [self performSelector:NSSelectorFromString(keyValue)];
    }
    if ([self respondsToSelector:NSSelectorFromString(isKeyValue)]) {
        return [self performSelector:NSSelectorFromString(isKeyValue)];
    }
    if ([self respondsToSelector:NSSelectorFromString(_keyValue)]) {
        return [self performSelector:NSSelectorFromString(_keyValue)];
    }
    if ([self respondsToSelector:NSSelectorFromString(countOfKeyValue)]) {
        if ([self respondsToSelector:NSSelectorFromString(objectInKeyValue)]) {
            int num = (int)[self performSelector:NSSelectorFromString(countOfKeyValue)];
            NSMutableArray * mArray = [[NSMutableArray alloc] init];
            for (int i = 0; i < num - 1; i++) {
                num = (int)[self performSelector:NSSelectorFromString(countOfKeyValue)];
            }
            for (int j = 0; j < num; j++) {
                id objc = [self performSelector:NSSelectorFromString(objectInKeyValue)];
                [mArray addObject:objc];
            }
            return  mArray;
        }
        return [self performSelector:NSSelectorFromString(countOfKeyValue)];
    }
#pragma clang diagnostic pop
    // 2.判断accessInstanceVariablesDirectly，为YES继续；为NO抛出异常
    if (![self.class accessInstanceVariablesDirectly]) {
        @throw [NSException exceptionWithName:@"ZBYKVC-unknowKey" reason:[NSString stringWithFormat:@"[%@ valueForUndefinedKey:]:this calss is not key value coding", self] userInfo:nil];
    }
    
    // 3.寻找属性，找到返回值；没找到，抛出异常，返回nil
    // 属性值：_firstName、_isFirstName、firstName、isFirstName
    NSString * _key = [NSString stringWithFormat:@"_%@", key];
    NSString * _isKeyValue = [NSString stringWithFormat:@"_is%@", keyCapital];
//    NSString * key = key;
    NSString * isKey = [NSString stringWithFormat:@"is%@", keyCapital];
    NSArray * ivarList = [self getIvarNameList];
    if ([ivarList containsObject:_key]) {
        Ivar ivar = class_getInstanceVariable(self.class, _key.UTF8String);
        return object_getIvar(self, ivar);
    }
    if ([ivarList containsObject:_isKeyValue]) {
        Ivar ivar = class_getInstanceVariable(self.class, _isKeyValue.UTF8String);
        return object_getIvar(self, ivar);
    }
    if ([ivarList containsObject:key]) {
        Ivar ivar = class_getInstanceVariable(self.class, key.UTF8String);
        return object_getIvar(self, ivar);
    }
    if ([ivarList containsObject:isKey]) {
        Ivar ivar = class_getInstanceVariable(self.class, isKey.UTF8String);
        return object_getIvar(self, ivar);
    }
    return nil;
}

- (BOOL)zby_performSelectorWithMethodName:(NSString *)methodName withObject:(id)value {
    if ([self respondsToSelector:NSSelectorFromString(methodName)]) {
        
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
        [self performSelector:NSSelectorFromString(methodName) withObject:value];
#pragma clang diagnostic pop
        return YES;
    }
    return NO;
}

- (NSArray *)getIvarNameList {
    NSMutableArray * ivarList = [[NSMutableArray alloc] init];
    unsigned int count = 0;
    Ivar * ivars = class_copyIvarList(self.class, &count);
    for (int i = 0; i < count; i++) {
        Ivar ivar = ivars[i];
        const char * ivarNameChar = ivar_getName(ivar);
        NSString * ivarName = [NSString stringWithUTF8String:ivarNameChar];
        [ivarList addObject:ivarName];
    }
    free(ivars);
    return ivarList;
}

@end
