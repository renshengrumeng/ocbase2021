//
//  ZBYLearningViewController.m
//  OCBase2021
//
//  Created by 周保勇 on 2021/4/8.
//

#import "ZBYLearningViewController.h"
#import "ZBYKVCViewController.h"
#import "ZBYKVOViewController.h"
#import "ZBYGCDViewController.h"
#import "ZBYLockViewController.h"
#import "ZBYBlockViewController.h"
#import "ZBYGestureViewController.h"
#import "ZBYPerformanceOptimizationViewController.h"
#import "ZBYMemoryAllocationViewController.h"
#import "ZBYDesignPrinciplesViewController.h"
#import "ZBYDesignPatternsViewController.h"
#import "ZBYCopyViewController.h"
#import "ZBYRunTimeViewController.h"
#import "ZBYRunLoopViewController.h"
#import "ZBYMoreThreadViewController.h"
#import "ZBYCrashViewController.h"
#import "ZBYNotificationViewController.h"

@interface ZBYLearningViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, copy) NSArray * models;

@end

@implementation ZBYLearningViewController

- (NSArray *)models {
    if (_models == nil) {
        _models = @[
        @{@"title" : @"OC对象的内存分配和底层解析", @"subTitle" : @"OC对象的内存分配和底层解析"},
        @{@"title" : @"KVC", @"subTitle" : @"KVC-基础和底层"},
        @{@"title" : @"KVO", @"subTitle" : @"KVO-基础和底层"},
        @{@"title" : @"GCD", @"subTitle" : @"GCD-基础和底层"},
        @{@"title" : @"多线程中锁", @"subTitle" : @"线程安全与锁"},
        @{@"title" : @"Block", @"subTitle" : @"Block探索"},
        @{@"title" : @"触摸事件、手势、响应链", @"subTitle" : @"手势的基础和冲突的处理"},
        @{@"title" : @"性能优化", @"subTitle" : @"性能优化相关"},
        @{@"title" : @"设计原则", @"subTitle" : @"6大设计原则"},
        @{@"title" : @"设计模式", @"subTitle" : @"23种设计模式"},
        @{@"title" : @"OC中的copy", @"subTitle" : @"深拷贝和浅拷贝"},
        @{@"title" : @"runtime相关", @"subTitle" : @"底层分析和相关应用"},
        @{@"title" : @"RunLoop详解", @"subTitle" : @"OC的的RunLoop"},
        @{@"title" : @"多线程详解", @"subTitle" : @"线程和队列"},
        @{@"title" : @"常见的崩溃及其防治", @"subTitle" : @"Crash"},
        @{@"title" : @"Notification的使用和底层", @"subTitle" : @"通知的底层探索"}];
    }
    return _models;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITableView * tableView = [[UITableView alloc] initWithFrame:CGRectZero style:(UITableViewStylePlain)];
    tableView.delegate = self;
    tableView.dataSource = self;
    [self.view addSubview:tableView];
    
    tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    tableView.contentInset = UIEdgeInsetsMake(0, 0, (ZBYSize.tabBarHeight + ZBYSize.bottomSafeHeight), 0);
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(ZBYSize.navigationBarHeight);
        make.left.right.bottom.mas_equalTo(0);
    }];
}

// MARK: - UITableViewDelegate, UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.models.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString * identifier = @"123456";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.backgroundColor = [UIColor whiteColor];
        cell.textLabel.textColor = [UIColor blackColor];
        cell.detailTextLabel.textColor = [UIColor blackColor];
    }
    cell.textLabel.text = self.models[indexPath.row][@"title"];
    cell.detailTextLabel.text = self.models[indexPath.row][@"subTitle"];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSDictionary * info = self.models[indexPath.row];
    NSString * title = info[@"title"];
    if ([title containsString:@"OC对象的内存分配和底层解析"]) {
        ZBYMemoryAllocationViewController * memoryVC = [ZBYMemoryAllocationViewController new];
        memoryVC.title = title;
        [self.navigationController pushViewController:memoryVC animated:YES];
    }
    if ([title containsString:@"KVO"]) {
        ZBYKVOViewController * kvoVC = [ZBYKVOViewController new];
        kvoVC.title = title;
        [self.navigationController pushViewController:kvoVC animated:YES];
    }
    if ([info[@"title"] containsString:@"KVC"]) {
        ZBYKVCViewController * kvcVC = [ZBYKVCViewController new];
        kvcVC.title = title;
        [self.navigationController pushViewController:kvcVC animated:YES];
    }
    if ([info[@"title"] containsString:@"GCD"]) {
        ZBYGCDViewController * gcdVC = [ZBYGCDViewController new];
        gcdVC.title = title;
        [self.navigationController pushViewController:gcdVC animated:YES];
    }
    if ([info[@"title"] containsString:@"多线程中锁"]) {
        ZBYLockViewController * lockVC = [ZBYLockViewController new];
        lockVC.title = title;
        [self.navigationController pushViewController:lockVC animated:YES];
    }
    if ([info[@"title"] containsString:@"Block"]) {
        ZBYBlockViewController * blockVC = [ZBYBlockViewController new];
        blockVC.title = title;
        [self.navigationController pushViewController:blockVC animated:YES];
    }
    if ([info[@"title"] containsString:@"触摸事件、手势、响应链"]) {
        ZBYGestureViewController * gestureVC = [ZBYGestureViewController new];
        gestureVC.title = title;
        [self.navigationController pushViewController:gestureVC animated:YES];
    }
    if ([info[@"title"] containsString:@"性能优化"]) {
        ZBYPerformanceOptimizationViewController * optimizationVC = [ZBYPerformanceOptimizationViewController new];
        optimizationVC.title = title;
        [self.navigationController pushViewController:optimizationVC animated:YES];
    }
    if ([info[@"title"] containsString:@"设计原则"]) {
        ZBYDesignPrinciplesViewController * principlesVC = [ZBYDesignPrinciplesViewController new];
        principlesVC.title = title;
        [self.navigationController pushViewController:principlesVC animated:YES];
    }
    if ([info[@"title"] containsString:@"设计模式"]) {
        ZBYDesignPatternsViewController * patternsVC = [ZBYDesignPatternsViewController new];
        patternsVC.title = title;
        [self.navigationController pushViewController:patternsVC animated:YES];
    }
    if ([info[@"title"] containsString:@"OC中的copy"]) {
        ZBYCopyViewController * copyVC = [ZBYCopyViewController new];
        copyVC.title = title;
        [self.navigationController pushViewController:copyVC animated:YES];
    }
    if ([info[@"title"] containsString:@"runtime相关"]) {
        ZBYRunTimeViewController * runtimeVC = [ZBYRunTimeViewController new];
        runtimeVC.title = title;
        [self.navigationController pushViewController:runtimeVC animated:YES];
    }
    if ([info[@"title"] containsString:@"RunLoop详解"]) {
        ZBYRunLoopViewController * runLoopVC = [ZBYRunLoopViewController new];
        runLoopVC.title = title;
        [self.navigationController pushViewController:runLoopVC animated:YES];
    }
    if ([info[@"title"] containsString:@"多线程详解"]) {
        ZBYMoreThreadViewController * threadVC = [ZBYMoreThreadViewController new];
        threadVC.title = title;
        [self.navigationController pushViewController:threadVC animated:YES];
    }
    if ([info[@"title"] containsString:@"常见的崩溃及其防治"]) {
        ZBYCrashViewController * crashVC = [ZBYCrashViewController new];
        crashVC.title = title;
        [self.navigationController pushViewController:crashVC animated:YES];
    }
    if ([info[@"title"] containsString:@"Notification的使用和底层"]) {
        ZBYNotificationViewController * notificationVC = [ZBYNotificationViewController new];
        notificationVC.title = title;
        [self.navigationController pushViewController:notificationVC animated:YES];
    }
    
}

@end
