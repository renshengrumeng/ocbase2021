//
//  ZBYKVOViewController.m
//  OCBase2021
//
//  Created by 周保勇 on 2021/4/8.
//

#import "ZBYKVOViewController.h"
#import "ZBYKVOPerson.h"
#import "ZBYKVOSmallStudent.h"
#import "NSObject+ZBYKVO.h"
#import <KVOController.h>

@interface ZBYKVOViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, copy) NSArray * models;

@property (nonatomic, strong) ZBYKVOPerson * person;

@property (nonatomic, strong) ZBYKVOStudent * student;

@end

@implementation ZBYKVOViewController

- (NSArray *)models {
    if (_models == nil) {
        _models = @[
        @{@"title" : @"KVO的基本使用", @"subTitle" : @"基础使用"},
        @{@"title" : @"KVO底层原理", @"subTitle" : @"底层原理"},
        @{@"title" : @"KVO的自定义", @"subTitle" : @"getter寻找顺序KVO的自定义"},
        @{@"title" : @"OC调用实例对象的方法", @"subTitle" : @"实例对象调用方法的集合"},
        @{@"title" : @"KVO的自动移除", @"subTitle" : @"库KVOController的实现"},
//        @{@"title" : @"KVC注意的点", @"subTitle" : @"KVC注意的点"}
        ];
    }
    return _models;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
    UITableView * tableView = [[UITableView alloc] initWithFrame:CGRectZero style:(UITableViewStylePlain)];
    tableView.delegate = self;
    tableView.dataSource = self;
    [self.view addSubview:tableView];
    
    tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];
}

// MARK: - UITableViewDelegate, UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.models.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString * identifier = @"123456";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:identifier];
        cell.backgroundColor = [UIColor whiteColor];
        cell.textLabel.textColor = [UIColor blackColor];
        cell.detailTextLabel.textColor = [UIColor blackColor];
    }
    cell.textLabel.text = self.models[indexPath.row][@"title"];
    cell.detailTextLabel.text = self.models[indexPath.row][@"subTitle"];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSDictionary * info = self.models[indexPath.row];
    NSString * title = info[@"title"];
    if ([title containsString:@"KVO的基本使用"]) {
        [self KVOBaseUse];
    }
    if ([title containsString:@"KVO底层原理"]) {
        [self KVOImplementationDetails];
    }
    if ([title containsString:@"KVO的自定义"]) {
        [self customKVO];
    }
    if ([title containsString:@"OC调用实例对象的方法"]) {
        [self callInstenceObjectMethodType];
    }
    if ([title containsString:@"KVO的自动移除"]) {
        [self kvoAutoRemoveAndKVOController];
    }
    
}

// MARK: - KVO基础使用

- (void)KVOBaseUse {
    self.person = [ZBYKVOPerson new];
    self.person.student = [ZBYKVOStudent new];
    self.student = [ZBYKVOStudent new];
    // 1.KVO的添加和移除
    /** KVO基础使用步骤：
     1.调用addObserver:forKeyPath:options:context:添加需要观察的属性：
        1.1第一个参数是观察者，第二个参数是要观察的属性路径，第三个参数是需要观察的值的选项，第四个参数是上
            下文；
        1.2第四个参数用于区分不同类实例的相同属性路径或者相同类的不同实例属性，这种区分更加安全和高效；
            不进行区分可以传NULL
     2.观察者重写observeValueForKeyPath:ofObject:change:context:方法，用于监听属性的变化：
        2.1第一个参数是观察的属性路径，第二个参数是要观察的对象，第三个参数是改变的值的集合，第四个参数
            是1中传出的区分参数
     3.调用removeObserver:forKeyPath:移除观察者
        3.1如果不及时移除观察者，当观察者释放后，多线程或者延迟的被观察者属性变化后，会出现崩溃；
        3.2多次移除观察者，或者移除一个未注册的KeyPath，会出现崩溃；
        3.3添加的观察者，没有移除，在iOS11以前版本会造成崩溃，iOS11及以上版本不会。官方进行了优化。
     */
    [self.person addObserver:self forKeyPath:@"name" options:(NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew) context:PersionNameContext];
    [self.person addObserver:self forKeyPath:@"age" options:(NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew) context:NULL];
    [self.student addObserver:self forKeyPath:@"name" options:(NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew) context:StudentNameContext];
    self.person.name = @"周";
    self.person.age = 20;
    self.student.name = @"zhou";
    [self.person setValue:@"zhou_set" forKey:@"name"];
    
    // 没有及时移除，崩溃
//    ZBYKVOPerson * persion2 = [ZBYKVOPerson new];
//    [persion2 addObserver:self forKeyPath:@"name" options:(NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew) context:PersionNameContext];
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (ino64_t)(5 * NSEC_PER_SEC)), dispatch_get_global_queue(0, 0), ^{
//        NSLog(@"123456");
//        [persion2 setValue:@"zhou_set" forKey:@"name"];
//    });
    
    // 2.KVO的自动和手动开关
    /** 属性观察自动和手动：
     1.重写需要观察的属性所在类的automaticallyNotifiesObserversForKey:
        1.1属性的自动和手动观察：返回YES为自动观察，返回NO为手动观察。
     2.自动观察模式下addObserver添加观察后，在observeValueForKeyPath就能观察到属性的变化；
     3.手动观察模式下addObserver添加观察后，需要在修改观察属性前用观察的实例变量调用
        willChangeValueForKey，在修改后调用didChangeValueForKey；然后才能在observeValueForKeyPath观察到属性的变化；
     4.也可以在实例变量类的set方法中调用设置下划线变量前调用willChangeValueForKey和设置下划线变量后调用
        didChangeValueForKey
     */
    [self.student willChangeValueForKey:@"name"];
    self.student.name = @"周";
    [self.student didChangeValueForKey:@"name"];
    
    // 3.KVO多因素影响的观察
    /** 例如对象在下载进度进行观察；而下载进度取 = 当前下载量 / 总量；即当前下载量和总量都可以影响下载进度
     */
    [self.person addObserver:self forKeyPath:@"downloadProgress" options:(NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew) context:NULL];
    self.person.currentData = 10;
    self.person.totalData = 100;
    
    // 4.KVO观察可变数组：以ZBYKVOPerson的mArray属性为例
    /** 观察可变数组注意的点：
     1.KVO是在KVC基础上封装的，KVO依赖KVC；
     2.可变数组调用addObject添加元素，不触发可变数组属性的setter方法，所以不会触发KVO
     3.观察的实例变量调用mutableArrayValueForKey方法，再次调用addObject添加元素（或者
        removeObject移除元素），会触发KVO
     4.调用3的会走实例变量所在类重写的对应课表数组属性的方法insertObject:inMArrayAtIndex:
        （或者removeObjectFromMArrayAtIndex:）
     */
    self.person.mArray = [[NSMutableArray alloc] initWithCapacity:1];
    [self.person addObserver:self forKeyPath:@"mArray" options:(NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew) context:NULL];
    [self.person.mArray addObject:@"1"];
    [[self.person mutableArrayValueForKey:@"mArray"] addObject:@"2"];
    [[self.person mutableArrayValueForKey:@"mArray"] addObject:@"3"];
    [[self.person mutableArrayValueForKey:@"mArray"] removeObject:@"3"];
}

static char * PersionNameContext = "PersionNameContext";
static char * StudentNameContext = "StudentNameContext";

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
    if (context == PersionNameContext) {
        NSLog(@"%@-%@-%@", object, keyPath, change);
    }
    else if (context == StudentNameContext) {
        NSLog(@"%@-%@-%@", object, keyPath, change);
    }
    else {
        NSLog(@"%@-%@-%@", object, keyPath, change);
    }
}

- (void)dealloc {
//    [self.person removeObserver:self forKeyPath:@"name"];
//    [self.person removeObserver:self forKeyPath:@"name"]; // 多次移除崩溃，
//    [self.person removeObserver:self forKeyPath:@"nickName"];
//    [self.student removeObserver:self forKeyPath:@"name"];
//    [self.person removeObserver:self forKeyPath:@"age"];
//    [self.person removeObserver:self forKeyPath:@"downloadProgress"];
//    [self.person removeObserver:self forKeyPath:@"mArray"];
//    [self.person zby_removeObserver:self forKeyPath:@"name"];
    NSString * className = [NSString stringWithCString:object_getClassName(self.person) encoding:NSUTF8StringEncoding];
    NSLog(@"%@", className);
}

// MARK: - KVO底层原理

- (void)KVOImplementationDetails {
    self.person = [ZBYKVOPerson new];
    self.person.student = [ZBYKVOStudent new];
    self.student = [ZBYKVOStudent new];
    // 1.KVO底层的探索
    /** 文档步骤：https://developer.apple.com -> 滑倒底部（Documentation）Read API documentation -> 滑倒底部(Documentation Archive) -> 搜索Key-Value Observing KVO文档：https://developer.apple.com/library/archive/documentation/Cocoa/Conceptual/KeyValueObserving/KeyValueObserving.html#//apple_ref/doc/uid/10000177-BCICJDHA
     */
    /** KVO的原理：
     1.当实例对象添加观察者时动态生成子类：NSKVONotifying_xxx；
        1.1动态生成子类；
        1.2将实例变量的isa指向生成的子类
     2.KVO时观察属性的setter方法，不监听属性的ivar变量的改变
     3.动态子类重写很多方法：
        3.1setter方法：其中调用了willChangeValueForKey和didChangeValueForKey方法；且将观察切
            换为手动，即automaticallyNotifiesObserversForKey返回值设置为NO；
        3.2class方法
        3.3dealloc方法、_isKVOA方法
     4.消息转发给原类：
     5.消息发送-响应回调方法observeValueForKeyPath
     6.移除观察者后，isa会重新指向原类；
     7.动态生成的子类不会销毁，直到应用被杀死
     */
    NSString * className = [NSString stringWithCString:object_getClassName(self.person) encoding:NSUTF8StringEncoding];
    NSLog(@"%@", className);
    [self.person addObserver:self forKeyPath:@"name" options:(NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew) context:NULL];
    [self.person addObserver:self forKeyPath:@"nickName" options:(NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew) context:NULL];
    className = [NSString stringWithCString:object_getClassName(self.person) encoding:NSUTF8StringEncoding];
    NSLog(@"%@", className);
    NSLog(@"%@", [NSClassFromString(className) superClassNameList]);
    NSLog(@"%@", [NSClassFromString(className) methodNameList]);
    NSLog(@"%@", [NSClassFromString(className) classMethodNameList]);
    self.person.name = @"i_zhou";
    self.person->nickName = @"nick_zhou";   // 不监听属性的ivar变量的改变
}

// MARK: - 自定义KVO

- (void)customKVO {
    /** 自定义KVO步骤：按照系统KVO实现的详细步骤进行逐步实现
     1.检测是否有对应的setter方法，有进行下一步，没有提示或者报错；
     2.生成对应动态子类，给子类添加对应的方法（即重写setter方法、class方法、dealloc方法、_isKVOA方法）；
     3.将当前实例变量isa指向生成的动态子类；
     4.消息转发给原有类对象：将设置的新值传递给原有类的实例对象进行设置，即调用原类的setter方法
     5.消息发送-响应回调方法observeValueForKeyPath
     6.移除观察者后，isa会重新指向原类；
     7.动态生成的子类不会销毁，直到应用被杀死
     */
    self.person = [ZBYKVOPerson new];
    self.person.student = [ZBYKVOStudent new];
    self.student = [ZBYKVOStudent new];
    NSString * className = [NSString stringWithCString:object_getClassName(self.person) encoding:NSUTF8StringEncoding];
    NSLog(@"%@", className);
    [self.person zby_addObserver:self forKeyPath:@"name" options:(NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew) context:NULL];
    [self.person zby_addObserver:self forKeyPath:@"student" options:(NSKeyValueObservingOptionOld | NSKeyValueObservingOptionNew) context:NULL];
    className = [NSString stringWithCString:object_getClassName(self.person) encoding:NSUTF8StringEncoding];
    NSLog(@"%@", className);
    NSLog(@"%@", [NSClassFromString(className) superClassNameList]);
    NSLog(@"%@", [NSClassFromString(className) methodNameList]);
    NSLog(@"%@", [NSClassFromString(className) classMethodNameList]);
    NSLog(@"%@", [NSClassFromString(className) ivarNameList]);
    NSLog(@"%@", [NSClassFromString(className) propertyNameList]);
    self.person.name = @"123";
    self.person.student = [ZBYKVOStudent new];
    NSLog(@"%@", self.person.class);
}

- (void)zby_observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
    NSLog(@"%@-%@-%@", object, keyPath, change);
}

// MARK: - call instence object method

- (void)callInstenceObjectMethodType {
    /** OC调用实例对象的方法
     1.实例对象直接调用；
     2.Runtime objc_msgSender或者objc_msgSenderSuper调用
     */
    self.student = [ZBYKVOStudent new];
    SEL callSEL = @selector(callWithMethod:object:value:context:);
    // 第一种方法：调用observer的对象方法
            if ([self respondsToSelector:callSEL]) {
                [self callWithMethod:@"信息" object:self.student value:1 context:PersionNameContext];
            }
    // 第二种方法：调用observer的对象方法，与block很像
    void (*zby_objc_msgSend)(id, SEL, id, id, NSInteger, void *) = (void *)objc_msgSend;
    zby_objc_msgSend(self, callSEL, @"信息", self.student, 2, PersionNameContext);
    // block声明和调用
//            void (^blockName)(int value) = ^(int value) {
//                NSLog(@"%d", value);
//            };
//            blockName(2);
    // 第三种方法：强转和传值一起
    ((void(*)(id, SEL, id, id, NSInteger, void *))objc_msgSend)(self, callSEL, @"信息", self.student, 3, PersionNameContext);
}

- (void)callWithMethod:(NSString *)msg object:(id)object value:(NSInteger)value context:(char *)context {
    NSLog(@"%@-%@-%ld-%s", msg, object, (long)value, context);
}

// MARK: - KVO的自动移除和库KVOController的实现

- (void)kvoAutoRemoveAndKVOController {
    /**
     KVOController
     自动移除KVO实现：
     http://www.mobange.com/nav/m/99266.html
     */
}

@end
