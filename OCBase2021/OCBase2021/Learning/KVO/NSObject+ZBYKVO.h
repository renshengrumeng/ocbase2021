//
//  NSObject+ZBYKVO.h
//  OCBase2021
//
//  Created by 周保勇 on 2021/5/12.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZBYKVOInfo : NSObject

/// 观察者：使用weak避免引用循环；如果是strong，self(person) -> infoArray -> info -> observer(vc) -> person(self)就构成了循环引用
@property (nonatomic, weak) NSObject * observer;

/// 属性路径
@property (nonatomic, copy) NSString * keyPath;

/// 观察选项
@property (nonatomic, assign) NSKeyValueObservingOptions options;

- (instancetype)initWtihObserver:(NSObject *)observer forKeyPath:(NSString *)keyPath options:(NSKeyValueObservingOptions)options;

@end

@interface NSObject (ZBYKVO)

- (void)zby_addObserver:(NSObject *)observer forKeyPath:(NSString *)keyPath options:(NSKeyValueObservingOptions)options context:(nullable void *)context;

- (void)zby_observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context;

- (void)zby_removeObserver:(NSObject *)observer forKeyPath:(NSString *)keyPath;

@end

NS_ASSUME_NONNULL_END
