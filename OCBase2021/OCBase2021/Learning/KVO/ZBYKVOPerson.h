//
//  ZBYKVOPerson.h
//  OCBase2021
//
//  Created by 周保勇 on 2021/5/11.
//

#import <Foundation/Foundation.h>
#import "ZBYKVOStudent.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZBYKVOPerson : NSObject {
    @public
    NSString * nickName;
}

/// 名字
@property (nonatomic, copy) NSString * name;

/// 年龄
@property (nonatomic, assign) NSInteger age;

/// 学生
@property (nonatomic, strong) ZBYKVOStudent * student;

/// 可变数组
@property (nonatomic, strong) NSMutableArray * mArray;

/// 当前下载量
@property (nonatomic, assign) NSInteger currentData;

/// 数据总量
@property (nonatomic, assign) NSInteger totalData;

/// 下载进度
@property (nonatomic, assign) CGFloat downloadProgress;

@end

NS_ASSUME_NONNULL_END
