//
//  ZBYKVOStudent.m
//  OCBase2021
//
//  Created by 周保勇 on 2021/5/11.
//

#import "ZBYKVOStudent.h"

@interface ZBYKVOStudent () {
    NSString * _nickName;
}

@end

@implementation ZBYKVOStudent

/// 属性的自动和手动观察：YES为自动观察，NO为手动观察
/// @param key <#key description#>
+ (BOOL)automaticallyNotifiesObserversForKey:(NSString *)key {
    return NO;
}

- (void)setName:(NSString *)name {
//    [self willChangeValueForKey:@"name"];
    _name = [name copy];
//    [self didChangeValueForKey:@"name"];
}

- (void)setAge:(NSInteger)age {
    _age = age;
}

@end
