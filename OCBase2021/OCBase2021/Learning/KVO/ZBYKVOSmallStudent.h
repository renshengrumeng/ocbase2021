//
//  ZBYKVOSmallStudent.h
//  OCBase2021
//
//  Created by 周保勇 on 2021/5/11.
//

#import <Foundation/Foundation.h>
#import "ZBYKVOStudent.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZBYKVOSmallStudent : ZBYKVOStudent

/// 英文名称
@property (nonatomic, copy) NSString * eglishName;

+ (void)show;

@end

NS_ASSUME_NONNULL_END
