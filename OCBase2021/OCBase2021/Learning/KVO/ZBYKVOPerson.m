//
//  ZBYKVOPerson.m
//  OCBase2021
//
//  Created by 周保勇 on 2021/5/11.
//

#import "ZBYKVOPerson.h"

@implementation ZBYKVOPerson

- (void)setName:(NSString *)name {
    _name = [name copy];
    NSLog(@"%s", __func__);
}

- (CGFloat)downloadProgress {
    if (self.currentData <= 0 || self.totalData <= 0) {
        return 0;
    }
    return (CGFloat)self.currentData / (CGFloat)self.totalData;
}

/// 第一种：多个属性同时影响一个属性
+ (NSSet<NSString *> *)keyPathsForValuesAffectingDownloadProgress {
    return [NSSet setWithArray:@[@"currentData", @"totalData"]];
}

/// 第二种：多个属性同时影响一个属性
//+ (NSSet<NSString *> *)keyPathsForValuesAffectingValueForKey:(NSString *)key {
//    NSSet * keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
//    if ([key isEqualToString:@"downloadProgress"]) {
//        NSArray * affectingKeys = @[@"currentData", @"totalData"];
//        keyPaths = [keyPaths setByAddingObjectsFromArray:affectingKeys];
//    }
//    return keyPaths;
//}

- (void)insertObject:(id)object inMArrayAtIndex:(NSUInteger)index {
    [self.mArray insertObject:object atIndex:index];
}

- (void)removeObjectFromMArrayAtIndex:(NSUInteger)index {
    [self.mArray removeObjectAtIndex:index];
}

@end
