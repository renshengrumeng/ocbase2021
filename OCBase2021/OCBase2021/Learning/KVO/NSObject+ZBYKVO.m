//
//  NSObject+ZBYKVO.m
//  OCBase2021
//
//  Created by 周保勇 on 2021/5/12.
//

#import "NSObject+ZBYKVO.h"
//#import <objc/runtime.h>
#import <objc/message.h>

static const NSString * kZBYKVOChildClassPrefix = @"ZBYKVONotifying";
static const NSString * kZBYKVOInfoArrayAssociatedKey = @"kZBYKVOInfoArrayAssociatedKey";

@implementation ZBYKVOInfo

- (instancetype)initWtihObserver:(NSObject *)observer forKeyPath:(NSString *)keyPath options:(NSKeyValueObservingOptions)options {
    self = [super init];
    if (self) {
        self.observer = observer;
        self.keyPath = keyPath;
        self.options = options;
    }
    return self;
}

@end

@implementation NSObject (ZBYKVO)

- (void)zby_addObserver:(NSObject *)observer forKeyPath:(NSString *)keyPath options:(NSKeyValueObservingOptions)options context:(nullable void *)context {
    // 1.空值检查
    if (observer == nil || keyPath == nil || keyPath.length == 0) {
        return;
    }
    // 2.setter检查
    BOOL isExit = [self checkSelectorFromKeyPath:keyPath];
    if (!isExit) {
        @throw  [NSException exceptionWithName:NSInvalidArgumentException reason:[NSString stringWithFormat:@"没有%@的setter", keyPath] userInfo:nil];
    }
    // 3.生成对应的子类；重写class和setter方法
    Class newClass = [self creatChildClassWithKeyPath:keyPath];
    // 4.isa重新指向
    object_setClass(self, newClass);
    // 5.保存观察者相关信息：用关联对象保存观察者相关信息数组
    ZBYKVOInfo * info = [[ZBYKVOInfo alloc] initWtihObserver:observer forKeyPath:keyPath options:options];
    NSMutableArray * infoArray = objc_getAssociatedObject(self, (__bridge const void * _Nonnull)(kZBYKVOInfoArrayAssociatedKey));
    if (!infoArray) {
        infoArray = [[NSMutableArray alloc] initWithCapacity:1];
        [infoArray addObject:info];
        objc_setAssociatedObject(self, (__bridge const void * _Nonnull)(kZBYKVOInfoArrayAssociatedKey), infoArray, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
    else {
        BOOL isExit = NO;
        for (ZBYKVOInfo * item in infoArray) {
            if (item.observer == info.observer && item.keyPath == info.keyPath) {   // 存在更新数据
                item.options = info.options;
                isExit = YES;
                break;
            }
        }
        if (!isExit) {  // 不存在添加数据
            [infoArray addObject:info];
        }
    }
    // 6.在重写的setter方法中，回调给外界需要的信息
    // 7.
}


- (void)zby_observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
    
}

- (void)zby_removeObserver:(NSObject *)observer forKeyPath:(NSString *)keyPath {
    /** 移除当前属性的观察者
     1.从观察者数组中移除观察者信息模型
     2.将实例对象的isa指针指向原有类
     */
    // 1.移除
    NSMutableArray * infoArray = objc_getAssociatedObject(self, (__bridge const void * _Nonnull)(kZBYKVOInfoArrayAssociatedKey));
    if (infoArray.count <= 0) {
        return;
    }
    [infoArray enumerateObjectsUsingBlock:^(ZBYKVOInfo * item, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([item.keyPath isEqualToString:keyPath]) {
            [infoArray removeObject:item];
            *stop = YES;
            objc_setAssociatedObject(self, (__bridge const void * _Nonnull)(kZBYKVOInfoArrayAssociatedKey), infoArray, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
        }
    }];
    // 2.isa恢复
    if (infoArray.count <= 0) {
        Class superClass = class_getSuperclass(object_getClass(self));
        object_setClass(self, superClass);
    }
}

// MARK: - prvate

- (NSString *)getterNameFromKeyPath:(NSString *)keyPath {
    NSArray * keyPathList = [keyPath componentsSeparatedByString:@"."];
    NSString * getter = keyPathList.lastObject;
    return getter;
}

/// 验证是否存在相关属性的setter方法
/// @param keyPath YES存在，NO不存在
- (BOOL)checkSelectorFromKeyPath:(NSString *)keyPath {
    // 获取原有类
    Class originalClass = object_getClass(self);
    // 获取setter方法
    NSString * getter = [self getterNameFromKeyPath:keyPath];
    if (getter.length <= 0) {
        return NO;
    }
    SEL setterSelector = NSSelectorFromString([self setterStringForGetterString:getter]);
    Method setterMethod = class_getInstanceMethod(originalClass, setterSelector);
    return setterMethod != nil;
}

/// getter方法获取setter方法
- (NSString *)setterStringForGetterString:(NSString *)getter {
    NSString * firstUpperGetter = [getter stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:[[getter substringToIndex:1] capitalizedString]];
    NSString * setter = [NSString stringWithFormat:@"set%@:", firstUpperGetter];
    return setter;
}

/// setter方法获取getter方法
- (NSString *)getterStringForSetterString:(NSString *)setter {
    NSString * firstUpperGetter = [setter stringByReplacingCharactersInRange:NSMakeRange(0,3) withString:@""];
    NSString * getter = [firstUpperGetter stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:[[firstUpperGetter substringToIndex:1] lowercaseString]];
    return [getter substringWithRange:(NSMakeRange(0, getter.length - 1))];
}

- (Class)creatChildClassWithKeyPath:(NSString *)keyPath {
    NSString * originalClassName = NSStringFromClass(self.class);
    NSString * newClassName;
    if ([originalClassName hasPrefix:[NSString stringWithFormat:@"%@", kZBYKVOChildClassPrefix]]) { // 已经创建过子类
        newClassName = originalClassName;
    }
    else {  // 新建类
        newClassName = [NSString stringWithFormat:@"%@_%@", kZBYKVOChildClassPrefix, originalClassName];
    }
    Class newClass = NSClassFromString(newClassName);
    // 防止重复创建生成新类
    if (newClass) { // 已经创建，但是没有对应的setter
        // 3.2添加对应的setter
        NSString * getter = [self getterNameFromKeyPath:keyPath];
        NSString * setter = [self setterStringForGetterString:getter];
        NSArray * seletorNameList = [self methodNameList];
        if ([seletorNameList containsObject:setter]) {
            NSLog(@"setter已存在");
        }
        else {
            SEL setterSEL = NSSelectorFromString(setter);
            Method setterMethod = class_getInstanceMethod(self.class, setterSEL);
            const char * setterType = method_getTypeEncoding(setterMethod);
            class_addMethod(newClass, setterSEL, (IMP)zby_setter, setterType);
        }
        return newClass;
    }
    /** 如何在内存中创建和注册新类？如何向新类中添加方法？
     1.申请创建类内存；
     2.注册类；
     3.添加方法；
     */
    // 1.申请创建类内存：第一个参数是父类，第二个参数是新类的名称，第三个参数是新类开辟的额外空间
    newClass = objc_allocateClassPair(self.class, newClassName.UTF8String, 0);
    // 2.注册类：方法和属性可以注册前后添加，ivar只能注册前添加
//    const char * ivarName = "_lastName";
//    BOOL isSuccess = class_addIvar(newClass, ivarName, sizeof(NSString *), 0, "@");
    objc_registerClassPair(newClass);
//    const char * pName = "firstName";
//    objc_property_t eP = class_getProperty(self.class, "name");
//    unsigned int outCount;
//    objc_property_attribute_t * ePAtt = property_copyAttributeList(eP, &outCount);
//    BOOL isSuccess = class_addProperty(newClass, pName, ePAtt, outCount);
    // 3.1添加class，将class指向原类
    SEL classSEL = NSSelectorFromString(@"class");
    Method classMethod = class_getInstanceMethod(self.class, classSEL);
    const char * classType = method_getTypeEncoding(classMethod);
    class_addMethod(newClass, classSEL, (IMP)zby_class, classType);
    // 3.2添加dealloc，将class指向原类；用于实例对象释放后，自动移除观察者，不用主动调用removeObserver
    SEL deallocSEL = NSSelectorFromString(@"dealloc");
    Method deallocMethod = class_getInstanceMethod(self.class, deallocSEL);
    const char * deallocType = method_getTypeEncoding(deallocMethod);
    class_addMethod(newClass, deallocSEL, (IMP)zby_dealloc, deallocType);
    // 3.2添加对应的setter
    NSString * getter = [self getterNameFromKeyPath:keyPath];
    NSString * setter = [self setterStringForGetterString:getter];
    SEL setterSEL = NSSelectorFromString(setter);
    Method setterMethod = class_getInstanceMethod(self.class, setterSEL);
    const char * setterType = method_getTypeEncoding(setterMethod);
    class_addMethod(newClass, setterSEL, (IMP)zby_setter, setterType);
    return newClass;
}

Class zby_class(id self, SEL _cmd) {
    NSLog(@"zby_class");
    // object_getClass(self)获取的是已交换的isa指向的类，即新生成的子类
    // class_getSuperclass(object_getClass(self))是原类：即新生子类的父类
    // 该方法中调用[self class] 自己调用自己会陷入死循环
    return object_getClass(self);
}

static void zby_dealloc(id self, SEL _cmd) {
    // 1.移除
    objc_removeAssociatedObjects(self);
    // 2.isa恢复
    Class superClass = class_getSuperclass(object_getClass(self));
    object_setClass(self, superClass);
}

static void zby_setter(id self, SEL _cmd, id newValue) {
    NSLog(@"zby_setter:%@", newValue);
    /** 6.在重写的setter方法中，回调给外界需要的信息
     6.1调起原类（生成子类的父类）的对应的setter方法；
     6.2回调给外界信息：obsever的处理，新旧值的传出和处理
     */
    // 获取旧值
    NSString * keyPath = [self getterStringForSetterString:NSStringFromSelector(_cmd)];
    id oldValue = [self valueForKey:keyPath] == nil ? [NSNull new] : [self valueForKey:keyPath];
    // 6.1调起父类对应的setter
    struct objc_super superStruct = {
        .receiver = self,
        .super_class = class_getSuperclass([self class])
    };
    void (*zby_objc_msgSendSuper)(void *, SEL, id) = (void *)objc_msgSendSuper;
    // _cmd为当前setter方法，因为原有类的setter和生成子类的setter SEL是一样的，所以SEL传_cmd
    zby_objc_msgSendSuper(&superStruct, _cmd, newValue);
    // 6.2回调给外界信息：
    NSMutableArray * infoArray = objc_getAssociatedObject(self, (__bridge const void * _Nonnull)(kZBYKVOInfoArrayAssociatedKey));
    for (ZBYKVOInfo * item in infoArray) {
        NSString * getter = [self getterNameFromKeyPath:item.keyPath];
        if ([getter isEqualToString:keyPath]) {
            SEL observerSEL = @selector(zby_observeValueForKeyPath:ofObject:change:context:);
            NSMutableDictionary<NSKeyValueChangeKey,id> * change = [[NSMutableDictionary<NSKeyValueChangeKey,id> alloc] init];
            change[NSKeyValueChangeOldKey] = oldValue;
            change[NSKeyValueChangeNewKey] = newValue;
            // 第一种方法：调用observer的对象方法
//            if ([item.observer respondsToSelector:observerSEL]) {
//                [item.observer zby_observeValueForKeyPath:keyPath ofObject:self change:change context:NULL];
//            }
            // 第二种方法：调用observer的对象方法，与block很像
            void (*zby_objc_msgObserverSend)(id, SEL, id, id, id, void *) = (void *)objc_msgSend;
            zby_objc_msgObserverSend(item.observer, observerSEL, keyPath, self, change, NULL);
            // block声明和调用
//            void (^blockName)(int value) = ^(int value) {
//                NSLog(@"%d", value);
//            };
//            blockName(2);
            // 第三种方法：强转和传值一起
//            ((void(*)(id, SEL, id, id, id, void *))objc_msgSend)(item.observer, observerSEL, keyPath, self, change, NULL);
            break;
        }
    }
}

@end
