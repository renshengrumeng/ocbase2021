//
//  ZBYRunTimeStudent+Swizzle.m
//  OCBase2021
//
//  Created by anniekids good on 2022/2/24.
//

#import "ZBYRunTimeStudent+Swizzle.h"
#import "NSObject+Extension.h"

@implementation ZBYRunTimeStudent (Swizzle)

+ (void)load {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        [self swizzledOldMethod:@selector(sayHello) newMethod:@selector(s_sayHello)];
//        [ZBYRunTimePerson zby_swizzleMethod:@selector(sayHello) withMethod:@selector(s_sayHello) error:nil];
    });
}

- (void)s_sayHello {
    [self s_sayHello];

    NSLog(@"Student + swizzle say hello");
}

@end
