//
//  ZBYRunTimePerson.h
//  OCBase2021
//
//  Created by anniekids good on 2022/2/24.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZBYRunTimePerson : NSObject

+ (BOOL)zby_swizzleMethod:(SEL)origSel withMethod:(SEL)altSel error:(NSError**)error;

- (void)sayHello;

@end

NS_ASSUME_NONNULL_END
