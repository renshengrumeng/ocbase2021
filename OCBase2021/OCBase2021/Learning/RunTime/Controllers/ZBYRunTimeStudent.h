//
//  ZBYRunTimeStudent.h
//  OCBase2021
//
//  Created by anniekids good on 2022/2/24.
//

#import <Foundation/Foundation.h>
#import "ZBYRunTimePerson.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZBYRunTimeStudent : ZBYRunTimePerson

@end

NS_ASSUME_NONNULL_END
