//
//  ZBYRunTimeViewController.m
//  OCBase2021
//
//  Created by 周保勇 on 2021/8/5.
//

#import "ZBYRunTimeViewController.h"
#import <objc/runtime.h>
#import "ZBYRunTimePersionModel+AssociatedObject.h"
#import "ZBYRunTimeStudent.h"
//#import "Aspect.h"

@interface ZBYRunTimeViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, copy) NSArray * models;

@end

@implementation ZBYRunTimeViewController

- (NSArray *)models {
    if (_models == nil) {
        _models = @[
        @{@"title" : @"runtime官方文档和相关文章", @"subTitle" : @"官方文档和比较好的分析文章"},
        @{@"title" : @"消息发送", @"subTitle" : @"消息发送"},
        @{@"title" : @"消息转发", @"subTitle" : @"消息的转发"},
        @{@"title" : @"关联对象Associated Objects", @"subTitle" : @"关联对象的操作和分类添加属性"},
        @{@"title" : @"方法交换Method Swizzling", @"subTitle" : @"方法交换和注意事项"},
        @{@"title" : @"NSInvocation方法转发", @"subTitle" : @"方法签名和转发"},
        @{@"title" : @"OC中方法调用四种形式", @"subTitle" : @"方法调用的底层"},
        @{@"title" : @"OC语言的动态性", @"subTitle" : @"总结"},
        @{@"title" : @"synthesize和dynamic", @"subTitle" : @"属性的setter、getter和ivar"},
        @{@"title" : @"iOS界的毒瘤-MethodSwizzling", @"subTitle" : @"MethodSwizzling导致的问题"},
        @{@"title" : @"iOS hook的几种方法", @"subTitle" : @"hook的原理和三方库"}];
    }
    return _models;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITableView * tableView = [[UITableView alloc] initWithFrame:CGRectZero style:(UITableViewStylePlain)];
    tableView.delegate = self;
    tableView.dataSource = self;
    [self.view addSubview:tableView];
    
    tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];
}

// MARK: - UITableViewDelegate, UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.models.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString * identifier = @"123456";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.backgroundColor = [UIColor whiteColor];
        cell.textLabel.textColor = [UIColor blackColor];
        cell.detailTextLabel.textColor = [UIColor blackColor];
    }
    cell.textLabel.text = self.models[indexPath.row][@"title"];
    cell.detailTextLabel.text = self.models[indexPath.row][@"subTitle"];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSDictionary * info = self.models[indexPath.row];
    NSString * title = info[@"title"];
    if ([title containsString:@"消息发送"]) {
        [self aboutMsgSend];
    }
    if ([title containsString:@"消息转发"]) {
        [self aboutMsgForword];
    }
    if ([title containsString:@"关联对象Associated Objects"]) {
        [self associatedObjectsOperation];
    }
    if ([title containsString:@"方法交换Method Swizzling"]) {
        [self methodSwizzling];
    }
    if ([title containsString:@"NSInvocation方法转发"]) {
        [self invocationForwarding];
    }
    if ([title containsString:@"OC中方法调用四种形式"]) {
        [self callMethod];
    }
    if ([title containsString:@"OC语言的动态性"]) {
        [self ObjectCDynamic];
    }
    if ([title containsString:@"synthesize和dynamic"]) {
        [self synthesizeAndDynamic];
    }
    if ([title containsString:@"iOS界的毒瘤-MethodSwizzling"]) {
        [self methodSwizzlingProblem];
    }
}

// MARK: - 文档和基础

- (void)aboutRunTimeArticle {
    /**
     什么是runtime呢？
        1.runtime运行时；
        2.是OC代码底层解释，是OC代码动态运行和调用的基础；
        3.是C面向对象的封装，是C、C++和汇编语言的指令集。
     
     官方文档：
     寻找步骤：
        1.进入https://developer.apple.com/
        2.滑动到底部点击Documentation - Read API documentation
        3.滑动到底部点击Documentation Archive - Visit archive
        4.在Documents右侧搜索栏中输入需要的文档或者在左侧文档分类列表中寻找相应的文档
     
     相关文档：
        1.运行时文档Objective-C Runtime Programming：
            https://developer.apple.com/library/archive/documentation/Cocoa/Conceptual/ObjCRuntimeGuide/Introduction/Introduction.html#//apple_ref/doc/uid/TP40008048
        2.运行时参考Objective-C Runtime Reference：
            https://developer.apple.com/documentation/objectivec/objective-c_runtime
        3.KVC键值编码文档Key-Value Coding Programming：
            https://developer.apple.com/library/archive/documentation/Cocoa/Conceptual/KeyValueCoding/index.html#//apple_ref/doc/uid/10000107i
        4.KVO键值观察者文档Key-Value Observing Programming：
            https://developer.apple.com/library/archive/documentation/Cocoa/Conceptual/KeyValueObserving/KeyValueObserving.html#//apple_ref/doc/uid/10000177i
     
     Objective-C程序在三个不同的层次与运行时系统交互：
        1.通过Objective-C源代码；即程序员只需要编写的OC代码，编译和运行时自动将OC代码编译和调用；
        2.通过Foundation框架的NSObject类中定义的方法：例如isKindOfClass和isMemberOfClass、respondsToSelector、
        methodForSelector等；
        3.通过直接调用运行时函数：例如objc_allocateClassPair、class_copyIvarList等。
     
     pragramming 层面的 runtime 主要体现在以下几个方面：
        1.关联对象Associated Objects：https://nshipster.cn/associated-objects/
        2.消息发送Message Send：
            https://developer.apple.com/library/archive/documentation/Cocoa/Conceptual/ObjCRuntimeGuide/Articles/ocrtHowMessagingWorks.html#//apple_ref/doc/uid/TP40008048-CH104-SW1
        3.消息转发Message Forwarding：
            https://developer.apple.com/library/archive/documentation/Cocoa/Conceptual/ObjCRuntimeGuide/Articles/ocrtForwarding.html#//apple_ref/doc/uid/TP40008048-CH105-SW1
        4.方法交换Method Swizzling：https://nshipster.com/method-swizzling/
        5.类对象NSProxy：https://developer.apple.com/documentation/foundation/nsproxy
        6.KVC、KVO：文档链接同上
     
     源码下载地址：
     https://github.com/apple-oss-distributions/
     https://github.com/apple-oss-distributions/objc4/tags
     */
    
    /**
     其它比较好的文章：
     https://halfrost.com/objc_runtime_isa_class/
     https://github.com/DeveloperErenLiu/RuntimePDF
     */
}

// MARK: - 消息发送

- (void)aboutMsgSend {
    /**
     消息发送机制：
     对于C语言，函数的调用在编译的时候会决定调用哪个函数。编译完成之后直接顺序执行，无任何二义性。OC的函数调用称为消息发送。属于动态调用过程。在编译的时候并不能决定真正调用哪个函数（也就是说，在编译阶段，OC可以调用任何函数，即使这个函数并未实现，只要申明过就不会报错。而C语言在编译阶段就会报错）。只有在真正运行的时候才会根据函数的名称找 到对应的函数来调用。
     */
    /**
     消息发送底层实现：
     https://www.jianshu.com/p/0c4ac4fb1134
     https://www.jianshu.com/p/4cea1bc948d6
     https://www.jianshu.com/p/1972010b88d7
     OC中方法调用的实质是消息发送机制，最终都是转成objc_msgSend的函数调用。
     
     第一个参数是调用者，第二个是调用的方法，从第三个起是传入的其它参数
     objc_msgSend(id _Nullable self, SEL _Nonnull op, ...)
     
     消息发送的步骤及方法调用的顺序：
     第一步：查询接收者是否为空，如果为空，直接退出，如果不为空走第二步。
     第二步：查询缓存cache（缓存方法的列表）中是否已缓存该方法，如果有则调用方法，如果没有走第三步。值的注意的是缓存中存储方法使用散列表列表的方式存储的。
     第三步：查询class_rw_t中methods中是否有该方法，如果有则调用方法，并且添加到缓存中，如果没有走第四步。值的注意的是，这个查询方法是根据该方法列表是否已经排好序了，如果已经排序则用二分法查找，如果没有则是遍历查找
     第四步：通过superclass查询父类否有该方法，如果有则调用方法，并且添加到自己的缓存中，如果没有继续调用第四步，如果查找到基类都没有这个方法，那么就执行第二阶段动态解析。值的注意的是，这一步的查找方式是执行的父类中第二步跟第三步。
     
     消息发送的详细过程：源码版本为objc4-818.2，iPhone以arm64架构，以ZBYRunTimePersionModel的run方法为例
     1.runtime将方法转换为objc_msgSend，并执行该方法：例如将run方法转换执行((void(*)(id, SEL))objc_msgSend)(persion, run)；可以通过clang或者下符号断点获知。
     2.在源码中可以看到objc_msgSend方法是使用汇编来实现的；为什么要用汇编来实现呢？有以下几点原因
        2.1汇编更加容易被机器识别，效率更高。
        2.2C语言中不可以通过一个函数来保留未知的参数并且跳转到任意的函数指针。C语言没有满足这些事情的必要特性。
     3.查看objc_msgSend汇编代码：
             
             ENTRY _objc_msgSend
             UNWIND _objc_msgSend, NoFrame

             cmp    p0, #0            // nil check and tagged pointer check
         #if SUPPORT_TAGGED_POINTERS
             b.le    LNilOrTagged        // 判断是否是tagged pointer
         #else
             b.eq    LReturnZero    // 判断是否是nil
         #endif
             ldr    p13, [x0]        // p13 = isa，获取isa指针地址
             GetClassFromIsa_p16 p13, 1, x0    // p16 = class，用isa获取Class
         LGetIsaDone:
             // calls imp or objc_msgSend_uncached，CacheLookup缓存查找方法的实现指针（即IMP）
             CacheLookup NORMAL, _objc_msgSend, __objc_msgSend_uncached
     4.查找缓存
        .macro CacheLookup
             // do {    // 循环遍历缓存中的bucket
        1:    ldp    p17, p9, [x13], #-BUCKET_SIZE    //     {imp, sel} = *bucket--
        cmp    p9, p1                //     if (sel != _cmd) {  // 当前方法不是调用的方法，继续循环
        b.ne    3f                //         scan more
             //     } else {    // 当前方法是调用的方法，执行CacheHit
        2:    CacheHit \Mode                // hit:    call or return imp
             //     }
        3:    cbz    p9, \MissLabelDynamic        //     if (sel == 0) goto Miss;   // 循环结束缓存中没有调用的方法调用LCacheMiss
        cmp    p13, p10            // } while (bucket >= buckets)
        b.hs    1b
     注意：LCacheMiss方法在源码中没有找到实现，但是有CacheLookup中注释到LCacheMiss是调用__objc_msgSend_uncached或者__objc_msgLookup_uncached方法。
     
     5.调用__objc_msgSend_uncached或者__objc_msgLookup_uncached方法
     
         STATIC_ENTRY __objc_msgSend_uncached
         UNWIND __objc_msgSend_uncached, FrameWithNoSaves

         // THIS IS NOT A CALLABLE C FUNCTION
         // Out-of-band p15 is the class to search
         
         MethodTableLookup  // 调用MethodTableLookup方法
         TailCallFunctionPointer x17

         END_ENTRY __objc_msgSend_uncached


         STATIC_ENTRY __objc_msgLookup_uncached
         UNWIND __objc_msgLookup_uncached, FrameWithNoSaves

         // THIS IS NOT A CALLABLE C FUNCTION
         // Out-of-band p15 is the class to search
         
         MethodTableLookup  // 调用MethodTableLookup方法
         ret

         END_ENTRY __objc_msgLookup_uncached
     
     6.调用MethodTableLookup方法
         .macro MethodTableLookup
             
             SAVE_REGS MSGSEND

             // lookUpImpOrForward(obj, sel, cls, LOOKUP_INITIALIZE | LOOKUP_RESOLVER)
             // receiver and selector already in x0 and x1
             bl    _lookUpImpOrForward  // 调用_lookUpImpOrForward方法，即是调用lookUpImpOrForward方法

         .endmacro
     
     注意：以上为方法调用汇编部分（也叫做方法的快速查找，及在类的缓存列表中查找调用），下面是分析其慢速查找部分，也就是c语言部分。
     
     7.调用lookUpImpOrForward方法
         // curClass method list.搜索方法，搜索到直接跳转done
         Method meth = getMethodNoSuper_nolock(curClass, sel);
         if (meth) {
             imp = meth->imp(false);
             goto done;
         }

         // 没搜索到方法去父类查找（先缓存，再方法列表），没有父类进入_objc_msgForward_impcache方法
         if (slowpath((curClass = curClass->getSuperclass()) == nil)) {
             // No implementation found, and method resolver didn't help.
             // Use forwarding.
             imp = forward_imp;
             break;
         }
     
         // No implementation found. Try method resolver once.
         // 1.上述循环如果IMP没有找到，调用resolveMethod_locked方法尝试做一次动态方法解析。
         // 2.在动态解析后依然没有找到处理IMP，会调用_objc_msgForward_impcache方法

         if (slowpath(behavior & LOOKUP_RESOLVER)) {
             behavior ^= LOOKUP_RESOLVER;
             return resolveMethod_locked(inst, sel, cls, behavior);
         }
     
     8.调用_objc_msgForward_impcache方法，该方法是使用的汇编实现
         STATIC_ENTRY __objc_msgForward_impcache

         // No stret specialization.调用__objc_msgForward
         b    __objc_msgForward

         END_ENTRY __objc_msgForward_impcache

         
         ENTRY __objc_msgForward

         adrp    x17, __objc_forward_handler@PAGE
         ldr    p17, [x17, __objc_forward_handler@PAGEOFF]  // 转发未找到处理的IMP，调用__objc_forward_handler
         TailCallFunctionPointer x17
         
         END_ENTRY __objc_msgForward
     
     __objc_forward_handler在C中是_objc_forward_handler
     
         // Default forward handler halts the process.报告unrecognized selector sent to instance错误
         __attribute__((noreturn, cold)) void
         objc_defaultForwardHandler(id self, SEL sel)
         {
             _objc_fatal("%c[%s %s]: unrecognized selector sent to instance %p "
                         "(no message forward handler is installed)",
                         class_isMetaClass(object_getClass(self)) ? '+' : '-',
                         object_getClassName(self), sel_getName(sel), self);
         }
        // _objc_forward_handler = objc_defaultForwardHandler
         void *_objc_forward_handler = (void*)objc_defaultForwardHandler;
     */
    ZBYRunTimePersionModel * persion = [ZBYRunTimePersionModel new];
    SEL run = @selector(run);
    ((void(*)(id, SEL))objc_msgSend)(persion, run);
}

// MARK: - 消息转发

- (void)aboutMsgForword {
    /**
     文章：https://www.jianshu.com/p/b838f04a9249
     一旦消息发送阶段没有找到方法的实现，那么就会执行动态解析阶段，也就是进入消息转发阶段
     
     消息转发分为三个阶段：
     1.查找该类内部是否有其它方法代替执行该阶段也叫做方法的动态解析：
        实例方法调用resolveInstanceMethod:，类方法调用resolveClassMethod:
     2.查找是否有其它类实例执行同名方法（返回值和参数必须相同）代替执行：调用forwardingTargetForSelector:
     3.查找是否有其它签名方法代替执行：
        3.1获取方法的签名methodSignatureForSelector:；
        3.2再次进行方法的动态解析
        3.3执行forwardInvocation:签名方法。
     4.以上三个都未找到代替执行的方法，调用报错方法doesNotRecognizeSelector:
        报错常见错误unrecognized selector sent to instance 0x100615cc0'
     */
    ZBYRunTimePersionModel * persion = [ZBYRunTimePersionModel new];
    [persion walk];
}

// MARK: - 关联对象

// OC定义存储key
static const NSString * kAssociatedObjectsOperationKey = @"kAssociatedObjectsOperationKey";

// C定义存储key
//const void * kAssociatedObjectChar = "kAssociatedObjectChar";

- (void)associatedObjectsOperation {
    /**
     https://www.swvq.com/boutique/detail/57688
     
     1.关联对象：
     对象关联（或称为关联引用）本来是Objective-C 2.0运行时的一个特性，起始于OS X Snow Leopard和iOS 4。相关参考可以查看 <objc/runtime.h> 中定义的以下三个允许你将任何键值在运行时关联到对象上的函数：

     objc_setAssociatedObject
     objc_getAssociatedObject
     objc_removeAssociatedObjects
     为什么我说这个很有用呢？因为这允许开发者对已经存在的类在扩展中添加自定义的属性，这几乎弥补了Objective-C最大的缺点。
     
     2.关联对象的行为：
     属性可以根据定义在枚举类型 objc_AssociationPolicy 上的行为被关联在对象上：
     Behavior                   @property Equivalent                    Description
     OBJC_ASSOCIATION_ASSIGN    @property (assign)                      指定一个关联对象的弱引用。
     OBJC_ASSOCIATION_RETAIN_NONATOMIC @property (nonatomic, strong)   指定一个关联对象的强引用，不能被原子化使用。
     OBJC_ASSOCIATION_COPY_NONATOMIC   @property (nonatomic, copy)    指定一个关联对象的copy引用，不能被原子化使用。
     OBJC_ASSOCIATION_RETAIN    @property (atomic, strong)    指定一个关联对象的强引用，能被原子化使用。
     OBJC_ASSOCIATION_COPY      @property (atomic, copy)    指定一个关联对象的copy引用，能被原子化使用。
     
     以 OBJC_ASSOCIATION_ASSIGN 类型关联在对象上的弱引用不代表0 retian的 weak 弱引用，行为上更像 unsafe_unretained 属性，所以当在你的视线中调用weak的关联对象时要相当小心。
     
     注意：根据WWDC 2011, Session 322 (第36分钟左右)发布的内存销毁时间表，被关联的对象在生命周期内要比对象本身释放的晚很多。它们会在被 NSObject -dealloc 调用的 object_dispose() 方法中释放。
     
     3.关联对象添加：
     objc_setAssociatedObject(object, key, value, policy) -> _object_set_associative_reference -> 关联对象管理者AssociationsManager -> 传入对象object通过DisguisedPtr伪装后的对象为key，在AssociationsHashMap(ExplicitInitDenseMap)二级表中查找关联对象对应的关联表ObjectAssociationMap -> 如果有更新ObjectAssociationMap表中的内容，如果没有创建一个新的ObjectAssociationMap表 -> 在ObjectAssociationMap表中通过传入的key为key，查找对应的ObjectAssociation -> ObjectAssociation存储关联对象的策略值和关联对象的值(即传入的value)
     
     详情见图：AssociatedObject.webp
     
     3.删除关联对象：
     你可以会在刚开始接触对象关联时想要尝试去调用 objc_removeAssociatedObjects() 来进行删除操作，但如文档中所述，你不应该自己手动调用这个函数：
        此函数的主要目的是在“初试状态”时方便地返回一个对象。你不应该用这个函数来删除对象的属性，因为可能会导致其他客户对其添加的属性也被移除了。规范的方法是：调用 objc_setAssociatedObject 方法并传入一个 nil 值来清除一个关联。
     */
    
    // 1.存储关联对象
    NSArray * associatedList = @[@1, @2];
    objc_setAssociatedObject(self, (__bridge const void * _Nonnull)(kAssociatedObjectsOperationKey), associatedList, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    // 2.获取关联的存储对象
    NSArray * assList = objc_getAssociatedObject(self, (__bridge const void * _Nonnull)(kAssociatedObjectsOperationKey));
    NSLog(@"%@", assList);
    // 3.删除关联对象
    objc_setAssociatedObject(self, (__bridge const void * _Nonnull)(kAssociatedObjectsOperationKey), nil, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    assList = objc_getAssociatedObject(self, (__bridge const void * _Nonnull)(kAssociatedObjectsOperationKey));
    NSLog(@"%@", assList);
    objc_removeAssociatedObjects(self);
    assList = objc_getAssociatedObject(self, (__bridge const void * _Nonnull)(kAssociatedObjectsOperationKey));
    NSLog(@"%@", assList);
    
    // 添加分类关联属性
    ZBYRunTimePersionModel * persion = [ZBYRunTimePersionModel new];
    persion.name = @"zby";
    persion.age = 32;
    persion.height = 172;
    persion.surnames = @"zhou";
    NSLog(@"名字=%@，身高=%f，姓氏=%@", persion.name, persion.height, persion.surnames);
    
    /**
     比较好的使用场景：
        1.添加私有属性用于更好地去实现细节：
        当扩展一个内建类的行为时，保持附加属性的状态可能非常必要。注意以下说的是一种非常_教科书式_的关联对象的用例：
        AFNetworking在 UIImageView 的category上用了关联对象来保持一个operation对象，用于从网络上某URL
        异步地获取一张图片。
        2.添加public属性来增强category的功能：
        有些情况下这种(通过关联对象)让category行为更灵活的做法比在用一个带变量的方法来实现更有意义。在这些情况下，
        可以用关联对象实现一个一个对外开放的属性。回到上个AFNetworking的例子中的 UIImageView category，它的 imageResponseSerializer方法允许图片通过一个滤镜来显示、或在缓存到硬盘之前改变图片的内容。
        3.创建一个用于KVO的关联观察者：
        当在一个category的实现中使用KVO时，建议用一个自定义的关联对象而不是该对象本身作观察者。
     
     比起其他解决问题的方法，关联对象应该被视为最后的选择（事实上category也不应该作为首选方法）。
     */
}

// MARK: - 方法交换

- (void)methodSwizzling {
    /**
     1.方法交换
     Method swizzling 用于改变一个已经存在的 selector 的实现。这项技术使得在运行时通过改变 selector 在类的消息分发列表中的映射从而改变方法的掉用成为可能。
     
     2.实现
     例如：我们想要在一款 iOS app 中追踪每一个视图控制器被用户呈现了几次：这可以通过在每个视图控制器的 viewDidAppear: 方法中添加追踪代码来实现，但这样会大量重复的样板代码。继承是另一种可行的方式，但是这要求所有被继承的视图控制器如 UIViewController, UITableViewController, UINavigationController 都在 viewDidAppear：实现追踪代码，这同样会造成很多重复代码。 幸运的是，这里有另外一种可行的方式：从 category 实现 method swizzling 。
     交换方法如：NSObject+Extension的swizzledOldMethod:newMethod:方法；
     实际交换方法如：UIViewController+Extension用zby_ViewWillAppear:交换viewWillAppear:
     
     现在，UIViewController 或其子类的实例对象在调用 viewWillAppear: 的时候会有 log 的输出。

     在视图控制器的生命周期，响应事件，绘制视图或者 Foundation 框架的网络栈等方法中插入代码都是 method swizzling 能够为开发带来很好作用的例子。有很多的场景选择method swizzling 会是很合适的解决方式，这显然也会让 Objective-C 开发者的技术变得越来越成熟。
     
     3.如何使用 method swizzling
     3.1swizzling 应该只在 +load 中完成：
        在 Objective-C 的运行时中，每个类有两个方法都会自动调用。+load 是在一个类被初始装载时调用，+initialize 是在应用第一次调用该类的类方法或实例方法前调用的。两个方法都是可选的，并且只有在方法被实现的情况下才会被调用。
     3.2swizzling 应该只在 dispatch_once 中完成：
        由于 swizzling 改变了全局的状态，所以我们需要确保每个预防措施在运行时都是可用的。原子操作就是这样一个用于确保代码只会被执行一次的预防措施，就算是在不同的线程中也能确保代码只执行一次。Grand Central Dispatch 的 dispatch_once 满足了所需要的需求，并且应该被当做使用 swizzling 的初始化单例方法的标准。
     
     4.Selectors、Methods和Implementations（IMP）的区别
     在 Objective-C 的运行时中，selectors, methods, implementations 指代了不同概念，然而我们通常会说在消息发送过程中，这三个概念是可以相互转换的。 下面是苹果 Objective-C Runtime Reference中的描述：
     4.1 Selector（typedef struct objc_selector *SEL）:
        在运行时 Selectors 用来代表一个方法的名字。Selector 是一个在运行时被注册（或映射）的C类型字符串。Selector由编译器产生并且在当类被加载进内存时由运行时自动进行名字和实现的映射。
     4.2 Method（typedef struct objc_method *Method）:
        方法是一个不透明的用来代表一个方法的定义的类型。
         struct objc_method {
            SEL name;
            const char *types;
            IMP imp;
            struct SortBySELAddress :
            public std::binary_function<const struct method_t::big&,
                                 const struct method_t::big&, bool>
            {
                bool operator() (const struct method_t::big& lhs,
                          const struct method_t::big& rhs)
                { return lhs.name < rhs.name; }
            };
         }
     4.3 Implementation（typedef id (*IMP)(id, SEL,...)）:
        这个数据类型指向一个方法的实现的最开始的地方。该方法为当前CPU架构使用标准的C方法调用来实现。该方法的第一个参数指向调用方法的自身（即内存中类的实例对象，若是调用类方法，该指针则是指向元类对象 metaclass ）。第二个参数是这个方法的名字 selector，该方法的真正参数紧随其后。
     
     理解 selector, method, implementation 这三个概念之间关系的最好方式是：在运行时，类（Class）维护了一个消息分发列表来解决消息的正确发送。每一个消息列表的入口是一个方法（Method），这个方法映射了一对键值对，其中键值是这个方法的名字 selector（SEL），值是指向这个方法实现的函数指针 implementation（IMP）。 Method swizzling 修改了类的消息分发列表使得已经存在的 selector 映射了另一个实现 implementation，同时重命名了原生方法的实现为一个新的 selector。
     
     5._cmd
     5.1什么是_cmd
        1._cmd在Objective-C的方法中表示当前方法的selector，正如同self表示当前方法调用的对象实例；
        2._cmd是在编译时候(compile-time)就已经确定的值，所以可以直接使用。
     5.2调用 _cmd
     下面代码在正常情况下会出现循环：
     - (void)zby_ViewWillAppear:(BOOL)animated {
         [self zby_ViewWillAppear:animated];
         NSLog(@"进入了 %@", self.class);
     }
     
     然而在交换了方法实现后就不会出现循环了。好的程序员应该对这里出现的方法的递归调用有所警觉，这里我们应该理清在 method swizzling 后方法的实现究竟变成了什么。在交换了方法的实现后，xxx_viewWillAppear:方法的实现已经被替换为了 UIViewController -viewWillAppear：的原生实现，所以这里并不是在递归调用。由于 xxx_viewWillAppear: 这个方法的实现已经被替换为了 viewWillAppear: 的实现，所以，当我们在这个方法中再调用 viewWillAppear: 时便会造成递归循环。

     注意：记住给需要转换的所有方法加个前缀以区别原生方法。
     
     6.很多人认为交换方法实现会带来无法预料的结果。然而采取了以下预防措施后, method swizzling 会变得很可靠：
     
     6.1在交换方法实现后记得要调用原生方法的实现（除非你非常确定可以不用调用原生方法的实现）：
        APIs 提供了输入输出的规则，而在输入输出中间的方法实现就是一个看不见的黑盒。交换了方法实现并且一些回调方法不会调用原生方法的实现这可能会造成底层实现的崩溃。
     6.2避免冲突：
        为分类的方法加前缀，一定要确保调用了原生方法的所有地方不会因为你交换了方法的实现而出现意想不到的结果。
     6.3理解实现原理：
        只是简单的拷贝粘贴交换方法实现的代码而不去理解实现原理不仅会让 App 很脆弱，并且浪费了学习 Objective-C 运行时的机会。阅读 Objective-C Runtime Reference 并且浏览 <obje/runtime.h> 能够让你更好理解实现原理。
     6.4持续的预防：
        不管你对你理解 swlzzling 框架，UIKit 或者其他内嵌框架有多自信，一定要记住所有东西在下一个发行版本都可能变得不再好使。做好准备，在使用这个黑魔法中走得更远，不要让程序反而出现不可思议的行为。
     */
    NSObject * o = [[NSObject alloc] init];
    [NSObject initialize];
    NSLog(@"%@", NSStringFromSelector(_cmd));
}

// MARK: - NSInvocation方法转发

- (void)invocationForwarding {
    /**
     NSInvocation方法转发和Aspect库和JRSwizzle库
     
     */
    [NSInvocation new];
    // hook 单个实例对象的实例方法
//    [self hookSelector:@selector(instanceMethod) position:AspectPositionBefore usingBlock:^{
//        NSLog(@"instance hook instance method");
//    }];
    
    // hook 类的实例对象的实例方法
    [ZBYRunTimeViewController hookSelector:@selector(instanceMethod) position:AspectPositionBefore usingBlock:^{
        NSLog(@"class hook instance method");
    }];
//    [self.class hookSelector:@selector(classMethod) position:AspectPositionBefore usingBlock:^(AspectObject *aspect, NSURLSessionConfiguration *configuration){
//        NSLog(@"class hook string");
//    }];
    
    // hook 类的类方法
    [ZBYRunTimeViewController hookSelector:@selector(classMethod) position:AspectPositionBefore usingBlock:^{
        NSLog(@"class hook class method")
    }];
    [self instanceMethod];
    [self.class classMethod];
}

- (void)instanceMethod {
    NSLog(@"%s", __func__);
}

+ (void)classMethod {
    NSLog(@"%s", __func__);
}

// MARK: - 调用方法
    
- (void)callMethod {
    /**
     对象调用方法的四种形式：
     1.[obj methodName];
     2.[obj performSelector......];
     3.NSInvocation
     4.objc_msgSend，runtime底层方法
     */
    
    ZBYRunTimePersionModel * person = [ZBYRunTimePersionModel new];
    /**
     1.最常用的方式，直接调用，缺点不能通过方法名字符串来执行方法
     */
    NSString * r1 = [person callTestWithStr1:@"a" str2:@"b" str3:@"c"];
    NSLog(@"%@", r1);
    
    /**
     2.通过NSObject继承的底层方法进行调用；无法进行2个以上参数的传递
     */
    
    // 2.1用方法调用
    NSString * r21 = [person performSelector:@selector(callTestWithStr1:str2:str3:) withObject:@"a" withObject:@"b"];
    NSLog(@"%@", r21);
    
    // 2.2用方法对应的字符串初始化为方法，再进行调用
    SEL selected = NSSelectorFromString(@"callTestWithStr1:str2:str3:");
    NSString * r22 = [person performSelector:selected withObject:@"a" withObject:@"b"];
    NSLog(@"%@", r22);
    
    /**
     3.NSInvocation调用方法
     3.1进行方法签名；NSMethodSignature的两个参数：numberOfArguments方法参数的个数；methodReturnLength方法返回值类型
        的长度，大于0表示有返回值
     3.2初始化NSInvocation并设置参数
        第一个参数下标为0是target，及响应者；
        第二个参数是selector，及需要调用的方法；
        第三个起是自定义参数，必须传递参数的地址，不能直接传值，例如：str1，str2，str3
     3.3获取返回值；可以在调用invoke前，也可以在invoke之后
     */
    NSMethodSignature * sign = [person methodSignatureForSelector:selected];
    
    // 或者
//    NSMethodSignature * sign = [[self class] instanceMethodSignatureForSelector:selected];
    NSInvocation * invocation = [NSInvocation invocationWithMethodSignature:sign];
    invocation.target = person;
    invocation.selector = selected;
    NSString *arg1 = @"a";
    NSString *arg2 = @"b";
    NSString *arg3 = @"c";
    [invocation setArgument:&arg1 atIndex:2];
    [invocation setArgument:&arg2 atIndex:3];
    [invocation setArgument:&arg3 atIndex:4];
    [invocation invoke];
    if (sign.methodReturnLength > 0) {
        NSString * result = nil;
        [invocation getReturnValue:&result];
        NSLog(@"%@", result);
    }
    else {
        NSLog(@"没有返回值")
    }
    
    // 4.第四种方法：底层调用，方法到底层都是通过objc_msgSend调用的，直接用objc_msgSend强制转换进行调用
    
    // 4.1强制转换和调用分开
    void *(*zby_runtime_msgObserverSend)(id, SEL, void *, void *, void *) = (void *)objc_msgSend;
    NSString * r = (__bridge NSString *)(zby_runtime_msgObserverSend(person, selected, @"a", @"b", @"c"));
    NSLog(@"%@", r);
    
    // 4.2强制转换和调用一起
    ((void(*)(id, SEL, void *, void *, void *))objc_msgSend)(person, selected, @"a", @"b", @"c");
//    NSString * r2 = (__bridge NSString *)((void(*)(id, SEL, void *, void *, void *))objc_msgSend)(person, selected, @"a", @"b", @"c");
//    NSLog(@"%@", r2);
}

// MARK: - OC语言的动态性

- (void)ObjectCDynamic {
    /**
     代码的三个阶段：
     1.预编译阶段：代码在编译之前做的工作；主要包括宏定义的替换(#define)，文件包含（import），条件编译（#if，#program等）
        https://www.jianshu.com/p/60a2d6178af7
     2.编译阶段：编译器对代码的编译阶段，编译时只是对语言进行最基本的检查报错，包括词法分析、语法分析等等，将程序代码翻译成计算机
        能够识别的语言（例如汇编等），编译通过并不意味着程序就可以成功运行。
     3.运行时：将编译好的代码，加载进内存中执行调用的过程的阶段；这个时候会具体对类型进行检查，而不仅仅是对代码的简单扫描分析，此时若出错程序会崩溃。
     相关文档：
     https://blog.csdn.net/cordova/article/details/53876682
     https://mp.weixin.qq.com/s/wxigL1Clem1dR8Nkt8LLMw
     */
    
    /**
     OC的动态性即是在运行时进行检查的具体体现；包含三个方面：
     1.动态类型(Dynamic typing)
     2.动态绑定(Dynamic binding)
     3.动态加载(Dynamic loading)
     */
    
    /**
     1.动态类型就是说运行时才确定对象的真正类型。例如我们可以向一个 id 类型的对象发送任何消息，这在编译期都是合法的，因为类型是可以动态确定的，消息真正起作用的时机也是在运行时这个对象的类型确定以后。我们甚至可以在运行时动态修改一个对象的 isa 指针从而修改其类型，OC 中 KVO 的实现正是对动态类型的典型应用。
     也就是说id修饰的对象为动态类型对象，其他在编译器指明类型的为静态类型对象，通常如果不需要涉及到多态的话还是要尽量使用静态类型（原因上面已经说到：错误可以在编译器提前查出，可读性好）。
     
     底层id声明：
     
     typedef struct objc_object *id;
     */
    
    /**
     示例：对于语句NSString* testObject = [[NSData alloc] init]; testObject在编译时和运行时分别是什么类型的对象？
     
     首先testObject是一个指向某个对象的指针，不论何时指针的空间大小是固定的。

     编译时： 指针的类型为NSString，即编译时会被当成一个NSString实例来处理，编译器在类型检查的时候如果发现类型不匹配则会给出黄色警告，该语句给指针赋值用的是一个NSData对象，则编译时编译器则会给出类型不匹配警告。但编译时如果testObject调用NSString的方法编译器会认为是正确的，既不会警告也不会报错。

     运行时： 运行时指针指向的实际是一个NSData对象，因此如果指针调用了NSString的方法，虽然编译时通过了，但运行时会崩溃，因为NSData对象没有该方法；另外，虽然运行时指针实际指向的是NSData，但编译时编译器并不知道（前面说了编译器会把指针当成NSString对象处理），因此如果试图用这个指针调用NSData的方法会直接编译不通过，给出红色报错，程序也运行不起来。
     */
    // 1.编译时编译器认为testObject是一个NSString对象，这里赋给它一个NSData对象编译器给出黄色类型错误警告，但运行时却是指向一个NSData对象
    NSString * testObject = [[NSData alloc] init];
    // 2.编译器认为testObject是NSString对象，所以允许其调用NSString的方法，这里编译通过无警告和错误；
//    [testObject stringByAppendingString:@"string"];
    // 3.但不允许其调用NSData的方法，下面这里编译不通过给出红色报错
//    [testObject base64EncodedDataWithOptions:NSDataBase64Encoding64CharacterLineLength];
    
    /**将上面第三句编译不通过的注释掉，然后在第二句打断点，编译后让程序跑起来到断点出会看到testObject指针的类型是_NSZeroData,指向一个NSData对象。继续运行程序会崩溃，因为NSData对象没有NSString的stringByAppendingString这个方法。
     */
    
    // 那么，假设testObject是id类型会怎样呢？
    // 1.id任意类型，编译器就不会把testObject在当成NSString对象了
    id testObject2 = [[NSData alloc] init];
    // 2.调用NSData的方法编译通过
    [testObject2 base64EncodedDataWithOptions:NSDataBase64Encoding64CharacterLineLength];
    // 3.调用NSString的方法编译也通过
    [testObject2 stringByAppendingString:@"string"];
    
    /**
     结果是编译完全通过，编译时编译器把testObject指针当成任意类型，运行时才确定testObject为NSData对象（断点看指针的类型和上面的例子中结果一样还是_NSZeroData，指向一个NSData对象），因此执行NSData的函数正常，但执行NSString的方法时还是崩溃了。通过这个例子也可以很清楚的知道id类型的作用了，将类型的确定延迟到了运行时，体现了OC语言的一种动态性：动态类型。
     */
    
    /**
     动态绑定指的是方法确定的动态性，具体指的是利用OC的消息传递机制将要执行的方法的确定推迟到运行时，可以动态添加方法。也就是说，一个OC对象是否调用某个方法不是由编译器决定的，而是由运行时决定的；另外关于动态绑定的关键一点是基于消息传递机制的消息转发机制，主要处理应对一些接受者无法处理的消息，此时有机会将消息转发给其他接收者处理，具体见下面介绍。

     动态绑定是基于动态类型的，在运行时对象的类型确定后，那么对象的属性和方法也就确定了(包括类中原来的属性和方法以及运行时动态新加入的属性和方法)，这也就是所谓的动态绑定了。动态绑定的核心就该是在运行时动态的为类添加属性和方法，以及方法的最后处理或转发，主要用到C语言语法，因为涉及到运行时，因此要引入运行时头文件：#include <objc/runtime.h>。

     消息传递机制：

     在OC中,方法的调用不再理解为对象调用其方法，而是要理解成对象接收消息，消息的发送采用‘动态绑定’机制，具体会调用哪个方法直到运行时才能确定，确定后才会去执行绑定的代码。方法的调用实际就是告诉对象要干什么，给对象(的指针)传送一个消息，对象为接收者（receiver），调用的方法及其参数即消息（message），给一个对象传消息表达为：[receiver message]; 接受者的类型可以通过动态类型识别于运行时确定。

     在消息传递机制中，当开发者编写[receiver message];语句发送消息后，编译器都会将其转换成对应的一条objc_msgSend C语言消息发送原语，具体格式为：
     void objc_msgSend (id self, SEL cmd, ...)

     这个原语函数参数可变，第一个参数填入消息的接受者，第二个参数是消息‘选择子’，后面跟着可选的消息的参数。有了这些参数，objc_msgSend就可以通过接受者的的isa指针，到其类对象中的方法列表中以选择子的名称为‘键’寻找对应的方法，找到则转到其实现代码执行，找不到则继续根据继承关系从父类中寻找，如果到了根类还是无法找到对应的方法，说明该接受者对象无法响应该消息，则会触发‘消息转发机制’，给开发者最后一次挽救程序崩溃的机会。
     */
    
    /**
     动态加载主要包括两个方面，一个是动态资源加载，一个是一些可执行代码模块的加载，这些资源在运行时根据需要动态的选择性的加入到程序中，是一种代码和资源的‘懒加载’模式，可以降低内存需求，提高整个程序的性能，另外也大大提高了可扩展性。

     例如：资源动态加载中的图片资源的屏幕适配，同一个图片对象可能需要准备几种不同分辨率的图片资源，程序会根据当前的机型动态选择加载对应分辨率的图片，像iphone4之前老机型使用的是@1x的原始图片，而retina显示屏出现之后每个像素点被分成了四个像素，因此同样尺寸的屏幕需要4倍分辨率（宽高各两倍）的@2x图片，最新的针对iphone6/6+以上的机型则需要@3x分辨率的图片。
     */
    
}

// MARK: - synthesize和Dynamic

- (void)synthesizeAndDynamic {
    /**
     1.@property声明属性时，会自动生成该属性的setter、getter方法还有一个对应的_ivar(带下划线的成员变量)；
     
     2.@property有两个对应的修饰符，一个是 @synthesize，一个是 @dynamic。如果 @synthesize 和 @dynamic都没写，那么默认的就是@syntheszie var = _var;

     3.@synthesize的语义是如果你没有手动实现 setter 方法和 getter 方法，那么编译器会自动为你加上这两个方法。

     4.@dynamic告诉编译器：属性的 setter 与 getter 方法由用户自己实现，不自动生成。（当然对于 readonly 的属性只需提供 getter 即可）。编译时没问题，运行时才执行相应的方法，这就是所谓的动态绑定。
     */
    
    /**
     synthesize合成实例变量的规则：
     1.1如果指定了成员变量的名称,会生成一个指定名称的成员变量
        例如：@synthesize firstName = _myFirstName;
     1.2如果这个成员变量已经存在， @property就不再生成新成员变量
        例如：
         // 添加一个成员变量  _firstName
         @interface TestObject(){
             NSString *_firstName;
         }
     1.3如果是 @synthesize firstName 还会生成一个名称为firstName的成员变量，
        也就是说：如果没有指定成员变量的名称会自动生成一个属性同名的成员变量
        例如：
         // 如果没有指定成员变量的名称会自动生成一个属性同名的成员变量
         @synthesize firstName;
     */
    
    /**
     在有了自动合成属性实例变量之后， @synthesize还有哪些使用场景？
     回答这个问题前，我们要搞清楚一个问题，什么情况下不会自动合成成员变量？

     1）同时重写了 setter 和 getter 时
     2）重写了只读属性的 getter 时
     3）使用了 @dynamic 时
     4）在 @protocol 中定义的所有属性
     5）在 category 中定义的所有属性
     6）重载的属性
     */
}

// MARK: - iOS界的毒瘤

- (void)methodSwizzlingProblem {
    /**
     文章链接：http://events.jianshu.io/p/19c5736c5d9a
     https://www.valiantcat.cn/index.php/2017/11/03/53.html
     http://sindrilin.com/2018/09/14/watch_on_swizzling.html
     
     MethodSwizzling泛滥下的隐患

     Github有一个�很健壮的库 RSSwizzle(这也是本文推荐Swizzling的最终方式) 指出了上面代码带来的风险点。

     1.只在 +load 中执行 swizzling 才是安全的。

     2.被 hook 的方法必须是当前类自身的方法，如果把继承来的 IMP copy 到自身上面会存在问题。父类的方法应该在调用的时候使用，而不是 swizzling 的时候 copy 到子类。

     3.被 Swizzled 的方法如果依赖与 cmd ，hook 之后 cmd 发送了变化，就会有问题(一般你 hook 的是系统类，也不知道系统用没用 cmd 这个参数)。

     4.命名如果冲突导致之前 hook 的失效 或者是循环调用。
     
     5.方法多次交换会出现错误，且该错误很难被定位，与2、3相关联。

     上述问题中第一条和第四条说的是通常的 MethodSwizzling 是在分类里面实现的, 而分类的 Method 是被Runtime 加载的时候追加到类的 MethodList ，如果不是在 +load 是执行的 Swizzling 一旦出现重名，那么 SEL 和 IMP 不匹配致 hook 的结果是循环调用。

     第三条是一个不容易被发现的问题。
     我们都知道 Objective-C Method 都会有两个隐含的参数 �self, cmd，有的时候开发者在使用关联属性的适合可能懒得声明 (void *) 的 key，直接使用 cmd 变量 objc_setAssociatedObject(self, _cmd, xx, 0); 这会导致对当前IMP对 cmd 的依赖。

     一旦此方法被 Swizzling，那么方法的 cmd 势必会发生变化，出现了 bug 之后想必你一定找不到，等你找到之后心里一定会问候那位 Swizzling 你的方法的开发者祖宗十八代安好的，再者如果你 Swizzling 的是系统的方法恰好系统的方法内部用到了 cmd ..._（此处后背惊起一阵冷汗）。
     */
//    ZBYRunTimePerson * person = [ZBYRunTimePerson new];
//    [person sayHello];    // 父类和子类多次交换sayHello，导致最后person无法调用sayHello方法
    ZBYRunTimeStudent * student = [ZBYRunTimeStudent new];
    [student sayHello];
}

// MARK: - hook方法

- (void)hookMethods {
    /**
     hook方法的三种方式和原理：
     Method Swizzle：
     主要用于OC方法，利用OC的Runtime特性，动态改变SEL（方法编号）和IMP（方法实现）的对应关系，达到OC方法调用流程改变的目的，这种技术在之前的11 - 代码注入使用过了。
     
     JRSwizzle库通过改原理实现方法的hook
     https://github.com/rentzsch/jrswizzle
     
     fishhook：Facebook提供的一个动态修改链接MachO文件的工具。利用MachO文件加载原理，通过修改懒加载和非懒加载两个表的指针，达到对C函数进行HOOK的目的
     Github地址：https://github.com/facebook/fishhook
     原理讲解：https://www.jianshu.com/p/43ef7ddd0081
     
     Aspect库：
     NSInvocation消息转发的原理实现
     Github地址：https://github.com/steipete/Aspects
     原理讲解：
     https://www.jianshu.com/p/43499b20df08
      NSInvocation方法转发和Aspect库和库
     */
}

@end
