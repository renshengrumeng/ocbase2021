//
//  ZBYRunTimePerson.m
//  OCBase2021
//
//  Created by anniekids good on 2022/2/24.
//

#import "ZBYRunTimePerson.h"

@implementation ZBYRunTimePerson

+ (BOOL)zby_swizzleMethod:(SEL)origSel withMethod:(SEL)altSel error:(NSError**)error {

    Method origMethod = class_getInstanceMethod(self, origSel);
    if (!origMethod) {
//        SetNSError(error_, @"original method %@ not found for class %@", NSStringFromSelector(origSel_), [self class]);
        return NO;
    }

    Method altMethod = class_getInstanceMethod(self, altSel);
    if (!altMethod) {
//        SetNSError(error_, @"alternate method %@ not found for class %@", NSStringFromSelector(altSel_), [self class]);
        return NO;
    }

    class_addMethod(self,
                    origSel,
                    class_getMethodImplementation(self, origSel),
                    method_getTypeEncoding(origMethod));

    class_addMethod(self,
                    altSel,
                    class_getMethodImplementation(self, altSel),
                    method_getTypeEncoding(altMethod));

    method_exchangeImplementations(class_getInstanceMethod(self, origSel), class_getInstanceMethod(self, altSel));
    return YES;
}

- (void)sayHello {
    NSLog(@"person say hello");
}

@end
