//
//  ZBYRunTimePersion+Swizzle.m
//  OCBase2021
//
//  Created by anniekids good on 2022/2/24.
//

#import "ZBYRunTimePersion+Swizzle.h"
#import "NSObject+Extension.h"

@implementation ZBYRunTimePerson (Swizzle)

+ (void)load {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        [self swizzledOldMethod:@selector(sayHello) newMethod:@selector(p_sayHello)];
//        [ZBYRunTimePerson zby_swizzleMethod:@selector(sayHello) withMethod:@selector(p_sayHello) error:nil];
    });
}

- (void)p_sayHello {
    [self p_sayHello];

    NSLog(@"Person + swizzle say hello");
}

@end
