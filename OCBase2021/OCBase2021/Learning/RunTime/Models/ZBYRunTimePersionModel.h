//
//  ZBYRunTimePersionModel.h
//  OCBase2021
//
//  Created by 周保勇 on 2021/8/5.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZBYRunTimePersionModel : NSObject

/** 名称 */
@property (nonatomic, copy) NSString * name;

/** 年龄 */
@property (nonatomic, assign) NSUInteger age;

// MARK: - synthesize和Dynamic

@property (nonatomic, copy) NSString * first;
@property (nonatomic, copy) NSString * second;
@property (nonatomic, copy, readonly) NSString * third;
@property (nonatomic, copy) NSString * forth;

// MARK: - 消息发送和消息转发

/// 跑步
- (void)run;

/// 散步
- (void)walk;

// MARK: - 消息调用的四种方法

- (NSString *)callTestWithStr1:(NSString *)str1 str2:(NSString *)str2 str3:(NSString *)str3;

@end

NS_ASSUME_NONNULL_END
