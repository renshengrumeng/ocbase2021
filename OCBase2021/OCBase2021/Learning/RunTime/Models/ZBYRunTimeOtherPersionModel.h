//
//  ZBYRunTimeOtherPersionModel.h
//  OCBase2021
//
//  Created by 周保勇 on 2021/8/24.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZBYRunTimeOtherPersionModel : NSObject

- (void)walk;

- (void)testOtherWalk;

@end

NS_ASSUME_NONNULL_END
