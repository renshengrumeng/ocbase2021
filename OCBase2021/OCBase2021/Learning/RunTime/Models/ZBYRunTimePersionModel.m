//
//  ZBYRunTimePersionModel.m
//  OCBase2021
//
//  Created by 周保勇 on 2021/8/5.
//

#import "ZBYRunTimePersionModel.h"
#import "ZBYRunTimeOtherPersionModel.h"

@interface ZBYRunTimePersionModel () {
    NSString * _second;
    NSString * _third;
    NSString * _forth;
}

@property (nonatomic, copy, readwrite) NSString * third;

@end

@implementation ZBYRunTimePersionModel

@synthesize first = _first;
@dynamic third;
@synthesize forth;  // 自动生成一个forth的成员变量

- (NSString *)callTestWithStr1:(NSString *)str1 str2:(NSString *)str2 str3:(NSString *)str3 {
    NSString * result = [NSString stringWithFormat:@"%@-%@-%@", str1, str2, str3];
    return result;
}

- (void)setFirst:(NSString *)first {
    _first = first;
}

- (NSString *)first {
    return _first;
}

- (void)setSecond:(NSString *)second {
    _second = second;
}

- (NSString *)second {
    return _second;
}

- (void)setThird:(NSString *)third {
    _third = third;
}

- (NSString *)third {
    return _third;
}

- (void)setForth:(NSString *)forth {
    _forth = forth;
}

- (NSString *)forth {
    return _forth;
}

// MARK: - 消息转发

- (void)run {
    NSLog(@"跑步");
}

- (void)testWalk {
    NSLog(@"散步的测试版")
}

void testOtherWalk(void) {
    NSLog(@"persion-test-other-walk");
}

//+ (BOOL)resolveInstanceMethod:(SEL)sel {
//    if (sel == @selector(walk)) {
//        // 获取要动态添加的方法
//        Method mothod = class_getInstanceMethod(self, @selector(testWalk));
//        // 获取添加方法的Imp也就是方法地址
//        IMP imp = method_getImplementation(mothod);
//        // 获取字符串编码 typeEconding
//        const char * types = method_getTypeEncoding(mothod);
//        // 添加方法
//        class_addMethod(self, sel, imp, types);
//        return YES;
//
//        /**
//         1. 这里动态添加方法是添加到了类或者是元类中的class_rw_t中的methods方法列表中，而没有存到方法的缓存中；
//         2. 动态解析阶段，允许用户调用其他类的方法，我们只需把消息接收者改变为对应类就行了，比如我们想要调用Student的study方法，我们只要把消息接收这改为Student方法改为study就可以了Method mothod = class_getInstanceMethod([DDStudent class], @selector(study));
//         3. 动态解析过后，程序把这个方法标记为已经动态解析，之后又会重新走第一个阶段(消息发送)
//         */
//    }
//    return [super resolveInstanceMethod:sel];
//}

//- (id)forwardingTargetForSelector:(SEL)aSelector {
//    /**
//     返回对象中必须有一个跟调用方法一样的方法，才能返回这个对象；否则程序会调用doesNotRecognizeSelector方法，终止程序运行，并且提示unrecognized selector sent to instance 0x100615cc0' 这个常见提示
//     */
//    ZBYRunTimeOtherPersionModel * oPersion = [ZBYRunTimeOtherPersionModel new];
//    return oPersion;
//}

- (NSMethodSignature *)methodSignatureForSelector:(SEL)aSelector {
    if (aSelector == @selector(walk)) {
        SEL selector = NSSelectorFromString(@"testOtherWalk");
        // 1.获取其它类实例的签名
        NSMethodSignature * signature = [ZBYRunTimeOtherPersionModel instanceMethodSignatureForSelector:selector];
        // 2.获取C函数的签名
//        NSMethodSignature * signature = [NSMethodSignature signatureWithObjCTypes:"v"];
        return signature;
    }
    return [super methodSignatureForSelector:aSelector];
}

- (void)forwardInvocation:(NSInvocation *)anInvocation {
    /**
     1.获取其它类实例的签名：需要重新绑定获取签名的方法和调用者，在进行invoke调用
     2.获取C函数的签名：该方法是将walk方法的IMP指针与签名的C函数进行绑定，不需要重新绑定方法，只需绑定调用者，进行
        invokeWithTarget调用
     Type Encodings：
     https://developer.apple.com/library/archive/documentation/Cocoa/Conceptual/ObjCRuntimeGuide/Articles/ocrtTypeEncodings.html#//apple_ref/doc/uid/TP40008048-CH100-SW1
     */
    if (anInvocation.selector == @selector(walk)) {
        ZBYRunTimeOtherPersionModel * oPersion = [ZBYRunTimeOtherPersionModel new];
        SEL selector = NSSelectorFromString(@"testOtherWalk");
        anInvocation.selector = selector;
        anInvocation.target = oPersion;
        [anInvocation invoke];
//        [anInvocation invokeWithTarget:self];
    }
}

@end
