//
//  ZBYRunTimePersionModel+AssociatedObject.h
//  OCBase2021
//
//  Created by 周保勇 on 2021/8/5.
//

#import "ZBYRunTimePersionModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZBYRunTimePersionModel (AssociatedObject)

/** 身高 */
@property (nonatomic, assign) CGFloat height;

/** 姓氏 */
@property (nonatomic, copy) NSString * surnames;

@end

NS_ASSUME_NONNULL_END
