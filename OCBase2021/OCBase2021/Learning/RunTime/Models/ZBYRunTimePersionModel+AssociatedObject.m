//
//  ZBYRunTimePersionModel+AssociatedObject.m
//  OCBase2021
//
//  Created by 周保勇 on 2021/8/5.
//

#import "ZBYRunTimePersionModel+AssociatedObject.h"
#import <objc/runtime.h>

const void * kHeight = "kHeight";
const void * kSurnames = "kSurnames";

@implementation ZBYRunTimePersionModel (AssociatedObject)

- (void)setHeight:(CGFloat)height {
    objc_setAssociatedObject(self, kHeight, [NSNumber numberWithFloat:height], OBJC_ASSOCIATION_ASSIGN);
}

- (CGFloat)height {
    NSNumber * height = objc_getAssociatedObject(self, kHeight);
    return height.floatValue;
}

- (void)setSurnames:(NSString *)surnames {
    objc_setAssociatedObject(self, kSurnames, surnames, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

- (NSString *)surnames {
    return objc_getAssociatedObject(self, kSurnames);
}

@end
