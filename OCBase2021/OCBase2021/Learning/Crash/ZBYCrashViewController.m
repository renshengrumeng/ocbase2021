//
//  ZBYCrashViewController.m
//  OCBase2021
//
//  Created by anniekids good on 2023/3/28.
//

#import "ZBYCrashViewController.h"

@interface ZBYCrashViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, copy) NSArray * models;

@end

@implementation ZBYCrashViewController

- (NSArray *)models {
    if (_models == nil) {
        _models = @[
        @{@"title" : @"应用中常见的崩溃", @"subTitle" : @"crash"},
        @{@"title" : @"空指针", @"subTitle" : @"NSNull"},
        @{@"title" : @"没有响应的方法", @"subTitle" : @"Unrecognized Selector Sent to Instance"},
        @{@"title" : @"键值观察者机制未及时释放", @"subTitle" : @"KVO"},
        @{@"title" : @"通知未及时释放", @"subTitle" : @"NSNotification"},
        @{@"title" : @"野指针和僵尸对象", @"subTitle" : @"Zombie Pointer"},
        @{@"title" : @"定时器未及时清除", @"subTitle" : @"NSTimer"},
        @{@"title" : @"三方库JJException", @"subTitle" : @"简介和防止crash的原理"}];
    }
    
    return _models;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITableView * tableView = [[UITableView alloc] initWithFrame:CGRectZero style:(UITableViewStylePlain)];
    tableView.delegate = self;
    tableView.dataSource = self;
    [self.view addSubview:tableView];
    
    tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];
}

// MARK: - UITableViewDelegate, UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.models.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString * identifier = @"123456";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.backgroundColor = [UIColor whiteColor];
        cell.textLabel.textColor = [UIColor blackColor];
        cell.detailTextLabel.textColor = [UIColor blackColor];
    }
    cell.textLabel.text = self.models[indexPath.row][@"title"];
    cell.detailTextLabel.text = self.models[indexPath.row][@"subTitle"];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

// MARK: - 三方库JJException

- (void)jjExceptionShow {
    // github地址：https://github.com/jezzmemo/JJException
    // 原理：https://github.com/jezzmemo/JJException/blob/master/JJExceptionPrinciple.md
}

@end
