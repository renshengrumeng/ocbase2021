//
//  ZBYNotificationViewController.m
//  OCBase2021
//
//  Created by anniekids good on 2023/9/15.
//

#import "ZBYNotificationViewController.h"
#import "ZBYNotificationPersionModel.h"

@interface ZBYNotificationViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, copy) NSArray * models;

@property (nonatomic, strong) ZBYNotificationPersionModel * persion1;

@property (nonatomic, strong) ZBYNotificationPersionModel * persion2;

@end

@implementation ZBYNotificationViewController

- (NSArray *)models {
    if (_models == nil) {
        _models = @[
        @{@"title" : @"基础和使用", @"subTitle" : @"Notification"},
        @{@"title" : @"通知的同步和异步", @"subTitle" : @"同步和异步处理"},
        @{@"title" : @"底层探索", @"subTitle" : @"Notification"},
        @{@"title" : @"自定义实现", @"subTitle" : @"Notification自定义实现"}];
    }
    return _models;
}

- (ZBYNotificationPersionModel *)persion1 {
    if(_persion1 == nil) {
        _persion1 = [[ZBYNotificationPersionModel alloc] init];
    }
    return _persion1;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITableView * tableView = [[UITableView alloc] initWithFrame:CGRectZero style:(UITableViewStylePlain)];
    tableView.delegate = self;
    tableView.dataSource = self;
    [self.view addSubview:tableView];
    
    tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];
    
    [self addNotifationObserver];
}

- (void)dealloc {
//    [[NSNotificationCenter defaultCenter] removeObserver:self];
    // selector通知在iOS9以后不用显式的移除
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"kCustomNotificationName" object:self.persion1];
    // block通知必须释放，不然造成强引用环
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"kCustomBlockNotificationName" object:nil];
}

// MARK: - UITableViewDelegate, UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.models.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString * identifier = @"123456";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.backgroundColor = [UIColor whiteColor];
        cell.textLabel.textColor = [UIColor blackColor];
        cell.detailTextLabel.textColor = [UIColor blackColor];
    }
    cell.textLabel.text = self.models[indexPath.row][@"title"];
    cell.detailTextLabel.text = self.models[indexPath.row][@"subTitle"];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSDictionary * info = self.models[indexPath.row];
    NSString * title = info[@"title"];
    if ([title containsString:@"基础和使用"]) {
        [self notificationBase];
    }
    if ([title containsString:@"通知的同步和异步"]) {
        [self notificationAsyc];
    }
    if ([title containsString:@"底层探索"]) {
        [self notificationRealize];
    }
    if ([title containsString:@"自定义实现"]) {
        [self notificationCustom];
    }
}

// MARK: - 通知基础

- (void)notificationBase {
    /**
     https://juejin.cn/post/6844904082516213768#heading-5
     https://www.lmlphp.com/user/17325/article/item/537013/
     
     OC中通知是通过通知中心(NSNotificationCenter)对通知进行统一管理和调度。
     通知分为两类：selector通知、block通知
     
     通知使用：
     1. 在需要监听事件变化的实例(该实例也叫做观察者或接收者)中添加通知；
     
     selector通知添加的具体方法：
     addObserver:selector:name: object:
     向通知中心注册观察者，观察者接收到通知后执行任务的代码(selector)在 发送通知的线程 中执行
     参数说明：
     observer 观察者（不能为nil，通知中心会弱引用，ARC是iOS9之前是unsafe_unretained，iOS9及以后是weak，MRC是assign，所以这也是MRC不移除会crash，ARC不移除不会crash的原因，建议都要严格移除。）
     selector 收到消息后要执行的方法
     name 消息通知的名字（如果name设置为nil，则表示接收所有消息）
     object 消息发送者（表示接收某个特定发送者的通知，如果为nil，接收所有发送者的通知）
     
     注意：
     每次调用addObserver时(两类通知都包含)，都会在通知中心重新注册一次，即使是同一对象监听同一个消息，而不是去覆盖原来的监听。这样，当通知中心转发某一消息时，如果同一对象多次注册了这个通知的观察者，则会收到多个通知。
     
     block通知添加的具体方法：
     addObserverForName:object:queue:usingBlock:
     向通知中心注册观察者，观察者接受到通知后执行任务的代码在 指定的操作队列 中执行
     参数说明：
     queue 如果queue为nil，则消息是默认在post线程中同一线程同步处理；但如果我们指定了操作队列，就在指定的队列里面执行。
     block 块会被通知中心拷贝一份(执行copy操作)，需要注意的就是避免引起循环引用的问题,block里面不能使用self，需要使用weakSelf。
     返回值，observer观察者的对象，最后需要[[NSNotificationCenter defaultCenter] removeObserver:observer];
     
     注意：
     1.1 block通知block块会被通知中心拷贝一份(执行copy操作)，需要注意的就是避免引起循环引用的问题，block里面不能使用self，需要使用weakSelf。
     1.2 block通知一定要记得[[NSNotificationCenter defaultCenter] removeObserver:observer];
     1.3 block通知如果queue为nil，则消息是默认在post线程中同一线程同步处理；但如果指定了操作队列，就在指定的队列里面执行。
     
     2. 在事件变化的实例(该实例也叫做被观察者或发送者)中发送该通知；
     
     具体的实现方法：
     postNotificationName:object:userInfo:
     参数说明：
     name 消息通知的名字（如果name设置为nil，则表示接收所有消息）
     object 消息发送者
     userInfo 传递的数据
     
     3. 观察者接受到通知执行回调函数；
     观察者接收到通知后执行任务的代码(selector)是在 发送通知的线程 中执行；block通知还可以指定在某个队列中执行通知后的任务(block块)，不指定队列其执行的线程与selector通知相同。
     
     4. 在合适的时机移除观察者。
     block通知必须在合适的时机进行移除，不然容易造成强引用环；
     selector通知iOS9后可以不显式的移除，ARC会自动移除。
     */
    self.persion1.name = @"zhou";
    
    if (self.persion2 == nil) {
        ZBYNotificationPersionModel * persion2 = [ZBYNotificationPersionModel new];
        self.persion2 = persion2;
    }
    self.persion2.name = @"zhou_2";
    
    /**
     KVO和通知的区别与联系：
     
     */
}

- (void)addNotifationObserver {
    
    NSNotificationCenter * center = [NSNotificationCenter defaultCenter];
    
    // object指定persion1，只接受persion1实例发送的通知
    [center addObserver:self selector:@selector(sendNotifation:) name:@"kCustomNotificationName" object:self.persion1];
    
    //__block __weak id<NSObject> observer 可以在block里面移除
    // 只监听一次的使用（消息执行完，在block里面移除observer）
    // 或者在dealloc中移除
    // object为nil，persion1和persion2实例发送的通知都接受
    __block __weak id<NSObject> observer = [center addObserverForName:@"kCustomBlockNotificationName" object:nil queue:nil usingBlock:^(NSNotification * _Nonnull note) {
        NSLog(@"block-\n%@-%@-%@", [NSThread currentThread], note.object, note.userInfo);
//        [[NSNotificationCenter defaultCenter] removeObserver:observer name:@"kCustomBlockNotificationName" object:nil];
    }];
    
    // 发送通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleNotification:) name:@"TestNotification" object:@1];
    // 接收通知
    [NSNotificationCenter.defaultCenter postNotificationName:@"TestNotification" object:nil];
}

- (void)sendNotifation:(NSNotification *)notification {
    NSLog(@"selector-\n%@-%@-%@", [NSThread currentThread], notification.object, notification.userInfo);
}

- (void)handleNotification:(NSNotification *)notification {
    NSLog(@"selector-\n%@-%@-%@", [NSThread currentThread], notification.object, notification.userInfo);
}

// MARK: - 通知的同步和异步处理

- (void)notificationAsyc {
    /**
     通知的同步和异步处理：
     1. 将发送通知时和通知后观察者执行代码(selector和block)加上线程打印信息：
     通过打印信息可知，在没有进行开启线程时，发送通知和通知后观察者执行的代码都是在主线程顺序进行的。
     2. 在发送通知时，进行异步处理，例如ZBYNotificationPersionModel的串行异步处理：
     通过打印信息可知，在开启多个线程时，发送通知和通知后观察者执行的代码是在同一个线程中顺序执行的。
     
     结论：
     1. 通知后观察者执行的代码是在发送通知的的线程中执行的；
     2. 发送通知和通知后观察者执行的代码是同步顺序执行。
     
     所以关于界面的操作用到通知就需要注意：通知发送是什么线程？执行监听方法是不是耗时的任务？
     
     监听方法里面开线程或者异步执行：见多线程详解中的线程间的通信
     */
    // 主线程
    [self notificationBase];
    // 异步线程
    self.persion1.age = 18;
    self.persion2.age = 36;
}

// MARK: - 通知底层

- (void)notificationRealize {
    /**
     GNUStep：https://wwwmain.gnustep.org/resources/downloads.php
     GNUStep github地址：https://github.com/gnustep/libs-base
     
     苹果没有对相关源码开放，所以以GNUStep源码为基础进行研究，GNUStep虽然不是苹果官方的源码，但很具有参考意义，根据实现原理来猜测和实践，更重要的还可以学习观察者模式的架构设计。
     
     关键类结构：
     
     NSNotification：
     用于描述通知的类，一个NSNotification对象就包含了一条通知的信息，所以当创建一个通知时通常包含如下属性：
     @interface NSNotification : NSObject <NSCopying, NSCoding>

     @property (readonly, copy) NSNotificationName name;    // 通知名称
     @property (nullable, readonly, retain) id object;      // 携带的对象
     @property (nullable, readonly, copy) NSDictionary *userInfo;   // 配置信息，传递的数据
     
     ...
     @end
     
     NSNotificationCenter：
     这是个单例类，负责管理通知的创建、发送和移除，属于最核心的类了。
     
     关键接口：
     // 添加selector通知
     - (void)addObserver:(id)observer selector:(SEL)aSelector name:(nullable NSNotificationName)aName object:(nullable id)anObject;

     // 发送通知
     - (void)postNotificationName:(NSNotificationName)aName object:(nullable id)anObject userInfo:(nullable NSDictionary *)aUserInfo;

     // 移除通知
     - (void)removeObserver:(id)observer name:(nullable NSNotificationName)aName object:(nullable id)anObject;

     // 添加block通知
     - (id <NSObject>)addObserverForName:(nullable NSNotificationName)name object:(nullable id)obj queue:(nullable NSOperationQueue *)queue usingBlock:(void (NS_SWIFT_SENDABLE ^)(NSNotification *note))block API_AVAILABLE(macos(10.6), ios(4.0), watchos(2.0), tvos(9.0));
     
     NSNotificationQueue：
     通知队列，用于异步发送消息，这个异步并不是开启线程，而是把通知存到双向链表实现的队列里面，等待某个时机触发时调用NSNotificationCenter的发送接口进行发送通知，这么看NSNotificationQueue最终还是调用NSNotificationCenter进行消息的分发
     另外NSNotificationQueue是依赖runloop的，所以如果线程的runloop未开启则无效，至于为什么依赖runloop下面会解释
     
     NSNotificationQueue主要做了两件事：添加通知到队列、删除通知
     // 把通知添加到队列中
     - (void)enqueueNotification:(NSNotification *)notification postingStyle:(NSPostingStyle)postingStyle coalesceMask:(NSNotificationCoalescing)coalesceMask forModes:(nullable NSArray<NSRunLoopMode> *)modes;

     // 删除通知，把满足条件的通知从队列中删除
     - (void)dequeueNotificationsMatching:(NSNotification *)notification coalesceMask:(NSUInteger)coalesceMask;
     
     队列的合并策略和发送时机
     
     把通知添加到队列等待发送，同时提供了一些附加条件供开发者选择，如：什么时候发送通知、如何合并通知等，系统给了如下定义
     
     // 表示通知的发送时机
     typedef NS_ENUM(NSUInteger, NSPostingStyle) {
         NSPostWhenIdle = 1, // runloop空闲时发送通知
         NSPostASAP = 2, // 尽快发送，这种情况稍微复杂，这种时机是穿插在每次事件完成期间来做的
         NSPostNow = 3 // 立刻发送或者合并通知完成之后发送
     };
     // 通知合并的策略，有些时候同名通知只想存在一个，这时候就可以用到它了
     typedef NS_OPTIONS(NSUInteger, NSNotificationCoalescing) {
         NSNotificationNoCoalescing = 0, // 默认不合并
         NSNotificationCoalescingOnName = 1, // 只要name相同，就认为是相同通知
         NSNotificationCoalescingOnSender = 2  // object相同
     };
     
     GSNotificationObserver
     
     这个类是GNUStep源码中定义的，它的作用是代理观察者，主要用来实现接口：addObserverForName：object: queue: usingBlock:时用到，即要实现在指定队列回调block，那么GSNotificationObserver对象保存了queue和block信息，并且作为观察者注册到通知中心，等到接收通知时触发了响应方法，并在响应方法中把block抛到指定queue中执行，定义如下：
     
     @implementation GSNotificationObserver
     {
         NSOperationQueue *_queue; // 保存传入的队列
         GSNotificationBlock _block; // 保存传入的block
     }
     - (id) initWithQueue: (NSOperationQueue *)queue
                    block: (GSNotificationBlock)block
     {
     ......初始化操作
     }

     - (void) dealloc
     {
     ....
     }
     // 响应接收通知的方法，并在指定队列中执行block
     - (void) didReceiveNotification: (NSNotification *)notif
     {
         if (_queue != nil)
         {
             GSNotificationBlockOperation *op = [[GSNotificationBlockOperation alloc]
                 initWithNotification: notif block: _block];

             [_queue addOperation: op];
         }
         else
         {
             CALL_BLOCK(_block, notif);
         }
     }

     @end
     
     存储容器
     上面介绍了一些类的功能，但是要想实现通知中心的逻辑必须设计一套合理的存储结构，对于通知的存储基本上围绕下面几个结构体来做（大致了解下，后面章节会用到），后面会详细介绍具体逻辑的
     
     // 根容器，NSNotificationCenter持有
     typedef struct NCTbl {
       Observation        *wildcard;    // 链表结构，保存既没有name也没有object的通知
       GSIMapTable        nameless;    // 存储没有name但是有object的通知
       GSIMapTable        named;        // 存储带有name的通知，不管有没有object
         ...
     } NCTable;

     // Observation 存储观察者和响应结构体，基本的存储单元
     typedef    struct    Obs {
       id        observer;    // 观察者，接收通知的对象
       SEL        selector;    // 响应方法
       struct Obs    *next;        // Next item in linked list.
       ...
     } Observation;
     */
    
    /**
     逻辑说明
     从上面介绍的存储容器中我们了解到NCTable结构体中核心的三个变量以及功能：wildcard、named、nameless，在源码中直接用宏定义表示了：WILDCARD、NAMELESS、NAMED，下面逻辑会用到
     建议如果看文字说明觉得复杂不好理解，就看看下节介绍的存储关系图
     
     1. 存在name（无论object是否存在）
     
     
     1.1 注册通知，如果通知的name存在，则以name为key从named字典中取出值n(这个n其实被MapNode包装了一层，便于理解这里直接认为没有包装)，这个n还是个字典，各种判空新建逻辑不讨论
     1.2 然后以object为key，从字典n中取出对应的值，这个值就是Observation类型(后面简称obs)的链表，然后把刚开始创建的obs对象o存储进去
     
     */
    
}

// MARK: - 自定义实现

- (void)notificationCustom {
    
}

@end
