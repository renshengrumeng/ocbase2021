//
//  ZBYNotificationPersionModel.m
//  OCBase2021
//
//  Created by anniekids good on 2023/9/22.
//

#import "ZBYNotificationPersionModel.h"

@interface ZBYNotificationPersionModel ()

@end

@implementation ZBYNotificationPersionModel

- (void)setName:(NSString *)name {
    _name = [name copy];
    
    // 主线程发送通知
    NSLog(@"%@", [NSThread currentThread]);
    [[NSNotificationCenter defaultCenter] postNotificationName:@"kCustomNotificationName" object:self userInfo:@{@"params" : name}];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"kCustomBlockNotificationName" object:self userInfo:@{@"params" : name}];
}

- (void)setAge:(NSInteger)age {
    _age = age;
    
    // 其它线程发送通知
    dispatch_queue_t sQueue = dispatch_queue_create("notification-queue", NULL);
    dispatch_async(sQueue, ^{
        NSLog(@"%@", [NSThread currentThread]);
        [[NSNotificationCenter defaultCenter] postNotificationName:@"kCustomNotificationName" object:self userInfo:@{@"params" : @(age)}];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"kCustomBlockNotificationName" object:self userInfo:@{@"params" : @(age)}];
    });
}

- (void)dealloc {
    NSLog(@"%s-%@", __FUNCTION__, self)
}

@end
