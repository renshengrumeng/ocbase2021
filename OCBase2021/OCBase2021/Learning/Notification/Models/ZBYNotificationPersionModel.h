//
//  ZBYNotificationPersionModel.h
//  OCBase2021
//
//  Created by anniekids good on 2023/9/22.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZBYNotificationPersionModel : NSObject

/** 名称 */
@property (nonatomic, copy) NSString * name;

/** 年龄 */
@property (nonatomic, assign) NSInteger age;

@end

NS_ASSUME_NONNULL_END
