//
//  ZBYGCDViewController.m
//  OCBase2021
//
//  Created by 周保勇 on 2021/4/8.
//

#import "ZBYGCDViewController.h"
#import "ZBYGCDSourceViewController.h"

@interface ZBYGCDViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, copy) NSArray * models;

@end

@implementation ZBYGCDViewController

- (NSArray *)models {
    if (_models == nil) {
        _models = @[
        @{@"title" : @"异步函数", @"subTitle" : @"不会阻塞当前线程"},
        @{@"title" : @"同步函数", @"subTitle" : @"阻塞当前线程"},
        @{@"title" : @"队列组", @"subTitle" : @"队列组"},
        @{@"title" : @"异步串行执行", @"subTitle" : @"适合顺序依赖的操作"},
        @{@"title" : @"异步并发执行", @"subTitle" : @"适合相互之间没有依赖的操作"},
        @{@"title" : @"队列组异步并行", @"subTitle" : @"一个操作依赖组中其它的操作"},
        @{@"title" : @"串行队列中的死锁", @"subTitle" : @"串行队列和同步函数造成的死锁"},
        @{@"title" : @"多线程访问数据", @"subTitle" : @"数据安全问题"},
        @{@"title" : @"信号量下的多线程", @"subTitle" : @"数据安全问题"},
        @{@"title" : @"互斥锁下的多线程", @"subTitle" : @"数据安全问题"},
        @{@"title" : @"异步栅栏函数", @"subTitle" : @"阻塞当前队列，不阻塞当前线程"},
        @{@"title" : @"同步栅栏函数", @"subTitle" : @"即阻塞当前队列，又阻塞当前线程"},
        @{@"title" : @"异步栅栏函数多线程访问数据", @"subTitle" : @"异步栅栏函数的数据安全访问，同加锁效果"},
        @{@"title" : @"GCD的Source", @"subTitle" : @"Soure增量和定时器"},
        @{@"title" : @"GCD的Cancel", @"subTitle" : @"取消Block和其中的问题"},
        @{@"title" : @"主队列和主线程", @"subTitle" : @""}
        ];
    }
    return _models;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITableView * tableView = [[UITableView alloc] initWithFrame:CGRectZero style:(UITableViewStylePlain)];
    tableView.delegate = self;
    tableView.dataSource = self;
    [self.view addSubview:tableView];
    
    tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];
}

// MARK: - UITableViewDelegate, UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.models.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString * identifier = @"123456";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:identifier];
        cell.backgroundColor = [UIColor whiteColor];
        cell.textLabel.textColor = [UIColor blackColor];
        cell.detailTextLabel.textColor = [UIColor blackColor];
    }
    cell.textLabel.text = self.models[indexPath.row][@"title"];
    cell.detailTextLabel.text = self.models[indexPath.row][@"subTitle"];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.row) {
        case 0:
            [self asyncDemo];
            break;
        case 1:
            [self syncDemo];
            break;
        case 2:
            break;
        case 3:
            [self asyncSerial];
            break;
        case 4:
            [self asyncConcurrent];
            break;
        case 5:
            [self groupAsyncConcurrentComplete];
            break;
        case 6:
            [self serialLock];
            break;
        case 7:
            [self multithreadingAsyncData];
            break;
        case 8:
            [self semaphoreMultithreadingData];
            break;
        case 9:
            [self lockMultithreadingData];
            break;
        case 10:
            [self barrierAsync];
            break;
        case 11:
            [self barrierSync];
            break;
        case 12:
            [self barrierAsyncData];
            break;
        case 13: {
            ZBYGCDSourceViewController * sourceVC = [ZBYGCDSourceViewController new];
            [self.navigationController pushViewController:sourceVC animated:YES];
            break;
        }
        case 14: {
            [self cancelBlock];
            break;
        }
        case 15: {
            [self mainQueueAndMainThread];
            break;
        }
        default:
            break;
    }
}

// MARK: - GCD

- (void)GCDBase {
    /*
     GCD简介：
     全称Grand Central Dispatch，大中枢派发系统；
     是纯C语言编写的一系列用于异步多线程执行任务的技术之一；
     是线程管理代码在系统级中的实现。
     GCD的优势：
     GCD是苹果公司为多核并行运算提出的解决方案；因此CGD会自动利用更多的CPU内核（比如双核、四核）；
     GCD会自动管理线程的生命周期（线程的创建、调度任务、销毁等）；因此我们不需要编写任何线程管理的代码，只需向GCD中添加任务即可。
     
     GCD对比NSOperation：
     1.性能：GCD更接近底层，而NSOperationQueue则更高级抽象，所以GCD在追求性能的底层操作来说，是速度最快的。
     2.从异步操作之间的事务性，顺序行，依赖关系。GCD需要自己写更多的代码来实现，而NSOperationQueue已经内
     建了这些支持。
     3.如果异步操作的过程需要更多的被交互和UI呈现出来，NSOperationQueue会是一个更好的选择。底层代码中，任务
     之间不太互相依赖，而需要更高的并发能力，GCD则更有优势
     */
    
    /*
     GCD中的队列：
     串行队列：Serial Dispatch Queue；等待现有的操作处理结束，再进行下一操作，遵循先进先出原则
     并发队列：Concurrent Dispatch Queue；可同时进行多个操作，根据操作的具体耗时，进行出队，不遵循先进先出原则
     */
    // 自定义串行队列
//    dispatch_queue_t sQueue = dispatch_queue_create("", NULL); // DISPATCH_QUEUE_SERIAL等价于NULL
    // 主队列：在主线程中执行的串行队列
//    dispatch_queue_t mainQueue = dispatch_get_main_queue();
    // 自定义并发队列
//    dispatch_queue_t cQueue = dispatch_queue_create("", DISPATCH_QUEUE_CONCURRENT);
    // 全局并发队列：所有应用程序都能够使用的系统级并发队列，有四个优先级
//    dispatch_queue_t globalQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    /*
     GCD的同步和异步函数：
     dispatch_sync：同步函数，没有开启线程的能力，阻塞当前线程，直到该函数添加的操作完成后，才能进行后续操作；
     dispatch_async：异步函数，有开启线程的能力，不会阻塞当前线程，该函数添加的操作在另一个线程中进行，不影响当前线程的操作；
     */
    
    /*
     阻塞线程的一些操作：
     死锁：操作之间的相互等待
     大次循环操作
     网络请求
     大数据处理等
     后三种都是比较耗时的操作
     */
    
    /*
     GCD队列组：
     概念：作为单个单元监视的一组任务，该组任务可以在不同的队列中；
     
     特性：
     */
    // 创建队列组
    dispatch_group_t group = dispatch_group_create();
    // 进组：在添加队列组操作是调用
    dispatch_group_enter(group);
    // 组添加异步操作
    dispatch_group_async(group, dispatch_get_global_queue(0, 0), ^{
        // 出组：在添加到队列组操作完成时调用，与进组成对出现
        dispatch_group_leave(group);
    });
    // 组中操作完成时调用
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        
    });
    
    /*
     信号量：Semaphore，是在多线程环境下使用的一种设施，是可以用来保证两个或多个关键代码段不被并发调用。
     
     特性：信号量是一个非负整数（车位数），所有通过它的线程/进程（车辆）都会将该整数减一（通过它当然是为了使用资源），
     当该整数值为零时，所有试图通过它的线程都将处于等待状态。在信号量上我们定义两种操作： Wait（等待） 和 Release
     （释放）。当一个线程调用Wait操作时，它要么得到资源然后将信号量减一，要么一直等下去（指放入阻塞队列），直到信号
     量大于等于1时。Release（释放）实际上是在信号量上执行加操作，对应于车辆离开停车场，该操作之所以叫做“释放”是因
     为释放了由信号量守护的资源。
    
     描述：以一个停车场的运作为例。简单起见，假设停车场只有三个车位，一开始三个车位都是空的。这时如果同时来了五辆车，
     看门人允许其中三辆直接进入，然后放下车拦，剩下的车则必须在入口等待，此后来的车也都不得不在入口处等待。这时，有一
     辆车离开停车场，看门人得知后，打开车拦，放入外面的一辆进去，如果又离开两辆，则又可以放入两辆，如此往复。在这个停
     车场系统中，车位是公共资源，每辆车好比一个线程，看门人起的就是信号量的作用。
     
     使用场景：
     1.加锁(互斥)：线程安全访问数据
     2.异步返回
     3.控制线程并发数：信号量value的值表示最大并发线程数
     */
    // 这value是初始化多少个信号量
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(1);
    // 这个方法是P操作对信号量减一，dsema这个参数表示对哪个信号量进行减一，如果该信号量为0则等待，timeout这个参数
    // 则是传入等待的时长。
    dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
    // 这个方法是V操作对信号量加一，dsema这个参数表示对哪个信号量进行加一
    dispatch_semaphore_signal(semaphore);
    
    /*
     栅栏函数：
     dispatch_barrier_sync：阻塞当前队列和当前线程，直到该函数添加的操作完成后，才能继续后续的操作；
     dispatch_barrier_async：只阻塞当前队列，在该函数前加入队列的操作执行完毕后，再次执行该函数添加的操作，最后执行该函数后加入队列的操作；不阻塞当前线程，可以进行该函数后，不加入队列的操作，例如：打印等
     
     栅栏函数只能和自定义的并发队列进行使用，不与全局并发队列使用；
     异步栅栏函数只对和它同一个队列的操作起作用；例如：AFNetworking请求时，在内部自己创建了队列
     异步栅栏函数与线程锁有相似的效果；
     特性：
     1.起到同步的效果
     2.起到安全访问数据效果
     3.栅栏函数本质是阻塞队列
     4.栅栏函数不能与全局队列使用，用自定义的并发队列；栅栏函数只能阻塞同一个队列中的操作
     */
    dispatch_queue_t queue = dispatch_queue_create("barrierQueue", DISPATCH_QUEUE_CONCURRENT);
    dispatch_barrier_sync(queue, ^{
        
    });
    dispatch_barrier_async(queue, ^{
        
    });
    
    /*
     GCD中的Source:
     概念：协调处理特定低级系统事件(如文件系统事件、计时器和UNIX信号)的对象；它是BSD系内核惯有功能kqueue的包装。
     kqueue是在XNU内核中发生各种事件时，在应用程序方执行处理的技术。其CPU负荷非常小，尽量不占用资源。kqueue可以说是应用程序处理XNU内核中发生的各种事件的方法中最优秀的一种。
     
     应用场景：
     1.绑定特定事件处理；
     2.定时器：与NSTimer相比，时间更精确
     */
    // 创建Source
    dispatch_source_t source = dispatch_source_create(DISPATCH_SOURCE_TYPE_DATA_ADD, 0, 0, dispatch_get_main_queue());
    // 设置回调代码块
    dispatch_source_set_event_handler(source, ^{
        NSInteger value = dispatch_source_get_data(source);
        NSLog(@"%ld", value);
    });
    // 取消Source
    dispatch_source_cancel(source);
    // 指定Source取消时的处理
    dispatch_source_set_cancel_handler(source, ^{
        
    });
    // 开启
    dispatch_resume(source);
    // 暂停
    dispatch_suspend(source);
    dispatch_suspend(dispatch_get_main_queue());
    
    /*
     dispatch_suspend：挂起函数；比如挂起queue（队列）、Source（系统级事件处理机制）等
     dispatch_resume：恢复函数；比如恢复queue（队列）、Source（系统级事件处理机制）等
     dispatch_once：保证应用程序只执行一次制定的操作；
     */
//    dispatch_suspend(<#dispatch_object_t  _Nonnull object#>);
//    dispatch_resume(<#dispatch_object_t  _Nonnull object#>);
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
    });
}

/// GCD异步函数：有开启线程的能力，不会阻塞当前线程，该函数添加的操作在另一个线程中进行，不影响当前线程的操作；
- (void)asyncDemo {
    dispatch_queue_t cQueue = dispatch_queue_create("asyncDemo", DISPATCH_QUEUE_CONCURRENT);
    NSLog(@"1");
    dispatch_async(cQueue, ^{   // 异步线程处理，和添加操作，需要耗时，由于该函数不会阻塞线程，所以会先执行NSLog(@"5");
        NSLog(@"2");
        dispatch_async(cQueue, ^{   // 异步线程处理，和添加操作，需要耗时，由于该函数不会阻塞线程，所以会先执行NSLog(@"4");
            NSLog(@"3");
        });
        NSLog(@"4");
    });
    NSLog(@"5");
    
    // 综上：打印顺序为1、5、2、4、3
}

/// GCD同步函数：没有开启线程的能力，阻塞当前线程，直到该函数添加的操作完成后，才能进行后续操作
- (void)syncDemo {
    dispatch_queue_t cQueue = dispatch_queue_create("asyncDemo", DISPATCH_QUEUE_CONCURRENT);
    dispatch_async(cQueue, ^{   // 开启线程，异步执行
        NSLog(@"1");
    });
    dispatch_async(cQueue, ^{   // 开启线程，异步执行
        NSLog(@"2");
    });
    
    // 该函数阻塞当前线程，直到该函数添加的操作完成后，才能进行后续操作NSLog(@"0");
    dispatch_sync(cQueue, ^{
        NSLog(@"3");
    });
    // 在该线程中NSLog(@"3");执行完毕后，会执行NSLog(@"0");；因为NSLog(@"1");和NSLog(@"2");都是其它线程异步执行，所以这两个操作的的顺序不一定
    NSLog(@"0");
    
    dispatch_async(cQueue, ^{   // NSLog(@"0");执行完毕后，异步线程处理和添加操作，需要耗时
        NSLog(@"7");
    });
    dispatch_async(cQueue, ^{
        NSLog(@"8");
    });
    dispatch_async(cQueue, ^{
        NSLog(@"9");
    });
    
    // 综上：3在0前打印，而0会在7、8、9前打印；1、2的顺序不确定；7、8、9只会在0后，其它顺序不确定
}

/// 异步顺序执行：开启一条线程顺序执行，适合顺序依赖的操作
- (void)asyncSerial {
    dispatch_queue_t queue = dispatch_queue_create("asyncSerial", NULL);
    dispatch_async(queue, ^{
        NSLog(@"1---%@", [NSThread currentThread]);
    });
    dispatch_async(queue, ^{
        NSLog(@"2---%@", [NSThread currentThread]);
    });
    dispatch_async(queue, ^{
        NSLog(@"3---%@", [NSThread currentThread]);
    });
    for (NSInteger i = 0; i < 20; i++) {
        dispatch_async(queue, ^{
            NSLog(@"%ld---%@", (long)i + 4, [NSThread currentThread]);
        });
    }
}

/// 异步并发执行：开启多条线程同时执行，适合相互之间没有依赖的操作
- (void)asyncConcurrent {
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0);
    dispatch_async(queue, ^{
        NSLog(@"1---%@", [NSThread currentThread]);
    });
    dispatch_async(queue, ^{
        NSLog(@"2---%@", [NSThread currentThread]);
    });
    dispatch_async(queue, ^{
        NSLog(@"3---%@", [NSThread currentThread]);
    });
    dispatch_async(queue, ^{
        NSLog(@"4---%@", [NSThread currentThread]);
    });
    dispatch_async(queue, ^{
        NSLog(@"5---%@", [NSThread currentThread]);
    });
    dispatch_async(queue, ^{
        NSLog(@"6---%@", [NSThread currentThread]);
    });
    dispatch_async(queue, ^{
        NSLog(@"7---%@", [NSThread currentThread]);
    });
    dispatch_async(queue, ^{
        NSLog(@"8---%@", [NSThread currentThread]);
    });
}

/// 队列组实现异步并发执行，且其中的一个操作依赖其它的所有操作：开启多条线程同时执行，其它操作执行完后，该操作才能执行；例如：给下载两个图片，下载完后合并两个图片进行加水印等；一个页面多个请求，请求全部完成后，刷新页面等
- (void)groupAsyncConcurrentComplete {
    dispatch_group_t group = dispatch_group_create();
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0);
    
    // 进组操作，和出组操作成对出现，表示该操作已经完成
//    dispatch_group_enter(group);
    dispatch_group_async(group, queue, ^{
        // 在发送请求结束时，不论成功和失败都要进行出组操作
//        dispatch_group_leave(group);
        NSLog(@"1---%@", [NSThread currentThread]);
    });
    dispatch_group_async(group, queue, ^{
        NSLog(@"2---%@", [NSThread currentThread]);
    });
    dispatch_group_async(group, queue, ^{
        NSLog(@"3---%@", [NSThread currentThread]);
    });
    dispatch_group_async(group, queue, ^{
        NSLog(@"4---%@", [NSThread currentThread]);
    });
    dispatch_group_async(group, queue, ^{
        NSLog(@"5---%@", [NSThread currentThread]);
    });
    dispatch_group_async(group, queue, ^{
        NSLog(@"6---%@", [NSThread currentThread]);
    });
    dispatch_group_async(group, queue, ^{
        NSLog(@"7---%@", [NSThread currentThread]);
    });
    dispatch_group_async(group, queue, ^{
        NSLog(@"8---%@", [NSThread currentThread]);
    });
    dispatch_group_async(group, queue, ^{
        NSLog(@"9---%@", [NSThread currentThread]);
    });
    dispatch_group_async(group, queue, ^{
        NSLog(@"10---%@", [NSThread currentThread]);
    });
    dispatch_group_notify(group, queue, ^{  // 全局队列中执行
        NSLog(@"11---%@", [NSThread currentThread]);
        dispatch_async(dispatch_get_main_queue(), ^{
            NSLog(@"finish---%@", [NSThread currentThread]);
        });
    });
//    dispatch_group_notify(group, dispatch_get_main_queue(), ^{    // 主队列中执行
//        NSLog(@"11---%@", [NSThread currentThread]);
//    });
}

/// 串行队列中的死锁：串行队列和同步函数造成的死锁
- (void)serialLock {
    // 串行队列：操作任务先进先出
    dispatch_queue_t queue = dispatch_queue_create("lock", DISPATCH_QUEUE_SERIAL);
    NSLog(@"1---%@", [NSThread currentThread]);
    dispatch_async(queue, ^{    // 异步添加操作任务block1，比较耗时，先执行NSLog(@"5---%@", [NSThread currentThread]);
        NSLog(@"2---%@", [NSThread currentThread]);
        dispatch_sync(queue, ^{ // 同步添加操作block2，阻塞线程等待block2执行完毕；但是该队列是串行队列，先添加的block1，必须block1先执行完毕，出队后，才能执行block2；这样就造成了两个block相互等待的死锁
            NSLog(@"3---%@", [NSThread currentThread]);
        });
        NSLog(@"4---%@", [NSThread currentThread]);
    });
    NSLog(@"5---%@", [NSThread currentThread]);
    
    // 综上：输出1、5、2后会卡住，不会向下执行
}

/// 多线程访问数据的问题
- (void)multithreadingAsyncData {
    __block int i = 0;
    while (i < 5) { // while循环阻塞主线程，直到跳出该循环
        dispatch_async(dispatch_get_global_queue(0, 0), ^{  // 开启线程，异步并行执行操作，无法确认其执行顺序
            i++;    // 该操作为异步执行，导致该while循环的次数一定会大于5次
            NSLog(@"内边 i = %d", i);
        });
    }
    // 当i = 5时跳出循环；而未执行的异步操作所访问的变量i是同一个变量，为已跳出循环的值5；而下方的打印操作，有可能在未执行的异步操作之前，也可能在之后；因此下方打印的结果是>=5
    NSLog(@"外边 i = %d", i);
    // 该i的不是最终结果，因为打印时，还有未执行的异步操作对i进行+1操作
    
    // 综上：输出结果是>=5
}

/// 信号量下的多线程安全访问
- (void)semaphoreMultithreadingData {
    dispatch_semaphore_t semaphore = dispatch_semaphore_create(1);
//    __block int i = 0;
//    while (i < 10) {
//        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);
//        dispatch_async(dispatch_get_global_queue(0, 0), ^{
//            NSLog(@"%d-%@", i, [NSThread currentThread]);
//            i++;
//            dispatch_semaphore_signal(semaphore);
//        });
//    }
    int i = 0;
    for (i = 0; i < 10; i++) {
        dispatch_semaphore_wait(semaphore, DISPATCH_TIME_FOREVER);  // 等待下面操作执行完成，信号量-1
        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            NSLog(@"%d-%@", i, [NSThread currentThread]);
            dispatch_semaphore_signal(semaphore);   // 操作完成，信号量+1，可以进行下一操作执行
        });
    }
    NSLog(@"外边 %d-%@", i, [NSThread currentThread]);
}

/// 互斥锁下的多线程安全访问
- (void)lockMultithreadingData {
//    __block int i = 0;
//    while (i < 10) {  // 添加多于10个的并发操作
//        dispatch_async(dispatch_get_global_queue(0, 0), ^{    // 添加异步并发操作
//            @synchronized (self) {    // 加锁，同一时间，只允许一个线程执行下列操作
//                NSLog(@"%d-%@", i, [NSThread currentThread]);
//                i++;
//            }
//        });
//    }
//    NSLog(@"外边 %d-%@", i, [NSThread currentThread]);
    
    int i = 0;
    for (int i = 0; i < 10; i++) {  // 循环加入10个异步并发操作
        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            @synchronized (self) {  // 加锁，同一时间，只允许一个线程执行下列操作
                NSLog(@"%d-%@", i, [NSThread currentThread]);
            }
        });
    }
    NSLog(@"外边 %d-%@", i, [NSThread currentThread]);
}

/// 异步栅栏函数
- (void)barrierAsync {
    dispatch_queue_t queue = dispatch_queue_create("barrierAsync", DISPATCH_QUEUE_CONCURRENT);
    for (int i = 0; i < 10; i++) {
        dispatch_async(queue, ^{
            NSLog(@"前%d-%@", i, [NSThread currentThread]);
        });
    }
    dispatch_barrier_async(queue, ^{
        NSLog(@"栅栏操作-%@", [NSThread currentThread]);
    });
    for (int i = 0; i < 10; i++) {
        dispatch_async(queue, ^{
            NSLog(@"后%d-%@", i, [NSThread currentThread]);
        });
    }
    NSLog(@"当前线程打印");
}

/// 同步栅栏函数
- (void)barrierSync {
    dispatch_queue_t queue = dispatch_queue_create("barrierAsync", DISPATCH_QUEUE_CONCURRENT);
    for (int i = 0; i < 10; i++) {
        dispatch_async(queue, ^{
            NSLog(@"前%d-%@", i, [NSThread currentThread]);
        });
    }
    dispatch_barrier_sync(queue, ^{
        NSLog(@"栅栏操作-%@", [NSThread currentThread]);
    });
    for (int i = 0; i < 10; i++) {
        dispatch_async(queue, ^{
            NSLog(@"后%d-%@", i, [NSThread currentThread]);
        });
    }
    NSLog(@"当前线程打印");
}

/// 异步栅栏函数的数据安全访问
- (void)barrierAsyncData {
    dispatch_queue_t queue = dispatch_queue_create("barrierAsyncData", DISPATCH_QUEUE_CONCURRENT);
    // 栅栏函数不能与全局队列使用，全局队列是所有应用程序共享的，不能被阻塞
//    dispatch_queue_t  queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0);
    NSMutableArray<NSNumber *> * numberList = [[NSMutableArray alloc] init];
    for (int i = 0; i < 1500; i++) {
        dispatch_async(queue, ^{
            // 异步栅栏函数添加的操作，会阻塞队列组，直到该函数操作完成后，进行后续操作，相当于队列的操作顺序执行，
            // 达到多线程访问数据安全的目的
            dispatch_barrier_async(queue, ^{    // 如果换为dispatch_barrier_sync会和dispatch_sync一样的原因，造成相互等待的死锁
                [numberList addObject:@(i)];
            });
            
            // 上述异步栅栏函数达到与下面加锁的效果
//            @synchronized (self) {
//                [numberList addObject:@(i)];
//            }
            // 多线程对可变数组数据访问，数据是不安全的，下面代码会造成崩溃
//            [numberList addObject:@(i)];
//            NSLog(@"%@", [NSThread currentThread]);
        });
    }
}

/// 取消GCD中的Block执行代码块
- (void)cancelBlock {
    /**iOS8之后可以调用dispatch_block_cancel来取消，需要注意以下两点：
     1.需要注意必须用dispatch_block_create创建dispatch_block_t；
     2.dispatch_block_cancel也只能取消尚未执行的任务，对正在执行的任务不起作用。
     */
    dispatch_queue_t queue = dispatch_queue_create("com.gcdtest.www", DISPATCH_QUEUE_CONCURRENT);
    dispatch_block_t block1 = dispatch_block_create(0, ^{
        sleep(5);
        NSLog(@"block1 %@",[NSThread currentThread]);
    });
    dispatch_block_t block2 = dispatch_block_create(0, ^{
        NSLog(@"block2 %@",[NSThread currentThread]);
    });
    dispatch_block_t block3 = dispatch_block_create(0, ^{
        NSLog(@"block3 %@",[NSThread currentThread]);
    });
    dispatch_async(queue, block1);
    dispatch_async(queue, block2);
    dispatch_block_cancel(block3);
    
    /**定义外部变量，用于标记block是否需要取消
     该方法是模拟NSOperation，在执行block前先检查isCancelled = YES ？在block中及时的检测标记变量，当发现需要取消时，终止后续操作。
     */
    dispatch_queue_t globalQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0);
    __block BOOL isCancel = NO;
    dispatch_async(globalQueue, ^{
        NSLog(@"任务001 %@",[NSThread currentThread]);
    });
    dispatch_async(globalQueue, ^{
        NSLog(@"任务002 %@",[NSThread currentThread]);
    });
    dispatch_async(globalQueue, ^{
        NSLog(@"任务003 %@",[NSThread currentThread]);
        isCancel = YES;
    });
    dispatch_async(queue, ^{
        // 模拟：线程等待3秒，确保任务003完成 isCancel＝YESsleep(3);
        if(isCancel){
            NSLog(@"任务004已被取消 %@",[NSThread currentThread]);
        }else{
            NSLog(@"任务004 %@",[NSThread currentThread]);
        }
    });
}

// MARK: -主队列和主线程

- (void)mainQueueAndMainThread {
    /**
     连接地址：https://www.jianshu.com/p/210d2e867782
     
     执行代码mainQueueForOtherThread方法代码：
     before dispatch_main
     main thread: 0 // 不是主线程
     main queue: 1  // 是主队列
     
     什么情况？派发给主队列的任务不是在主线程上运行，跟我们平常用的和理解的完全不一样。
     不要激动，导致这种原因最关键的是这行代码dispatch_main() ,就是这货让主队列的任务在非主线程运行。
     dispatch_main苹果注释：
     *!
      * @function dispatch_main
      *
      * @abstract
      * Execute blocks submitted to the main queue.
      * 执行提交给主队列的任务blocks
      *
      * @discussion
      * This function "parks" the main thread and waits for blocks to be submitted
      * to the main queue. This function never returns.
      * 这个函数会阻塞主线程并且等待提交给主队列的任务blocks完成，这个函数永远不会返回
      *
      * Applications that call NSApplicationMain() or CFRunLoopRun() on the
      * main thread do not need to call dispatch_main().
      *
     意思是这个方法会阻塞主线程，然后在其它线程中执行主队列中的任务，这个方法永远不会返回（意思会卡住主线程）
     
     如果去掉dispatch_main()这行代码，就会正常在主线程里执行任务：
     main thread: 1 // 主线程
     main queue: 1  // 主队列
     
     所以在主队列的任务通常是在主线程里执行，但是不一定，我们可以主动去执行被添加到主队列MainQueue的任务task（也就是说我们可以主动来调用添加到主线程队列的blocks）。可以使用以下任一个来实现：dispatch_main()、UIApplicationMain() 、CFRunLoopRun()
     
     那我们再思考一下，主线程是否可以运行非主队列的任务blocks吗？答案是可以的，比如下面的代码：
     // 同步加入全局队列里
     dispatch_sync(globalQueue, ^{
         // 判断是否是主线程
         NSLog(@"main thread: %d", [NSThread isMainThread]);
         // 判断是否是主队列
         void *value = dispatch_get_specific("key");
         NSLog(@"main queue: %d", value != NULL);
     });
     
     执行结果：
     main thread: 1 // 主线程
     main queue: 0 // 全局队列
     
     所以通过dispatch_sync()执行的block不会开辟新的线程，而是在当前的线程（即主线程）中同步执行block
     */
    [self mainQueueForOtherThread];
}

/// 主队列的任务不是在主线程上运行
- (void)mainQueueForOtherThread {
    // 主队列
    dispatch_queue_t mainQueue = dispatch_get_main_queue();
    // 给主队列设置一个标记
    dispatch_queue_set_specific(mainQueue, "key", "main", NULL);

    // 定义一个block任务
    dispatch_block_t log = ^{
        // 判断是否是主线程
        NSLog(@"main thread: %d", [NSThread isMainThread]);
        // 判断是否是主队列
        void *value = dispatch_get_specific("key");
        NSLog(@"main queue: %d", value != NULL);
    };

    // 全局队列
    dispatch_queue_t globalQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
    // 异步加入全局队列里
    dispatch_async(globalQueue, ^{
        // 异步加入主队列里
        dispatch_async(dispatch_get_main_queue(), log);
    });

    NSLog(@"before dispatch_main");
    dispatch_main();
    NSLog(@"after dispatch_main");
}

@end
