//
//  ZBYGCDSourceViewController.m
//  OCBase2021
//
//  Created by 周保勇 on 2021/4/11.
//

#import "ZBYGCDSourceViewController.h"

@interface ZBYGCDSourceViewController ()

@property (nonatomic, strong) dispatch_queue_t queue;
@property (nonatomic, strong) dispatch_source_t source;
@property (nonatomic, strong) dispatch_source_t timer;
@property (nonatomic, assign) NSInteger timerValue;
@property (nonatomic, assign) NSInteger totalValue;
@property (nonatomic, assign) BOOL isRunning;

@end

@implementation ZBYGCDSourceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.totalValue = 0;
    UIButton * btn = [UIButton new];
    btn.backgroundColor = [UIColor random];
    [btn setSelected:YES];
    [btn setTitle:@"暂停中..." forState:UIControlStateNormal];
    [btn setTitle:@"加载中..." forState:UIControlStateSelected];
    [btn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn];
    [btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(100);
        make.left.mas_equalTo(100);
        make.width.mas_equalTo(100);
        make.height.mas_equalTo(40);
    }];
    
    UIButton * timerBtn = [UIButton new];
    timerBtn.backgroundColor = [UIColor random];
    [timerBtn setTitle:@"开始定时器" forState:UIControlStateNormal];
    [timerBtn setTitle:@"暂停定时器" forState:UIControlStateSelected];
    [timerBtn addTarget:self action:@selector(timerBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:timerBtn];
    [timerBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(btn.mas_bottom).offset(40);
        make.left.mas_equalTo(100);
        make.width.mas_equalTo(100);
        make.height.mas_equalTo(40);
    }];
    
    [self initSource];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self beginSourceAdd];
}

- (void)btnClick:(UIButton *)sender {
    [sender setSelected:!sender.isSelected];
    self.isRunning = sender.isSelected;
    if (sender.isSelected) {
        dispatch_resume(self.source);
        dispatch_resume(self.queue);
//        [self beginSourceAdd];
    }
    else {
        dispatch_suspend(self.source);
        dispatch_suspend(self.queue);
    }
}

- (void)timerBtnClick:(UIButton *)sender {
    [sender setSelected:!sender.isSelected];
    if (sender.isSelected) {
        [self initTimer];
        // 启动
        dispatch_resume(self.timer);
    }
    else {
        // 暂停
        dispatch_suspend(self.timer);
    }
}

- (void)initSource {
    if (self.queue) {
        return;
    }
    self.queue = dispatch_queue_create("source.queue.com", NULL);
    self.source = dispatch_source_create(DISPATCH_SOURCE_TYPE_DATA_ADD, 0, 0, dispatch_get_main_queue());
    dispatch_source_set_event_handler(self.source, ^{
        NSInteger value = dispatch_source_get_data(self.source);
        self.totalValue += value;
        NSLog(@"%ld", (long)self.totalValue);
    });
//    dispatch_suspend(self.queue);
    dispatch_resume(self.source);
    self.isRunning = YES;
}

- (void)beginSourceAdd {
    for (NSInteger i = 0; i < 20; i++) {
        dispatch_async(self.queue, ^{
            if (!self.isRunning) {
                return;
            }
            sleep(2);
            dispatch_source_merge_data(self.source, 1);
        });
    }
}

- (void)initTimer {
    if (self.timer) {
        return;
    }
    self.timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, dispatch_get_main_queue());
    
    /*
     第二个参数：表示定时器开始时间；当我们使用dispatch_time 或者 DISPATCH_TIME_NOW 时，系统会使用默认时钟来进行计时。然而当系统休眠的时候，默认时钟是不走的，也就会导致计时器停止。使用 dispatch_walltime 可以让计时器按照真实时间间隔进行计时；
        当前参数dispatch_time(DISPATCH_TIME_NOW, 5ull * NSEC_PER_SEC)表示5秒后开始，
     第三个参数：表示定时器重复执行的间隔；DISPATCH_TIME_FOREVER表示只执行一次
        当前参数表示1秒执行一次
     第四个参数：表示允许延迟时间；推荐设置一个合理的值。需要注意，就算指定值为0，系统也无法保证完全精确的触发时间，只是会尽可能满足这个需求。
        当前参数表示允许延迟1秒
     */
    dispatch_source_set_timer(self.timer, dispatch_time(DISPATCH_TIME_NOW, 5ull * NSEC_PER_SEC), 1ull * NSEC_PER_SEC, 1ull * NSEC_PER_SEC);
    dispatch_source_set_event_handler(self.timer, ^{
        self.timerValue += 1;
        NSLog(@"%ld", (long)self.timerValue);
        // 取消定时器Source
//        dispatch_source_cancel(self.timer);
    });
    dispatch_source_set_cancel_handler(self.timer, ^{
        NSLog(@"canceled");
    });
}

@end
