//
//  ZBYAbstractFactory.h
//  OCBase2021
//
//  Created by 周保勇 on 2021/5/26.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZBYAbstractFactory : NSObject

/// 获取中间工厂类
+ (instancetype)instanceWithFactoryType:(NSString *)factoryType;

/// 由中间工厂类，直接获取需要的子类，屏蔽了中间细节
+ (instancetype)instanceWithFactoryType:(NSString *)factoryType type:(NSString *)type;

/// 由中间工厂类，直接获取需要的子类，屏蔽了中间细节
+ (instancetype)redColor;

/// 由中间工厂类，直接获取需要的子类，屏蔽了中间细节
+ (instancetype)greenColor;

/// 由中间工厂类，直接获取需要的子类，屏蔽了中间细节
+ (instancetype)blueColor;

+ (instancetype)rectanglePolygon;

+ (instancetype)squarePolygon;

@end

NS_ASSUME_NONNULL_END
