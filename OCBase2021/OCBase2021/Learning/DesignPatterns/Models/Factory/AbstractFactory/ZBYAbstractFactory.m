//
//  ZBYAbstractFactory.m
//  OCBase2021
//
//  Created by 周保勇 on 2021/5/26.
//

#import "ZBYAbstractFactory.h"
#import "ZBYAbstractFactoryColor.h"
#import "ZBYAbstractFactoryPolygon.h"

@implementation ZBYAbstractFactory

+ (instancetype)instanceWithFactoryType:(NSString *)factoryType {
    if ([factoryType isEqualToString:@"color"]) {
        return [ZBYAbstractFactoryColor factory];
    }
    else if ([factoryType isEqualToString:@"polygon"]) {
        return [ZBYAbstractFactoryPolygon new];
    }
    else {
        return nil;
    }
}

+ (instancetype)instanceWithFactoryType:(NSString *)factoryType type:(NSString *)type {
    if ([factoryType isEqualToString:@"color"]) {
        if ([type isEqualToString:@"red"]) {
            return [ZBYAbstractFactoryColor redColor];
        }
        else if ([type isEqualToString:@"green"]) {
            return [ZBYAbstractFactoryColor greenColor];
        }
        else if ([type isEqualToString:@"blue"]) {
            return [ZBYAbstractFactoryColor blueColor];
        }
        else {
            return nil;
        }
    }
    else if ([factoryType isEqualToString:@"polygon"]) {
        if ([type isEqualToString:@"rectangle"]) {
            return [ZBYAbstractFactoryColor redColor];
        }
        else if ([type isEqualToString:@"square"]) {
            return [ZBYAbstractFactoryColor greenColor];
        }
        else {
            return nil;
        }
    }
    else {
        return nil;
    }
}

+ (instancetype)redColor {
    return [ZBYAbstractFactoryColor redColor];
}

+ (instancetype)greenColor {
    return [ZBYAbstractFactoryColor greenColor];
}

+ (instancetype)blueColor {
    return [ZBYAbstractFactoryColor blueColor];
}

+ (instancetype)rectanglePolygon {
    return [ZBYAbstractFactoryPolygon rectanglePolygon];
}

+ (instancetype)squarePolygon {
    return [ZBYAbstractFactoryPolygon squarePolygon];
}

@end
