//
//  ZBYAbstractFactoryPolygonRectangle.m
//  OCBase2021
//
//  Created by 周保勇 on 2021/5/26.
//

#import "ZBYAbstractFactoryPolygonRectangle.h"

@interface ZBYAbstractFactoryPolygonRectangle ()

/// 类型描述字符串
@property (nonatomic, copy, readwrite) NSString * type;

@end

@implementation ZBYAbstractFactoryPolygonRectangle

+ (instancetype)polygon {
    ZBYAbstractFactoryPolygonRectangle * polygon = [ZBYAbstractFactoryPolygonRectangle new];
    polygon.type = @"长方形";
    return polygon;
}

@end
