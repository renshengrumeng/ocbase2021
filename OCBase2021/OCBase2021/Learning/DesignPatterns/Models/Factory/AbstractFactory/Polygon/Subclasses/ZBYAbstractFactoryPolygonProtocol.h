//
//  ZBYAbstractFactoryPolygonProtocol.h
//  OCBase2021
//
//  Created by 周保勇 on 2021/5/26.
//

#ifndef ZBYAbstractFactoryPolygonProtocol_h
#define ZBYAbstractFactoryPolygonProtocol_h

@protocol ZBYAbstractFactoryPolygonProtocol <NSObject>

@optional

@required

+ (id _Nullable)polygon;

@end


#endif /* ZBYAbstractFactoryPolygonProtocol_h */
