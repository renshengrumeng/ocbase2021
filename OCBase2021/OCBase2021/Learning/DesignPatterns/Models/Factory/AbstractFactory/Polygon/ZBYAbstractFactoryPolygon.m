//
//  ZBYAbstractFactoryPolygon.m
//  OCBase2021
//
//  Created by 周保勇 on 2021/5/26.
//

#import "ZBYAbstractFactoryPolygon.h"
#import "ZBYAbstractFactoryPolygonRectangle.h"
#import "ZBYAbstractFactoryPolygonSquare.h"

@implementation ZBYAbstractFactoryPolygon

+ (instancetype)factory {
    return [self new];
}

+ (instancetype)rectanglePolygon {
    return [ZBYAbstractFactoryPolygonRectangle polygon];
}

+ (instancetype)squarePolygon {
    return [ZBYAbstractFactoryPolygonSquare polygon];
}

@end
