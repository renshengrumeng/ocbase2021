//
//  ZBYAbstractFactoryPolygonSquare.m
//  OCBase2021
//
//  Created by 周保勇 on 2021/5/26.
//

#import "ZBYAbstractFactoryPolygonSquare.h"

@interface ZBYAbstractFactoryPolygonSquare ()

/// 类型描述字符串
@property (nonatomic, copy, readwrite) NSString * type;

@end

@implementation ZBYAbstractFactoryPolygonSquare

+ (instancetype)polygon {
    ZBYAbstractFactoryPolygonSquare * polygon = [ZBYAbstractFactoryPolygonSquare new];
    polygon.type = @"正方形";
    return polygon;
}

@end
