//
//  ZBYAbstractFactoryPolygon.h
//  OCBase2021
//
//  Created by 周保勇 on 2021/5/26.
//

#import <Foundation/Foundation.h>
#import "ZBYAbstractFactoryProtocol.h"
#import "ZBYAbstractFactory.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZBYAbstractFactoryPolygon : ZBYAbstractFactory<ZBYAbstractFactoryProtocol>

+ (instancetype)factory;

+ (instancetype)rectanglePolygon;

+ (instancetype)squarePolygon;

@end

NS_ASSUME_NONNULL_END
