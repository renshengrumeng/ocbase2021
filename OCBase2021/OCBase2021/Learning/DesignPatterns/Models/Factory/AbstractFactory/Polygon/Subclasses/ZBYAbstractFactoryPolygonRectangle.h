//
//  ZBYAbstractFactoryPolygonRectangle.h
//  OCBase2021
//
//  Created by 周保勇 on 2021/5/26.
//

#import <Foundation/Foundation.h>
#import "ZBYAbstractFactoryPolygonProtocol.h"
#import "ZBYAbstractFactoryPolygon.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZBYAbstractFactoryPolygonRectangle : ZBYAbstractFactoryPolygon<ZBYAbstractFactoryPolygonProtocol>

/// 类型描述字符串
@property (nonatomic, copy, readonly) NSString * type;

+ (instancetype)polygon;

@end

NS_ASSUME_NONNULL_END
