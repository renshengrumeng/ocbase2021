//
//  ZBYAbstractFactoryProtocol.h
//  OCBase2021
//
//  Created by 周保勇 on 2021/5/26.
//

#ifndef ZBYAbstractFactoryProtocol_h
#define ZBYAbstractFactoryProtocol_h

@protocol ZBYAbstractFactoryProtocol <NSObject>

@optional

@required

+ (id _Nullable)factory;

@end


#endif /* ZBYAbstractFactoryProtocol_h */
