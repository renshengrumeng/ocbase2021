//
//  ZBYAbstractFactoryColorGreen.m
//  OCBase2021
//
//  Created by 周保勇 on 2021/5/26.
//

#import "ZBYAbstractFactoryColorGreen.h"

@interface ZBYAbstractFactoryColorGreen ()

/// 类型描述字符串
@property (nonatomic, copy, readwrite) NSString * type;

@end

@implementation ZBYAbstractFactoryColorGreen

+ (instancetype)color {
    ZBYAbstractFactoryColorGreen * color = [ZBYAbstractFactoryColorGreen new];
    color.type = @"绿色";
    return color;
}

@end
