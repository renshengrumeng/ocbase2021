//
//  ZBYAbstractFactoryColorGreen.h
//  OCBase2021
//
//  Created by 周保勇 on 2021/5/26.
//

#import <Foundation/Foundation.h>
#import "ZBYAbstractFactoryColor.h"
#import "ZBYAbstractFactoryColorProtocol.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZBYAbstractFactoryColorGreen : ZBYAbstractFactoryColor<ZBYAbstractFactoryColorProtocol>

/// 类型描述字符串
@property (nonatomic, copy, readonly) NSString * type;

+ (instancetype)color;

@end

NS_ASSUME_NONNULL_END
