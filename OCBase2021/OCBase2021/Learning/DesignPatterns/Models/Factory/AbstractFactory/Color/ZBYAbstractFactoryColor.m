//
//  ZBYAbstractFactoryColor.m
//  OCBase2021
//
//  Created by 周保勇 on 2021/5/26.
//

#import "ZBYAbstractFactoryColor.h"
#import "ZBYAbstractFactoryColorRed.h"
#import "ZBYAbstractFactoryColorGreen.h"
#import "ZBYAbstractFactoryColorBlue.h"

@implementation ZBYAbstractFactoryColor

+ (instancetype)factory {
    return [ZBYAbstractFactoryColor new];
}

+ (instancetype)redColor {
    return [ZBYAbstractFactoryColorRed color];
}

+ (instancetype)greenColor {
    return [ZBYAbstractFactoryColorGreen color];
}

+ (instancetype)blueColor {
    return [ZBYAbstractFactoryColorBlue color];
}

@end
