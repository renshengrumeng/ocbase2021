//
//  ZBYAbstractFactoryColor.h
//  OCBase2021
//
//  Created by 周保勇 on 2021/5/26.
//

#import <Foundation/Foundation.h>
#import "ZBYAbstractFactoryProtocol.h"
#import "ZBYAbstractFactory.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZBYAbstractFactoryColor : ZBYAbstractFactory <ZBYAbstractFactoryProtocol>

+ (instancetype)factory;

+ (instancetype)redColor;

+ (instancetype)greenColor;

+ (instancetype)blueColor;

@end

NS_ASSUME_NONNULL_END
