//
//  ZBYAbstractFactoryColorRed.m
//  OCBase2021
//
//  Created by 周保勇 on 2021/5/26.
//

#import "ZBYAbstractFactoryColorRed.h"

@interface ZBYAbstractFactoryColorRed ()

/// 类型描述字符串
@property (nonatomic, copy, readwrite) NSString * type;

@end

@implementation ZBYAbstractFactoryColorRed

+ (instancetype)color {
    ZBYAbstractFactoryColorRed * color = [ZBYAbstractFactoryColorRed new];
    color.type = @"红色";
    return color;
}

@end
