//
//  ZBYAbstractFactoryColorBlue.m
//  OCBase2021
//
//  Created by 周保勇 on 2021/5/26.
//

#import "ZBYAbstractFactoryColorBlue.h"

@interface ZBYAbstractFactoryColorBlue ()

/// 类型描述字符串
@property (nonatomic, copy, readwrite) NSString * type;

@end

@implementation ZBYAbstractFactoryColorBlue

+ (instancetype)color {
    ZBYAbstractFactoryColorBlue * color = [ZBYAbstractFactoryColorBlue new];
    color.type = @"蓝色";
    return color;
}

@end
