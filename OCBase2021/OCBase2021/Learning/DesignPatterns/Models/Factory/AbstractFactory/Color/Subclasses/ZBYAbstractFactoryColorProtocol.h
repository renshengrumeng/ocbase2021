//
//  ZBYAbstractFactoryColorProtocol.h
//  OCBase2021
//
//  Created by 周保勇 on 2021/5/26.
//

#ifndef ZBYAbstractFactoryColorProtocol_h
#define ZBYAbstractFactoryColorProtocol_h

@protocol ZBYAbstractFactoryColorProtocol <NSObject>

@optional

@required

+ (id _Nullable)color;

@end


#endif /* ZBYAbstractFactoryColorProtocol_h */
