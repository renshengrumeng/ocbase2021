//
//  ZBYSimpleFactoryPhone.m
//  OCBase2021
//
//  Created by 周保勇 on 2021/5/26.
//

#import "ZBYSimpleFactoryPhone.h"
#import "ZBYSimpleFactoryApplePhone.h"
#import "ZBYSimpleFactoryHuaWeiPhone.h"

@implementation ZBYSimpleFactoryPhone

- (instancetype)phoneWithType:(NSString *)type {
    if ([type isEqualToString:@"苹果"]) {
        return [ZBYSimpleFactoryApplePhone phone];
    }
    else if ([type isEqualToString:@"华为"]) {
        return [ZBYSimpleFactoryHuaWeiPhone phone];
    }
    else {
        return nil;
    }
}

- (instancetype)applePhone {
    return [ZBYSimpleFactoryApplePhone phone];
}

- (instancetype)huaWeiPhone {
    return [ZBYSimpleFactoryHuaWeiPhone phone];
}

+ (instancetype)phoneWithType:(NSString *)type {
    if ([type isEqualToString:@"苹果"]) {
        return [ZBYSimpleFactoryApplePhone phone];
    }
    else if ([type isEqualToString:@"华为"]) {
        return [ZBYSimpleFactoryHuaWeiPhone phone];
    }
    else {
        return nil;
    }
}

+ (instancetype)applePhone {
    return [ZBYSimpleFactoryApplePhone phone];
}

+ (instancetype)huaWeiPhone {
    return [ZBYSimpleFactoryHuaWeiPhone phone];
}

@end
