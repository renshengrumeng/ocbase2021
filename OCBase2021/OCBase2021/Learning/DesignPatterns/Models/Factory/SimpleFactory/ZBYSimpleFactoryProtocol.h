//
//  ZBYSimpleFactoryProtocol.h
//  OCBase2021
//
//  Created by 周保勇 on 2021/5/26.
//

#ifndef ZBYSimpleFactoryProtocol_h
#define ZBYSimpleFactoryProtocol_h

@protocol ZBYSimpleFactoryProtocol <NSObject>

@optional

@required

+ (id _Nullable)phone;

@end


#endif /* ZBYSimpleFactoryProtocol_h */
