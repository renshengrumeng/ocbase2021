//
//  ZBYSimpleFactoryHuaWeiPhone.m
//  OCBase2021
//
//  Created by 周保勇 on 2021/5/26.
//

#import "ZBYSimpleFactoryHuaWeiPhone.h"

@interface ZBYSimpleFactoryHuaWeiPhone ()

/// 手机类型字符串
@property (nonatomic, copy, readwrite) NSString * type;

@end

@implementation ZBYSimpleFactoryHuaWeiPhone

+ (instancetype)phone {
    ZBYSimpleFactoryHuaWeiPhone * phone = [ZBYSimpleFactoryHuaWeiPhone new];
    phone.type = @"华为";
    return phone;
}


@end
