//
//  ZBYSimpleFactoryPhone.h
//  OCBase2021
//
//  Created by 周保勇 on 2021/5/26.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZBYSimpleFactoryPhone : NSObject

// MARK: - 1.简单工厂方法

- (instancetype)phoneWithType:(NSString *)type;

// MARK: - 2.多工厂方法

- (instancetype)applePhone;
- (instancetype)huaWeiPhone;

// MARK: - 3.静态工厂方法

+ (instancetype)phoneWithType:(NSString *)type;

+ (instancetype)applePhone;
+ (instancetype)huaWeiPhone;

@end

NS_ASSUME_NONNULL_END
