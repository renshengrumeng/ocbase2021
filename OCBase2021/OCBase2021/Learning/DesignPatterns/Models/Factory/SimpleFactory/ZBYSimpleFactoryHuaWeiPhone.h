//
//  ZBYSimpleFactoryHuaWeiPhone.h
//  OCBase2021
//
//  Created by 周保勇 on 2021/5/26.
//

#import <Foundation/Foundation.h>
#import "ZBYSimpleFactoryPhone.h"
#import "ZBYSimpleFactoryProtocol.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZBYSimpleFactoryHuaWeiPhone : ZBYSimpleFactoryPhone <ZBYSimpleFactoryProtocol>

/// 手机类型字符串
@property (nonatomic, copy, readonly) NSString * type;

+ (instancetype)phone;

@end

NS_ASSUME_NONNULL_END
