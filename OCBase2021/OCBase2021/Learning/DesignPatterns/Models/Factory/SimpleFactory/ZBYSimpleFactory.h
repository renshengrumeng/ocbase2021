//
//  ZBYSimpleFactory.h
//  OCBase2021
//
//  Created by 周保勇 on 2021/5/26.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZBYSimpleFactoryPhone : NSObject

+ (instancetype)phoneWithType:(NSString *)type;

@end

NS_ASSUME_NONNULL_END
