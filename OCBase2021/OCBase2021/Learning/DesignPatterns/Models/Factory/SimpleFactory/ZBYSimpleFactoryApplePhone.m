//
//  ZBYSimpleFactoryApplePhone.m
//  OCBase2021
//
//  Created by 周保勇 on 2021/5/26.
//

#import "ZBYSimpleFactoryApplePhone.h"

@interface ZBYSimpleFactoryApplePhone ()

/// 手机类型字符串
@property (nonatomic, copy, readwrite) NSString * type;

@end

@implementation ZBYSimpleFactoryApplePhone

+ (instancetype)phone {
    ZBYSimpleFactoryApplePhone * phone = [ZBYSimpleFactoryApplePhone new];
    phone.type = @"苹果";
    return phone;
}

@end
