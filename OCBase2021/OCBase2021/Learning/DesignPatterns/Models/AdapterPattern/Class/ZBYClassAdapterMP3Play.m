//
//  ZBYClassAdapterMP3Play.m
//  OCBase2021
//
//  Created by 周保勇 on 2021/5/31.
//

#import "ZBYClassAdapterMP3Play.h"

@implementation ZBYClassAdapterMP3Play

- (void)playWithMP3Url:(NSString *)url {
    NSLog(@"音频播放，播放地址：%@", url);
}

@end
