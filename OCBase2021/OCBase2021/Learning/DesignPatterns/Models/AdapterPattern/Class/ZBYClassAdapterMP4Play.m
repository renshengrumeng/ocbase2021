//
//  ZBYClassAdapterMP4Play.m
//  OCBase2021
//
//  Created by 周保勇 on 2021/5/31.
//

#import "ZBYClassAdapterMP4Play.h"

@implementation ZBYClassAdapterMP4Play

- (void)playWithMP4Url:(NSString *)url {
    NSLog(@"视频播放，播放地址：%@", url);
}

@end
