//
//  ZBYClassAdapterMP3Play.h
//  OCBase2021
//
//  Created by 周保勇 on 2021/5/31.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZBYClassAdapterMP3Play : NSObject

- (void)playWithMP3Url:(NSString *)url;

@end

NS_ASSUME_NONNULL_END
