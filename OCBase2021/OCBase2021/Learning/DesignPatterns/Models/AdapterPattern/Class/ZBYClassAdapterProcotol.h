//
//  ZBYClassAdapterProcotol.h
//  OCBase2021
//
//  Created by 周保勇 on 2021/5/31.
//

#ifndef ZBYClassAdapterProcotol_h
#define ZBYClassAdapterProcotol_h

@protocol ZBYClassAdapterProcotol <NSObject>

- (void)playWithUrl:(NSString *)url;

@end

#endif /* ZBYClassAdapterProcotol_h */
