//
//  ZBYClassAdapterMP4Play.h
//  OCBase2021
//
//  Created by 周保勇 on 2021/5/31.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZBYClassAdapterMP4Play : NSObject

- (void)playWithMP4Url:(NSString *)url;

@end

NS_ASSUME_NONNULL_END
