//
//  ZBYLazySigleton.h
//  OCBase2021
//
//  Created by 周保勇 on 2021/5/27.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZBYLazySigleton : NSObject

+ (instancetype)sharedInstance;

@end

NS_ASSUME_NONNULL_END
