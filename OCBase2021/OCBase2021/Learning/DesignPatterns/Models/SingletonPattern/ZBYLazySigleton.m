//
//  ZBYLazySigleton.m
//  OCBase2021
//
//  Created by 周保勇 on 2021/5/27.
//

#import "ZBYLazySigleton.h"

@implementation ZBYLazySigleton

// 用来保存唯一的单例对象
static id _instance;
//static dispatch_once_t onceToken;

+ (instancetype)allocWithZone:(struct _NSZone *)zone
{
//    dispatch_once(&onceToken, ^{  // GCD 加载一次
//        _instace = [super allocWithZone:zone];
//    });
    @synchronized(self) {   // 加锁，线程安全
        if (_instance == nil) {
            _instance = [super allocWithZone:zone];
        }
    }
    return _instance;
}

+ (instancetype)sharedInstance {
    if (_instance == nil) {
        _instance = [[self alloc] init];
    }
    return _instance;
}

@end
