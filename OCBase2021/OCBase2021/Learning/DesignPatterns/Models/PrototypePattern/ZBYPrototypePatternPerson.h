//
//  ZBYPrototypePatternPerson.h
//  OCBase2021
//
//  Created by 周保勇 on 2021/5/27.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZBYPrototypePatternPerson : NSObject<NSCopying>

/// 名字
@property (nonatomic, copy) NSString * name;

/// 年龄
@property (nonatomic, assign) NSInteger age;

@end

NS_ASSUME_NONNULL_END
