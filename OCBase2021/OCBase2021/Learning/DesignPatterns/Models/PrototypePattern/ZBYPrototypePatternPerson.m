//
//  ZBYPrototypePatternPerson.m
//  OCBase2021
//
//  Created by 周保勇 on 2021/5/27.
//

#import "ZBYPrototypePatternPerson.h"

@implementation ZBYPrototypePatternPerson

- (nonnull id)copyWithZone:(nullable NSZone *)zone {
    ZBYPrototypePatternPerson * p = [[ZBYPrototypePatternPerson allocWithZone:zone] init];
    p.name = self.name;
    p.age = self.age;
    return p;
}

@end
