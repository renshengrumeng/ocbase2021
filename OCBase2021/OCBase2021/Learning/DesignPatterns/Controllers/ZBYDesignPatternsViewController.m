//
//  ZBYDesignPatternsViewController.m
//  OCBase2021
//
//  Created by 周保勇 on 2021/5/24.
//

#import "ZBYDesignPatternsViewController.h"
#import "ZBYSimpleFactoryApplePhone.h"
#import "ZBYSimpleFactoryHuaWeiPhone.h"
#import "ZBYAbstractFactory.h"
#import "ZBYLazySigleton.h"
#import "ZBYPrototypePatternPerson.h"

@interface ZBYDesignPatternsViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, copy) NSArray * models;

@end

@implementation ZBYDesignPatternsViewController

- (NSArray *)models {
    if (_models == nil) {
        _models = @[
        @{@"title" : @"简介", @"subTitle" : @"设计模式简介"},
        @{@"title" : @"工厂模式", @"subTitle" : @"3种工厂模式"},
        @{@"title" : @"抽象工厂模式", @"subTitle" : @"OC中的类族模式"},
        @{@"title" : @"单例模式（Singleton Pattern）", @"subTitle" : @"2中单例模式"},
        @{@"title" : @"原型模式（Prototype Pattern）", @"subTitle" : @""},
        @{@"title" : @"适配器模式（Adapter Pattern）", @"subTitle" : @"也叫中介者模式"}
        ];
    }
    return _models;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
    UITableView * tableView = [[UITableView alloc] initWithFrame:CGRectZero style:(UITableViewStylePlain)];
    tableView.delegate = self;
    tableView.dataSource = self;
    [self.view addSubview:tableView];
    
    tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];
}

// MARK: - UITableViewDelegate, UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.models.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString * identifier = @"123456";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:identifier];
        cell.backgroundColor = [UIColor whiteColor];
        cell.textLabel.textColor = [UIColor blackColor];
        cell.detailTextLabel.textColor = [UIColor blackColor];
    }
    cell.textLabel.text = self.models[indexPath.row][@"title"];
    cell.detailTextLabel.text = self.models[indexPath.row][@"subTitle"];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSDictionary * info = self.models[indexPath.row];
    NSString * title = info[@"title"];
    if ([title containsString:@"简介"]) {
        [self designPatternsBaseDescription];
    }
    if ([title containsString:@"工厂模式"]) {
        [self factoryMethodModel];
    }
    if ([title containsString:@"抽象工厂模式"]) {
        [self abstractFactoryMethodPattern];
    }
    if ([title containsString:@"单例模式"]) {
        [self singletonPattern];
    }
    if ([title containsString:@"原型模式"]) {
        [self prototypePattern];
    }
    
}

// MARK: - 简介

- (void)designPatternsBaseDescription {
    /**
     总体来说23种设计模式分为三大类：
     创建型模式定义：在软件工程中，创建型模式是处理对象创建的设计模式，试图根据实际情况使用合适的方式创建对象。基本的对象创建方式可能会导致设计上的问题，或增加设计的复杂度。创建型模式通过以某种方式控制对象的创建来解决问题。
     创建型模式由两个主导思想构成。一是将系统使用的具体类封装起来，二是隐藏这些具体类的实例创建和结合的方式。
     创建型模式又分为对象创建型模式和类创建型模式。对象创建型模式处理对象的创建，类创建型模式处理类的创建。详细地说，对象创建型模式把对象创建的一部分推迟到另一个对象中，而类创建型模式将它对象的创建推迟到子类中。
     创建型模式（5种）：工厂方法模式、抽象工厂模式、单例模式、建造者模式、原型模式。
     结构型模式（7种）：适配器模式、装饰器模式、代理模式、外观模式、桥接模式、组合模式、享元模式。
     行为型模式（11种）：策略模式、模板方法模式、观察者模式、迭代子模式、责任链模式、命令模式、备忘录模式、状态模式、访问者模式、中介者模式、解释器模式。
     */
}

// MARK: - 工厂方法模式

- (void)factoryMethodModel {
    /**
     工厂模式：Factory Pattern是高级编程语言中最常用的设计模式之一。这种类型的设计模式属于创建型模式，它提供了一种创建对象的最佳方式。
     在工厂模式中，我们在创建对象时不会对客户端暴露创建逻辑，并且是通过使用一个共同的接口来指向新创建的对象。
     
     优点： 1、一个调用者想创建一个对象，只要知道其名称就可以了。 2、扩展性高，如果想增加一个产品，只要扩展一个工厂类就可以。 3、屏蔽产品的具体实现，调用者只关心产品的接口。
     
     缺点：每次增加一个产品时，都需要增加一个具体类和对象实现工厂，使得系统中类的个数成倍增加，在一定程度上增加了系统的复杂度，同时也增加了系统具体类的依赖。这并不是什么好事。
     
     使用场景： 1、日志记录器：记录可能记录到本地硬盘、系统事件、远程服务器等，用户可以选择记录日志到什么地方。 2、数据库访问，当用户不知道最后系统采用哪一类数据库，以及数据库可能有变化时。 3、设计一个连接服务器的框架，需要三个协议，"POP3"、"IMAP"、"HTTP"，可以把这三个作为产品类，共同实现一个接口。
     
     注意事项：作为一种创建类模式，在任何需要生成复杂对象的地方，都可以使用工厂方法模式。有一点需要注意的地方就是复杂对象适合使用工厂模式，而简单对象，特别是只需要通过 new 就可以完成创建的对象，无需使用工厂模式。如果使用工厂模式，就需要引入一个工厂类，会增加系统的复杂度。
     
     工厂方法模式分为三种：
     1.简单工厂方法模式：定义一个工厂类的实例方法根据传入类型判断返回的子类实例
     2.多工厂方法模式：定义多个工厂类的实例方法，与返回的子类一一对应
     3.静态工厂方法模式：将1或者2中的工厂类的实例方法定义为静态方法，OC中由实例方法改为类方法；不用在初始化工厂类实例，
     直接用工厂类的静态方法（OC中类方法）直接生成对应子类的实例
     
     例子：以生产手机为例；两种生产手机的厂商华为和苹果，华为只生产华为手机，苹果只生产苹果手机
     做法：抽象出来创建方法为一个协议方法；两个手机类型的累遵循该协议，并用该协议的方法创建手机；创建一个工厂类，添加一个类方法根据传入的类型初始化手机实例或者添加多个方法一一对应的返回子类对象；如果要添加一个生产手机的厂商小米，需要一个小米手机厂商的类，还需要自在工厂类中添加与小米对应的判断返回类型或者一个生成小米手机的方法
     */
    
    // 1.简单工厂方法模式
//    ZBYSimpleFactoryApplePhone * apple = [ZBYSimpleFactoryApplePhone phoneWithType:@"苹果"];
    ZBYSimpleFactoryApplePhone * apple = [ZBYSimpleFactoryApplePhone applePhone];
    ZBYSimpleFactoryHuaWeiPhone * huaWei = [ZBYSimpleFactoryHuaWeiPhone phoneWithType:@"华为"];
    ZBYSimpleFactoryPhone * nonePhone = [ZBYSimpleFactoryPhone phoneWithType:@"none"];
    NSLog(@"%@-%@-%@", apple.type, huaWei.type, nonePhone);
}

// MARK: - 抽象工厂模式

- (void)abstractFactoryMethodPattern {
    /**
     抽象工厂模式（Abstract Factory Pattern）是围绕一个超级工厂创建其他工厂。该超级工厂又称为其他工厂的工厂。这种类型的设计模式属于创建型模式，它提供了一种创建对象的最佳方式。
     在抽象工厂模式中，接口是负责创建一个相关对象的工厂，不需要显式指定它们的类。每个生成的工厂都能按照工厂模式提供对象。
     将工厂模式下的创建子类的方法抽象成抽象类，OC中定义为协议。
     
     意图：提供一个创建一系列相关或相互依赖对象的接口，而无需指定它们具体的类。
     主要解决：主要解决接口选择的问题。
     何时使用：系统的产品有多于一个的产品族，而系统只消费其中某一族的产品。
     如何解决：在一个产品族里面，定义多个产品。
     关键代码：在一个工厂里聚合多个同类产品。
     优点：当一个产品族中的多个对象被设计成一起工作时，它能保证客户端始终只使用同一个产品族中的对象。
     缺点：产品族扩展非常困难，要增加一个系列的某一产品，既要在抽象的 Creator 里加代码，又要在具体的里面加代码。
     使用场景： 1、QQ 换皮肤，一整套一起换。 2、生成不同操作系统的程序。
     注意事项：产品族难扩展，产品等级易扩展。
     
     例子：有一个类族，可以生成两种不同的类，其中颜色类包含红、绿、蓝，多边形类包含长方形、正方形、圆形；请用抽象工厂模式对其进行各个类的创建。
     做法：
        1.定义一个抽象类（OC中为协议），用于定义该类族的初始化方法；
        2.定义一个颜色类和一个多边形类，实现抽象类（OC中为协议）的方法；
        3.定义一个颜色类的抽象类（OC中为协议）和多边形类的抽象类（OC中为协议）；
        4.定义红、绿、蓝颜色实现颜色
     */
    
    ZBYAbstractFactory * red = [ZBYAbstractFactory redColor];
    ZBYAbstractFactory * square = [ZBYAbstractFactory squarePolygon];
    NSLog(@"%@-%@", red.class, square.class);
    
    /** OC中的类族：
     Class Clusters是抽象工厂模式在iOS下的一种实现，众多常用类，如NSString、NSArray、NSDictionary以及
     NSNumber都运作在这一模式下，它是接口简单性和扩展性的权衡体现，在我们完全不知情的情况下，偷偷隐藏了很多具体的实现类，只暴露出简单的接口。
     
     NSNumber举例来说，对于int，bool, unsigned int 等等数据类型，我们如何把它们封装成类的形式呢？ 通常情况下我们可能会想到，对于每一种数据类型独立封装成一个类，比如对于int 类型我们可以做一个NSInt的类，以此类推。这样想是正确的，但是，我们再来想想这样会有什么问题呢？当需要支持的数据类型越来越多时，这些类也会相应的越来越多，那么对于开发者来说，我们就需要记住越来越多的类。那么还有没有好的方案呢？哈哈，这个时候类簇就闪亮登场了。
     现在我们遇到的问题是，相似的类越来越多，对于开发者来说需要太多的记忆。显然，我们解决问题的方向肯定是朝着减少暴露给开发者的类的数量这个方向。让我们来回忆一下设计模式，抽象工场模式不就是干这个事情的嘛！
     对于NSNumber，我们使用一个NSNumber来作为int，bool，long等原生数据类型的工厂，对外定义一堆的工厂方法，在NSNumber内部则转调给相应的像NSInt这样的内部类去实现具体的逻辑。这样，我们遇到的问题就得到了完美的解决。
     */
    NSArray * obj = [NSArray alloc];
    id obj1 = [NSArray alloc];
    id obj2 = [NSMutableArray alloc];
    id obj22 = [NSMutableArray alloc];
    id obj3 = [obj1 init];
    id obj4 = [obj2 init];
    NSLog(@"%@",NSStringFromClass([obj class]));
    NSLog(@"%@",NSStringFromClass([obj1 class]));
    NSLog(@"%@",NSStringFromClass([obj2 class]));
    NSLog(@"%@",NSStringFromClass([obj3 class]));
    NSLog(@"%@",NSStringFromClass([obj4 class]));
    NSLog(@"%p-%p-%p-%p-%p-%p", obj, obj1, obj2, obj22, obj3, obj4);
    
    /**
     发现+alloc之后并非生成了我们期望的类实例，而是一个__NSPlaceholderArray的中间对象； 后面的-init或者-initWithxx等方法调用都是把消息发送给这个中间对象，再由它做工厂 生成真正的对象。
     （这里的__NSArrayI和__NSArrayM分别对应immutable和mutable。）
     
     obj obj1地址相同 obj2 obj22地址相同。
     那么猜测NSDictionary NSSet NSString等是一样的情况。
     */
    
    NSNumber * number = [NSNumber alloc];
    id number1 = [NSNumber alloc];
    id nmuber2 = [number initWithBool:YES];
    id nmuber3 = [number1 initWithInt:3];
    NSLog(@"%@",NSStringFromClass([number class]));
    NSLog(@"%@",NSStringFromClass([number1 class]));
    NSLog(@"%@",NSStringFromClass([nmuber2 class]));
    NSLog(@"%@",NSStringFromClass([nmuber3 class]));
}

// MARK: - 单例模式

- (void)singletonPattern {
    /**
     一个单一的类，该类负责创建自己的对象，同时确保只有单个对象被创建。这个类提供了一种访问其唯一的对象的方式，可以直接访问，不需要实例化该类的对象。
     条件：
     1、单例类只能有一个实例。
     2、单例类必须自己创建自己的唯一实例。
     3、单例类必须给所有其他对象提供这一实例。
     
     意图：保证一个类仅有一个实例，并提供一个访问它的全局访问点。
     主要解决：一个全局使用的类频繁地创建与销毁。
     何时使用：当您想控制实例数目，节省系统资源的时候。
     如何解决：判断系统是否已经有这个单例，如果有则返回，如果没有则创建。
     关键代码：构造函数是私有的。
     
     优点：
     1、在内存里只有一个实例，减少了内存的开销，尤其是频繁的创建和销毁实例（比如管理学院首页页面缓存）。
     2、避免对资源的多重占用（比如写文件操作）。
     
     缺点：没有接口，不能继承，与单一职责原则冲突，一个类应该只关心内部逻辑，而不关心外面怎么样来实例化。
     
     按照是否提前创建分为：懒汉式和饿汉式
     */
    
    // 1.懒汉式：在第一次使用前实例化单例对象
    ZBYLazySigleton * sigleton1 = [ZBYLazySigleton sharedInstance];
    ZBYLazySigleton * sigleton2 = [[ZBYLazySigleton alloc] init];
    NSLog(@"%@-%@", sigleton1, sigleton2);
}

// MARK: - 原型模式（Prototype Pattern）

- (void)prototypePattern {
    /**
     原型模式（Prototype Pattern）是用于创建重复的对象，同时又能保证性能；在OC中主要以NSCopying和
     NSMutableCopying协议体现。
     意图：用原型实例指定创建对象的种类，并且通过拷贝这些原型创建新的对象。
     主要解决：在运行期建立和删除原型。
     
     优点：
     1.简化对象的创建，把创建的过程封闭到对象的内部。
     2.由于复制的过程是封闭的，这样就降低了与客户端的耦合，提升了稳定性。
     3.由于创建的过程封闭在对象内部完成，只需修改对象内部一处，所有调用深复制的地方全部生效。
     主要解决：通过深拷贝来快速而方便的创建一个新对象。
     
     使用场景：
     1.需要创建的对象不依赖于具体的类型以及创建方式
     2.具体实例化的对象类型是在运行期决定的
     3.不同类型之间的差异紧紧是状态的组合
     4.类型创建复杂,例如类型有复杂的嵌套
     
     */
    ZBYPrototypePatternPerson * p = [ZBYPrototypePatternPerson new];
    ZBYPrototypePatternPerson * p2 = [p copy];
    NSLog(@"%p-%p-%p-%p", p, p2, p.name, p2.name);
    
    NSArray * a = [[NSArray alloc] init];
    NSArray * a1 = [a copy];
    NSArray * a2 = [a mutableCopy];
    NSLog(@"%p-%p-%p", a, a1, a2);
    NSLog(@"%@-%@-%@", a.class, a1.class, a2.class);
    
    NSMutableArray * ma = [[NSMutableArray alloc] init];
    NSMutableArray * ma1 = [a copy];
    NSMutableArray * ma2 = [a mutableCopy];
    NSLog(@"%p-%p-%p", ma, ma1, ma2);
    NSLog(@"%@-%@-%@", ma.class, ma1.class, ma2.class);
}

// MARK: -

- (void)adapterPattern {
    /**
     适配器模式（Adapter Pattern）
     将一个类的接口变换成客户端所期待的另一种接口，从而使原本因接口不匹配而无法在一起工作的两个类能够在一起工作。
     有时候也称包装样式或者包装(wrapper)。将一个类的接口转接成用户所期待的。一个适配使得因接口不兼容而不能在一起工作的类能在一起工作，做法是将类自己的接口包裹在一个已存在的类中。
     
     主要解决：主要解决在软件系统中，常常要将一些"现存的对象"放到新的环境中，而新环境要求的接口是现对象不能满足的。
     
     应用场景：
     1.我们在使用第三方的类库，或者说第三方的API的时候，我们通过适配器转换来满足现有系统的使用需求；
     2.你想使用现有的一些类的功能，但其接口不匹配你的要求，你又不想修改原始类的情况；
     3.需要建立一个可以重复使用的类，用于一些彼此关系不大的类，并易于扩展，以便于面对将来会出现的类；
     4.需要一个统一的输出接口，但是输入类型却不可预知。
     
     适配器设计模型主要由三个角色组成，分别是：
     适配器角色（adapter）-- 将已有接口转换成目标接口
     目标角色(target) -- 目标可以是具体的或抽象的类，也可以是接口
     源角色或被适配者(adaptee) -- 已有接口
     
     适配器类型可以分为三种：
     类适配器模式
     对象适配器模式
     接口适配器模式或缺省适配器模式
     
     注意事项：适配器不是在详细设计时添加的，而是解决正在服役的项目的问题。
     
     现实例子：
     1.读卡器是作为内存卡和笔记本之间的适配器。您将内存卡插入读卡器，再将读卡器插入笔记本，这样就可以通过笔记本
     来读取内存卡。
     2.我们通过下面的实例来演示适配器模式的使用。其中，音频播放器设备只能播放 mp3 文件，通过使用一个更高级的
     音频播放器来播放 mp4 文件。
     3.手机充电需要的电压一般为5V。国内家用供电为220V，而日本则是110V，针对不同地区的电压，我们需要一个适配器，
     这样才能对手机进行充电。
     */
    
    /**
     以第二个现实的例子进行适配器三种类型的实现
     */
    
    // 类适配器模式：通过继承被适配者来实现适配功能，即通过继承实现统一的接口或者方法
}

@end
