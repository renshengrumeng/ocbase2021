//
//  AppDelegate.h
//  OCBase2021
//
//  Created by 周保勇 on 2021/4/7.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow * window;


@end

