//
//  SceneDelegate.h
//  OCBase2021
//
//  Created by 周保勇 on 2021/4/7.
//

#import <UIKit/UIKit.h>

API_AVAILABLE(ios(13.0)) @interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

