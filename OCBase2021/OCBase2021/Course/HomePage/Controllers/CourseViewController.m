//
//  CourseViewController.m
//  OCBase2021
//
//  Created by 周保勇 on 2021/4/8.
//

#import "CourseViewController.h"
#import "ZBYDataStructureViewController.h"
#import "ZBYAlgorithmBasedViewController.h"
#import "ZBYAlgorithmCourseViewController.h"

@interface CourseViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, copy) NSArray * models;

@end

@implementation CourseViewController

- (NSArray *)models {
    if (_models == nil) {
        _models = @[
        @{@"title" : @"数据结构", @"subTitle" : @"数据结构"},
        @{@"title" : @"算法基础", @"subTitle" : @"算法基础"},
        @{@"title" : @"算法课程", @"subTitle" : @"代码随想录"}];
    }
    return _models;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITableView * tableView = [[UITableView alloc] initWithFrame:CGRectZero style:(UITableViewStylePlain)];
    tableView.delegate = self;
    tableView.dataSource = self;
    [self.view addSubview:tableView];
    
    tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];
}

// MARK: - UITableViewDelegate, UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.models.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString * identifier = @"123456";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.backgroundColor = [UIColor whiteColor];
        cell.textLabel.textColor = [UIColor blackColor];
        cell.detailTextLabel.textColor = [UIColor blackColor];
    }
    cell.textLabel.text = self.models[indexPath.row][@"title"];
    cell.detailTextLabel.text = self.models[indexPath.row][@"subTitle"];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSDictionary * info = self.models[indexPath.row];
    NSString * title = info[@"title"];
    if ([title containsString:@"数据结构"]) {
        ZBYDataStructureViewController * sturctVC = [ZBYDataStructureViewController new];
        sturctVC.title = title;
        [self.navigationController pushViewController:sturctVC animated:YES];
    }
    if ([title containsString:@"算法基础"]) {
        ZBYAlgorithmBasedViewController * algorithmVC = [ZBYAlgorithmBasedViewController new];
        algorithmVC.title = title;
        [self.navigationController pushViewController:algorithmVC animated:YES];
    }
    if ([title containsString:@"算法课程"]) {
        ZBYAlgorithmCourseViewController * courseVC = [ZBYAlgorithmCourseViewController new];
        courseVC.title = title;
        [self.navigationController pushViewController:courseVC animated:YES];
    }
}

@end
