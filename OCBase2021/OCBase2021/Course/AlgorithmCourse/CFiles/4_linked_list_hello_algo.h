//
//  4_linked_list_hello_algo.h
//  OCBase2021
//
//  Created by anniekids good on 2023/10/26.
//  单链表的实现

#ifndef __linked_list_hello_algo_h
#define __linked_list_hello_algo_h

#include <stdio.h>
#include <stdbool.h>

struct ListNode {
    int value;
    struct ListNode * next;
};

typedef struct ListNode LNode, *LNodePointer;

LNodePointer newLinkedList(void);

void deallocLinkedList(LNodePointer list);

/// 链表为空
bool isLinkedListEmpty(LNodePointer list);

/// 链表长度
int linkedListSize(LNodePointer list);

/// 查询指定索引所在节点的值：不存在返回-1
int indexNode(LNodePointer list, int index);

/// 查询指定值所在节点的索引值：不存在返回-1
int targetIndex(LNodePointer list, int target);

/// 头部插入
void topInsert(LNodePointer list, int value);

/// 尾部插入
void bottomInsert(LNodePointer list, int value);

/// 索引插入
void indexInsert(LNodePointer list, int index, int value);

/// 打印链表的值
void showLinkedListElement(LNodePointer list);

#endif /* __linked_list_hello_algo_h */
