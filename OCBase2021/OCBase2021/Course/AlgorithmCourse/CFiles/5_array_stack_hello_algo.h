//
//  5_array_stack_hello_algo.h
//  OCBase2021
//
//  Created by anniekids good on 2023/10/26.
//

#ifndef __array_stack_hello_algo_h
#define __array_stack_hello_algo_h

#include <stdio.h>
#include "4_array_hello_algo.h"

struct ArrayStack {
    DArrayPointer nodes;
    int size;
};

typedef struct ArrayStack AStack, *ASPointer;

#endif /* __array_stack_hello_algo_h */
