//
//  5_array_stack_hello_algo.c
//  OCBase2021
//
//  Created by anniekids good on 2023/10/26.
//

#include "5_array_stack_hello_algo.h"
#include <malloc/_malloc.h>
#include <stdbool.h>
#include <limits.h>
#include "4_array_hello_algo.h"

ASPointer newArrayStack(void) {
    DArrayPointer nodes = newDynimicArray(10);
    ASPointer stack = (ASPointer)malloc(sizeof(AStack));
    stack->nodes = nodes;
    stack->size = 0;
    return stack;
}

void deallocArrayStack(ASPointer stack) {
    delDynimicArray(stack->nodes);
    free(stack);
}

int arrayStackSize(ASPointer stack) {
    return stack->size;
}

bool isArrayStackEmpty(ASPointer stack) {
    return stack->size == 0;
}

void arrayStackPush(ASPointer stack, int value) {
    add(stack->nodes, value);
    stack->size++;
}

int arrayStackPop(ASPointer stack) {
    if (isArrayStackEmpty(stack)) {
        return INT_MAX;
    }
    int value = removeIndex(stack->nodes, stack->nodes->size - 1);
    stack->size--;
    return value;
}
