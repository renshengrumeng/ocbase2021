//
//  4_array_hello_algo.c
//  OCBase2021
//
//  Created by anniekids good on 2023/10/12.
//

#include "4_array_hello_algo.h"
#include <malloc/malloc.h>
#include <malloc/_malloc.h>
#include <assert.h>

/// 构造函数
DArrayPointer newDynimicArray(int capacity) {
    DArrayPointer dap = (DArrayPointer)malloc(sizeof(DArray));
    dap->capacity = capacity;
    dap->arr = (int *)malloc(sizeof(int) * dap->capacity);
    dap->size = 0;
    dap->factor = 2;
    return dap;
}

/// 析构函数
void delDynimicArray(DArrayPointer array) {
    free(array->arr);
    free(array);
}

/// 获取动态数组长度
int size(DArrayPointer array) {
    return array->size;
}

/// 获取动态数组容量
int capacity(DArrayPointer array) {
    return array->capacity;
}

/// 下标访问元素
int get(DArrayPointer array, int index) {
    assert(index > 0 && array->size > index);
    return array->arr[index];
}

/// 更新或者设置元素
void set(DArrayPointer array, int index, int value) {
    if (index < 0) {
        printf("下标不能小于零\n");
    }
    if (index > array->size - 1) {
        printf("下标不能大于现有数组元素最大的下标\n");
    }
    array->arr[index] = value;
}

/// 扩容：2倍扩容
void extendCapacity(DArrayPointer array) {
    int currentCapacity = array->capacity * 2;
    int * currentArray = (int *)malloc(sizeof(int) * currentCapacity);
    for (int index = 0; index < array->size; index++) {
        currentArray[index] = array->arr[index];
    }
    array->capacity = currentCapacity;
    free(array->arr);
    array->arr = currentArray;
}

/// 尾部添加元素
void add(DArrayPointer array, int value) {
    int index = array->size;
    int currentSize = array->size + 1;
    if (array->capacity < currentSize) {
        extendCapacity(array);
    }
    array->arr[index] = value;
    array->size = currentSize;
}

/// 中间插入元素
void insert(DArrayPointer array, int index, int value) {
    if (index < 0) {
        printf("下标不能小于零\n");
    }
    if (index > array->size - 1) {
        printf("下标不能大于现有数组元素最大的下标\n");
    }
    int currentSize = array->size + 1;
    if (array->capacity < currentSize) {
        extendCapacity(array);
    }
    for (int i = currentSize - 1; i > index; i--) {
        array->arr[i] = array->arr[i - 1];
    }
    array->arr[index] = value;
    array->size = currentSize;
}

/// 删除元素：stdio.h 占用了 remove 关键词
int removeIndex(DArrayPointer array, int index) {
    if (index < 0) {
        printf("下标不能小于零\n");
    }
    if (index > array->size - 1) {
        printf("下标不能大于现有数组元素最大的下标\n");
    }
    int value = array->arr[index];
    for (int i = index; i < array->size; i++) {
        array->arr[i] = array->arr[i + 1];
    }
    array->size--;
    return value;
}

/// 打印数组中的值
void showArray(DArrayPointer array) {
    if (array->size <= 0) {
        printf("数组为空\n");
        return;
    }
    for (int i = 0; i < array->size; i++) {
        printf("%d, ", array->arr[i]);
    }
    printf("\n");
}
