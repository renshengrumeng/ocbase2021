//
//  5_linked_list_stack_hello_algo.h
//  OCBase2021
//
//  Created by anniekids good on 2023/10/24.
//

#ifndef __linked_list_stack_hello_algo_h
#define __linked_list_stack_hello_algo_h

#include <stdio.h>
#include <stdbool.h>
#include "4_linked_list_hello_algo.h"

struct LinkedListStack {
    LNodePointer top;   // 栈顶：头节点
    int size;
};

typedef struct LinkedListStack LLStack, *LLStackPointer;

LLStackPointer newLinkedListStack(void);

void deallocLinkedListStack(LLStackPointer stack);

int linkedListStackSize(LLStackPointer stack);

bool isLinkedListStackEmpty(LLStackPointer stack);

int linkedListStackTopValue(LLStackPointer stack);

void linkedListStackPush(LLStackPointer stack, int value);

int linkedListStackPop(LLStackPointer stack);

#endif /* __linked_list_stack_hello_algo_h */
