//
//  4_linked_list_hello_algo.c
//  OCBase2021
//
//  Created by anniekids good on 2023/10/26.
//  头节点不存储数据

#include "4_linked_list_hello_algo.h"
#include <malloc/_malloc.h>
#include <limits.h>

LNodePointer newLinkedList(void) {
    LNodePointer topNode = (LNodePointer)malloc(sizeof(LNode));
    topNode->value = INT_MAX;
    topNode->next = NULL;
    return topNode;
}

void deallocLinkedList(LNodePointer list) {
    while (list != NULL) {
        LNodePointer node = list;
        list = node->next;
        node->next = NULL;
        free(node);
    }
}

/// 链表为空
bool isLinkedListEmpty(LNodePointer list) {
    return list->next == NULL;
}

/// 链表长度
int linkedListSize(LNodePointer list) {
    int size = 0;
    LNodePointer node = list->next;
    while (node != NULL) {
        size++;
        node = node->next;
    }
    return size;
}

/// 查询指定索引所在节点的值：不存在返回-1，时间复杂度O(1)~O(n)
int indexNode(LNodePointer list, int index) {
    LNodePointer node = list->next;
    while (index > 0 && node != NULL) {
        index--;
        node = node->next;
    }
    return node != NULL ? node->value : -1;
}

/// 查询指定值所在节点的索引值：不存在返回-1，时间复杂度O(1)~O(n)
int targetIndex(LNodePointer list, int target) {
    int index = 0;
    while (list->next != NULL) {
        if (list->next->value == target) {
            return index;
        }
        index++;
        list = list->next;
    }
    return -1;
}

/// 头部插入：时间复杂度O(1)
void topInsert(LNodePointer list, int value) {
    LNodePointer node = (LNodePointer)malloc(sizeof(LNode));
    node->value = value;
    node->next = list->next;
    list->next = node;
}

/// 尾部插入：时间复杂度O(n)
void bottomInsert(LNodePointer list, int value) {
    LNodePointer node = (LNodePointer)malloc(sizeof(LNode));
    node->value = value;
    node->next = NULL;
    while (list->next != NULL) {
        list = list->next;
    }
    list->next = node;
}

/// 中间下标插入：时间复杂度O(1)~O(n)
/// - Parameters:
///   - list: 头节点
///   - index: 插入下标
///   - value: 插入值
void indexInsert(LNodePointer list, int index, int value) {
    if (index < 0) {
        printf("索引值应该为大于等于0的数\n");
        return;
    }
    LNodePointer insert = (LNodePointer)malloc(sizeof(LNode));
    insert->value = value;
    insert->next = NULL;
    
    int tempIndex = 0;
    LNodePointer pre = list;
    LNodePointer node = list->next;
    while (node != NULL) {
        if (tempIndex == index) {
            insert->next = node;
            pre->next = insert;
            return;
        }
        tempIndex++;
        pre = node;
        node = node->next;
    }
    printf("插入位置大于链表的长度，进行尾部插入\n");
    pre->next = insert;
}

/// 在指定节点后插入：时间复杂度O(1)
void insertNode(LNodePointer node, LNodePointer insert) {
    insert->next = node->next;
    node->next = insert;
}

/// 头部删除节点：时间复杂度O(1)
int topRemove(LNodePointer list) {
    if (isLinkedListEmpty(list)) {
        printf("链表为空\n");
        return -1;
    }
    else {
        int value = 0;
        LNodePointer top = list->next;
        value = top->value;
        list->next = top->next;
        top->next = NULL;
        free(top);
        return value;
    }
}

/// 尾部删除节点：时间复杂度O(n)
int bottomRemove(LNodePointer list) {
    if (isLinkedListEmpty(list)) {
        printf("链表为空\n");
        return -1;
    }
    else {
        int value = 0;
        LNodePointer pre = list;
        LNodePointer node = list->next;
        while (node != NULL) {
            pre = node;
            node = node->next;
        }
        pre->next = NULL;
        value = node->value;
        free(node);
        return value;
    }
}

/// 删除指定索引节点：时间复杂度O(1)~O(n)
int indexRemove(LNodePointer list, int index) {
    if (isLinkedListEmpty(list)) {
        printf("链表为空\n");
        return -1;
    }
    else {
        int value = 0;
        LNodePointer pre = list;
        LNodePointer node = list->next;
        while (node != NULL && index > 0) {
            pre = node;
            node = node->next;
            index--;
        }
        if (node == NULL) {
            printf("删除索引超出链表长度\n");
            return -1;
        }
        pre->next = node->next;
        node->next = NULL;
        value = node->value;
        free(node);
        return value;
    }
}

/// 删除指定节点的首个节点：时间复杂度O(1)
int removeNode(LNodePointer node) {
    if (node->next == NULL) {
        printf("指定节点为最后节点，无法删除\n");
        return -1;
    }
    else {
        LNodePointer target = node->next;
        node->next = target->next;
        int value = 0;
        value = target->value;
        target->next = NULL;
        free(target);
        return value;
    }
}

void showLinkedListElement(LNodePointer list) {
    if (list->next == NULL) {
        printf("链表为空\n");
        return;
    }
    LNodePointer node = list->next;
    while (node != NULL) {
        if (node->next == NULL) {
            printf("%d", node->value);
        }
        else {
            printf("%d, ", node->value);
        }
        node = node->next;
    }
    printf("\n");
}
