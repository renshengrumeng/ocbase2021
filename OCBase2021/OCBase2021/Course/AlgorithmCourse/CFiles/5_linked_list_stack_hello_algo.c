//
//  5_linked_list_stack_hello_algo.c
//  OCBase2021
//
//  Created by anniekids good on 2023/10/24.
//  头插法实现：链表的头部插入和删除节点

#include "5_linked_list_stack_hello_algo.h"
#include <malloc/malloc.h>
#include <malloc/_malloc.h>
#include <limits.h>

LLStackPointer newLinkedListStack(void) {
    LLStackPointer stack = (LLStackPointer)malloc(sizeof(LLStack));
    stack->top = NULL;
    stack->size = 0;
    return stack;
}

void deallocLinkedListStack(LLStackPointer stack) {
    while (stack->top != NULL) {
        LNodePointer top = stack->top;
        stack->top = top->next;
        top->next = NULL;
        free(top);
    }
    free(stack);
}

int linkedListStackSize(LLStackPointer stack) {
    return stack->size;
}

bool isLinkedListStackEmpty(LLStackPointer stack) {
    return (stack->size == 0 || stack->top == NULL);
}

int linkedListStackTopValue(LLStackPointer stack) {
    if (isLinkedListStackEmpty(stack)) {
        return INT_MAX;
    }
    return stack->top->value;
}

void linkedListStackPush(LLStackPointer stack, int value) {
    LNodePointer node = (LNodePointer)malloc(sizeof(LNode));
    node->value = value;
    node->next = stack->top;
    stack->top = node;
    stack->size++;
}

int linkedListStackPop(LLStackPointer stack) {
    if (stack->size == 0 || stack->top == NULL) {
        printf("空栈");
        return INT_MAX;
    }
    LNodePointer node = stack->top;
    int value = node->value;
    stack->top = node->next;
    stack->size--;
    free(node);
    return value;
}
