//
//  4_array_hello_algo.h
//  OCBase2021
//
//  Created by anniekids good on 2023/10/12.
//

#ifndef __array_hello_algo_h
#define __array_hello_algo_h

#include <stdio.h>

struct DynimicArray {   // 动态数组实现
    int * arr;  //  存储数组指针
    int capacity;   // 动态数组容量
    int size;       // 动态数组已经存储元素的数量
    int factor;     // 扩容因子
};

typedef struct DynimicArray DArray, * DArrayPointer;

/// 构造函数
DArrayPointer newDynimicArray(int capacity);

/// 析构函数
void delDynimicArray(DArrayPointer array);

/// 获取动态数组长度
int size(DArrayPointer array);

/// 获取动态数组容量
int capacity(DArrayPointer array);

/// 下标访问元素
int get(DArrayPointer array, int index);

/// 更新或者设置元素
void set(DArrayPointer array, int index, int value);

/// 尾部添加元素
void add(DArrayPointer array, int value);

/// 中间插入元素
void insert(DArrayPointer array, int index, int value);

/// 删除元素：stdio.h 占用了 remove 关键词
int removeIndex(DArrayPointer array, int index);


/// 打印数组中的值
void showArray(DArrayPointer array);

#endif /* __array_hello_algo_h */
