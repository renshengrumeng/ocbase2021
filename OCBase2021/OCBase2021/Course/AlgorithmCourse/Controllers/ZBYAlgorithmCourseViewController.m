//
//  ZBYAlgorithmCourseViewController.m
//  OCBase2021
//
//  Created by anniekids good on 2023/9/28.
//

#import "ZBYAlgorithmCourseViewController.h"
#import "4_array_hello_algo.h"
#import "4_linked_list_hello_algo.h"

@interface ZBYAlgorithmCourseViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, copy) NSArray * models;

@end

@implementation ZBYAlgorithmCourseViewController

- (NSArray *)models {
    if(_models == nil) {
        _models = @[
            @{@"title" : @"算法课程-hello-algo", @"subTitle" : @"算法概念相关见文档hello-algo-1.0.0b5-zh-cpp.pdf"},
            @{@"title" : @"数组与链表", @"subTitle" : @"数组与链表的概念和实现"},
            @{@"title" : @"动态数组-列表", @"subTitle" : @"动态数组的实现"},
            @{@"title" : @"栈与队列", @"subTitle" : @"概念和实现"},
            @{@"title" : @"算法课程-代码随想录", @"subTitle" : @"代码随想录"}];
    }
    return _models;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITableView * tableView = [[UITableView alloc] initWithFrame:CGRectZero style:(UITableViewStylePlain)];
    tableView.delegate = self;
    tableView.dataSource = self;
    [self.view addSubview:tableView];
    
    tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];
}

// MARK: - UITableViewDelegate, UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.models.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString * identifier = @"123456";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:identifier];
        cell.backgroundColor = [UIColor whiteColor];
        cell.textLabel.textColor = [UIColor blackColor];
        cell.detailTextLabel.textColor = [UIColor blackColor];
    }
    cell.textLabel.text = self.models[indexPath.row][@"title"];
    cell.detailTextLabel.text = self.models[indexPath.row][@"subTitle"];
    cell.detailTextLabel.textColor = [UIColor colorWithHex:@"#DC143C"];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSDictionary * info = self.models[indexPath.row];
    NSString * title = info[@"title"];
    if ([title containsString:@"算法课程-hello-algo"]) {
        // 刷题：https://programmercarl.com/
        // hallo算法：https://www.hello-algo.com/
    }
    if ([title containsString:@"数组与链表"]) {
        [self arrayAndLinkedList];
    }
    if ([title containsString:@"栈与队列"]) {
        [self stackAndQueue];
    }
}

// MARK: - 数组与链表

- (void)arrayAndLinkedList {
//    [self dynimicArray];
    [self linkedList];
}

/// 动态数组实现
- (void)dynimicArray {
    // 动态数组的实现
    DArrayPointer array = newDynimicArray(2);
    NSLog(@"size = %d, capacity = %d", size(array), capacity(array));
    showArray(array);
    add(array, 10);
    NSLog(@"size = %d, capacity = %d", size(array), capacity(array));
    showArray(array);
    insert(array, 0, 9);
    NSLog(@"size = %d, capacity = %d", size(array), capacity(array));
    showArray(array);
    insert(array, 1, 5);
    NSLog(@"size = %d, capacity = %d", size(array), capacity(array));
    showArray(array);
    removeIndex(array, 1);
    NSLog(@"size = %d, capacity = %d", size(array), capacity(array));
    showArray(array);
}

/// 单链表的实现
- (void)linkedList {
    LNodePointer list = newLinkedList();
    showLinkedListElement(list);
    topInsert(list, 10);
    topInsert(list, 9);
    showLinkedListElement(list);
    bottomInsert(list, 8);
    showLinkedListElement(list);
    indexInsert(list, 1, 7);
    showLinkedListElement(list);
    indexInsert(list, 10, 5);
    showLinkedListElement(list);
    printf("%d\n", indexNode(list, 2));
    printf("%d\n", indexNode(list, 5));
    printf("%d\n", targetIndex(list, 10));
    printf("%d\n", targetIndex(list, 11));
    printf("size = %d\n", linkedListSize(list));
    deallocLinkedList(list);
}

// MARK: - 栈与队列

- (void)stackAndQueue {
    [self linkedStackAndArrayStack];
//    [self linkedList];
}

- (void)linkedStackAndArrayStack {
    
}

@end
