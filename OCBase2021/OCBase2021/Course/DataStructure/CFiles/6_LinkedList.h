//
//  6_LinkedList.h
//  CBase
//
//  Created by 周保勇 on 2021/1/21.
//

#ifndef __LinkedList_h
#define __LinkedList_h

#include <stdio.h>
#include <stdbool.h>

struct LinkedList {
    int data;   // 链表节点存储数据域
    struct LinkedList * pNext;  // 链表节点指针域
};

typedef struct LinkedList * PList, List;

PList initLinkedList(int length);

PList getListElement(PList ph, int index);

void showLinkedList(PList pl);

int listLength(PList ph);

bool isEmptyList(PList ph);

bool insertListElement(PList ph, int index, int element);

bool removeListElement(PList ph, int index, int * elementValue);

void sortList(PList ph, bool isAsc);

#endif /* __LinkedList_h */
