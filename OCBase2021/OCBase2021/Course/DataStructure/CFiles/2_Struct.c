//
//  2_Struct.c
//  CBase
//
//  Created by 周保勇 on 2021/1/18.
//

#include "2_Struct.h"
#include <string.h>

struct Student {
    int sid;
    char name[200];
    int age;
};

void setStudent(struct Student * pst);

void getStudent(struct Student * pst);

void structVar() {
    struct Student st = {1000, "周保勇", 32};
    struct Student st2;
    st2.sid = 10001;
    strcpy(st2.name, "卢布");
    st2.age = 23;
    
    struct Student * pst = &st;
    pst->sid = 10002;  // pst->age 等价于 (*pst).age 等价于 st.age
    strcpy(pst->name, "貂蝉");
    pst->age = 18;
    
    struct Student st3;
//    struct Student * pst2 = &st3;
    setStudent(&st3);
    getStudent(&st3);
}

void setStudent(struct Student * pst) {
    pst->sid = 10003;
    strcpy(pst->name, "诸葛亮");
    pst->age = 24;
}

void getStudent(struct Student * pst) {
    printf("sid = %d, name = %s, age = %d\n", pst->sid, pst->name, pst->age);
}
