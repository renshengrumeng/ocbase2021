//
//  13_Sorting.h
//  CBase
//
//  Created by 周保勇 on 2021/2/23.
//

#ifndef _2_Sorting_h
#define _2_Sorting_h

#include <stdio.h>

void showSortArray(int a[], int length);

int * bubbleSort(int a[], int length);

int * selectionSort(int a[], int length);

int * insertionSort(int a[], int length);

int * quickSort(int a[], int length);

#endif /* _2_Sorting_h */
