//
//  9_StaticStack.h
//  CBase
//
//  Created by 周保勇 on 2021/1/27.
//

#ifndef __StaticStack_h
#define __StaticStack_h

#include <stdio.h>
#include <stdbool.h>

struct StaticStack {
    /// 容器指针；以整形数组为例
    int * containerPointer;
    /// 顶部元素指针
    int * topPointer;
    /// 尾部元素指针
    int * bottomPointer;
    /// 容纳元素的最大个数
    int capacity;
    /// 已经存入的元素个数
    int length;
};

typedef struct StaticStack SStack, * SStackPointer;

SStackPointer initStaticStack(int capacity);

bool isEmptyStaticStack(SStackPointer sp);

bool isFullStaticStack(SStackPointer sp);

void showStaticStack(SStackPointer sp);

bool pushStaticStack(SStackPointer sp, int element);

bool popStaticStack(SStackPointer sp, int * elementPointer);

void clearStaticStack(SStackPointer sp);

void freeStaticStack(SStackPointer sp);

#endif /* __StaticStack_h */
