//
//  13_Sorting.c
//  CBase
//
//  Created by 周保勇 on 2021/2/23.
//  排序

#include "13_Sorting.h"

void showSortArray(int a[], int length) {
    if (a == NULL || length == 0) {
        printf("数组为空\n");
        return;
    }
    for (int i = 0; i < length; i++) {
        printf("%d ", a[i]);
    }
    printf("\n");
}

/// 冒泡排序：拿第一个元素和其它元素依次比较，
/// @param a 数组
int * bubbleSort(int a[], int length) {
    for (int i = 0; i < length; i++) {
        for (int j = i + 1; j < length - (i); j++) {
            int temp = a[j];
            if (a[i] > a[j]) {
                a[j] = a[i];
                a[i] = temp;
            }
        }
    }
    return a;
}

/// 选择排序：选择数组中的最小值，放到第一位置；选择选择其它元素中次最小值，放到底二个位置；依次进行。
/// @param a 数组
int * selectionSort(int a[], int length) {
    int min, j, temp;
    for (int i = 0; i < length; i++) {
        for (min = i, j = i + 1; j < length; j++) { // 冒泡获取最小索引值：可以用其他排序方法
            if (a[min] > a[j]) {
                min = j;
            }
        }
        if (min != i) {
            temp = a[min];
            a[min] = a[i];
            a[i] = temp;
        }
    }
    return a;
}

/// 插入排序：将第二个元素插入第一个元素中，排序；将第三个元素插入前两个元素中，排序；依次进行。
/// @param a 数组
int * insertionSort(int a[], int length) {
    for (int i = 1; i < length; i++) {
        if(a[i] < a[i-1]) { // 若第i个元素大于 i-1 元素则直接插入；反之，需要找到适当的插入位置后在插入。
            int j = i-1;
            int x = a[i];
            while(j > -1 && x < a[j]){ // 采用顺序查找方式找到插入的位置，在查找的同时，将数组中的元素进行后移操作，给插入元素腾出空间
                a[j + 1] = a[j];
                j--;
            }
            a[j + 1] = x;      //插入到正确位置
        }
    }
    return a;
}

/// 快速排序（折半查找、二分法排序）获取元素的下标
/// @param a 数组
/// @param low 最低下标
/// @param hight 最高下标
int quickSortElementIndex(int a[], int low, int hight) {
    int temp = a[low];
    while (low < hight) {
        while (low < hight && a[hight] >= temp) {
            hight--;
        }
        a[low] = a[hight];
        while (low < hight && a[low] <= temp) {
            low++;
        }
        a[hight] = a[low];
    }
    a[low] = temp;
    return low;
}

/// 快速排序
/// @param a 数组
/// @param low 最低下标
/// @param hight 最高下标
void quickSortSub(int a[], int low, int hight) {
    if (low < hight) {
        int eIndex = quickSortElementIndex(a, low, hight);
        quickSortSub(a, low, eIndex - 1);
        quickSortSub(a, eIndex + 1, hight);
    }
}

/// 快速排序：又叫折半查找、二分法排序
/// @param a 数组
int * quickSort(int a[], int length) {
    quickSortSub(a, 0, length - 1);
    return a;
}
