//
//  10_StaticLoopQueue.h
//  CBase
//
//  Created by 周保勇 on 2021/1/28.
//

#ifndef _0_StaticLoopQueue_h
#define _0_StaticLoopQueue_h

#include <stdio.h>
#include <stdbool.h>

/// 单向静态循环队列：初始化时，对容量固定；出队和入队操作不会释放内存，对其容量没有影响。
struct StaticLoopQueue {
    /// 容器指针；以整形数组为例
    int * containerPointer;
    /// 队列头部元素索引
    int frontIndex;
    /// 队列尾部元素索引
    int rearIndex;
    /// 队列的容量
    int capacity;
    /// 队列的已有元素的长度
    int length;
};

typedef struct StaticLoopQueue SLQueue, * SLQueuePointer;

SLQueuePointer initStaticLoopQueue(int capacity);

bool isNULLStaticLoopQueue(SLQueuePointer qp);

bool isEmptyStaticLoopQueue(SLQueuePointer qp);

void showStaticLoopQueue(SLQueuePointer qp);

bool enterStaticLoopQueue(SLQueuePointer qp, int element);

bool outStaticLoopQueue(SLQueuePointer qp, int * elementPointer);

void clearStaticLoopQueue(SLQueuePointer qp);

void freeStaticLoopQueue(SLQueuePointer qp);

#endif /* _0_StaticLoopQueue_h */
