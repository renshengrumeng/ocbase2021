//
//  11_DynamicQueue.c
//  CBase
//
//  Created by 周保勇 on 2021/1/29.
//  单向动态循环队列：以单向链表为基础进行实现

#include "11_DynamicQueue.h"
#include <malloc/_malloc.h>

DQueuePointer initDynamicQueue() {
    DQueuePointer qp = (DQueuePointer)malloc(sizeof(DQueue));
    DQueueNodePointer qNodeP = (DQueueNodePointer)malloc(sizeof(DQueueNode));
    if (qp == NULL || qNodeP == NULL) {
        printf("队列内存分配失败或者队列头节点空间分配失败\n");
        return NULL;
    }
    qNodeP->nextPointer = NULL;
    qp->frontPoint = qNodeP;
    qp->rearPoint = qNodeP;
    return qp;
}

bool isNULLDynamicQueue(DQueuePointer qp) {
    if (qp == NULL) {
        printf("队列内存分配失败或者队列头节点空间分配失败\n");
        return true;
    }
    return false;
}

bool isEmptyDynamicQueue(DQueuePointer qp) {
    if (isNULLDynamicQueue(qp)) {
        return true;
    }
    return qp->frontPoint == qp->rearPoint;
}

int lengthDynamicQueue(DQueuePointer qp) {
    if (isEmptyDynamicQueue(qp)) {
        return 0;
    }
    DQueueNodePointer tempNodePointer = qp->frontPoint;
    int length = 0;
    while (tempNodePointer->nextPointer != NULL) {
        length++;
        tempNodePointer = tempNodePointer->nextPointer;
    }
    return length;
}

void showDynamicQueue(DQueuePointer qp) {
    if (isEmptyDynamicQueue(qp)) {
        printf("空队列\n");
    }
    DQueueNodePointer nodePointer = qp->frontPoint;
    while (nodePointer->nextPointer != NULL) {
        printf("%d ", nodePointer->nextPointer->data);
        nodePointer = nodePointer->nextPointer;
    }
    printf("\n");
}

bool pushDynamicQueue(DQueuePointer qp, int element) {
    DQueueNodePointer newNode = (DQueueNodePointer)malloc(sizeof(DQueueNode));
    if (newNode == NULL) {
        printf("入队节点空间分配失败\n");
        return false;
    }
    newNode->nextPointer = NULL;
    newNode->data = element;
    qp->rearPoint->nextPointer = newNode;
    qp->rearPoint = newNode;
    return true;
}

bool popDynamicQueue(DQueuePointer qp, int * elementPointer) {
    if (isEmptyDynamicQueue(qp)) {
        printf("空队列无法出队\n");
        return false;
    }
    DQueueNodePointer nodePointer = qp->frontPoint->nextPointer;
    *elementPointer = nodePointer->data;
    qp->frontPoint->nextPointer = nodePointer->nextPointer;
    if (nodePointer == qp->rearPoint) {
        qp->rearPoint = qp->frontPoint;
    }
    free(nodePointer);
    nodePointer = NULL;
    return true;
}

void freeDynamicQueue(DQueuePointer qp) {
    DQueueNodePointer temp;
    while (qp->frontPoint->nextPointer != NULL) {
        temp = qp->frontPoint->nextPointer;
        qp->frontPoint->nextPointer = temp->nextPointer;
        if (temp == qp->rearPoint) {
            qp->rearPoint = qp->frontPoint;
        }
        temp->nextPointer = NULL;
        free(temp);
        temp = NULL;
    }
    free(qp->frontPoint);
    qp->frontPoint = NULL;
    qp->rearPoint = NULL;
    free(qp);
    qp = NULL;
}
