//
//  1_Pointer.c
//  OCBase2021
//
//  Created by anniekids good on 2023/10/8.
//

#include "1_Pointer.h"

/// 传值函数
/// @param i 形参为传入变量的值
void someF(int i) {
    i = 20;
}

/// 传址函数
/// @param p 形参为传入变量的地址
void someP(int * p) {
    *p = 30;
}

/// 普通指针变量
void varP(void) {
    int i = 10;
    int * p = &i;
    // *p 等价于 i；p 等价于 &i；&*p 等价于 p或者&i
    printf("i = %d, p = %p, &*P = %p\n", i, p, &*p);
    someF(i);
    printf("传值函数：%d\n", i);
    someP(&i);
    printf("传址函数：%d\n", i);
}

/// 打印整形数组的每个元素的指针地址
/// @param p 数组地址
/// @param length 数组长度
void printArrayElementPoint(int * p, int length) {
    for(int i = 0; i < length; i++) {
        printf("%p\n", p + i);
    }
}

/// 打印整形数组的每个元素的值
/// @param p 数组地址
/// @param length 数组长度
void printArrayElement(int * p, int length) {
    for(int i = 0; i < length; i++) {
        printf("%d\n", p[i]);
    }
}

/// 修改数组中某个元素的值
/// @param p 数组地址
/// @param index 元素索引
void refreshArray(int * p, int index) {
    p[index] = 50;
}

/// 数组指针变量
void arrayP(void) {
    int a[5] = {1, 2, 3, 4, 5};
    printArrayElementPoint(a, 5);   // a是数组的首地址
    printArrayElement(a, 5);
    refreshArray(a, 2);
    printf("%d\n", a[2]);   // a[i] 等价于 *(a+i)
    printf("%d\n", *(a+2));
}
