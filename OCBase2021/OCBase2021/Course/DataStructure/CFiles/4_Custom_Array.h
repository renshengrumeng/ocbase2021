//
//  4_Custom_Array.h
//  CBase
//
//  Created by 周保勇 on 2021/1/20.
//

#ifndef __Custom_Array_h
#define __Custom_Array_h

#include <stdio.h>
#include <stdbool.h>

struct Array {
    int * pBase;    // 数组存储空间的首地址
    int length;     // 数组的能够存储的元素个数
    int count;      // 数组已经存储的元素个数
    int increaseFactor; // 数组增长因子：数组每次增加的长度
};

void initArray(struct Array *, int);

struct Array * initArrayPointer(int length);

/// 判断数组是否为空
bool isEmpty(struct Array *);

/// 判断数组是否已满
bool isFull(struct Array *);

/// 打印数组中的元素
void show(struct Array *);

/// 末尾追加元素
bool append(struct Array *, int);

/// 在指定索引插入元素
bool insertArray(struct Array *, int, int);

/// 删除指定索引的元素，并返回该元素的值
bool removeElement(struct Array * array, int index, int * elementValue);

/// 数组排序：isAsc为true为升序，false为降序
void sortArray(struct Array * array, bool isAsc);

/// 倒置数组
void inversionArray(struct Array * array);

/// 释放存储数据的内存
void freeArrayData(struct Array * array);

/// 释放存储数据的内存和结构体的内存
void freeArray(struct Array * array);

#endif /* __Custom_Array_h */
