//
//  9_StaticStack.c
//  CBase
//
//  Created by 周保勇 on 2021/1/27.
//  数组实现静态栈：栈底的元素为空

#include "9_StaticStack.h"
#include <malloc/_malloc.h>

SStackPointer initStaticStack(int capacity) {
    if (capacity <= 0) {
        printf("容量不能小于0\n");
        return NULL;
    }
    SStackPointer sp = (SStackPointer)malloc(sizeof(SStack));
    sp->containerPointer = (int *)malloc(sizeof(int) * (capacity + 1));
    if (sp == NULL || sp->containerPointer == NULL) {
        printf("分配内存出错\n");
        return NULL;
    }
    sp->bottomPointer = sp->containerPointer;
    sp->topPointer = sp->containerPointer;
    sp->capacity = capacity;
    sp->length = 0;
    return sp;
}

/// 该栈是否有创建成功：true失败，false成功
bool isNULLStaticStack(SStackPointer sp) {
    if (sp == NULL) {
        printf("栈内存无法分陪的空栈\n");
        return true;
    }
    if (sp->containerPointer == NULL) {
        printf("栈存储元素的内存无法分陪的空栈\n");
        free(sp);
        sp = NULL;
        return true;
    }
    return false;
}

/// 该栈是否为空：true为空，false为有元素
bool isEmptyStaticStack(SStackPointer sp) {
    if (isNULLStaticStack(sp)) {
        return true;
    }
    return sp->topPointer == sp->bottomPointer;
}

bool isFullStaticStack(SStackPointer sp) {
    if (isNULLStaticStack(sp)) {
        return true;
    }
    return sp->topPointer == (sp->containerPointer + sp->capacity);
}

void showStaticStack(SStackPointer sp) {
    if (isEmptyStaticStack(sp)) {
        return;
    }
    long offset = (sp->topPointer - sp->bottomPointer);
    for (long i = 1; i <= offset; i++) {
        printf("%d ", *(sp->containerPointer + i));
    }
//    while (p != sp->bottomPointer) {
//        printf("%d ", *p);
//        p = (p - 1);
//    }
    printf("\n");
    return;
}

int * getStaticStackElement(SStackPointer sp, int index) {
    if (index < 0 || (index + 1 > sp->length)) {
        printf("索引所在元素不存在\n");
        return NULL;
    }
    return (sp->containerPointer + index);
}

bool pushStaticStack(SStackPointer sp, int element) {
    if (sp == NULL || sp->containerPointer == NULL) {
        printf("栈指针为空或者栈分配存储内存出错\n");
        return false;
    }
    if (isFullStaticStack(sp)) {
        printf("栈已满\n");
        return false;
    }
    (sp->length)++;
    sp->containerPointer[sp->length] = element;
    sp->topPointer = ((sp->containerPointer) + sp->length);
    return true;
}

bool popStaticStack(SStackPointer sp, int * elementPointer) {
    if (isEmptyStaticStack(sp)) {
        printf("栈已空\n");
        return false;
    }
    long offset = (sp->topPointer - sp->bottomPointer);
    *elementPointer = sp->containerPointer[offset];
    sp->containerPointer[offset] = *(sp->bottomPointer);
    sp->topPointer = sp->containerPointer + (offset - 1);
    sp->length--;
    return true;
}

void clearStaticStack(SStackPointer sp) {
    if (isEmptyStaticStack(sp)) {
        return;
    }
    long offset = (sp->topPointer - sp->bottomPointer);
    while (offset > 0) {
        sp->containerPointer[offset] = *(sp->bottomPointer);
        offset--;
        sp->topPointer = (sp->containerPointer + offset);
    }
    return;
}

void freeStaticStack(SStackPointer sp) {
    free(sp->containerPointer);
    free(sp);
    sp->containerPointer = NULL;
    sp->topPointer = NULL;
    sp->bottomPointer = NULL;
}
