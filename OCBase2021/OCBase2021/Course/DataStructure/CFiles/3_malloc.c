//
//  3_malloc.c
//  CBase
//
//  Created by 周保勇 on 2021/1/18.
//

#include "3_malloc.h"
#include <malloc/malloc.h>
#include <malloc/_malloc.h>

void printElement(int * array, int length);

void mallocA(void);

void arrayForC(void) {
    int x[] = {10, 20, 30};
    int *px = x;
    printf("%d\n", ++*px);
    printf("%d\n", *px);
    // x[] = {11, 20, 30}
    px = x;
    printf("%d\n", (*px)++);
    printf("%d\n", *px);
    // x[] = {12, 20, 30}
    px = x;
    printf("%d\n", *px++); // *px++ 等价于*(px++)
    printf("%d\n", *px);
    // x[] = {12, 20, 30}
    px = x;
    printf("%d\n", *++px);
    printf("%d\n", *px);
}

void mallocApplication(void) {
    int a[5] = {1, 2, 3, 4, 5};
    printElement(a, 5);
    
    printf("自定义数组\n");
    
    mallocA();
}

void printElement(int * array, int length) {
    for (int i = 0; i < length; i++) {
        printf("%d\n", array[i]);
    }
}

void mallocA(void) {
    int length = 0;
    printf("请输入自定义数组的长度：length = ");
    scanf("%d", &length);
    // malloc动态开辟指定字节数的内存空间并返回该内存空间的首个字节的地址，供指针使用；首地址是一个无意义的地址，强制转换(int *)后，使其有意义，该处表示指向整形变量的指针。
    int * customArray = (int *)malloc(sizeof(int) * length);
    *customArray = 5;
    customArray[1] = 4;
    customArray[2] = 3;
    customArray[3] = 2;
    customArray[4] = 1;
    printElement(customArray, 5);
    free(customArray);
}
