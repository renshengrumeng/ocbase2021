//
//  7_MemeryAlign.c
//  CBase
//
//  Created by 周保勇 on 2021/1/25.
//  内存对其原则

#include "7_MemeryAlign.h"

/** 内存对其原则
 第一条：第一个成员的首地址为0
 第二条：每个成员的首地址是自身大小的整数倍
        第二条补充：以4字节对齐为例，如果自身大小大于4字节，都以4字节整数倍为基准对齐。
        例如：以前的成员内存对齐到13字节，成员本身是4字节，将13补齐为4的倍数，即为16；从16的以后的4个字节为该成员的存储地址；
             如果以前的成员内存对齐到12字节，成员本身是4字节，因12是4的倍数，不需要补齐；那么12以后的4个字节为该成员的存储地址。
 第三条：最后以结构总体对齐。
         第三条补充：取结构体中最大成员类型倍数；以8字节对齐为例，如果超过8字节，都以8字节整数倍为基准对齐。（其中这一条还有个名字叫：“补齐”，补齐的目的就是多个结构变量挨着摆放的时候也满足对齐的要求。）
        例如：结构体中最大成员所占字节数为8，对齐后结构体所占的字节数为28，需要补齐为8的倍数，即为32；最后该结构体所占内存字节数为32；
             如果结构体中最大成员所占字节数为8，对齐后结构体所占的字节数为32，不需要补齐；最后该结构体所占内存字节数为32。
 */

//struct MemeryAlign1 {
//    int a;
//    char b[3];
//    int c;
//};


