//
//  11_DynamicQueue.h
//  CBase
//
//  Created by 周保勇 on 2021/1/29.
//

#ifndef _1_DynamicQueue_h
#define _1_DynamicQueue_h

#include <stdio.h>
#include <stdbool.h>

/// 链式队列即单向动态队列：初始化时，不用固定容量，动态添加和释放内存；出队和入队会添加和释放相应的内存，且其长度在出队时-1，
/// 入队时长度+1
/// 该链表头指针指向的
struct DynamicQueue {
    /// 队列头部指针（头指针不存储具体数据）
    struct DynamicQueueNode * frontPoint;
    /// 队列尾部指针
    struct DynamicQueueNode * rearPoint;
};

struct DynamicQueueNode {
    /// 数据
    int data;
    /// 下一个元素的指针
    struct DynamicQueueNode * nextPointer;
};

typedef struct DynamicQueue DQueue, * DQueuePointer;
typedef struct DynamicQueueNode DQueueNode, * DQueueNodePointer;

DQueuePointer initDynamicQueue(void);

int lengthDynamicQueue(DQueuePointer qp);

void showDynamicQueue(DQueuePointer qp);

bool pushDynamicQueue(DQueuePointer qp, int element);

bool popDynamicQueue(DQueuePointer qp, int * elementPointer);

void freeDynamicQueue(DQueuePointer qp);

#endif /* _1_DynamicQueue_h */
