//
//  8_DynamicStack.h
//  CBase
//
//  Created by 周保勇 on 2021/1/25.
//

#ifndef __DynamicStack_h
#define __DynamicStack_h

#include <stdio.h>
#include <stdbool.h>

struct DynamicStackNode {
    int data;
    struct DynamicStackNode * nextP;
};

struct DynamicStack {
    struct DynamicStackNode * topP;
    struct DynamicStackNode * bottomP;
};

typedef struct DynamicStackNode DSNote, * DSNotePointer;

typedef struct DynamicStack DStack, * DStackPointer;

void initDynamicStack(DStackPointer stackPoint);

DStackPointer initDynamicStack2(void);

int dynamicStackLength(DStackPointer stackPoint);

void showDynamicStack(DStackPointer stackPoint);

bool pushDynamicStack(DStackPointer stackPoint, int element);

bool popDynamicStack(DStackPointer stackPoint);

void freeDynamicStack(DStackPointer stackPoint);

#endif /* __DynamicStack_h */
