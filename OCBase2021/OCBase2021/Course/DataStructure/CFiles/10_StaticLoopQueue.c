//
//  10_StaticLoopQueue.c
//  CBase
//
//  Created by 周保勇 on 2021/1/28.
//  单向静态循环队列：以存储整形数组为例

#include "10_StaticLoopQueue.h"
#include <malloc/_malloc.h>

SLQueuePointer initStaticLoopQueue(int capacity) {
    if (capacity <= 0) {
        printf("队列存储元素数组容量必须大于0\n");
        return NULL;
    }
    SLQueuePointer qp = (SLQueuePointer)malloc(sizeof(SLQueue));
    qp->containerPointer = (int *)malloc(sizeof(int) * (capacity + 1));
    if (qp == NULL || qp->containerPointer == NULL) {
        printf("队列内存分配失败或者队列存储元素数组空间分配失败\n");
        return NULL;
    }
    qp->frontIndex = 0;
    qp->rearIndex = 0;
    qp->capacity = capacity;
    qp->length = 0;
    return qp;
}

bool isNULLStaticLoopQueue(SLQueuePointer qp) {
    if (qp == NULL) {
        printf("队列内存分配失败\n");
        return true;
    }
    if (qp->containerPointer == NULL) {
        printf("队列存储元素数组空间分配失败\n");
        return true;
    }
    return false;
}

bool isEmptyStaticLoopQueue(SLQueuePointer qp) {
    if (isNULLStaticLoopQueue(qp)) {
        return true;
    }
    return qp->frontIndex == qp->rearIndex;
}

bool isFullStaticLoopQueue(SLQueuePointer qp) {
    if (isNULLStaticLoopQueue(qp)) {
        return true;
    }
    return qp->frontIndex == ((qp->rearIndex + 1) % (qp->capacity + 1));
}

void showStaticLoopQueue(SLQueuePointer qp) {
    if (isEmptyStaticLoopQueue(qp)) {
        printf("空队列\n");
        return;
    }
    int index = qp->frontIndex;
    while ((index % (qp->capacity + 1)) != qp->rearIndex) {
        printf("%d ", qp->containerPointer[index]);
        index++;
        index = index % (qp->capacity + 1);
    }
    printf("\n");
    return;
}

bool enterStaticLoopQueue(SLQueuePointer qp, int element) {
    if (isFullStaticLoopQueue(qp)) {
        printf("队列已满\n");
        return false;
    }
    int rearTempValue = qp->containerPointer[qp->rearIndex];
    qp->containerPointer[qp->rearIndex] = element;
    qp->rearIndex = (qp->rearIndex + 1) % (qp->capacity + 1);
    qp->containerPointer[qp->rearIndex] = rearTempValue;
    qp->length++;
    return true;
}

bool outStaticLoopQueue(SLQueuePointer qp, int * elementPointer) {
    if (isEmptyStaticLoopQueue(qp)) {
        printf("空队列\n");
        return false;
    }
    int rearTempValue = qp->containerPointer[qp->rearIndex];
    *elementPointer = qp->containerPointer[qp->frontIndex];
    qp->containerPointer[qp->frontIndex] = rearTempValue;
    qp->frontIndex = (qp->frontIndex + 1) % (qp->capacity + 1);
    qp->length--;
    return true;
}

void clearStaticLoopQueue(SLQueuePointer qp) {
    if (isEmptyStaticLoopQueue(qp)) {
        return;
    }
    int rearTempValue = qp->containerPointer[qp->rearIndex];
    while ((qp->frontIndex % (qp->capacity + 1)) != qp->rearIndex) {
        qp->containerPointer[qp->frontIndex] = rearTempValue;
        (qp->frontIndex)++;
        qp->frontIndex = qp->frontIndex % (qp->capacity + 1);
        qp->length--;
    }
    return;
}

void freeStaticLoopQueue(SLQueuePointer qp) {
    free(qp->containerPointer);
    qp->containerPointer = NULL;
    free(qp);
}
