//
//  6_LinkedList.c
//  CBase
//
//  Created by 周保勇 on 2021/1/21.
//  链表的实现

#include "6_LinkedList.h"
#include <malloc/_malloc.h>

PList initLinkedList(int length) {
    // 输入值
    int temp;
    
    // 头节点
    PList pHeader = (PList)malloc(sizeof(List));
    if (pHeader == NULL) {
        printf("头节点分配内存失败\n");
        return NULL;
    }
    PList pTail = pHeader;
    pTail->pNext = NULL;
    // 遍历添加其它节点
    for (int i = 0; i < length; i++) {
        PList pNew = (PList)malloc(sizeof(List));
        if (pNew == NULL) {
            printf("存储节点分配内存失败\n");
            return NULL;
        }
        printf("请输入要存储的第%d个整数值temp = ", i+1);
        scanf("%d", &temp);
        pNew->data = temp;
        pNew->pNext = NULL;
        pTail->pNext = pNew;
        pTail = pNew;
    }
    return  pHeader;
}

void showLinkedList(PList pListHeader) {
    PList p = pListHeader->pNext;
    if (p == NULL) {
        printf("链表为空\n");
        return;
    }
    while (p != NULL) {
        printf("%d ", p->data);
        p = p->pNext;
    }
    printf("\n");
    return;
}

bool isEmptyList(PList ph) {
    PList p = ph->pNext;
    if (p == NULL) {
        printf("链表为空\n");
        return true;
    }
    return false;
}

int listLength(PList ph) {
    if (isEmptyList(ph)) {
        return 0;
    }
    PList p = ph->pNext;
    int i = 0;
    while (p != NULL) {
        i++;
        p = p->pNext;
    }
    return i;
}

PList getListElement(PList ph, int index) {
    int length = listLength(ph);
    // 索引不符合范围(0-100)
    if (index < 0 || index > length) {
        printf("元素不存在");
        return NULL;
    }
    PList preP = ph;
    int i = 0;
    while (preP->pNext != NULL && i < index) {
        preP = preP->pNext;
        i++;
    }
    return preP->pNext;
}

bool insertListElement(PList ph, int index, int element) {
    int length = listLength(ph);
    // 索引不符合范围(0-100)
    if (index < 0 || index > length) {
        printf("插入索引不在0-%d有效范围\n", length);
        return false;
    }
    // 获取插入位置的前一个元素
    PList preP = ph;
    int i = 0;
    while (preP->pNext != NULL && i < index) {
        i++;
        preP = preP->pNext;
    }
    // 插入设置
    PList pNew = (PList)malloc(sizeof(PList));
    pNew->data = element;
    if (preP->pNext == NULL) {
        preP->pNext = pNew;
        pNew->pNext = NULL;
    }
    else {
        PList pIndex = preP->pNext;
        preP->pNext = pNew;
        pNew->pNext = pIndex;
    }
    return true;
}

bool removeListElement(PList ph, int index, int * elementValue) {
    int length = listLength(ph);
    if (index < 0 || index > length - 1) {
        printf("删除索引的位置元素不存在\n");
        return false;
    }
    // 获取删除位置的前一个元素
    PList preP = ph;
    int i = 0;
    while (preP->pNext != NULL && i < index) {
        i++;
        preP = preP->pNext;
    }
    // 删除设置
    PList indexP = preP->pNext;
    if (indexP->pNext == NULL) {
        free(indexP);
        indexP = NULL;
        preP->pNext = NULL;
    }
    else {
        preP->pNext = indexP->pNext;
        free(indexP);
        indexP = NULL;
    }
    return true;
}

void sortList(PList ph, bool isAsc) {
    if (isEmptyList(ph)) {
        return;
    }
    int length = listLength(ph);
    PList p, q;
    int i, j, temp;
    for (i = 0, p = ph->pNext; i < length - 1; i++, p = p->pNext) {
        for (j = i + 1, q = p->pNext; j < length; j++, q = q->pNext) {
            temp = p->data;
            if (isAsc) {
                if (p->data > q->data) {
                    p->data = q->data;
                    q->data = temp;
                }
            }
            else {
                if (p->data < q->data) {
                    p->data = q->data;
                    q->data = temp;
                }
            }
        }
    }
}

void freeList(PList ph) {
    if (isEmptyList(ph)) {
        free(ph);
    }
    PList pTail = ph;
    PList pTemp;
    while (pTail->pNext != NULL) {
        pTemp = pTail;
        pTail = pTemp->pNext;
        free(pTemp);
        pTemp = NULL;
    }
    if (pTail != NULL) {
        free(pTail);
        pTail = NULL;
    }
}
