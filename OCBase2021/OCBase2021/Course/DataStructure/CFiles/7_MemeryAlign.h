//
//  7_MemeryAlign.h
//  CBase
//
//  Created by 周保勇 on 2021/1/25.
//

#ifndef __MemeryAlign_h
#define __MemeryAlign_h

#include <stdio.h>

struct MemeryAlign1 {
    int a;  // 4
//    char b[3];  // 3
    char f[9];  // 9*1
//    int c;  // 4
//    int *p; // 8
};

struct MemeryAlign2 {
    char a[18]; // 18*1：数组内存对齐以数组中的类型进行对齐，不是以总数为倍数对齐
    double b;   // 8 ((18 + 6) + 8)
    char c;     // 1 ((18 + 6) + 8) + 1
    int d;      // 4 (((18 + 6) + 8) + (1 + 3) + 4)
    short e;    // 2 (((18 + 6) + 8) + (1 + 3) + 4) + 2
                // 最大为8，18的倍数，40最近18的倍数为48，最后该结构体占用48字节的存储空间
};

typedef struct MemeryAlign1 MA1;
typedef struct MemeryAlign2 MA2;

struct Student1 {
    double d1;  // 8 8
    char c1;    // 1 8 + 1
    double d2;  // 8 (8 + (1 + 7) + 8)
    int i1;     // 4 ((8 + (1 + 7) + 8) + 4)
                // 最大为8，8的倍数，28最近的8的倍数为32，最后该结构体占用32字节的存储空间
};

struct Student2 {
    double d1;  // 8
    double d2;  // 8 (8 + 8)
    int i1;     // 4 (8 + 8 + 4)
    char c1;    // 1 (8 + 8 + 4 + 1)
                // 最大为8，8的倍数，21最近的8的倍数为24，最后该结构体占用24字节的存储空间
};

#endif /* __MemeryAlign_h */
