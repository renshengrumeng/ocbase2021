//
//  8_DynamicStack.c
//  CBase
//
//  Created by 周保勇 on 2021/1/25.
//  链表实现动态栈

#include "8_DynamicStack.h"
#include <malloc/_malloc.h>

void initDynamicStack(DStackPointer stackPoint) {
    DSNotePointer notePoint = (DSNotePointer)malloc(sizeof(DSNotePointer));
    if (notePoint == NULL) {
        printf("分配内存出错\n");
        return;
    }
    stackPoint->bottomP = notePoint;
    stackPoint->topP = notePoint;
    notePoint->nextP = NULL;
    return;
}

DStackPointer initDynamicStack2() {
    DStackPointer stackPoint = (DStackPointer)malloc(sizeof(DStack));
    DSNotePointer notePoint = (DSNotePointer)malloc(sizeof(DSNote));
    if (stackPoint == NULL || notePoint == NULL) {
        printf("分配内存出错\n");
        return NULL;
    }
    stackPoint->bottomP = notePoint;
    stackPoint->topP = notePoint;
    notePoint->nextP = NULL;
    return stackPoint;
}

int dynamicStackLength(DStackPointer stackPoint) {
    if (stackPoint->topP->nextP == NULL) {
        printf("空栈\n");
        return 0;
    }
    DSNotePointer notePoint = stackPoint->topP;
    int i = 0;
    while (notePoint->nextP != NULL) {
        DSNotePointer temp = notePoint;
        notePoint = temp->nextP;
        i++;
    }
    return i;
}

void showDynamicStack(DStackPointer stackPoint) {
    if (stackPoint->topP->nextP == NULL) {
        printf("显示为空栈\n");
        return;
    }
    DSNotePointer notePoint = stackPoint->topP;
    while (notePoint->nextP != NULL) {
        DSNotePointer temp = notePoint;
        notePoint = temp->nextP;
        printf("%d ", temp->data);
    }
    printf("\n");
    return;
}

bool pushDynamicStack(DStackPointer stackPoint, int element) {
    DSNotePointer notePoint = (DSNotePointer)malloc(sizeof(DSNote));
    if (notePoint == NULL) {
        printf("分配内存出错\n");
        return false;
    }
    DSNotePointer nextPoint = stackPoint->topP;
    notePoint->data = element;
    notePoint->nextP = nextPoint;
    stackPoint->topP = notePoint;
    return true;
}

bool popDynamicStack(DStackPointer stackPoint) {
    DSNotePointer notePoint = stackPoint->topP;
    if (notePoint->nextP == NULL) {
        printf("空栈，无法出栈\n");
        return false;
    }
    DSNotePointer nextPoint = notePoint->nextP;
    stackPoint->topP = nextPoint;
    notePoint->nextP = NULL;
    free(notePoint);
    notePoint = NULL;
    return true;
}

void freeDynamicStack(DStackPointer stackPoint) {
    DSNotePointer notePoint = stackPoint->topP;
    while (notePoint->nextP != NULL) {
        DSNotePointer temp = notePoint;
        stackPoint->topP = temp->nextP;
        notePoint = temp->nextP;
        temp->nextP = NULL;
        free(temp);
        temp = NULL;
    }
    free(notePoint);
    notePoint = NULL;
    stackPoint->bottomP = NULL;
    stackPoint->topP = NULL;
    free(stackPoint);
    stackPoint = NULL;
}
