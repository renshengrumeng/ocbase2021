//
//  5_TypeDef.c
//  CBase
//
//  Created by 周保勇 on 2021/1/20.
//  别名声明

#include "5_TypeDef.h"
#include <string.h>

typedef int ZBYInt;

struct ZBYStudent {
    int sid;
    char name[200];
    int age;
};

// 3.第三种：多个声明

typedef struct ZBYStudent * TDSP, TDS;

// 2.第二种：单个声明

//typedef struct ZBYStudent TDS;    // 声明结构体的别名
//
//typedef struct ZBYStudent * TDSP; // 声明结构体指针类型的别名

// 1.第一种：带结构的声明的别名

//typedef struct ZBYStudent {   // 声明结构体的别名
//    int sid;
//    char name[200];
//    int age;
//} TDS;

//typedef struct ZBYStudent {   // 声明结构体指针类型的别名
//    int sid;
//    char name[200];
//    int age;
//} * TDSP;

void showTypedefUse(void) {
    ZBYInt j = 100;
    printf("别名ZBYInt j = %d\n", j);
    TDS s1;
    s1.sid = 1001;
    strcpy(s1.name, "大蛇");
    s1.age = 18;
    TDSP s1p = &s1;
    printf("name = %s\n", s1p->name);
}


