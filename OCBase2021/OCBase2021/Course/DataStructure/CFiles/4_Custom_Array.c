//
//  4_Custom_Array.c
//  CBase
//
//  Created by 周保勇 on 2021/1/20.
//  数组的实现

#include "4_Custom_Array.h"
#include <malloc/malloc.h>
#include <malloc/_malloc.h>

void initArray(struct Array *, int);
struct Array * initArrayPointer(int);
bool isEmpty(struct Array *);
bool isFull(struct Array *);
void show(struct Array *);
bool append(struct Array *, int);
bool insertArray(struct Array *, int, int);
bool removeElement(struct Array *, int, int *);
void sortArray(struct Array *, bool);
void inversionArray(struct Array *);
void freeArray(struct Array *);

void initArray(struct Array * array, int length) {
    array->pBase = (int *)malloc(sizeof(int) * length);
    if (array->pBase == NULL) {
//        a.length = 0;
//        a.count = 0;
//        a.increaseFactor = 0;
        printf("内存分配失败\n");
        return;
    }
    else {
        array->length = length;
        array->count = 0;
        array->increaseFactor = 10;
        return;
    }
}

/// 在函数结束后结构体Array的内存已释放，而malloc开辟的内存没有释放，在外边在调用结构体的成员会报野指针崩溃
//struct Array * initArray(int length) {
//    struct Array a;   // 函数栈中分配结构体的内存空间，结构体的属性是垃圾值；该栈在函数执行完毕后，自动释放其占用的内存空间
//    struct Array * array = &a;
//    array->pBase = (int *)malloc(sizeof(int) * length);   // malloc内存分配到堆中，需要手动调用free方法，来释放其分配的内存空间；否则会造成内存泄漏
//    if (array->pBase == NULL) {
////        a.length = 0;
////        a.count = 0;
////        a.increaseFactor = 0;
//        printf("内存分配失败\n");
//        return NULL;
//    }
//    else {
//        array->length = length;
//        array->count = 0;
//        array->increaseFactor = 10;
//        return array;
//    }
//}

struct Array * initArrayPointer(int length) {
    // 分配结构体内存
    struct Array * ap = (struct Array *)malloc(sizeof(struct Array));
    // 分配结构体中存储数据的内存
    ap->pBase = (int *)malloc(sizeof(int) * length);
    if (ap == NULL || ap->pBase == NULL) {
        printf("内存分配失败\n");
        return NULL;
    }
    else {
        ap->length = length;
        ap->count = 0;
        ap->increaseFactor = 10;
        return ap;
    }
}

bool isEmpty(struct Array * array) {
    if (array->pBase == NULL) {
        return true;
    }
    return array->count == 0;
}

bool isFull(struct Array * array) {
    return array->count == array->length;
}

void show(struct Array * array) {
    if (isEmpty(array)) {
        printf("数组为空\n");
        return;
    }
    for (int i = 0; i < array->count; i++) {
        printf("%d ", array->pBase[i]);
    }
    printf("\n");
}

bool append(struct Array * array, int element) {
    if (isFull(array)) {
        return false;
    }
    array->pBase[array->count] = element;
    (array->count)++;
    return true;
}

bool insertArray(struct Array * array, int index, int element) {
    // 已满
    if (isFull(array)) {
        printf("已满\n");
        return false;
    }
    // 索引超出数组容纳元素的索引值有效范围
    if (index >= array->length || index < 0) {
        printf("索引超出数组容纳元素的索引值有效范围\n");
        return false;
    }
    // 插入位置没有元素
    if (array->count < (index + 1)) {
        for (int i = array->count; i < index; i++) {
            array->pBase[i] = 0;
        }
        array->pBase[index] = element;
        array->count = index + 1;
        return true;
    }
    // 插入位置有元素
    for (int i = (array->count) - 1; i >= index; i--) {
        int temp = array->pBase[i];
        array->pBase[i+1] = temp;
    }
    array->pBase[index] = element;
    (array->count)++;
    return true;
}

bool removeElement(struct Array * array, int index, int * elementValue) {
    // 索引超出数组容纳元素的索引值有效范围
    if (index >= array->length || index < 0) {
        printf("索引超出数组容纳元素的索引值有效范围\n");
        return false;
    }
    // 删除索引的位置没有元素
    if (array->count < index + 1) {
        printf("该索引的位置没有元素\n");
        return false;
    }
    // 删除位置有元素
    *elementValue = array->pBase[index];
    for (int i = index; i < (array->count - 1); i++) {
        array->pBase[i] = array->pBase[i+1];
    }
    (array->count)--;
    return true;
}

void sortArray(struct Array * array, bool isAsc) {
    int temp;
    for (int i = 0; i < array->count; i++) {
        for (int j = i+1; j < array->count; j++) {
            temp = array->pBase[i];
            if (isAsc) {
                if (temp > array->pBase[j]) {
                    array->pBase[i] = array->pBase[j];
                    array->pBase[j] = temp;
                }
            }
            else {
                if (temp < array->pBase[j]) {
                    array->pBase[i] = array->pBase[j];
                    array->pBase[j] = temp;
                }
            }
        }
    }
}

void inversionArray(struct Array * array) {
    int i = 0;
    int j = array->count - 1;
    int temp;
    while (i < j) {
        temp = array->pBase[i];
        array->pBase[i] = array->pBase[j];
        array->pBase[j] = temp;
        i++;
        j--;
    }
}

void freeArrayData(struct Array * array) {
    free(array->pBase);
}

void freeArray(struct Array * array) {
    free(array->pBase);
    free(array);
}
