//
//  ZBYDataStructureViewController.m
//  OCBase2021
//
//  Created by 周保勇 on 2021/6/30.
//

#import "ZBYDataStructureViewController.h"
#import "1_Pointer.h"
#include "2_Struct.h"
#include "3_malloc.h"
#include "4_Custom_Array.h"
#include "5_TypeDef.h"
#include "6_LinkedList.h"
#include "7_MemeryAlign.h"
#include "8_DynamicStack.h"
#include "9_StaticStack.h"
#include "10_StaticLoopQueue.h"
#include "11_DynamicQueue.h"
#include "13_Sorting.h"

@interface ZBYDataStructureViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, copy) NSArray * models;

@end

@implementation ZBYDataStructureViewController

- (NSArray *)models {
    if (_models == nil) {
        _models = @[
        @{@"title" : @"指针基础", @"subTitle" : @"指针基础"},
        @{@"title" : @"结构体", @"subTitle" : @"结构体"},
        @{@"title" : @"malloc", @"subTitle" : @"malloc函数"},
        @{@"title" : @"C中数组和自定义Array", @"subTitle" : @"malloc使用"},
        @{@"title" : @"typedef", @"subTitle" : @"定义数据类型别名"},
        @{@"title" : @"Linked list", @"subTitle" : @"单向链表的实现"},
        @{@"title" : @"内存对齐", @"subTitle" : @"内存对齐原则"},
        @{@"title" : @"Dynamic Stack", @"subTitle" : @"动态堆栈-单链表实现"},
        @{@"title" : @"Static Stack", @"subTitle" : @"静态堆栈-数组实现"},
        @{@"title" : @"Static Loop Queue", @"subTitle" : @"静态循环队列-数组实现"},
        @{@"title" : @"Dynamic Queue", @"subTitle" : @"动态队列-链表实现"},
        @{@"title" : @"Recursive", @"subTitle" : @"递归思想"},
        @{@"title" : @"Sorting", @"subTitle" : @"排序"}];
    }
    return _models;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITableView * tableView = [[UITableView alloc] initWithFrame:CGRectZero style:(UITableViewStylePlain)];
    tableView.delegate = self;
    tableView.dataSource = self;
    [self.view addSubview:tableView];
    
    tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];
}

// MARK: - UITableViewDelegate, UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.models.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString * identifier = @"123456";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.backgroundColor = [UIColor whiteColor];
        cell.textLabel.textColor = [UIColor blackColor];
        cell.detailTextLabel.textColor = [UIColor blackColor];
    }
    cell.textLabel.text = self.models[indexPath.row][@"title"];
    cell.detailTextLabel.text = self.models[indexPath.row][@"subTitle"];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSDictionary * info = self.models[indexPath.row];
    NSString * title = info[@"title"];
    if ([title containsString:@"指针基础"]) {
        [self pointerBase];
    }
    if ([title containsString:@"结构体"]) {
        [self structBase];
    }
    if ([title containsString:@"malloc"]) {
        [self mallocBase];
    }
    if ([title containsString:@"C中数组和自定义Array"]) {
        [self customArray];
    }
    if ([title containsString:@"typedef"]) {
        [self showAnotherName];
    }
    if ([title containsString:@"Linked list"]) {
        [self linkList];
    }
    if ([title containsString:@"内存对齐"]) {
        [self memoryAlignmentPrinciple];
    }
    if ([title containsString:@"Dynamic Stack"]) {
        [self dynamicStackRealize];
    }
    if ([title containsString:@"Static Stack"]) {
        [self staticStackRealize];
    }
    if ([title containsString:@"Static Loop Queue"]) {
        [self staticLoopQueue];
    }
    if ([title containsString:@"Dynamic Queue"]) {
        [self dynamicQueue];
    }
    if ([title containsString:@"Recursive"]) {
        [self recursiveBase];
    }
    if ([title containsString:@"Sorting"]) {
        [self sorting];
    }
}

// MARK: - 指针基础：C语言中的指针
- (void)pointerBase {
    varP();
    arrayP();
}

// MARK: - 结构体：C语言中结构体
- (void)structBase {
    structVar();
}

// MARK: - malloc
- (void)mallocBase {
    mallocApplication();
}

/// C语言数组自定义Array
- (void)customArray {
    arrayForC();
//    mallocApplication();
}

// MARK: - typedef：声明类型的别名
- (void)showAnotherName {
    showTypedefUse();
}

// MARK: - Linked List
- (void)linkList {
    int length;
    printf("请输入链表长度\n");
    scanf("%d", &length);
    PList p = initLinkedList(length);
    showLinkedList(p);
//    int index;
//    printf("请输入要插入位置的索引\n");
//    scanf("%d", &index);
//    int value;
//    printf("请输入要插入的值\n");
//    scanf("%d", &value);
//    insertListElement(p, index, value);
    int index;
    printf("请输入要删除的索引\n");
    scanf("%d", &index);
    removeListElement(p, index, NULL);
    showLinkedList(p);
    printf("请输入要获取的索引\n");
    scanf("%d", &index);
    PList eP = getListElement(p, index);
    printf("获取元素的值：%d\n", eP->data);
}

// MARK: - 内存对齐原则：结构体内存对齐
- (void)memoryAlignmentPrinciple {
    printf("%lu\n", sizeof(struct Student1));
    printf("%lu\n", sizeof(struct Student2));
    printf("%lu\n", sizeof(struct MemeryAlign1));
    printf("%lu\n", sizeof(struct MemeryAlign2));
}

// MARK: - Dynamic Stack：单链表实现
- (void)dynamicStackRealize {
    DStack stack;
    printf("%lu\n", sizeof(DStack));
    initDynamicStack(&stack);
    printf("%p\n", &stack);
    showDynamicStack(&stack);
    
    DStackPointer stack2 = initDynamicStack2();
    printf("%p\n", stack2);
    showDynamicStack(stack2);
    pushDynamicStack(stack2, 3);
    pushDynamicStack(stack2, 6);
    printf("栈的长度：%d\n", dynamicStackLength(stack2));
    showDynamicStack(stack2);
    popDynamicStack(stack2);
    showDynamicStack(stack2);
    popDynamicStack(stack2);
    showDynamicStack(stack2);
    popDynamicStack(stack2);
    freeDynamicStack(stack2);
}

// MARK: - Static Stack：数组实现静态栈
- (void)staticStackRealize {
    SStackPointer sp = initStaticStack(5);
    showStaticStack(sp);
    pushStaticStack(sp, 1);
    pushStaticStack(sp, 2);
    pushStaticStack(sp, 3);
    pushStaticStack(sp, 4);
    pushStaticStack(sp, 5);
    pushStaticStack(sp, 6);
    showStaticStack(sp);
    int popElement;
    popStaticStack(sp, &popElement);
    popStaticStack(sp, &popElement);
    popStaticStack(sp, &popElement);
    popStaticStack(sp, &popElement);
    popStaticStack(sp, &popElement);
    popStaticStack(sp, &popElement);
    showStaticStack(sp);
    printf("出栈元素的值：%d\n", popElement);
    clearStaticStack(sp);
    showStaticStack(sp);
    freeStaticStack(sp);
}

// MARK: - Static Loop Queue：单向静态循环队列
- (void)staticLoopQueue {
    SLQueuePointer qp = initStaticLoopQueue(2);
    printf("队列的容量：%d\n", qp->capacity);
    printf("队列的长度：%d\n", qp->length);
    enterStaticLoopQueue(qp, 34);
    enterStaticLoopQueue(qp, -12);
    enterStaticLoopQueue(qp, 3);
    showStaticLoopQueue(qp);
    int first, second, third;
    outStaticLoopQueue(qp, &first);
    outStaticLoopQueue(qp, &second);
    outStaticLoopQueue(qp, &third);
    showStaticLoopQueue(qp);
    printf("first = %d, second = %d, third = %d \n", first, second, third);
    enterStaticLoopQueue(qp, 1);
    enterStaticLoopQueue(qp, 2);
    showStaticLoopQueue(qp);
    clearStaticLoopQueue(qp);
    showStaticLoopQueue(qp);
}

// MARK: - Dynamic Queue：动态队列
- (void)dynamicQueue {
    DQueuePointer qp = initDynamicQueue();
    pushDynamicQueue(qp, 1);
    pushDynamicQueue(qp, 2);
    pushDynamicQueue(qp, 3);
    showDynamicQueue(qp);
    printf("队列的元素个数：%d\n", lengthDynamicQueue(qp));
    int popValue;
    popDynamicQueue(qp, &popValue);
    printf("出队元素值：%d\n", popValue);
    popDynamicQueue(qp, &popValue);
    printf("出队元素值：%d\n", popValue);
    popDynamicQueue(qp, &popValue);
    printf("出队元素值：%d\n", popValue);
    popDynamicQueue(qp, &popValue);
    pushDynamicQueue(qp, 3);
    freeDynamicQueue(qp);
}

// MARK: - Recursive：递归思想
- (void)recursiveBase {
    
}

// MARK: - 排序：基础排序
- (void)sorting {
    int a[6] = {12, -3, 0, -3, 80, 77};
    int * temp = insertionSort(a, 6);
//    int * temp2 = quickSort(a, 6);
    showSortArray(temp, 6);
}

@end
