//
//  ZBYAlgorithmBasedViewController.m
//  OCBase2021
//
//  Created by 周保勇 on 2021/6/30.
//

#import "ZBYAlgorithmBasedViewController.h"

@interface ZBYAlgorithmBasedViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, copy) NSArray * models;

@end

@implementation ZBYAlgorithmBasedViewController

- (NSArray *)models {
    if (_models == nil) {
        _models = @[
        @{@"title" : @"常用的基础排序算法", @"subTitle" : @"6中基础排序算法"},
        @{@"title" : @"算法基础", @"subTitle" : @"算法基础"}];
    }
    return _models;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITableView * tableView = [[UITableView alloc] initWithFrame:CGRectZero style:(UITableViewStylePlain)];
    tableView.delegate = self;
    tableView.dataSource = self;
    [self.view addSubview:tableView];
    
    tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];
}

// MARK: - UITableViewDelegate, UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.models.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString * identifier = @"123456";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.backgroundColor = [UIColor whiteColor];
        cell.textLabel.textColor = [UIColor blackColor];
        cell.detailTextLabel.textColor = [UIColor blackColor];
    }
    cell.textLabel.text = self.models[indexPath.row][@"title"];
    cell.detailTextLabel.text = self.models[indexPath.row][@"subTitle"];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSDictionary * info = self.models[indexPath.row];
    NSString * title = info[@"title"];
    if ([title containsString:@"常用的基础排序算法"]) {
        [self sortBase];
    }
    if ([title containsString:@"算法基础"]) {
        
    }
}

// MARK: - 排序算法

- (void)sortBase {
    NSMutableArray * numbers = [NSMutableArray arrayWithArray:@[@12, @-3, @-3, @0, @43, @56]];
    NSLog(@"%@", [self insertedSort:numbers]);
}

/// 冒泡排序：拿第一个元素和其它元素依次比较，
- (NSArray *)buddleSort:(NSMutableArray *)a {
    if (a.count <= 1) {
        return a;
    }
    for (NSInteger i = 0; i < a.count; i++) {
        for (NSInteger j = i + 1; j < a.count - i; j++) {
            NSNumber * temp = a[j];
            NSNumber * ai = a[i];
            if ([ai intValue] > [temp intValue]) {
                a[j] = a[i];
                a[i] = temp;
            }
        }
    }
    return a;
}

/// 选择排序：选择数组中的最小值，放到第一位置；选择选择其它元素中次最小值，放到底二个位置；依次进行
- (NSArray *)selectedSort:(NSMutableArray *)a {
    if (a.count <= 1) {
        return a;
    }
    NSInteger min, j;
    for (NSInteger i = 0; i < a.count; i++) {
        for (min = i, j = i + 1; j < a.count; j++) {    // 冒泡获取最小索引值：可以用其他排序方法
            NSNumber * temp = a[j];
            NSNumber * ai = a[min];
            if ([ai intValue] > [temp intValue]) {
                min = j;
            }
        }
        if (min != i) {
            NSNumber * temp = a[min];
            a[min] = a[i];
            a[i] = temp;
        }
    }
    return a;
}

/// 插入排序：将第二个元素插入第一个元素中，排序；将第三个元素插入前两个元素中，排序；依次进行。
- (NSArray *)insertedSort:(NSMutableArray *)a {
    if (a.count <= 1) {
        return a;
    }
    for (NSInteger i = 1; i < a.count; i++) {
        if ([a[i - 1] intValue] < [a[i] intValue]) {   // 如果插入的大于最后的元素的值，不用修改
            
        }
        else {  // 查找合适的位置插入
            // @[@12, @-3, @-3, @0, @43, @56]
            NSNumber * temp = a[i];
            NSInteger j = i - 1;
            while (j > -1 && [temp intValue] < [a[j] intValue]) {   // 判断i元素在新的排序，并将他后边的元素依次后移
                a[j + 1] = a[j];
                j--;
            }
            a[j + 1] = temp;    // 将获取的位置设置为该元素的值
        }
    }
    return a;
}

@end
