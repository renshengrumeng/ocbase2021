//
//  ZBYControllerUIViewController.m
//  OCBase2021
//
//  Created by 周保勇 on 2021/6/2.
//

#import "ZBYControllerUIViewController.h"
#import "ZBYTableViewController.h"

@interface ZBYControllerUIViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, copy) NSArray * models;

@end

@implementation ZBYControllerUIViewController

- (NSArray *)models {
    if (_models == nil) {
        _models = @[
        @{@"title" : @"滚动按钮列表视图", @"subTitle" : @"控制器布局"},
//        @{@"title" : @"自定义View展示", @"subTitle" : @"一些常用的自定义视图"},
        ];
    }
    return _models;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITableView * tableView = [[UITableView alloc] initWithFrame:CGRectZero style:(UITableViewStylePlain)];
    tableView.delegate = self;
    tableView.dataSource = self;
    [self.view addSubview:tableView];
    
    tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];
}

// MARK: - UITableViewDelegate, UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.models.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString * identifier = @"123456";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.backgroundColor = [UIColor whiteColor];
        cell.textLabel.textColor = [UIColor blackColor];
        cell.detailTextLabel.textColor = [UIColor blackColor];
    }
    cell.textLabel.text = self.models[indexPath.row][@"title"];
    cell.detailTextLabel.text = self.models[indexPath.row][@"subTitle"];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSDictionary * info = self.models[indexPath.row];
    NSString * title = info[@"title"];
    if ([title containsString:@"滚动按钮列表视图"]) {
        ZBYTableViewController * tableListVC = [ZBYTableViewController new];
        tableListVC.title = title;
        [self.navigationController pushViewController:tableListVC animated:YES];
    }
//    if ([title containsString:@"自定义View展示"]) {
//        ZBYCustomViewController * customViewVC = [ZBYCustomViewController new];
//        customViewVC.title = title;
//        [self.navigationController pushViewController:customViewVC animated:YES];
//    }
//    if ([info[@"title"] containsString:@"KVC"]) {
//        ZBYKVCViewController * kvcVC = [ZBYKVCViewController new];
//        kvcVC.title = title;
//        [self.navigationController pushViewController:kvcVC animated:YES];
//    }
//    if ([info[@"title"] containsString:@"GCD"]) {
//        ZBYGCDViewController * gcdVC = [ZBYGCDViewController new];
//        gcdVC.title = title;
//        [self.navigationController pushViewController:gcdVC animated:YES];
//    }
//    if ([info[@"title"] containsString:@"多线程中锁"]) {
//        ZBYLockViewController * lockVC = [ZBYLockViewController new];
//        lockVC.title = title;
//        [self.navigationController pushViewController:lockVC animated:YES];
//    }
}

@end
