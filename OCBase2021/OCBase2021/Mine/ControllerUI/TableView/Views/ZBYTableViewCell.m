//
//  ZBYTableViewCell.m
//  OCBase2021
//
//  Created by 周保勇 on 2021/6/10.
//

#import "ZBYTableViewCell.h"

@implementation ZBYTableViewCell

+ (instancetype)cellWithTableView:(UITableView *)tableView {
    static NSString * identifier;
    identifier = NSStringFromClass([self class]);
    ZBYTableViewCell * cell = [[self alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    return cell;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self initSubviews];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
    [super setHighlighted:highlighted animated:animated];
}

- (void)setModel:(ZBYTableViewModel *)model {
    _model = model;
    self.textLabel.text =  model.name;
}

- (void)initSubviews {
    
}

@end
