//
//  ZBYTableViewCell.h
//  OCBase2021
//
//  Created by 周保勇 on 2021/6/10.
//

#import <UIKit/UIKit.h>
#import "ZBYTableViewModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZBYTableViewCell : UITableViewCell

@property (nonatomic, strong) ZBYTableViewModel * model;

+ (instancetype)cellWithTableView:(UITableView *)tableView;

@end

NS_ASSUME_NONNULL_END
