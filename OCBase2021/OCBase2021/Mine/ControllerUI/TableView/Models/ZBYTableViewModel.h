//
//  ZBYTableViewModel.h
//  OCBase2021
//
//  Created by 周保勇 on 2021/6/10.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZBYTableViewModel : NSObject

@property (nonatomic, copy) NSString * name;

@end

NS_ASSUME_NONNULL_END
