//
//  ZBYTableViewController.m
//  OCBase2021
//
//  Created by 周保勇 on 2021/6/10.
//

#import "ZBYTableViewController.h"
#import "ZBYTableViewControllerModel.h"

@interface ZBYTableViewController ()

@property (nonatomic, strong) ZBYTableViewControllerModel * viewModel;

@end

@implementation ZBYTableViewController

- (ZBYTableViewControllerModel *)viewModel {
    if(_viewModel == nil) {
        _viewModel = [[ZBYTableViewControllerModel alloc] init];
    }
    return _viewModel;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    UITableView * tableView = [self.viewModel getTableView];
    [self.view addSubview:tableView];
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.mas_equalTo(0);
        make.top.mas_equalTo(ZBYSize.navigationBarHeight);
    }];
}

@end
