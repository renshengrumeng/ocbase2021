//
//  ZBYTableViewControllerModel.m
//  OCBase2021
//
//  Created by 周保勇 on 2021/6/10.
//

#import "ZBYTableViewControllerModel.h"
#import "ZBYTableViewCell.h"

@interface ZBYTableViewControllerModel () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) NSMutableArray * listData;

@end

@implementation ZBYTableViewControllerModel

- (UITableView *)getTableView {
    UITableView * tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    tableView.separatorInset = UIEdgeInsetsMake(0, 15, 0, 15);
    tableView.separatorColor = [UIColor greenColor];
    tableView.rowHeight = 44;
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ZBYSize.screenWidth, CGFLOAT_MIN)];
    tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, ZBYSize.screenWidth, CGFLOAT_MIN)];
    return tableView;
}

// MARK: - UITableViewDataSource, UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ZBYTableViewCell * cell = [ZBYTableViewCell cellWithTableView:tableView];
    cell.model = self.listData[indexPath.row];
    return cell;
}

@end
