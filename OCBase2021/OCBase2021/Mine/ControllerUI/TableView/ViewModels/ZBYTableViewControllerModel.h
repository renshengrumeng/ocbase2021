//
//  ZBYTableViewControllerModel.h
//  OCBase2021
//
//  Created by 周保勇 on 2021/6/10.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, ZBYTableViewControllerModelDelegateRefreshType) {
    /// 下拉刷新
    ZBYTableViewControllerModelDelegateRefreshTypeRefresh = 0,
    /// 上拉加载更多数据
    ZBYTableViewControllerModelDelegateRefreshTypeLoadMore = 1
};

@protocol ZBYTableViewControllerModelDelegate <NSObject>

//@property (nonatomic, assign) NSInteger pageIndex;
//@property (nonatomic, assign) NSInteger pageCount;
//@property (nonatomic, assign) ZBYTableViewControllerModelDelegateRefreshType loadType;

- (void)viewModelRefreshData:(ZBYTableViewControllerModelDelegateRefreshType)type;

@end

NS_ASSUME_NONNULL_BEGIN

@interface ZBYTableViewControllerModel : NSObject

@property (nonatomic, weak) id<ZBYTableViewControllerModelDelegate> delegate;

- (UITableView *)getTableView;

@end

NS_ASSUME_NONNULL_END
