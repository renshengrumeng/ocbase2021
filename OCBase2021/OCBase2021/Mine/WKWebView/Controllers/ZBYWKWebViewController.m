//
//  ZBYWKWebViewController.m
//  OCBase2021
//
//  Created by anniekids good on 2022/3/18.
//

#import "ZBYWKWebViewController.h"
#import <WebKit/WebKit.h>
#import "WeakWebViewScriptMessageDelegate.h"

@interface ZBYWKWebViewController () <WKUIDelegate, WKNavigationDelegate, WKScriptMessageHandler>

/// webView
@property (nonatomic, strong) WKWebView * webView;

/// 网页加载进度视图
@property (nonatomic, strong) UIProgressView * progressView;

@end

@implementation ZBYWKWebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.webView = [self setWebView];
    [self.view addSubview:self.webView];
    [self.webView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];
    // 添加监测网页加载进度的观察者
    [self.webView addObserver:self forKeyPath:@"estimatedProgress" options:0 context:nil];
    // 添加监测网页标题title的观察者
    [self.webView addObserver:self forKeyPath:@"title" options:NSKeyValueObservingOptionNew context:nil];
    [self loadLocalUrl:self.webView];
    
    _progressView = [[UIProgressView alloc] initWithFrame:CGRectZero];
    _progressView.tintColor = [UIColor blueColor];
    _progressView.trackTintColor = [UIColor clearColor];
    [self.view addSubview:_progressView];
    [_progressView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.mas_equalTo(0);
        make.top.mas_equalTo(ZBYSize.navigationBarHeight + 2);
        make.height.mas_equalTo(4);
    }];
}

- (void)didReceiveMemoryWarning {
    [self clearWebCache];
}

- (void)dealloc {
    // 移除观察者
    [_webView removeObserver:self forKeyPath:NSStringFromSelector(@selector(estimatedProgress))];
    [_webView removeObserver:self forKeyPath:NSStringFromSelector(@selector(title))];
    
    
    // 使用完毕需要移除注册的js调用原生的方法
    [[_webView configuration].userContentController removeScriptMessageHandlerForName:@"jsToOcNoPrams"];
    [[_webView configuration].userContentController removeScriptMessageHandlerForName:@"jsToOcWithPrams"];
}

// MARK: - Observer

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
    if ([keyPath isEqualToString:NSStringFromSelector(@selector(estimatedProgress))]
            && object == _webView) {
           NSLog(@"网页加载进度 = %f",_webView.estimatedProgress);
            self.progressView.progress = _webView.estimatedProgress;
            if (_webView.estimatedProgress >= 1.0f) {
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    self.progressView.progress = 0;
                });
            }
        }else if([keyPath isEqualToString:@"title"]
                 && object == _webView){
            self.navigationItem.title = _webView.title;
        }else{
            [super observeValueForKeyPath:keyPath
                                 ofObject:object
                                   change:change
                                  context:context];
        }
}

// MARK: - setter

- (WKWebView *)setWebView {
    // WKWebViewConfiguration WKWebView的配置信息
    WKWebViewConfiguration * config = [[WKWebViewConfiguration alloc] init];
    // 是使用h5的视频播放器在线播放, 还是使用原生播放器全屏播放
    config.allowsInlineMediaPlayback = YES;
    // 设置视频是否需要用户手动播放  设置为NO则会允许自动播放
    config.mediaTypesRequiringUserActionForPlayback = WKAudiovisualMediaTypeAll;
//    config.requiresUserActionForMediaPlayback = YES;
    // 设置是否允许画中画技术 在特定设备上有效
    config.allowsPictureInPictureMediaPlayback = YES;
    // 设置请求的User-Agent信息中应用程序名称 iOS9后可用
    config.applicationNameForUserAgent = @"ChinaDailyForiPad";
    // 创建设置对象
    WKPreferences *preference = [[WKPreferences alloc]init];
    //最小字体大小 当将javaScriptEnabled属性设置为NO时，可以看到明显的效果
    preference.minimumFontSize = 0;
    //设置是否支持javaScript 默认是支持的
    preference.javaScriptEnabled = YES;
    // 在iOS上默认为NO，表示是否允许不经过用户交互由javaScript自动打开窗口
    preference.javaScriptCanOpenWindowsAutomatically = YES;
    config.preferences = preference;
    
    // 自定义的WKScriptMessageHandler 是为了解决内存不释放的问题
    WeakWebViewScriptMessageDelegate * weakScriptMessageDelegate = [[WeakWebViewScriptMessageDelegate alloc] initWithDelegate:self];
    
    // 这个类主要用来做native与JavaScript的交互管理
    WKUserContentController * wkUController = [[WKUserContentController alloc] init];
    // 注册一个name为jsToOcNoPrams的js方法
    /**
     需要注意的是addScriptMessageHandler很容易引起循环引用，导致控制器无法被释放，所以需要移除MessageHandler
     
     控制器self -> WKWebView -> WKWebView copy -> configuration -> configuration copy -> userContentController -> 控制器self
     */
    // 造成循环引用
    // [wkUController addScriptMessageHandler:self name:@"jsToOcNoPrams"];
    [wkUController addScriptMessageHandler:weakScriptMessageDelegate name:@"jsToOcNoPrams"];
    [wkUController addScriptMessageHandler:weakScriptMessageDelegate name:@"jsToOcWithPrams"];
    config.userContentController = wkUController;
    
    // WKWebViewConfiguration配置信息初始化webView
    WKWebView * webView = [[WKWebView alloc] initWithFrame:CGRectZero configuration:config];
    // 视图的用户界面委托代理
    webView.UIDelegate = self;
    // 导航代理
    webView.navigationDelegate = self;
    // 是否允许手势左滑返回上一级, 类似导航控制的左滑返回
    webView.allowsBackForwardNavigationGestures = YES;
    // 可返回的页面列表, 存储已打开过的网页
    WKBackForwardList * backForwardList = [webView backForwardList];
    
//    // 是否可以返回上一历史列表项
//    [webView canGoBack];
//    // 返回上一历史列表项
//    [webView goBack];
//    // 是否可以进入下一列表项
//    [webView canGoForward];
//    // 进入下一列表项
//    [webView goForward];
//    // 进入下一指定列表项
//    [webView goToBackForwardListItem:backForwardList.forwardList[2]];
//    // 刷新当前页面
//    [webView reload];
//    // webView调用js字符串
//    [webView evaluateJavaScript:JSCookieString completionHandler:nil];
    return webView;
}

/// 加载本地html文件
- (void)loadLocalUrl:(WKWebView *)webView {
    NSString *path = [[NSBundle mainBundle] pathForResource:@"JStoOC.html" ofType:nil];
    NSString *htmlString = [[NSString alloc]initWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    // 加载本地html文件
    [webView loadHTMLString:htmlString baseURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] bundlePath]]];
}

/// 加载远程html文件
- (void)loadRequest:(WKWebView *)webView {
    NSMutableURLRequest * request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http://www.chinadaily.com.cn"]];
    // 添加头字段
//    [request addValue:[self readCurrentCookieWithDomain:@"http://www.chinadaily.com.cn"] forHTTPHeaderField:@"Cookie"];
    [webView loadRequest:request];
}

/// 手动向html页面注入js代码
- (void)insertJS:(WKWebView *)webView {
    // 以下代码适配文本大小，由UIWebView换为WKWebView后，会发现字体小了很多，这应该是WKWebView与html的兼容问题，解决办法是修改原网页，要么我们手动注入JS
    NSString *jSString = @"var meta = document.createElement('meta'); meta.setAttribute('name', 'viewport'); meta.setAttribute('content', 'width=device-width'); document.getElementsByTagName('head')[0].appendChild(meta);";
    //用于进行JavaScript注入
    WKUserScript *wkUScript = [[WKUserScript alloc] initWithSource:jSString injectionTime:WKUserScriptInjectionTimeAtDocumentEnd forMainFrameOnly:YES];
    [webView.configuration.userContentController addUserScript:wkUScript];
}

/// 清理webView的缓存和存储
- (void)clearWebCache {
    NSDate * date = [[NSDate alloc] initWithTimeIntervalSince1970:0];
    /**
         在磁盘缓存上。
         WKWebsiteDataTypeDiskCache,
         
         html离线Web应用程序缓存。
         WKWebsiteDataTypeOfflineWebApplicationCache,
         
         内存缓存。
         WKWebsiteDataTypeMemoryCache,
         
         本地存储。
         WKWebsiteDataTypeLocalStorage,
         
         Cookies
         WKWebsiteDataTypeCookies,
         
         会话存储
         WKWebsiteDataTypeSessionStorage,
         
         IndexedDB数据库。
         WKWebsiteDataTypeIndexedDBDatabases,
         
         查询数据库。
         WKWebsiteDataTypeWebSQLDatabases
     */
    NSSet * types = [WKWebsiteDataStore allWebsiteDataTypes];
    /**
     WKWebsiteDataStore表示网站可能存在的各种类型的数据
     利用。这包括cookie，磁盘和内存缓存，以及诸如WebSQL之类的持久数据，
     IndexedDB数据库和本地存储。
     */
    WKWebsiteDataStore * store = [WKWebsiteDataStore defaultDataStore];
    // cookie存储
    WKHTTPCookieStore * cookieStore = store.httpCookieStore;
    // 设置cookie
    // [cookieStore setCookie:<#(nonnull NSHTTPCookie *)#> completionHandler:<#^(void)completionHandler#>];
    // 获取所有的cookie
    // cookieStore getAllCookies:<#^(NSArray<NSHTTPCookie *> * _Nonnull)completionHandler#>
    [store removeDataOfTypes:types modifiedSince:date completionHandler:^{
        NSLog(@"清理完毕");
    }];
}

// MARK: - getter


/// 解决 页面内跳转（a标签等）还是取不到cookie的问题
- (void)getCookie {
    //取出cookie
    NSHTTPCookieStorage *cookieStorage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    //js函数
    NSString *JSFuncString =
    @"function setCookie(name,value,expires)\
    {\
    var oDate=new Date();\
    oDate.setDate(oDate.getDate()+expires);\
    document.cookie=name+'='+value+';expires='+oDate+';path=/'\
    }\
    function getCookie(name)\
    {\
    var arr = document.cookie.match(new RegExp('(^| )'+name+'=([^;]*)(;|$)'));\
    if(arr != null) return unescape(arr[2]); return null;\
    }\
    function delCookie(name)\
    {\
    var exp = new Date();\
    exp.setTime(exp.getTime() - 1);\
    var cval=getCookie(name);\
    if(cval!=null) document.cookie= name + '='+cval+';expires='+exp.toGMTString();\
    }";
    
    // 拼凑js字符串
    NSMutableString *JSCookieString = JSFuncString.mutableCopy;
    for (NSHTTPCookie *cookie in cookieStorage.cookies) {
        NSString *excuteJSString = [NSString stringWithFormat:@"setCookie('%@', '%@', 1);", cookie.name, cookie.value];
        [JSCookieString appendString:excuteJSString];
    }
    // 执行js的代码字符串JSCookieString
    [_webView evaluateJavaScript:JSCookieString completionHandler:^(_Nullable id resut, NSError * _Nullable error) {    // 调用js结果
        if (error) {
            NSLog(@"%@", error);
        }
        else {
            NSLog(@"%@", resut);
        }
    }];
}

// MARK: - WKNavigationDelegate主要处理一些跳转、加载处理操作

/// 页面开始加载时调用
- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation {
    NSLog(@"页面开始加载时调用");
}

/// 页面加载失败时调用
- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(null_unspecified WKNavigation *)navigation withError:(NSError *)error {
    [self.progressView setProgress:0.0f animated:NO];
}

/// 当内容开始返回时调用
- (void)webView:(WKWebView *)webView didCommitNavigation:(WKNavigation *)navigation {
    
}

/// 页面加载完成之后调用
- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    [self getCookie];
}

/// 提交发生错误时调用
- (void)webView:(WKWebView *)webView didFailNavigation:(WKNavigation *)navigation withError:(NSError *)error {
    [self.progressView setProgress:0.0f animated:NO];
}

/// 接收到服务器跳转请求即服务重定向时之后调用
- (void)webView:(WKWebView *)webView didReceiveServerRedirectForProvisionalNavigation:(WKNavigation *)navigation {
}

/// 根据WebView对于即将跳转的HTTP请求头信息和相关信息来决定是否跳转和做相应的处理
- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler {
    NSString * urlStr = navigationAction.request.URL.absoluteString;
    NSLog(@"发送跳转请求：%@",urlStr);
    // 自己定义的协议头
    NSString *htmlHeadString = @"github://";
    if([urlStr hasPrefix:htmlHeadString]){
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"通过截取URL调用OC" message:@"你想前往我的Github主页?" preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:([UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        }])];
        [alertController addAction:([UIAlertAction actionWithTitle:@"打开" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            NSURL * url = [NSURL URLWithString:[urlStr stringByReplacingOccurrencesOfString:@"github://callName_?" withString:@""]];
            [[UIApplication sharedApplication] openURL:url options:nil completionHandler:nil];
        }])];
        [self presentViewController:alertController animated:YES completion:nil];
        decisionHandler(WKNavigationActionPolicyCancel);
    }else{
        decisionHandler(WKNavigationActionPolicyAllow);
    }
}

/// 根据客户端受到的服务器响应头以及response相关信息来决定是否可以跳转
- (void)webView:(WKWebView *)webView decidePolicyForNavigationResponse:(WKNavigationResponse *)navigationResponse decisionHandler:(void (^)(WKNavigationResponsePolicy))decisionHandler{
    NSString * urlStr = navigationResponse.response.URL.absoluteString;
    NSLog(@"当前跳转地址：%@",urlStr);
    //允许跳转
    decisionHandler(WKNavigationResponsePolicyAllow);
    //不允许跳转
    //decisionHandler(WKNavigationResponsePolicyCancel);
}

/// 需要响应身份验证时调用 同样在block中需要传入用户身份凭证
- (void)webView:(WKWebView *)webView didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition disposition, NSURLCredential * _Nullable credential))completionHandler{
    // 用户身份信息
    NSURLCredential * newCred = [[NSURLCredential alloc] initWithUser:@"user123" password:@"123" persistence:NSURLCredentialPersistenceNone];
    // 为 challenge 的发送方提供 credential
    [challenge.sender useCredential:newCred forAuthenticationChallenge:challenge];
    completionHandler(NSURLSessionAuthChallengeUseCredential,newCred);
}

/// 进程被终止时调用
- (void)webViewWebContentProcessDidTerminate:(WKWebView *)webView{
    
}

// MARK: - WKUIDelegate ：主要处理JS脚本，确认框，警告框等

/// web界面中有弹出警告框时调用
/// @param webView <#webView description#>
/// @param message <#message description#>
/// @param frame <#frame description#>
/// @param completionHandler <#completionHandler description#>
- (void)webView:(WKWebView *)webView runJavaScriptAlertPanelWithMessage:(NSString *)message initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(void))completionHandler {
   UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"HTML的弹出框" message:message?:@"" preferredStyle:UIAlertControllerStyleAlert];
   [alertController addAction:([UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
       completionHandler();
   }])];
   [self presentViewController:alertController animated:YES completion:nil];
}

/// 确认框：JavaScript调用confirm方法后回调的方法 confirm是js中的确定框，需要在block中把用户选择的情况传递进去
- (void)webView:(WKWebView *)webView runJavaScriptConfirmPanelWithMessage:(NSString *)message initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(BOOL))completionHandler{
   UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"" message:message?:@"" preferredStyle:UIAlertControllerStyleAlert];
   [alertController addAction:([UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
       completionHandler(NO);
   }])];
   [alertController addAction:([UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
       completionHandler(YES);
   }])];
   [self presentViewController:alertController animated:YES completion:nil];
}

/// 输入框：JavaScript调用prompt方法后回调的方法 prompt是js中的输入框 需要在block中把用户输入的信息传入
- (void)webView:(WKWebView *)webView runJavaScriptTextInputPanelWithPrompt:(NSString *)prompt defaultText:(NSString *)defaultText initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(NSString * _Nullable))completionHandler{
   UIAlertController *alertController = [UIAlertController alertControllerWithTitle:prompt message:@"" preferredStyle:UIAlertControllerStyleAlert];
   [alertController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
       textField.text = defaultText;
   }];
   [alertController addAction:([UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
       completionHandler(alertController.textFields[0].text?:@"");
   }])];
   [self presentViewController:alertController animated:YES completion:nil];
}

/// 页面是弹出窗口 _blank 处理
- (WKWebView *)webView:(WKWebView *)webView createWebViewWithConfiguration:(WKWebViewConfiguration *)configuration forNavigationAction:(WKNavigationAction *)navigationAction windowFeatures:(WKWindowFeatures *)windowFeatures {
   if (!navigationAction.targetFrame.isMainFrame) {
       [webView loadRequest:navigationAction.request];
   }
   return nil;
}

// MARK: - WKScriptMessageHandler、WeakWebViewScriptMessageDelegate

/// 被自定义的WKScriptMessageHandler在回调方法里通过代理回调回来，绕了一圈就是为了解决内存不释放的问题
/// 通过接收JS传出消息的name进行捕捉的回调方法
- (void)userContentController:(WKUserContentController *)userContentController didReceiveScriptMessage:(WKScriptMessage *)message{
    NSLog(@"name:%@\\\\n body:%@\\\\n frameInfo:%@\\\\n", message.name, message.body, message.frameInfo);
    // 用message.body获得JS传出的参数体
    NSDictionary * parameter = message.body;
    // JS调用OC
    if ([message.name isEqualToString:@"jsToOcNoPrams"]) {
        // 调用OC方法
    }
    if([message.name isEqualToString:@"jsToOcNoPrams"]){
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"js调用到了oc" message:@"不带参数" preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:([UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        }])];
        [self presentViewController:alertController animated:YES completion:nil];
        
    }else if([message.name isEqualToString:@"jsToOcWithPrams"]){
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"js调用到了oc" message:parameter[@"params"] preferredStyle:UIAlertControllerStyleAlert];
        [alertController addAction:([UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        }])];
        [self presentViewController:alertController animated:YES completion:nil];
    }
}


@end
