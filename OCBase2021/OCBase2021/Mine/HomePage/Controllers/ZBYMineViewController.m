//
//  ZBYMineViewController.m
//  OCBase2021
//
//  Created by 周保勇 on 2021/4/8.
//

#import "ZBYMineViewController.h"
#import "ZBYControllerUIViewController.h"
#import "ZBYCustomViewController.h"
#import "ZBYWKWebViewController.h"
#import "ZBYProjectionScreenViewController.h"

@interface ZBYMineViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, copy) NSArray * models;

@end

@implementation ZBYMineViewController

- (NSArray *)models {
    if (_models == nil) {
        _models = @[
        @{@"title" : @"控制器整体布局实例", @"subTitle" : @"控制器布局"},
        @{@"title" : @"自定义View展示", @"subTitle" : @"一些常用的自定义视图"},
        @{@"title" : @"WKWebView", @"subTitle" : @"WKWebView和js交互"},
        @{@"title" : @"投屏技术", @"subTitle" : @"PlatinumKit投屏实现"}];
    }
    return _models;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITableView * tableView = [[UITableView alloc] initWithFrame:CGRectZero style:(UITableViewStylePlain)];
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    [self.view addSubview:tableView];
    
    tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    tableView.contentInset = UIEdgeInsetsMake(0, 0, (ZBYSize.tabBarHeight + ZBYSize.bottomSafeHeight), 0);
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(ZBYSize.navigationBarHeight);
        make.left.right.bottom.mas_equalTo(0);
    }];
}

// MARK: - UITableViewDelegate, UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.models.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString * identifier = @"123456";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.backgroundColor = [UIColor whiteColor];
        cell.textLabel.textColor = [UIColor blackColor];
        cell.detailTextLabel.textColor = [UIColor blackColor];
    }
    cell.textLabel.text = self.models[indexPath.row][@"title"];
    cell.detailTextLabel.text = self.models[indexPath.row][@"subTitle"];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSDictionary * info = self.models[indexPath.row];
    NSString * title = info[@"title"];
    if ([title containsString:@"控制器整体布局实例"]) {
        ZBYControllerUIViewController * controlleUIVC = [ZBYControllerUIViewController new];
        controlleUIVC.title = title;
        [self.navigationController pushViewController:controlleUIVC animated:YES];
    }
    if ([title containsString:@"自定义View展示"]) {
        ZBYCustomViewController * customViewVC = [ZBYCustomViewController new];
        customViewVC.title = title;
        [self.navigationController pushViewController:customViewVC animated:YES];
    }
    if ([info[@"title"] containsString:@"WKWebView"]) {
        ZBYWKWebViewController * webViewVC = [ZBYWKWebViewController new];
        webViewVC.title = title;
        [self.navigationController pushViewController:webViewVC animated:YES];
    }
    if ([info[@"title"] containsString:@"投屏技术"]) {
        ZBYProjectionScreenViewController * psVC = [ZBYProjectionScreenViewController new];
        psVC.title = title;
        [self.navigationController pushViewController:psVC animated:YES];
    }
//    if ([info[@"title"] containsString:@"多线程中锁"]) {
//        ZBYLockViewController * lockVC = [ZBYLockViewController new];
//        lockVC.title = title;
//        [self.navigationController pushViewController:lockVC animated:YES];
//    }
//    if ([info[@"title"] containsString:@"Block"]) {
//        ZBYBlockViewController * blockVC = [ZBYBlockViewController new];
//        blockVC.title = title;
//        [self.navigationController pushViewController:blockVC animated:YES];
//    }
//    if ([info[@"title"] containsString:@"手势"]) {
//        ZBYGestureViewController * gestureVC = [ZBYGestureViewController new];
//        gestureVC.title = title;
//        [self.navigationController pushViewController:gestureVC animated:YES];
//    }
//    if ([info[@"title"] containsString:@"启动优化"]) {
//        ZBYLessLaunchViewController * launchVC = [ZBYLessLaunchViewController new];
//        launchVC.title = title;
//        [self.navigationController pushViewController:launchVC animated:YES];
//    }
//    if ([info[@"title"] containsString:@"设计原则"]) {
//        ZBYDesignPrinciplesViewController * principlesVC = [ZBYDesignPrinciplesViewController new];
//        principlesVC.title = title;
//        [self.navigationController pushViewController:principlesVC animated:YES];
//    }
//    if ([info[@"title"] containsString:@"设计模式"]) {
//        ZBYDesignPatternsViewController * patternsVC = [ZBYDesignPatternsViewController new];
//        patternsVC.title = title;
//        [self.navigationController pushViewController:patternsVC animated:YES];
//    }
//    if ([info[@"title"] containsString:@"OC中的copy"]) {
//        ZBYCopyViewController * copyVC = [ZBYCopyViewController new];
//        copyVC.title = title;
//        [self.navigationController pushViewController:copyVC animated:YES];
//    }
}

@end
