//
//  ZBYCustomViewController.m
//  OCBase2021
//
//  Created by 周保勇 on 2021/6/2.
//

#import "ZBYCustomViewController.h"
#import "ZBYCustomPopView.h"
#import "ZBYScrollTitleListView.h"
#import "ZBYCustomCollectionViewCell.h"

@interface ZBYCustomViewController () <UITableViewDelegate, UITableViewDataSource, UICollectionViewDataSource, UICollectionViewDelegate, ZBYScrollTitleListViewDelegate>

@property (nonatomic, copy) NSArray * models;

@property (nonatomic, weak) UIScrollView * scrollView;
@property (nonatomic, weak) UIView * customView;

/// 标题数组
@property (nonatomic, copy) NSArray<NSString *> * titleList;

@end

@implementation ZBYCustomViewController

- (NSArray *)models {
    if (_models == nil) {
        _models = @[
        @{@"title" : @"滚动按钮列表视图", @"subTitle" : @"控制器布局"},
        @{@"title" : @"网络请求其它视图", @"subTitle" : @"网络请求为空、失败等显示视图"},
        ];
    }
    return _models;
}

- (NSArray<NSString *> *)titleList {
    if(_titleList == nil) {
        _titleList = @[@"噪声", @"早生华发", @"早生华发1", @"早生华发2", @"早生345华发", @"早生5678华发", @"早生华发"];
    }
    return _titleList;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITableView * tableView = [[UITableView alloc] initWithFrame:CGRectZero style:(UITableViewStylePlain)];
    tableView.delegate = self;
    tableView.dataSource = self;
    [self.view addSubview:tableView];
    
    tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];
    
}

// MARK: - UITableViewDelegate, UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.models.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString * identifier = @"123456";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.backgroundColor = [UIColor whiteColor];
        cell.textLabel.textColor = [UIColor blackColor];
        cell.detailTextLabel.textColor = [UIColor blackColor];
    }
    cell.textLabel.text = self.models[indexPath.row][@"title"];
    cell.detailTextLabel.text = self.models[indexPath.row][@"subTitle"];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSDictionary * info = self.models[indexPath.row];
    NSString * title = info[@"title"];
    if ([title containsString:@"滚动按钮列表视图"]) {
        [self scrollTitleListView];
    }
//    if ([title containsString:@"自定义View展示"]) {
//        ZBYCustomViewController * customViewVC = [ZBYCustomViewController new];
//        customViewVC.title = title;
//        [self.navigationController pushViewController:customViewVC animated:YES];
//    }
//    if ([info[@"title"] containsString:@"KVC"]) {
//        ZBYKVCViewController * kvcVC = [ZBYKVCViewController new];
//        kvcVC.title = title;
//        [self.navigationController pushViewController:kvcVC animated:YES];
//    }
//    if ([info[@"title"] containsString:@"GCD"]) {
//        ZBYGCDViewController * gcdVC = [ZBYGCDViewController new];
//        gcdVC.title = title;
//        [self.navigationController pushViewController:gcdVC animated:YES];
//    }
//    if ([info[@"title"] containsString:@"多线程中锁"]) {
//        ZBYLockViewController * lockVC = [ZBYLockViewController new];
//        lockVC.title = title;
//        [self.navigationController pushViewController:lockVC animated:YES];
//    }
//    if ([info[@"title"] containsString:@"Block"]) {
//        ZBYBlockViewController * blockVC = [ZBYBlockViewController new];
//        blockVC.title = title;
//        [self.navigationController pushViewController:blockVC animated:YES];
//    }
//    if ([info[@"title"] containsString:@"手势"]) {
//        ZBYGestureViewController * gestureVC = [ZBYGestureViewController new];
//        gestureVC.title = title;
//        [self.navigationController pushViewController:gestureVC animated:YES];
//    }
//    if ([info[@"title"] containsString:@"启动优化"]) {
//        ZBYLessLaunchViewController * launchVC = [ZBYLessLaunchViewController new];
//        launchVC.title = title;
//        [self.navigationController pushViewController:launchVC animated:YES];
//    }
//    if ([info[@"title"] containsString:@"设计原则"]) {
//        ZBYDesignPrinciplesViewController * principlesVC = [ZBYDesignPrinciplesViewController new];
//        principlesVC.title = title;
//        [self.navigationController pushViewController:principlesVC animated:YES];
//    }
//    if ([info[@"title"] containsString:@"设计模式"]) {
//        ZBYDesignPatternsViewController * patternsVC = [ZBYDesignPatternsViewController new];
//        patternsVC.title = title;
//        [self.navigationController pushViewController:patternsVC animated:YES];
//    }
//    if ([info[@"title"] containsString:@"OC中的copy"]) {
//        ZBYCopyViewController * copyVC = [ZBYCopyViewController new];
//        copyVC.title = title;
//        [self.navigationController pushViewController:copyVC animated:YES];
//    }
}

// MARK: - 滚动按钮列表视图

- (void)scrollTitleListView {
    ZBYScrollTitleStyleSettingsModel * settings = [ZBYScrollTitleStyleSettingsModel new];
    settings.bottomStyle = ZBYScrollTitleStyleSettingsModelBottomStyleColorLine;
    settings.bottomColor = [UIColor greenColor];
    settings.bottomSize = CGSizeMake(22, 4);
    settings.bottomToScrollBottomHeight = 0;
    settings.bottomAlignment = ZBYScrollTitleStyleSettingsModelBottomAlignmentCenter;
    settings.margin = 0;
    settings.titleSpacing = 0;
    settings.titleInsetMargin = 15;
    settings.titleLayoutStyle = ZBYScrollTitleStyleSettingsModelTitleLayoutStyleAuto;
//    settings.titleWidth = 80;
    settings.isScale = NO;
    settings.scale = 1.375;
    settings.titleFont = [UIFont systemFontOfSize:16 weight:(UIFontWeightMedium)];
    settings.normalColor = [UIColor blackColor];
    settings.selectedColor = [UIColor greenColor];
    settings.normalBackgroundColor = [UIColor grayColor];
    settings.selectedBackgroundColor = [UIColor whiteColor];
    ZBYScrollTitleListView * listView = [ZBYScrollTitleListView scrollTitleListViewWithStyleSettings:settings titleList:self.titleList selectedIndex:2];
    listView.delegate = self;
    listView.frame = CGRectMake(0, 0, ZBYSize.screenWidth, 50);
    ZBYCustomPopView * popView = [ZBYCustomPopView popView];
    [popView.contentView addSubview:listView];
    self.customView = listView;
    
    UICollectionViewFlowLayout * layout = [UICollectionViewFlowLayout new];
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    layout.itemSize = CGSizeMake(ZBYSize.screenWidth, 200);
    layout.minimumLineSpacing = CGFLOAT_MIN;
    layout.minimumInteritemSpacing = CGFLOAT_MIN;
    UICollectionView * collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 50, ZBYSize.screenWidth, 100) collectionViewLayout:layout];
    collectionView.dataSource = self;
    collectionView.delegate = self;
    collectionView.pagingEnabled = YES;
    [collectionView registerClass:ZBYCustomCollectionViewCell.class forCellWithReuseIdentifier:ZBYCustomCollectionViewCell.description];
    [popView.contentView addSubview:collectionView];
    self.scrollView = collectionView;
    
    [popView show];
}

- (void)scrollSelectedTitleIndex:(NSInteger)index {
    NSLog(@"123");
}

- (void)clickSelectedTitleIndex:(NSInteger)index {
    NSLog(@"456");
    [self.scrollView setContentOffset:CGPointMake(ZBYSize.screenWidth * index, 0) animated:YES];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat scrollX = scrollView.contentOffset.x;
    CGFloat progress = scrollX / scrollView.width;
//    NSLog(@"%f", progress);
    ZBYScrollTitleListView * tempView = (ZBYScrollTitleListView *)self.customView;
    tempView.progress = progress;
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {
//    CGFloat scrollX = scrollView.contentOffset.x;
//    CGFloat progress = scrollX / scrollView.width;
//    ZBYScrollTitleListView * tempView = (ZBYScrollTitleListView *)self.customView;
//    tempView.progress = progress;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
//    CGFloat scrollX = scrollView.contentOffset.x;
//    CGFloat progress = scrollX / scrollView.width;
//    ZBYScrollTitleListView * tempView = (ZBYScrollTitleListView *)self.customView;
//    tempView.selectedIndex = progress;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.titleList.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ZBYCustomCollectionViewCell * cell = [ZBYCustomCollectionViewCell cellWithCollectionView:collectionView indexPath:indexPath];
    cell.backgroundColor = [UIColor random];
    return cell;
}

@end
