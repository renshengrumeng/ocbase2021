//
//  ZBYNetworkResultView.h
//  OCBase2021
//
//  Created by 周保勇 on 2021/7/1.
//

#import <UIKit/UIKit.h>
#import "ZBYNetworkResultStyleModel.h"

/// 加载数据异常视图类型
typedef NS_ENUM(NSInteger, ZBYNetworkResultViewType) {
    /// 返回的数据为空
    ZBYNetworkResultViewTypeNull = 0,
    /// 网络请求出错了：点击按钮刷新
    ZBYNetworkResultViewTypeClickRefresh = 1,
    /// 网络请求出错了：下拉刷新
    ZBYNetworkResultViewTypeDropdownRefresh = 2,
};

NS_ASSUME_NONNULL_BEGIN

@interface ZBYNetworkResultView : UIView

+ (instancetype)viewWithType:(ZBYNetworkResultViewType)type;

+ (instancetype)viewWithType:(ZBYNetworkResultViewType)type style:(ZBYNetworkResultStyleModel *)style;

/// 布局和信息
@property (nonatomic, strong) ZBYNetworkResultStyleModel * style;

@end

NS_ASSUME_NONNULL_END
