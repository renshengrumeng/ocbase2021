//
//  ZBYNetworkResultStyleModel.h
//  OCBase2021
//
//  Created by 周保勇 on 2021/7/5.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZBYNetworkResultStyleModel : NSObject

/// 图片名称
@property (nonatomic, copy) NSString * imageName;

/// 标题
@property (nonatomic, copy) NSString * title;

/// 图片距离顶部距离
@property (nonatomic, assign) CGFloat topMargin;

@end

NS_ASSUME_NONNULL_END
