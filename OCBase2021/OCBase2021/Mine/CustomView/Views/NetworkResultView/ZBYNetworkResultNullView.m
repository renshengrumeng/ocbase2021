//
//  ZBYNetworkResultNullView.m
//  OCBase2021
//
//  Created by 周保勇 on 2021/7/1.
//

#import "ZBYNetworkResultNullView.h"

@interface ZBYNetworkResultNullView ()

@property (nonatomic, strong) UIImageView * imageView;
@property (nonatomic, strong) UILabel * titleLabel;

@end

@implementation ZBYNetworkResultNullView

- (void)layoutSubviews {
    [super layoutSubviews];
    
}

@end
