//
//  ZBYNetworkResultView.m
//  OCBase2021
//
//  Created by 周保勇 on 2021/7/1.
//

#import "ZBYNetworkResultView.h"
#import "ZBYNetworkResultNullView.h"
#import "ZBYNetworkResultClickRefreshView.h"
#import "ZBYNetworkResultDropdownRefreshView.h"
#import "ZBYNetworkResultErrorView.h"

@implementation ZBYNetworkResultView

+ (instancetype)viewWithType:(ZBYNetworkResultViewType)type {
    ZBYNetworkResultStyleModel * style = [ZBYNetworkResultStyleModel new];
    style.imageName = @"";
    style.title = @"";
    style.topMargin = 100;
    return [self viewWithType:type style:style];
}

+ (instancetype)viewWithType:(ZBYNetworkResultViewType)type style:(ZBYNetworkResultStyleModel *)style {
    switch (type) {
        case ZBYNetworkResultViewTypeNull: {
            ZBYNetworkResultNullView * view = [ZBYNetworkResultNullView new];
            return view;
        }
        case ZBYNetworkResultViewTypeClickRefresh: {
            ZBYNetworkResultClickRefreshView * view = [ZBYNetworkResultClickRefreshView new];
            return view;
        }
        case ZBYNetworkResultViewTypeDropdownRefresh: {
            ZBYNetworkResultDropdownRefreshView * view = [ZBYNetworkResultDropdownRefreshView new];
            return view;
        }
    }
}

@end
