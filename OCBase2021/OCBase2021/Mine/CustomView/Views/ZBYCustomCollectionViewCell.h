//
//  ZBYCustomCollectionViewCell.h
//  OCBase2021
//
//  Created by 周保勇 on 2021/6/3.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZBYCustomCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) NSObject * model;

+ (instancetype)cellWithCollectionView:(UICollectionView *)collectionView indexPath:(NSIndexPath *)indexPath;

@end

NS_ASSUME_NONNULL_END
