//
//  ZBYCustomPopView.m
//  OCBase2021
//
//  Created by 周保勇 on 2021/6/3.
//

#import "ZBYCustomPopView.h"

@interface ZBYCustomPopView ()

@property (nonatomic, weak) UIView * contentView;

@end

@implementation ZBYCustomPopView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self initSubviews];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.contentView.frame = CGRectMake(0, ZBYSize.screenHeight * 0.25, ZBYSize.screenWidth, ZBYSize.screenHeight * 0.5);
    [UIView animateWithDuration:0.25 animations:^{
        self.backgroundColor = [[UIColor blackColor] colorWithAlpha:0.6];
        self.contentView.alpha = 1;
    } completion:^(BOOL finished) {
        
    }];
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    CGPoint point = [touches.anyObject locationInView:self];
    if (!CGRectContainsPoint(self.contentView.frame, point)) {
        [self dismiss];
    }
}

+ (instancetype)popView {
    ZBYCustomPopView * popView = [ZBYCustomPopView new];
    return popView;
}

- (void)show {
    self.frame = UIApplication.sharedApplication.currentWindow.bounds;
    self.backgroundColor = [[UIColor blackColor] colorWithAlpha:0.0];
    [UIApplication.sharedApplication.currentWindow addSubview:self];
}

- (void)dismiss {
    [UIView animateWithDuration:0.25 animations:^{
        self.backgroundColor = [[UIColor blackColor] colorWithAlpha:0.0];
        self.contentView.alpha = 0;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

- (void)initSubviews {
    UIView * contentView = [UIView new];
    contentView.backgroundColor = [UIColor whiteColor];
    contentView.alpha = 0;
    [self addSubview:contentView];
    self.contentView = contentView;
}

@end
