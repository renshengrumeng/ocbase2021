//
//  ZBYCustomPopView.h
//  OCBase2021
//
//  Created by 周保勇 on 2021/6/3.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZBYCustomPopView : UIView

@property (nonatomic, weak, readonly) UIView * contentView;

+ (instancetype)popView;

- (void)show;

@end

NS_ASSUME_NONNULL_END
