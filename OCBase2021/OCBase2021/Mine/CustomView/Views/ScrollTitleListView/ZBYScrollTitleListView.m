//
//  ZBYScrollTitleListView.m
//  OCBase2021
//
//  Created by 周保勇 on 2021/6/2.
//

#import "ZBYScrollTitleListView.h"

CGFloat const animationDuration = 0.25;

@interface ZBYScrollTitleListView ()

@property (nonatomic, weak) UIScrollView * scrollView;
@property (nonatomic, weak) UIImageView * bottomView;

/// 放文字视图的数组
@property (nonatomic, strong) NSMutableArray<UILabel *> * labelList;

@property (nonatomic, weak) UILabel * selectedLabel;

/// 滚动选中还是点击选中
@property (nonatomic, assign) BOOL isClickSelected;

/// 点击选中目标索引
@property (nonatomic, assign) NSInteger clickSelectedIndex;

@end

@implementation ZBYScrollTitleListView

- (void)setTitleList:(NSArray<NSString *> *)titleList {
    _titleList = [titleList copy];
    NSInteger titleCount = titleList.count;
    while (titleCount > self.labelList.count) {
        UILabel * titleLabel = [UILabel new];
        titleLabel.font = self.styleSettings.titleFont;
        titleLabel.textColor = self.styleSettings.normalColor;
        titleLabel.backgroundColor = self.styleSettings.normalBackgroundColor;
        titleLabel.textAlignment = NSTextAlignmentCenter;
        titleLabel.userInteractionEnabled = YES;
        
        UITapGestureRecognizer * tapG = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapClick:)];
        tapG.numberOfTapsRequired = 1;
        [titleLabel addGestureRecognizer:tapG];
        
        [self.labelList addObject:titleLabel];
        [self.scrollView addSubview:titleLabel];
    }
    [self.scrollView bringSubviewToFront:self.bottomView];
    [_titleList enumerateObjectsUsingBlock:^(NSString * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        self.labelList[idx].text = obj;
    }];
    [self setNeedsLayout];
}

- (void)setStyleSettings:(ZBYScrollTitleStyleSettingsModel *)styleSettings {
    _styleSettings = styleSettings;
    for (UILabel * item in self.labelList) {
        item.font = styleSettings.titleFont;
        item.backgroundColor = styleSettings.normalBackgroundColor;
    }
    [self setNeedsLayout];
}

- (void)setSelectedIndex:(NSInteger)selectedIndex {
    _selectedIndex = selectedIndex;
}

- (void)setProgress:(CGFloat)progress {
    _progress = progress;
    if (self.isClickSelected) {
        return;
    }
    if (self.selectedIndex == 0 && progress <= 0) {
        return;
    }
    if (self.selectedIndex == (self.titleList.count - 1) && progress >= (self.titleList.count - 1)) {
        return;
    }
    if (progress < 0) {
        switch (self.styleSettings.bottomAlignment) {
            case ZBYScrollTitleStyleSettingsModelBottomAlignmentCenter:
                self.bottomView.centerX = self.selectedLabel.centerX;
                break;
            case ZBYScrollTitleStyleSettingsModelBottomAlignmentLeft:
                self.bottomView.x = self.selectedLabel.x;
                break;
            case ZBYScrollTitleStyleSettingsModelBottomAlignmentRight:
                self.bottomView.x = self.selectedLabel.maxX - self.bottomView.width - self.styleSettings.titleInsetMargin;
                break;
                
            default:
                break;
        }
    }
    else if (progress > (self.titleList.count - 1)) {
        
    }
    else {
        CGFloat scrollProgress = progress - self.selectedIndex;
        NSInteger targetIndex = 0;
        if (scrollProgress >= 0) {
            targetIndex = self.selectedIndex + 1;
        }
        else {
            targetIndex = self.selectedIndex - 1;
        }
        // 滚动处理
        switch (self.styleSettings.bottomAlignment) {
            case ZBYScrollTitleStyleSettingsModelBottomAlignmentCenter: {
                CGFloat scrollMaxX = fabs(self.labelList[targetIndex].centerX - self.labelList[self.selectedIndex].centerX);
                CGFloat scrollX = scrollMaxX * scrollProgress;
                self.bottomView.centerX = (self.selectedLabel.centerX + scrollX);
                break;
            }
            case ZBYScrollTitleStyleSettingsModelBottomAlignmentLeft: {
                CGFloat scrollMaxX = fabs((self.labelList[targetIndex].x + self.styleSettings.titleInsetMargin) - (self.labelList[self.selectedIndex].x + self.styleSettings.titleInsetMargin));
                CGFloat scrollX = scrollMaxX * scrollProgress;
                self.bottomView.x = ((self.selectedLabel.x + self.styleSettings.titleInsetMargin) + scrollX);
                break;
            }
            case ZBYScrollTitleStyleSettingsModelBottomAlignmentRight: {
                CGFloat scrollMaxX = fabs((self.labelList[targetIndex].maxX - self.styleSettings.titleInsetMargin - self.styleSettings.bottomSize.width) - (self.labelList[self.selectedIndex].maxX - self.styleSettings.titleInsetMargin - self.styleSettings.bottomSize.width));
                CGFloat scrollX = scrollMaxX * scrollProgress;
                self.bottomView.x = self.selectedLabel.maxX - self.bottomView.width - self.styleSettings.titleInsetMargin + scrollX;
                break;
            }
            default:
                break;
        }
        // 临界值处理
        if (scrollProgress == 1 || scrollProgress == -1) {
            self.selectedIndex = targetIndex;
            [self selectedAnimation:targetIndex];
            if ([self.delegate respondsToSelector:@selector(scrollSelectedTitleIndex:)]) {
                dispatch_after(animationDuration, dispatch_get_main_queue(), ^{
                    [self.delegate scrollSelectedTitleIndex:self.selectedIndex];
                });
            }
        }
    }
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.labelList = [[NSMutableArray alloc] initWithCapacity:0];
        [self initSubviews];
    }
    return self;
}

+ (instancetype)scrollTitleListViewWithStyleSettings:(ZBYScrollTitleStyleSettingsModel *)styleSettings titleList:(NSArray<NSString *> *)titleList selectedIndex:(NSInteger)selectedIndex {
    ZBYScrollTitleListView * listView = [ZBYScrollTitleListView new];
    listView.styleSettings = styleSettings;
    listView.titleList = titleList;
    listView.selectedIndex = selectedIndex;
    return listView;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    CGFloat titleCount = self.titleList.count;
    if (titleCount <= 0) {
        return;
    }
    self.scrollView.frame = self.bounds;
    self.bottomView.size = self.styleSettings.bottomSize;
    self.bottomView.centerY = self.height - self.styleSettings.bottomToScrollBottomHeight - self.styleSettings.bottomSize.height * 0.5;
    switch (self.styleSettings.bottomStyle) {
        case ZBYScrollTitleStyleSettingsModelBottomStyleNone:
//            self.bottomView.hidden = YES;
            break;
        case ZBYScrollTitleStyleSettingsModelBottomStyleColorLine:
//            self.bottomView.hidden = NO;
            self.bottomView.backgroundColor = self.styleSettings.bottomColor;
            self.bottomView.layer.cornerRadius = 0;
            self.bottomView.layer.masksToBounds = NO;
            break;
        case ZBYScrollTitleStyleSettingsModelBottomStyleColorRoundedCornersLine:
//            self.bottomView.hidden = NO;
            self.bottomView.backgroundColor = self.styleSettings.bottomColor;
            self.bottomView.layer.cornerRadius = self.styleSettings.bottomSize.height * 0.5;
            self.bottomView.layer.masksToBounds = YES;
            break;
        case ZBYScrollTitleStyleSettingsModelBottomStyleImage:
//            self.bottomView.hidden = NO;
            self.bottomView.image = self.styleSettings.bottomImage;
            self.bottomView.layer.cornerRadius = self.styleSettings.bottomSize.height * 0.5;
            self.bottomView.layer.masksToBounds = YES;
            break;
            
        default:
            break;
    }
    CGFloat margin = self.styleSettings.margin; // 文字视图距离滚动视图的左右边距
    CGFloat titleSpacing = self.styleSettings.titleSpacing;  // 文字视图之间的间距
    __block CGFloat labelX = margin;    // 文字视图的x点位置
    __block CGFloat labelW = 0.0;       // 文字的宽度
    CGFloat titleInsetMargin = self.styleSettings.titleInsetMargin; // 文字视图内部文字距离文字视图左右的边距
    switch (self.styleSettings.titleLayoutStyle) {
        case ZBYScrollTitleStyleSettingsModelTitleLayoutStyleAuto: {
            [self.labelList enumerateObjectsUsingBlock:^(UILabel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                obj.hidden = idx >= titleCount;
                labelW = ceilf([self.titleList[idx] widthWithFont:self.styleSettings.titleFont]) + 2 * titleInsetMargin;
                obj.size = CGSizeMake(labelW , self.height);
                obj.center = CGPointMake(labelX + labelW * 0.5, self.height * 0.5);
                labelX += (labelW + titleSpacing);
            }];
            self.scrollView.contentSize = CGSizeMake(labelX + margin - titleSpacing, self.height);
            break;
        }
        case ZBYScrollTitleStyleSettingsModelTitleLayoutStyleDivide: {
            labelW = self.width / titleCount;
            labelX = 0.0;
            [self.labelList enumerateObjectsUsingBlock:^(UILabel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                obj.hidden = idx >= titleCount;
                obj.size = CGSizeMake(labelW, self.height);
                obj.center = CGPointMake(idx * labelW + labelW * 0.5, self.height * 0.5);
                labelX += labelW;
            }];
            self.scrollView.contentSize = CGSizeMake(labelX, self.height);
            break;
        }
        case ZBYScrollTitleStyleSettingsModelTitleLayoutStyleWidth: {
            labelW = self.styleSettings.titleWidth + 2 * titleInsetMargin;
            [self.labelList enumerateObjectsUsingBlock:^(UILabel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                obj.hidden = idx >= titleCount;
                obj.size = CGSizeMake(labelW, self.height);
                obj.center = CGPointMake(labelX + labelW * 0.5, self.height * 0.5);
                labelX += (labelW + titleSpacing);
            }];
            self.scrollView.contentSize = CGSizeMake(labelX + margin - titleSpacing, self.height);
            break;
        }
        default:
            break;
    }
    if (self.styleSettings.isScale) {
        for (UILabel * item in self.labelList) {
            item.alpha = 0.4;
        }
    }
    [self selectedAnimation:self.selectedIndex];
    self.isClickSelected = YES;
    if ([self.delegate respondsToSelector:@selector(clickSelectedTitleIndex:)]) {
        dispatch_after(animationDuration, dispatch_get_main_queue(), ^{
            [self.delegate clickSelectedTitleIndex:self.selectedIndex];
        });
    }
}

- (void)initSubviews {
    UIScrollView * scrollView = [UIScrollView new];
    [self addSubview:scrollView];
    self.scrollView = scrollView;
    
    UIImageView * bottomView = [UIImageView new];
    bottomView.hidden = YES;
    [scrollView addSubview:bottomView];
    self.bottomView = bottomView;
}

- (void)tapClick:(UIGestureRecognizer *)sender {
    UIView * gestureView = sender.view;
    [self.labelList enumerateObjectsUsingBlock:^(UILabel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (obj == gestureView) {
            if (self.selectedIndex != idx) {
                self.selectedIndex = idx;
                self.isClickSelected = YES;
                [self selectedAnimation:idx];
                if ([self.delegate respondsToSelector:@selector(clickSelectedTitleIndex:)]) {
                    dispatch_after(animationDuration, dispatch_get_main_queue(), ^{
                        [self.delegate clickSelectedTitleIndex:self.selectedIndex];
                    });
                }
                *stop = YES;
            }
        }
    }];
}

- (void)selectedWithoutAnimation {
    if (self.styleSettings.isScale) {
        self.selectedLabel.alpha = 0.4;
        self.selectedLabel.transform = CGAffineTransformIdentity;
        self.labelList[self.selectedIndex].alpha = 1.0;
        self.labelList[self.selectedIndex].transform = CGAffineTransformMakeScale(self.styleSettings.scale, self.styleSettings.scale);
    }
    self.selectedLabel.backgroundColor = self.styleSettings.normalBackgroundColor;
    self.selectedLabel.textColor = self.styleSettings.normalColor;
    self.labelList[self.selectedIndex].backgroundColor = self.styleSettings.selectedBackgroundColor;
    self.labelList[self.selectedIndex].textColor = self.styleSettings.selectedColor;
    switch (self.styleSettings.bottomAlignment) {
        case ZBYScrollTitleStyleSettingsModelBottomAlignmentCenter:
            self.bottomView.centerX = self.labelList[self.selectedIndex].centerX;
            break;
        case ZBYScrollTitleStyleSettingsModelBottomAlignmentLeft:
            self.bottomView.x = self.labelList[self.selectedIndex].x + self.styleSettings.titleInsetMargin;
            break;
        case ZBYScrollTitleStyleSettingsModelBottomAlignmentRight:
            self.bottomView.x = self.labelList[self.selectedIndex].maxX - self.bottomView.width - self.styleSettings.titleInsetMargin;
            break;
            
        default:
            break;
    }
    self.selectedLabel = self.labelList[self.selectedIndex];
}

- (void)selectedAnimation:(NSInteger)selectedIndex {
    self.userInteractionEnabled = NO;
    [UIView animateWithDuration:animationDuration animations:^{
        if (self.styleSettings.isScale) {
            self.selectedLabel.alpha = 0.4;
            self.selectedLabel.transform = CGAffineTransformIdentity;
            self.labelList[selectedIndex].alpha = 1.0;
            self.labelList[selectedIndex].transform = CGAffineTransformMakeScale(self.styleSettings.scale, self.styleSettings.scale);
        }
        self.selectedLabel.backgroundColor = self.styleSettings.normalBackgroundColor;
        self.selectedLabel.textColor = self.styleSettings.normalColor;
        self.labelList[selectedIndex].backgroundColor = self.styleSettings.selectedBackgroundColor;
        self.labelList[selectedIndex].textColor = self.styleSettings.selectedColor;
        switch (self.styleSettings.bottomAlignment) {
            case ZBYScrollTitleStyleSettingsModelBottomAlignmentCenter:
                self.bottomView.centerX = self.labelList[selectedIndex].centerX;
                break;
            case ZBYScrollTitleStyleSettingsModelBottomAlignmentLeft:
                self.bottomView.x = self.labelList[selectedIndex].x + self.styleSettings.titleInsetMargin;
                break;
            case ZBYScrollTitleStyleSettingsModelBottomAlignmentRight:
                self.bottomView.x = self.labelList[selectedIndex].maxX - self.bottomView.width - self.styleSettings.titleInsetMargin;
                break;
                
            default:
                break;
        }
    } completion:^(BOOL finished) {
        if (finished) {
            self.isClickSelected = false;
            self.userInteractionEnabled = YES;
            self.selectedLabel = self.labelList[selectedIndex];
            self.bottomView.hidden = self.styleSettings.bottomStyle == ZBYScrollTitleStyleSettingsModelBottomStyleNone;
            CGFloat selectedCenterX = self.selectedLabel.centerX;
            CGFloat scrollX = selectedCenterX - self.width * 0.5;
            CGFloat scrollMaxX = self.scrollView.contentSize.width - self.width * 0.5;
            if (scrollMaxX <= 0) {
                return;
            }
            if (selectedCenterX <= self.width * 0.5) {
                [self.scrollView setContentOffset:CGPointZero animated:YES];
            }
            else if (selectedCenterX > self.width * 0.5 && selectedCenterX < scrollMaxX) {
                [self.scrollView setContentOffset:CGPointMake(scrollX, 0) animated:YES];
            }
            else {
                [self.scrollView setContentOffset:CGPointMake(scrollMaxX - self.width * 0.5, 0) animated:YES];
            }
        }
    }];
}

@end
