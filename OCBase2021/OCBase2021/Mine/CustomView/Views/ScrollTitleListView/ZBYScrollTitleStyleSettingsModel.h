//
//  ZBYScrollTitleStyleSettingsModel.h
//  OCBase2021
//
//  Created by 周保勇 on 2021/6/2.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, ZBYScrollTitleStyleSettingsModelBottomStyle) {
    /// 隐藏
    ZBYScrollTitleStyleSettingsModelBottomStyleNone = 0,
    /// 默认纯色线条
    ZBYScrollTitleStyleSettingsModelBottomStyleColorLine = 1,
    /// 线条两头圆角
    ZBYScrollTitleStyleSettingsModelBottomStyleColorRoundedCornersLine = 2,
    /// 图片
    ZBYScrollTitleStyleSettingsModelBottomStyleImage = 3,
};

typedef NS_ENUM(NSUInteger, ZBYScrollTitleStyleSettingsModelBottomAlignment) {
    /// 和文字中间对齐
    ZBYScrollTitleStyleSettingsModelBottomAlignmentCenter = 0,
    /// 和文字左对齐
    ZBYScrollTitleStyleSettingsModelBottomAlignmentLeft = 1,
    /// 和文字右对齐
    ZBYScrollTitleStyleSettingsModelBottomAlignmentRight = 2,
};

typedef NS_ENUM(NSUInteger, ZBYScrollTitleStyleSettingsModelTitleLayoutStyle) {
    /// 根据文字的宽度布局
    ZBYScrollTitleStyleSettingsModelTitleLayoutStyleAuto = 0,
    /// 根据滚动的视图的宽度和文字的个数均分布局：所有边距都为0
    ZBYScrollTitleStyleSettingsModelTitleLayoutStyleDivide = 1,
    /// 文字固定宽度布局
    ZBYScrollTitleStyleSettingsModelTitleLayoutStyleWidth = 2,
};

@interface ZBYScrollTitleStyleSettingsModel : NSObject

/// 底部滚动条样式
@property (nonatomic, assign) ZBYScrollTitleStyleSettingsModelBottomStyle bottomStyle;

/// 底部滚动条图片
@property (nonatomic, strong) UIImage * bottomImage;

/// 底部滚动条颜色
@property (nonatomic, strong) UIColor * bottomColor;

/// 底部滚动条尺寸
@property (nonatomic, assign) CGSize bottomSize;

/// 底部滚动条距离底部的高度
@property (nonatomic, assign) CGFloat bottomToScrollBottomHeight;

/// 底部滚动条对其样式
@property (nonatomic, assign) ZBYScrollTitleStyleSettingsModelBottomAlignment bottomAlignment;

/// 边距
@property (nonatomic, assign) CGFloat margin;

/// 标题之间的间距
@property (nonatomic, assign) CGFloat titleSpacing;

/// 标题视图内部文字距离标题视图左右的边距
@property (nonatomic, assign) CGFloat titleInsetMargin;

/// 标题布局样式
@property (nonatomic, assign) ZBYScrollTitleStyleSettingsModelTitleLayoutStyle titleLayoutStyle;

/// 标题定宽布局的宽度
@property (nonatomic, assign) CGFloat titleWidth;

/// 标题是否可以放大和缩小
@property (nonatomic, assign) BOOL isScale;

/// 放大和缩小的比例
@property (nonatomic, assign) CGFloat scale;

/// 文字字体
@property (nonatomic, strong) UIFont * titleFont;

/// 未选中文字颜色
@property (nonatomic, strong) UIColor * normalColor;

/// 选中文字颜色
@property (nonatomic, strong) UIColor * selectedColor;

/// 未选中标题背景颜色
@property (nonatomic, strong) UIColor * normalBackgroundColor;

/// 选中标题背景颜色
@property (nonatomic, strong) UIColor * selectedBackgroundColor;

@end

NS_ASSUME_NONNULL_END
