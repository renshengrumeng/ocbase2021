//
//  ZBYScrollTitleListView.h
//  OCBase2021
//
//  Created by 周保勇 on 2021/6/2.
//

#import <UIKit/UIKit.h>
#import "ZBYScrollTitleStyleSettingsModel.h"

NS_ASSUME_NONNULL_BEGIN

@protocol ZBYScrollTitleListViewDelegate <NSObject>

@optional
/// 点击选中
- (void)clickSelectedTitleIndex:(NSInteger)index;
/// 滚动选中
- (void)scrollSelectedTitleIndex:(NSInteger)index;

@required

@end

@interface ZBYScrollTitleListView : UIView

@property (nonatomic, weak) id<ZBYScrollTitleListViewDelegate> delegate;

/// 标题列表
@property (nonatomic, copy) NSArray<NSString *> * titleList;

/// 选中的标题索引
@property (nonatomic, assign) NSInteger selectedIndex;

/// 滚动进度
@property (nonatomic, assign) CGFloat progress;

/// 样式
@property (nonatomic, strong) ZBYScrollTitleStyleSettingsModel * styleSettings;

/// 滚动标题列表
/// @param styleSettings 样式设置
/// @param titleList 标题数组
/// @param selectedIndex 选中索引
+ (instancetype)scrollTitleListViewWithStyleSettings:(ZBYScrollTitleStyleSettingsModel *)styleSettings titleList:(NSArray<NSString *> *)titleList selectedIndex:(NSInteger)selectedIndex;

@end

NS_ASSUME_NONNULL_END
