//
//  ZBYCustomCollectionViewCell.m
//  OCBase2021
//
//  Created by 周保勇 on 2021/6/3.
//

#import "ZBYCustomCollectionViewCell.h"

@implementation ZBYCustomCollectionViewCell

+ (instancetype)cellWithCollectionView:(UICollectionView *)collectionView indexPath:(NSIndexPath *)indexPath {
    static NSString * identifier;
    identifier = NSStringFromClass([self class]);
    ZBYCustomCollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    return cell;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initSubviews];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setModel:(NSObject *)model {
    _model = model;
    
}

- (void)initSubviews {}

@end
