//
//  NSObject+Extension.m
//  OC-framework
//
//  Created by 周保勇 on 2019/4/28.
//  Copyright © 2019 周保勇. All rights reserved.
//

#import "NSObject+Extension.h"
#import <objc/runtime.h>
#import <objc/message.h>

@implementation NSObject (Extension)

+ (void)swizzledOldMethod:(SEL)oldSelector newMethod:(SEL)newSelector {
    Method oldMethod = class_getInstanceMethod(self, oldSelector);
    Method newMethod = class_getInstanceMethod(self, newSelector);
    if (oldMethod == nil || newMethod == nil) {
        NSLog(@"交换的方法为空");
        return;
    }
    BOOL success = class_addMethod(self, newSelector, method_getImplementation(newMethod), method_getTypeEncoding(newMethod));
    if (success) {
        class_replaceMethod(self, oldSelector, method_getImplementation(newMethod), method_getTypeEncoding(newMethod));
    }
    else {
        method_exchangeImplementations(oldMethod, newMethod);
    }
}

@end
