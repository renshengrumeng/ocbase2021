//
//  NSObject+Runtime.h
//  OCBase2021
//
//  Created by 周保勇 on 2021/5/11.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSObject (Runtime)

/// 获取当前类的ivar名称列表
- (NSArray<NSString *> *)ivarNameList;

/// 获取当前类的ivar名称列表
+ (NSArray<NSString *> *)ivarNameList;

/// 获取类及其超类的所有ivar名称列表
- (NSArray<NSString *> *)allIvarNameList;

/// 获取类及其超类的所有ivar名称列表
+ (NSArray<NSString *> *)allIvarNameList;

/// 获取类及其子孙类的名称列表
- (NSArray<NSString *> *)subClassNameList;

/// 获取类及其子孙类的名称列表
+ (NSArray<NSString *> *)subClassNameList;

/// 获取类及其子孙类列表
- (NSArray<Class> *)subClassList;

/// 获取类及其超类的名称列表
- (NSArray<NSString *> *)superClassNameList;

/// 获取类及其超类的名称列表
+ (NSArray<NSString *> *)superClassNameList;

/// 获取类及其超类列表
- (NSArray<Class> *)superClassList;

/// 获取当前类的属性名称列表
- (NSArray<NSString *> *)propertyNameList;

/// 获取当前类的属性名称列表
+ (NSArray<NSString *> *)propertyNameList;

/// 获取类及其超类的所有属性名称列表
- (NSArray<NSString *> *)allPropertyNameList;

/// 获取类及其超类的所有属性名称列表
+ (NSArray<NSString *> *)allPropertyNameList;

/// 获取当前类的方法名称列表
- (NSArray<NSString *> *)methodNameList;

/// 获取当前类的方法名称列表
+ (NSArray<NSString *> *)methodNameList;

/// 获取类及其超类的所有方法名称列表
- (NSArray<NSString *> *)allMethodNameList;

/// 获取类及其超类的所有方法名称列表
+ (NSArray<NSString *> *)allMethodNameList;

/// 获取当前类的类方法名称列表
- (NSArray<NSString *> *)classMethodNameList;

/// 获取当前类的类方法名称列表
+ (NSArray<NSString *> *)classMethodNameList;

/// 获取类及其超类的所有类方法名称列表
- (NSArray<NSString *> *)allClassMethodNameList;

/// 获取类及其超类的所有类方法名称列表
+ (NSArray<NSString *> *)allClassMethodNameList;

// MARK: - Base Method

+ (NSArray<Class> *)allRegisteredClasseList;

/// 获取类及其子孙类列表
+ (NSArray<Class> *)subClassList;

/// 获取类及其超类列表
+ (NSArray<Class> *)superClassList;

/// 获取传入类的ivar名称列表
/// @param cls 出入类
+ (NSArray<NSString *> *)ivarNameListWithClass:(Class)cls;

/// 获取传入类的属性名称列表
/// @param cls 出入类
+ (NSArray<NSString *> *)propertyNameListWithClass:(Class)cls;

/// 获取传入类的方法名称列表
/// @param cls 出入类
+ (NSArray<NSString *> *)methodNameListWithClass:(Class)cls;

/// 获取传入类的类方法名称列表
/// @param cls 出入类
+ (NSArray<NSString *> *)classMethodNameListWithClass:(Class)cls;

@end

NS_ASSUME_NONNULL_END
