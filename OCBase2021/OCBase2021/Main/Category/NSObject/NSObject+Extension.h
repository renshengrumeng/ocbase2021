//
//  NSObject+Extension.h
//  OC-framework
//
//  Created by 周保勇 on 2019/4/28.
//  Copyright © 2019 周保勇. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSObject (Extension)

/// 替换方法
/// @param oldSelector 老方法
/// @param newSelector 就方法
+ (void)swizzledOldMethod:(SEL)oldSelector newMethod:(SEL)newSelector;

@end

NS_ASSUME_NONNULL_END
