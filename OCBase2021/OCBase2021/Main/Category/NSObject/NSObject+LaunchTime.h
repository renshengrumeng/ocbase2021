//
//  NSObject+LaunchTime.h
//  OCBase2021
//
//  Created by 周保勇 on 2021/7/6.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSObject (LaunchTime)

/// App进程开始时间：单位毫秒，13位时间戳
+ (NSTimeInterval)processStartTime;

/// CoreFoundation时间统计：单位毫秒，13位时间戳
+ (NSTimeInterval)launchTime;

/// 耗时标记：测试代码效率
+ (NSTimeInterval)mediaTime;

@end

NS_ASSUME_NONNULL_END
