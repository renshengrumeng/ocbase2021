//
//  NSObject+Runtime.m
//  OCBase2021
//
//  Created by 周保勇 on 2021/5/11.
//

#import "NSObject+Runtime.h"
#import <objc/runtime.h>

@implementation NSObject (Runtime)

- (NSArray<NSString *> *)ivarNameList {
    return [self.class ivarNameListWithClass:self.class];
}

+ (NSArray<NSString *> *)ivarNameList {
    return [self ivarNameListWithClass:self];
}

- (NSArray<NSString *> *)allIvarNameList {
    return [self.class allIvarNameList];
}

+ (NSArray<NSString *> *)allIvarNameList {
    NSMutableArray * ivarNameList = [[NSMutableArray alloc] init];
    NSArray<Class> * classList = [self superClassList];
    for (Class item in classList) {
        NSArray * itemIvarNameList = [self ivarNameListWithClass:item];
        [ivarNameList addObjectsFromArray:itemIvarNameList];
    }
    return ivarNameList;
}

- (NSArray<NSString *> *)subClassNameList {
    return [self.class subClassNameList];
}

+ (NSArray<NSString *> *)subClassNameList {
    NSMutableArray * classNameList = [[NSMutableArray alloc] init];
    NSArray<Class> * classList = [self subClassList];
    for (Class item in classList) {
        [classNameList addObject:NSStringFromClass(item)];
    }
    return classNameList;
}

- (NSArray<Class> *)subClassList {
    return [self.class subClassList];
}

- (NSArray<NSString *> *)superClassNameList {
    return [self.class superClassNameList];
}

+ (NSArray<NSString *> *)superClassNameList {
    NSMutableArray * classNameList = [[NSMutableArray alloc] init];
    NSArray<Class> * classList = [self superClassList];
    for (Class item in classList) {
        [classNameList addObject:NSStringFromClass(item)];
    }
    return classNameList;
}

- (NSArray<Class> *)superClassList {
    return [self.class superClassList];
}

- (NSArray<NSString *> *)propertyNameList {
    return [self.class propertyNameList];
}

+ (NSArray<NSString *> *)propertyNameList {
    return [self propertyNameListWithClass:self];
}

- (NSArray<NSString *> *)allPropertyNameList {
    return [self.class allPropertyNameList];
}

+ (NSArray<NSString *> *)allPropertyNameList {
    NSMutableArray * propertyNameList = [[NSMutableArray alloc] init];
    NSArray<Class> * classList = [self superClassList];
    for (Class item in classList) {
        NSArray * itemPropertyNameList = [self propertyNameListWithClass:item];
        for (NSString * pName in itemPropertyNameList) {
            // 去重
            if (![propertyNameList containsObject:pName]) {
                [propertyNameList addObject:pName];
            }
        }
    }
    return propertyNameList;
}

- (NSArray<NSString *> *)methodNameList {
    return [self.class methodNameList];
}

+ (NSArray<NSString *> *)methodNameList {
    return [self methodNameListWithClass:self];
}

- (NSArray<NSString *> *)allMethodNameList {
    return [self.class allMethodNameList];
}

+ (NSArray<NSString *> *)allMethodNameList {
    NSMutableArray * nameList = [[NSMutableArray alloc] init];
    NSArray<Class> * classList = [self superClassList];
    for (Class item in classList) {
        NSArray * itemNameList = [self methodNameListWithClass:item];
        for (NSString * name in itemNameList) {
            // 去重
            if (![nameList containsObject:name]) {
                [nameList addObject:name];
            }
        }
    }
    return nameList;
}

- (NSArray<NSString *> *)classMethodNameList {
    return [self.class methodNameList];
}

+ (NSArray<NSString *> *)classMethodNameList {
    return [self classMethodNameListWithClass:self];
}

- (NSArray<NSString *> *)allClassMethodNameList {
    return [self.class allClassMethodNameList];
}

+ (NSArray<NSString *> *)allClassMethodNameList {
    NSMutableArray * nameList = [[NSMutableArray alloc] init];
    NSArray<Class> * classList = [self superClassList];
    for (Class item in classList) {
        NSArray * itemNameList = [self classMethodNameListWithClass:item];
        for (NSString * name in itemNameList) {
            // 去重
            if (![nameList containsObject:name]) {
                [nameList addObject:name];
            }
        }
    }
    return nameList;
}

// MARK: - Base Method

+ (NSArray<Class> *)allRegisteredClasseList {
    // 注册类的总数
    int count = objc_getClassList(NULL, 0);
    // 创建数组
    NSMutableArray * classList = [[NSMutableArray alloc] initWithCapacity:count];
    // 获取所有已注册的类
    Class * classes = (Class *)malloc(sizeof(Class) * count);
    objc_getClassList(classes, count);
    for (int i = 0; i < count; i++) {
        [classList addObject:classes[i]];
    }
    free(classes);
    return classList;
}

+ (NSArray<Class> *)subClassList {
    // 注册类的总数
    int count = objc_getClassList(NULL, 0);
    // 创建数组
    NSMutableArray * classList = [NSMutableArray arrayWithObject:self];
    // 获取所有已注册的类
    Class * classes = (Class *)malloc(sizeof(Class) * count);
    objc_getClassList(classes, count);
    Class currentClass = self;
    for (int i = 0; i < count; i++) {
        if (currentClass == class_getSuperclass(classes[i])) {
            [classList addObject:classes[i]];
        }
    }
    free(classes);
    return classList;
}

+ (NSArray<Class> *)superClassList {
    // 创建数组
    NSMutableArray * classList = [NSMutableArray arrayWithObject:self];
    Class currentClass = [self superclass];
    while (currentClass != Nil) {
        [classList addObject:currentClass];
        currentClass = [currentClass superclass];
    }
    return classList;
}

+ (NSArray<NSString *> *)ivarNameListWithClass:(Class)cls {
    NSMutableArray * ivarList = [[NSMutableArray alloc] init];
    unsigned int count = 0;
    Ivar * ivars = class_copyIvarList(cls, &count);
    for (int i = 0; i < count; i++) {
        Ivar ivar = ivars[i];
        const char * ivarNameChar = ivar_getName(ivar);
        NSString * ivarName = [NSString stringWithUTF8String:ivarNameChar];
        [ivarList addObject:ivarName];
    }
    free(ivars);
    return ivarList;
}

+ (NSArray<NSString *> *)propertyNameListWithClass:(Class)cls {
    NSMutableArray * propertyList = [[NSMutableArray alloc] init];
    unsigned int count = 0;
    objc_property_t * propertys = class_copyPropertyList(cls, &count);
    for (int i = 0; i < count; i++) {
        objc_property_t property = propertys[i];
        const char * propertyNameChar = property_getName(property);
        NSString * propertyName = [NSString stringWithUTF8String:propertyNameChar];
        [propertyList addObject:propertyName];
    }
    free(propertys);
    return propertyList;
}

+ (NSArray<NSString *> *)methodNameListWithClass:(Class)cls {
    NSMutableArray * methodList = [[NSMutableArray alloc] init];
    unsigned int count = 0;
    Method * methods = class_copyMethodList(cls, &count);
    for (int i = 0; i < count; i++) {
        Method method = methods[i];
        SEL selector = method_getName(method);
        [methodList addObject:NSStringFromSelector(selector)];
    }
    free(methods);
    return methodList;
}

+ (NSArray<NSString *> *)classMethodNameListWithClass:(Class)cls {
    NSMutableArray * methodList = [[NSMutableArray alloc] init];
    unsigned int count = 0;
    Class meta = objc_getMetaClass(class_getName(cls));
    Method * methods = class_copyMethodList(meta, &count);
    for (int i = 0; i < count; i++) {
        Method method = methods[i];
        SEL selector = method_getName(method);
        [methodList addObject:NSStringFromSelector(selector)];
    }
    free(methods);
    return methodList;
}

@end
