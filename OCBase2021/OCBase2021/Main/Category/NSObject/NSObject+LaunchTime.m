//
//  NSObject+LaunchTime.m
//  OCBase2021
//
//  Created by 周保勇 on 2021/7/6.
//

#import "NSObject+LaunchTime.h"
#import <sys/sysctl.h>
#import <mach/mach.h>
#import <QuartzCore/QuartzCore.h>

@implementation NSObject (LaunchTime)

/// App进程开始
+ (NSTimeInterval)processStartTime {   // 单位是毫秒
    struct kinfo_proc kProcInfo;
    if ([self processInfoForPID:[[NSProcessInfo processInfo] processIdentifier] procInfo:&kProcInfo]) {
        return (kProcInfo.kp_proc.p_un.__p_starttime.tv_sec * 1000.0 + kProcInfo.kp_proc.p_un.__p_starttime.tv_usec / 1000.0);
        
    } else {
        NSAssert(NO, @"无法取得进程的信息");
        return 0;
    }
}

+ (BOOL)processInfoForPID:(int)pid procInfo:(struct kinfo_proc*)procInfo {
    int cmd[4] = {CTL_KERN, KERN_PROC, KERN_PROC_PID, pid};
    size_t size = sizeof(*procInfo);
    return sysctl(cmd, sizeof(cmd)/sizeof(*cmd), procInfo, &size, NULL, 0) == 0;
}

/// CoreFoundation框架下，获取日期或者时间戳
+ (NSTimeInterval)launchTime {
    /** CFAbsoluteTimeGetCurrent()
     其实等价于 [[NSDate new] timeIntervalSince...]
     */
    return (CFAbsoluteTimeGetCurrent() + kCFAbsoluteTimeIntervalSince1970) * 1000;
}

/// QuartzCore时间标记
+ (NSTimeInterval)mediaTime {
    /** mach_absolute_time() 和 CACurrentMediaTime()
     1.系统时间，是基于内建时钟的，能够更精确更原子化地测量；
     2.不会因为外部时间变化而变化（例如时区变化、夏时制、秒突变等）；
     3.系统的uptime有关,系统重启后CACurrentMediaTime()会被重置；
     4.常用于测试代码的效率。
     */
    return CACurrentMediaTime();
}

@end
