//
//  UIDevice+Extension.m
//  OCBase2021
//
//  Created by 周保勇 on 2021/4/8.
//

#import "UIDevice+Extension.h"
#import <sys/utsname.h>

@implementation UIDevice (Extension)

- (BOOL)isPhone {
    return self.userInterfaceIdiom == UIUserInterfaceIdiomPhone;
}

- (BOOL)isBang {
    return ZBYSize.statusHeight > 20;
}

- (BOOL)isPad {
    return self.userInterfaceIdiom == UIUserInterfaceIdiomPad;
}

- (NSString *)deviceName {
    struct utsname systemInfo;
    uname(&systemInfo);
    NSString * deviceModel = [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
    
    // iPhone
    if ([deviceModel isEqual: @"iPhone7,2"]) {
        return @"iPhone 6";
    }
    if ([deviceModel isEqual: @"iPhone7,1"]) {
        return @"iPhone 6 Plus";
    }
    if ([deviceModel isEqual: @"iPhone8,1"]) {
        return @"iPhone 6s";
    }
    if ([deviceModel isEqual: @"iPhone8,2"]) {
        return @"iPhone 6s Plus";
    }
    if ([deviceModel isEqual: @"iPhone8,4"]) {
        return @"iPhone SE";
    }
    if ([deviceModel isEqual: @"iPhone9,1"] || [deviceModel isEqual: @"iPhone9,3"]) {
        return @"iPhone 7";
    }
    if ([deviceModel isEqual: @"iPhone9,2"] || [deviceModel isEqual: @"iPhone9,4"]) {
        return @"iPhone 7 Plus";
    }
    if ([deviceModel isEqual: @"iPhone10,1"] || [deviceModel isEqual: @"iPhone10,4"]) {
        return @"iPhone 8";
    }
    if ([deviceModel isEqual: @"iPhone10,2"] || [deviceModel isEqual: @"iPhone10,5"]) {
        return @"iPhone 8 Plus";
    }
    if ([deviceModel isEqual: @"iPhone10,3"] || [deviceModel isEqual: @"iPhone10,6"]) {
        return @"iPhone X";
    }
    if ([deviceModel isEqual: @"iPhone11,8"]) {
        return @"iPhone Xr";
    }
    if ([deviceModel isEqual: @"iPhone11,2"]) {
        return @"iPhone Xs";
    }
    if ([deviceModel isEqual: @"iPhone11,6"]) {
        return @"iPhone Xs Max";
    }
    if ([deviceModel isEqual: @"iPhone12,1"]) {
        return @"iPhone 11";
    }
    if ([deviceModel isEqual: @"iPhone12,3"]) {
        return @"iPhone 11 Pro";
    }
    if ([deviceModel isEqual: @"iPhone12,5"]) {
        return @"iPhone 11 Pro Max";
    }
    if ([deviceModel isEqual: @"iPhone12,8"]) {
        return @"iPhone SE2";
    }
    if ([deviceModel isEqual: @"iPhone13,1"]) {
        return @"iPhone 12 mini";
    }
    if ([deviceModel isEqual: @"iPhone13,2"]) {
        return @"iPhone 12";
    }
    if ([deviceModel isEqual: @"iPhone13,3"]) {
        return @"iPhone 12 Pro";
    }
    if ([deviceModel isEqual: @"iPhone13,4"]) {
        return @"iPhone 12 Pro Max";
    }
    
    // iPad mini
    if ([deviceModel isEqual: @"iPad2,5"] || [deviceModel isEqual: @"iPad2,6"] || [deviceModel isEqual: @"iPad2,7"]) {
        return @"iPad mini";
    }
    if ([deviceModel isEqual: @"iPad4,4"] || [deviceModel isEqual: @"iPad4,5"] || [deviceModel isEqual: @"iPad4,6"]) {
        return @"iPad mini 2";
    }
    if ([deviceModel isEqual: @"iPad4,7"] || [deviceModel isEqual: @"iPad4,8"] || [deviceModel isEqual: @"iPad4,9"]) {
        return @"iPad mini 3";
    }
    if ([deviceModel isEqual: @"iPad5,1"] || [deviceModel isEqual: @"iPad5,2"]) {
        return @"iPad mini 4";
    }
    if ([deviceModel isEqual: @"iPad11,1"] || [deviceModel isEqual: @"iPad11,2"]) {
        return @"iPad mini 5";
    }
    
    // iPad
    if ([deviceModel isEqual: @"iPad1,1"]) {
        return @"iPad";
    }
    if ([deviceModel isEqual: @"iPad2,1"] || [deviceModel isEqual: @"iPad2,2"] || [deviceModel isEqual: @"iPad2,3"] || [deviceModel isEqual: @"iPad2,4"]) {
        return @"iPad 2";
    }
    if ([deviceModel isEqual: @"iPad3,1"] || [deviceModel isEqual: @"iPad3,2"] || [deviceModel isEqual: @"iPad3,3"]) {
        return @"iPad 3";
    }
    if ([deviceModel isEqual: @"iPad3,4"] || [deviceModel isEqual: @"iPad3,5"] || [deviceModel isEqual: @"iPad3,6"]) {
        return @"iPad 4";
    }
    if ([deviceModel isEqual: @"iPad6,11"] || [deviceModel isEqual: @"iPad6,12"]) {
        return @"iPad 5";
    }
    if ([deviceModel isEqual: @"iPad7,5"] || [deviceModel isEqual: @"iPad7,6"]) {
        return @"iPad 6";
    }
    if ([deviceModel isEqual: @"iPad7,11"] || [deviceModel isEqual: @"iPad7,12"]) {
        return @"iPad 7";
    }
    if ([deviceModel isEqual: @"iPad11,6"] || [deviceModel isEqual: @"iPad11,7"]) {
        return @"iPad 8";
    }
    
    // iPad Air
    if ([deviceModel isEqual: @"iPad4,1"] || [deviceModel isEqual: @"iPad4,2"] || [deviceModel isEqual: @"iPad4,3"]) {
        return @"iPad Air";
    }
    if ([deviceModel isEqual: @"iPad5,3"] || [deviceModel isEqual: @"iPad5,4"]) {
        return @"iPad Air 2";
    }
    if ([deviceModel isEqual: @"iPad11,3"] || [deviceModel isEqual: @"iPad11,4"]) {
        return @"iPad Air 3";
    }
    if ([deviceModel isEqual: @"iPad13,1"] || [deviceModel isEqual: @"iPad13,2"]) {
        return @"iPad Air 4";
    }
    
    // iPad Pro
    if ([deviceModel isEqual: @"iPad6,7"] || [deviceModel isEqual: @"iPad6,8"]) {
        return @"iPad Pro 12.9";
    }
    if ([deviceModel isEqual: @"iPad6,3"] || [deviceModel isEqual: @"iPad6,4"]) {
        return @"iPad Pro 9.7";
    }
    if ([deviceModel isEqual: @"iPad7,1"] || [deviceModel isEqual: @"iPad7,2"]) {
        return @"iPad Pro 12.9 2";
    }
    if ([deviceModel isEqual: @"iPad7,3"] || [deviceModel isEqual: @"iPad7,4"]) {
        return @"iPad Pro 10.5";
    }
    if ([deviceModel isEqual: @"iPad8,1"] || [deviceModel isEqual: @"iPad8,2"] || [deviceModel isEqual: @"iPad8,3"] || [deviceModel isEqual: @"iPad8,4"]) {
        return @"iPad Pro 11";
    }
    if ([deviceModel isEqual: @"iPad8,5"] || [deviceModel isEqual: @"iPad8,6"] || [deviceModel isEqual: @"iPad8,7"] || [deviceModel isEqual: @"iPad8,8"]) {
        return @"iPad Pro 12.9 3";
    }
    if ([deviceModel isEqual: @"iPad8,9"] || [deviceModel isEqual: @"iPad8,10"]) {
        return @"iPad Pro 11 2";
    }
    if ([deviceModel isEqual: @"iPad8,11"] || [deviceModel isEqual: @"iPad8,12"]) {
        return @"iPad Pro 12.9 4";
    }
    
    // Watch
    if ([deviceModel isEqual: @"Watch1,1"] || [deviceModel isEqual: @"Watch1,2"]) {
        return @"Apple Watch";
    }
    if ([deviceModel isEqual: @"Watch2,6"] || [deviceModel isEqual: @"Watch2,7"]) {
        return @"Apple Watch Series 1";
    }
    if ([deviceModel isEqual: @"Watch2,3"] || [deviceModel isEqual: @"Watch2,4"]) {
        return @"Apple Watch Series 2";
    }
    if ([deviceModel isEqual: @"Watch3,1"] || [deviceModel isEqual: @"Watch3,2"] || [deviceModel isEqual: @"Watch3,3"] || [deviceModel isEqual: @"Watch3,4"]) {
        return @"Apple Watch Series 3";
    }
    if ([deviceModel isEqual: @"Watch4,1"] || [deviceModel isEqual: @"Watch4,2"] || [deviceModel isEqual: @"Watch4,3"] || [deviceModel isEqual: @"Watch4,4"]) {
        return @"Apple Watch Series 4";
    }
    if ([deviceModel isEqual: @"Watch5,1"] || [deviceModel isEqual: @"Watch5,2"] || [deviceModel isEqual: @"Watch5,3"] || [deviceModel isEqual: @"Watch5,4"]) {
        return @"Apple Watch Series 5";
    }
    if ([deviceModel isEqual: @"Watch5,9"] || [deviceModel isEqual: @"Watch5,10"] || [deviceModel isEqual: @"Watch5,11"] || [deviceModel isEqual: @"Watch5,12"]) {
        return @"Apple Watch SE";
    }
    if ([deviceModel isEqual: @"Watch6,1"] || [deviceModel isEqual: @"Watch6,2"] || [deviceModel isEqual: @"Watch6,3"] || [deviceModel isEqual: @"Watch6,4"]) {
        return @"Apple Watch Series 6";
    }
    
    if ([deviceModel isEqual: @"i386"] || [deviceModel isEqual: @"x86_64"]) {
        return @"Simulator";
    }
    
    return deviceModel;
}

@end
