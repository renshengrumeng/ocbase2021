//
//  NSString+Other.h
//  NSStringExtension
//
//  Created by 周保勇 on 2018/11/12.
//  Copyright © 2018年 周保勇. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_OPTIONS(NSUInteger, NSChineseTransformPinyinStyle) {
    NSChineseTransformPinyinStyleNone = 0,
    NSChineseTransformPinyinStyleLatin = 1 << 0,        // 带声调的小写拼音
    NSChineseTransformPinyinStyleDiacritics = 1 << 1,   // 不带声调的小写拼音
    NSChineseTransformPinyinStyleFirstUpper = 1 << 2,   // 首字母大写拼音
    NSChineseTransformPinyinStyleAllUpper = 1 << 3,     // 全部大写拼音
    NSChineseTransformPinyinStyleAll = ~0UL             // 全部包含样式
};

@interface NSString (Other)

// MARK: - BOOL

/** 字符串是否为空 */
- (BOOL)isEmpty;

// MARK: - date string

/**
 获取当前时间的字符串表示
 
 @return 时间字符串
 */
+ (NSString*)getCurrentTime;

// MARK: -  chinese character transform

/**
 中文字符串转为拼音

 @param style 转换样式
 @return 字符串
 */
- (NSString *)transformPinyinWithStyle:(NSChineseTransformPinyinStyle)style;

/** 中文字符串转为小写拼音 */
- (NSString *)transformLowerPinyin;

/** 中文字符串转为大写拼音 */
- (NSString *)transformUpperPinyin;

/** 中文字符串转为首字母大写拼音 */
- (NSString *)transformCapitalizedPinyin;

@end
