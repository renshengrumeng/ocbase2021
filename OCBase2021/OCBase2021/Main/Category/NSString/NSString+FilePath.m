//
//  NSString+FilePath.m
//  NSStringExtension
//
//  Created by 周保勇 on 2018/11/12.
//  Copyright © 2018年 周保勇. All rights reserved.
//

#import "NSString+FilePath.h"

@implementation NSString (FilePath)

+ (NSString *)fileFullPathWithFileName:(NSString *)fileName fileType:(NSString *)fileType {
    return [[NSBundle mainBundle] pathForResource:fileName ofType:fileType];
}

+ (NSString *)homePath {
    return NSHomeDirectory();
}

+ (NSString *)documnetsPath {
    NSString * homePath = [self homePath];
    NSString * documnetsPath = [NSString stringWithFormat:@"%@%@", homePath, @"/Documents"];
    return documnetsPath;
}

+ (NSString *)cachesPath {
    return [NSString stringWithFormat:@"%@%@", [self homePath], @"/Library/Caches"];
}

+ (NSString *)preferencesPath {
    return [NSString stringWithFormat:@"%@%@", [self homePath], @"/Library/Preferences"];
}

+ (NSString *)tmpPath {
    return [NSString stringWithFormat:@"%@%@", [self homePath], @"/tmp"];
}

+ (NSString *)systemDataPath {
    return [NSString stringWithFormat:@"%@%@", [self homePath], @"/SystemData"];
}

@end
