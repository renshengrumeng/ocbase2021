//
//  NSString+Size.m
//  NSStringExtension
//
//  Created by 周保勇 on 2018/11/12.
//  Copyright © 2018年 周保勇. All rights reserved.
//

#import "NSString+Size.h"

@implementation NSString (Size)

// MARK: - public

- (CGSize)sizeWithFont:(UIFont *)font maxWidth:(CGFloat)maxWidth {
    return [self sizeWithFont:font maxWidth:maxWidth option:NSStringDrawingUsesLineFragmentOrigin];
}

- (CGSize)sizeWithFont:(UIFont *)font {
    return [self sizeWithFont:font maxWidth:MAXFLOAT option:NSStringDrawingUsesLineFragmentOrigin];
}

- (CGFloat)heightWithFont:(UIFont *)font {
    return [self sizeWithFont:font maxWidth:MAXFLOAT option:NSStringDrawingUsesDeviceMetrics].height;
}

- (CGFloat)heightWithFont:(UIFont *)font maxWidth:(CGFloat)maxWidth {
    return [self sizeWithFont:font maxWidth:maxWidth option:NSStringDrawingUsesDeviceMetrics].height;
}

- (CGFloat)widthWithFont:(UIFont *)font {
    return [self sizeWithFont:font maxWidth:MAXFLOAT option:NSStringDrawingUsesLineFragmentOrigin].width;
}

- (CGFloat)widthWithFont:(UIFont *)font maxWidth:(CGFloat)maxWidth {
    return [self sizeWithFont:font maxWidth:maxWidth option:NSStringDrawingUsesLineFragmentOrigin].width;
}

// MARK: - private

- (CGSize)sizeWithFont:(UIFont *)font maxWidth:(CGFloat)maxWidth option:(NSStringDrawingOptions)option {
    NSMutableDictionary * dict = [NSMutableDictionary dictionary];
    dict[NSFontAttributeName] = font;
    CGSize size = CGSizeMake(maxWidth, MAXFLOAT);
    return [self boundingRectWithSize:size options:option attributes:dict context:nil].size;
}

/**
 typedef NS_OPTIONS(NSInteger, NSStringDrawingOptions) {
 
 NSStringDrawingUsesLineFragmentOrigin = 1 << 0,
 // 整个文本将以每行组成的矩形为单位计算整个文本的尺寸
 // The specified origin is the line fragment origin, not the base line origin
 
 NSStringDrawingUsesFontLeading = 1 << 1,
 // 使用字体的行间距来计算文本占用的范围，即每一行的底部到下一行的底部的距离计算
 // Uses the font leading for calculating line heights
 
 NSStringDrawingUsesDeviceMetrics = 1 << 3,
 // 将文字以图像符号计算文本占用范围，而不是以字符计算。也即是以每一个字体所占用的空间来计算文本范围
 // Uses image glyph bounds instead of typographic bounds
 
 NSStringDrawingTruncatesLastVisibleLine
 // 当文本不能适合的放进指定的边界之内，则自动在最后一行添加省略符号。如果NSStringDrawingUsesLineFragmentOrigin没有设置，则该选项不生效
 // Truncates and adds the ellipsis character to the last visible line if the text doesn't fit into the bounds specified. Ignored if NSStringDrawingUsesLineFragmentOrigin is not also set.
 
 }
 */

@end
