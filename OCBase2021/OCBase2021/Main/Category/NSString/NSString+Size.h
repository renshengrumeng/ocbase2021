//
//  NSString+Size.h
//  NSStringExtension
//
//  Created by 周保勇 on 2018/11/12.
//  Copyright © 2018年 周保勇. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface NSString (Size)

/**
 字符串尺寸：字体大小、字体最大宽度
 
 @param font 字体大小
 @param maxWidth 字体最大宽度
 @return 字体尺寸
 */
- (CGSize)sizeWithFont:(UIFont *)font maxWidth:(CGFloat)maxWidth;

/**
 字符串尺寸：字体大小

 @param font 字体大小
 @return 字体尺寸
 */
- (CGSize)sizeWithFont:(UIFont *)font;

/**
 字符串宽度：字体大小、字体最大宽度

 @param font 字体大小
 @param maxWidth 字体最大宽度
 @return 字符串宽度
 */
- (CGFloat)widthWithFont:(UIFont *)font maxWidth:(CGFloat)maxWidth;

/**
 字符串宽度：字体大小、字体最大宽度
 
 @param font 字体大小
 @return 字符串宽度
 */
- (CGFloat)widthWithFont:(UIFont *)font;

/**
 字符串高度：字体大小、字体最大宽度

 @param font 字体大小
 @param maxWidth 字体最大宽度
 @return 字符串高度
 */
- (CGFloat)heightWithFont:(UIFont *)font maxWidth:(CGFloat)maxWidth;

/**
 字符串高度：字体大小
 
 @param font 字体大小
 @return 字符串高度
 */
- (CGFloat)heightWithFont:(UIFont *)font;

@end
