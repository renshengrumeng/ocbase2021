//
//  NSString+Emoji.h
//  NSStringExtension
//
//  Created by 周保勇 on 2018/11/12.
//  Copyright © 2018年 周保勇. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Emoji)

/**
 将十六进制的编码转为emoji字符

 @param intCode 16进制编码的整型数
 @return emoji字符串
 */
+ (NSString *)emojiWithIntCode:(long)intCode;

/**
 将十六进制的编码转为emoji字符

 @param stringCode 16进制编码的字符串
 @return emoji字符串
 */
+ (NSString *)emojiWithStringCode:(NSString *)stringCode;

/**
 将十六进制的编码转为emoji字符
 
 @return emoji字符串
 */
- (NSString *)emoji;

/** 判断是否是emoji表情 */
- (BOOL)isEmoji;

@end
