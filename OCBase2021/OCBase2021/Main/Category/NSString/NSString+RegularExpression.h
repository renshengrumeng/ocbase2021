//
//  NSString+RegularExpression.h
//  NSStringExtension
//
//  Created by 周保勇 on 2018/11/12.
//  Copyright © 2018年 周保勇. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (RegularExpression)

/** 字符串是否为电话号码 */
- (BOOL)isPhoneNumber;

/** 字符串是否为QQ号 */
- (BOOL)isQQ;

/** 字符串是否为email */
- (BOOL)isEmail;

/** 字符串是否为ip地址 */
- (BOOL)isIpAddress;

/** 字符串是否为验证码 */
- (BOOL)isVerificationCode;

/** 字符串是否为数字 */
- (BOOL)isNumber;

/** 字符串是否为支付数值 */
- (BOOL)isPayNumber;

/** 字符串是否为小说章节目录标题 */
- (BOOL)isChapter;

/** 用户密码是否是6-16位数字和字母组合 */
- (BOOL)isPassword;

@end
