//
//  NSString+Other.m
//  NSStringExtension
//
//  Created by 周保勇 on 2018/11/12.
//  Copyright © 2018年 周保勇. All rights reserved.
//

#import "NSString+Other.h"
#import <CoreText/CoreText.h>

@implementation NSString (Other)

// MARK: - BOOL

- (BOOL)isEmpty {
    if (self == nil) return YES;
    else if (self.length == 0) return YES;
    else if ([self isKindOfClass:[NSNull class]]) return YES;
    else return NO;
}

// MARK: - date string

+ (NSString*)getCurrentTime {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    // 设置你想要的格式,hh与HH的区别:分别表示12小时制,24小时制
    //    [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    [formatter setDateFormat:@"HH:mm"];
    // 现在时间,你可以输出来看下是什么格式
    NSDate *datenow = [NSDate date];
    // 将nsdate按formatter格式转成nsstring
    NSString *currentTimeString = [formatter stringFromDate:datenow];
    return currentTimeString;
}

// MARK: -  chinese character transform

- (NSString *)transformPinyinWithStyle:(NSChineseTransformPinyinStyle)style {
    NSMutableString * mutableString = [NSMutableString stringWithString:self];
    if (style & NSChineseTransformPinyinStyleLatin) {
        CFStringTransform((__bridge CFMutableStringRef)mutableString, NULL, kCFStringTransformMandarinLatin, NO);
    }
    if (style & NSChineseTransformPinyinStyleDiacritics) {
        CFStringTransform((__bridge CFMutableStringRef)mutableString, NULL, kCFStringTransformMandarinLatin, NO);
        CFStringTransform((__bridge CFMutableStringRef)mutableString, NULL, kCFStringTransformStripDiacritics, NO);
    }
    if (style & NSChineseTransformPinyinStyleFirstUpper) {
        CFStringTransform((__bridge CFMutableStringRef)mutableString, NULL, kCFStringTransformMandarinLatin, NO);
        CFStringTransform((__bridge CFMutableStringRef)mutableString, NULL, kCFStringTransformStripDiacritics, NO);
        mutableString = [NSMutableString stringWithString:[mutableString capitalizedString]];
    }
    if (style & NSChineseTransformPinyinStyleAllUpper) {
        CFStringTransform((__bridge CFMutableStringRef)mutableString, NULL, kCFStringTransformMandarinLatin, NO);
        CFStringTransform((__bridge CFMutableStringRef)mutableString, NULL, kCFStringTransformStripDiacritics, NO);
        mutableString = [NSMutableString stringWithString:[mutableString uppercaseString]];
    }
    return mutableString;
}

- (NSString *)transformLowerPinyin {
    return [self transformPinyinWithStyle:NSChineseTransformPinyinStyleDiacritics];
}

- (NSString *)transformUpperPinyin {
    return [self transformPinyinWithStyle:NSChineseTransformPinyinStyleAllUpper];
}

- (NSString *)transformCapitalizedPinyin {
    return [self transformPinyinWithStyle:NSChineseTransformPinyinStyleFirstUpper];
}

// MARK: - line count

/// 获取字符串的行数
/// @param font 字体大小
/// @param width 字符串的宽度
- (NSInteger)lineCountWithFont:(UIFont *)font withWidth:(CGFloat)width {
    if (!self || self.length == 0) {
        return 0;
    }
    CTFontRef myFont = CTFontCreateWithName((CFStringRef)([font fontName]), [font pointSize], NULL);
    NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] initWithString:self];
    [attStr addAttribute:(NSString *)kCTFontAttributeName value:(__bridge  id)myFont range:NSMakeRange(0, attStr.length)];
    CFRelease(myFont);
    CTFramesetterRef frameSetter = CTFramesetterCreateWithAttributedString(( CFAttributedStringRef)attStr);
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathAddRect(path, NULL, CGRectMake(0,0,width,MAXFLOAT));
    CTFrameRef frame = CTFramesetterCreateFrame(frameSetter, CFRangeMake(0, 0), path, NULL);
    NSArray *lines = ( NSArray *)CTFrameGetLines(frame);
    return lines.count;
}

@end
