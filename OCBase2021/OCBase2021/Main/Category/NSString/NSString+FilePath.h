//
//  NSString+FilePath.h
//  NSStringExtension
//
//  Created by 周保勇 on 2018/11/12.
//  Copyright © 2018年 周保勇. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (FilePath)

/**
 获取文件在应用包中的完整路径(如.plist、.bundle、.png等其它不是.swift的文件)
 
 @param fileName 文件名称
 @param fileType 文件扩展名（举例：Info.plist文件：该文件的扩展名为plist）
 @return 完整路径
 */
+ (NSString *)fileFullPathWithFileName:(NSString *)fileName fileType:(NSString *)fileType;

/** home文件夹路径 */
+ (NSString *)homePath;

/** documnet文件夹路径 */
+ (NSString *)documnetsPath;

/** caches文件夹路径 */
+ (NSString *)cachesPath;

/** preferences文件夹路径 */
+ (NSString *)preferencesPath;

/** systemData文件夹路径 */
+ (NSString *)systemDataPath;

/** tmp文件夹路径 */
+ (NSString *)tmpPath;

@end
