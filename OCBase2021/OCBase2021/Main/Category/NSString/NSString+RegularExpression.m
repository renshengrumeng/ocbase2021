//
//  NSString+RegularExpression.m
//  NSStringExtension
//
//  Created by 周保勇 on 2018/11/12.
//  Copyright © 2018年 周保勇. All rights reserved.
//

#import "NSString+RegularExpression.h"

@implementation NSString (RegularExpression)

// MARK: - public

- (BOOL)isQQ {
    /**QQ号码规则：
     1.开头数字不为0
     2.5-11的数字
     */
    NSString * pattern = @"^[1-9]\\d{4,10}$";
    return [self matchesInString:pattern];
    //注意JavaScrip的正则表达式前后都有\，使用时将前后\去掉就行了
}

- (BOOL)isPhoneNumber {
    /**手机号码规则：
     1.开头数字为13、15、18、17
     2.11位的数字
     */
    return [self matchesInString:@"^1[2345789]\\d{9}$"];
}

- (BOOL)isIpAddress {
    return [self matchesInString:@"\\d+\\.\\d+\\.\\d+\\.\\d+"];
}

- (BOOL)isPayNumber {
    NSString * pattern = @"^[1-9]\\d+(\\.\\d{2})?$|^0\\.\\d{2}$";
    return [self matchesInString:pattern];
}

- (BOOL)isEmail {
    NSString * pattern = @"^\\s*\\w+(?:\\.{0,1}[\\w-]+)*@[a-zA-Z0-9]+(?:[-.][a-zA-Z0-9]+)*\\.[a-zA-Z]+\\s*$";;
    //    @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
    //    @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
    //    "^\\s*\\w+(?:\\.{0,1}[\\w-]+)*@[a-zA-Z0-9]+(?:[-.][a-zA-Z0-9]+)*\\.[a-zA-Z]+\\s*$";
    return [self matchesInString:pattern];
}

- (BOOL)isVerificationCode {
//    NSString * pattern = @"^\\d{4}$"; // 四位验证码
    NSString * pattern = @"^\\d{6}$"; // 六位验证码
    return [self matchesInString:pattern];
}

- (BOOL)isNumber {
    if ([self isEqualToString:@""] || self == nil || self.integerValue <= 0) {
        return NO;
    }
    NSString * checkedNumString = [self stringByTrimmingCharactersInSet:[NSCharacterSet decimalDigitCharacterSet]];
    if(checkedNumString.length > 0) {
        return NO;
    }
    return YES;
}

- (BOOL)isChapter {
    NSString *pattern = @"^\\s*第[〇一二三四五六七八九十百千零0123456789]+[章卷篇节集回].*$";
    return [self matchesInString:pattern];
}

- (BOOL)isPassword {
    NSString *pattern = @"^(?![0-9]+$)(?![a-zA-Z]+$)[a-zA-Z0-9]{6,16}";
    return [self matchesInString:pattern];
}

// MARK: - private

- (BOOL)matchesInString:(NSString *)pattern {
    NSRegularExpression * regularExpression = [[NSRegularExpression alloc] initWithPattern:pattern options:0 error:nil];
    NSArray * resultArray = [regularExpression matchesInString:self options:0 range:NSMakeRange(0, self.length)];
    return (resultArray.count > 0);
}

@end
