//
//  UIColor+Extension.m
//  OCBase2021
//
//  Created by 周保勇 on 2021/4/8.
//

#import "UIColor+Extension.h"

@implementation UIColor (Extension)

+ (UIColor *)random {
    return [UIColor colorWithRed:arc4random_uniform(255)/255.0 green:arc4random_uniform(255)/255.0 blue:arc4random_uniform(255)/255.0 alpha:1.0];
}

- (NSString *)hex {
    CGFloat r = 0, g = 0, b = 0, a = 0;
    [self getRed:&r green:&g blue:&b alpha:&a];
    NSInteger ri = r * 0xff;
    NSInteger gi = g * 0xff;
    NSInteger bi = b * 0xff;
    return [NSString stringWithFormat:@"#%02lX%02lX%02lX", (long)ri, (long)gi, (long)bi];
}

+ (instancetype)colorWithHex:(NSString *)hex {
    if (hex == nil || hex.length != 7 || ![hex hasPrefix:@"#"]) {
        return nil;
    }
    NSScanner * scanner = [NSScanner scannerWithString:[hex substringFromIndex:1]];
    scanner.scanLocation = 0;
    unsigned int rgba = 0;
    [scanner scanHexInt:&rgba];
    unsigned int r = (rgba & 0xff0000) >> 16;
    unsigned int g = (rgba & 0xff00) >> 8;
    unsigned int b = rgba & 0xff;
    return [self colorWithRed:(CGFloat)r / 255.0 green:(CGFloat)g / 255.0 blue:(CGFloat)b / 255.0 alpha:1.0];
}

- (instancetype)colorWithAlpha:(CGFloat)alpha {
    CGFloat r = 0, g = 0, b = 0, a = 0;
    [self getRed:&r green:&g blue:&b alpha:&a];
    return [[UIColor alloc] initWithRed:r green:g blue:b alpha:alpha];
}

@end
