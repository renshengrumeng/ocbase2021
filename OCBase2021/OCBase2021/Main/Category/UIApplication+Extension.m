//
//  UIApplication+Extension.m
//  OCBase2021
//
//  Created by 周保勇 on 2021/4/8.
//

#import "UIApplication+Extension.h"

@implementation UIApplication (Extension)

- (NSString *)appId {
    return @"1376397714";
}

- (UIWindow *)currentWindow {
    if (@available(iOS 13, *)) {
        for (UIWindow * window in self.windows) {
            if (window.isHidden == NO) {
                return window;
            }
        }
        return self.windows[0];
    }
    else {
        return self.keyWindow;
    }
}

- (BOOL)canOpenScheme:(NSString *)scheme {
    NSURL * url = [NSURL URLWithString:scheme];
    if (url == nil) {
        NSLog(@"scheme URL is nil");
        return false;
    }
    return [self canOpenURL:url];
}

- (void)openUrlString:(NSString *)urlString {
    NSURL * url = [NSURL URLWithString:urlString];
    if (url == nil) {
        NSLog(@"url string is nil");
    }
    [self openURL:url options:@{} completionHandler:nil];
}

- (void)openPhone:(NSString *)phone {
    [self openUrlString:[NSString stringWithFormat:@"tel:%@", phone]];
}

- (void)openAppStore {
    [self openAppStore:self.appId];
}

- (void)openAppStore:(NSString *)appId {
    [self openUrlString:[NSString stringWithFormat:@"itms-apps://itunes.apple.com/app//id%@", appId]];
}

@end
