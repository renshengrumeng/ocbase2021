//
//  UIView+Controller.h
//  OCBase2021
//
//  Created by 周保勇 on 2021/4/8.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIView (Controller)

/// 视图所在的控制器
@property (nonatomic, copy, readonly) UIViewController * controller;

@end

NS_ASSUME_NONNULL_END
