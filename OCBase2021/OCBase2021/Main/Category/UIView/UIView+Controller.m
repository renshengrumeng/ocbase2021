//
//  UIView+Controller.m
//  OCBase2021
//
//  Created by 周保勇 on 2021/4/8.
//

#import "UIView+Controller.h"

@implementation UIView (Controller)

- (UIViewController *)controller {
    UIResponder * next = self.nextResponder;
    do {
        if ([next isKindOfClass:[UIViewController class]]) {
            return (UIViewController *)next;
        } else if ([next isKindOfClass:[UINavigationController class]]) {
            UINavigationController *nav = (UINavigationController *)next;
            return nav.viewControllers.lastObject;
        }
        next = next.nextResponder;
    } while (next != nil);
    return self.inputViewController;
}

@end
