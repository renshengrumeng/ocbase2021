//
//  UIView+CornerRaidus.h
//  ViewExtension
//
//  Created by 周保勇 on 2018/11/11.
//  Copyright © 2018年 周保勇. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (CornerRaidus)

/**
 设置圆角

 @param cornerRadius 圆角大小
 @param rectCorners 圆角位置
 */
- (void)setCornerRadius:(CGFloat)cornerRadius rectCorners:(UIRectCorner)rectCorners;

/**
 设置圆角
 
 @param cornerRadius 圆角大小
 */
- (void)setCornerRadius:(CGFloat)cornerRadius;

@end
