//
//  UIView+CornerRaidus.m
//  ViewExtension
//
//  Created by 周保勇 on 2018/11/11.
//  Copyright © 2018年 周保勇. All rights reserved.
//

#import "UIView+CornerRaidus.h"

@implementation UIView (CornerRaidus)

- (void)setCornerRadius:(CGFloat)cornerRadius rectCorners:(UIRectCorner)rectCorners {
    [self layoutIfNeeded]; // 这句代码很重要，不能忘了
    CAShapeLayer * maskLayer = [CAShapeLayer layer];
    maskLayer.frame = self.bounds;
    UIBezierPath * maskPath = [UIBezierPath bezierPathWithRoundedRect:self.bounds byRoundingCorners:rectCorners cornerRadii:CGSizeMake(cornerRadius, cornerRadius)]; // 圆角大小
    maskLayer.path = maskPath.CGPath;
    self.layer.mask = maskLayer;
}

- (void)setCornerRadius:(CGFloat)cornerRadius {
    [self setCornerRadius:cornerRadius rectCorners:UIRectCornerAllCorners];
}

@end
