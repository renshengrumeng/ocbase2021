//
//  UIView+Shadow.h
//  ViewExtension
//
//  Created by 周保勇 on 2018/11/11.
//  Copyright © 2018年 周保勇. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_OPTIONS(NSUInteger, UIViewShadowArea) {  // 多选枚举类型
    UIViewShadowAreaNone = 0,            // 没有阴影
    UIViewShadowAreaTop = 1 << 0,        // 顶部阴影
    UIViewShadowAreaBottom = 1 << 1,     // 底部阴影
    UIViewShadowAreaLeft = 1 << 2,       // 左侧阴影
    UIViewShadowAreaRight = 1 << 3,      // 右侧阴影
    UIViewShadowAreaAll = ~0UL           // 四周阴影
};

@interface UIView (Shadow)

/**
 设置阴影：阴影位置、阴影颜色、阴影透明度、阴影的圆角、上下阴影的高度（或者左右阴影的宽度）
 
 @param areas 阴影位置
 @param color 阴影颜色
 @param opacity 阴影透明度
 @param radius 阴影的圆角
 @param width 上下阴影的高度（或者左右阴影的宽度）
 */
- (void)setShadowWithAreas:(UIViewShadowArea)areas color:(UIColor *)color opacity:(CGFloat)opacity radius:(CGFloat)radius width:(CGFloat)width;

/**
 设置阴影：阴影位置、阴影颜色、阴影的圆角、上下阴影的高度（或者左右阴影的宽度）
 
 @param areas 阴影位置
 @param color 阴影颜色
 @param radius 阴影的圆角
 @param width 上下阴影的高度（或者左右阴影的宽度）
 */
- (void)setShadowWithAreas:(UIViewShadowArea)areas color:(UIColor *)color radius:(CGFloat)radius width:(CGFloat)width;

/**
 设置阴影：阴影位置、阴影颜色
 
 @param areas 阴影位置
 @param color 阴影颜色
 */
- (void)setShadowWithAreas:(UIViewShadowArea)areas color:(UIColor *)color;

/**
 设置阴影：阴影位置
 
 @param areas 阴影位置
 */
- (void)setShadowWithAreas:(UIViewShadowArea)areas;

@end
