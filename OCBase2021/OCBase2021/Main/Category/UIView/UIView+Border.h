//
//  UIView+Border.h
//  ViewExtension
//
//  Created by 周保勇 on 2018/11/11.
//  Copyright © 2018年 周保勇. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_OPTIONS(NSUInteger, UIViewBorderPosition) {  // 多选枚举类型
    UIViewBorderPositionNone = 0,            // 没有边框
    UIViewBorderPositionTop = 1 << 0,        // 顶部边框
    UIViewBorderPositionBottom = 1 << 1,     // 底部边框
    UIViewBorderPositionLeft = 1 << 2,       // 左侧边框
    UIViewBorderPositionRight = 1 << 3,      // 右侧边框
    UIViewBorderPositionAll = ~0UL           // 四周边框
};

@interface UIView (Border)

/**
 设置边框的：边框位置、圆角大小、边框颜色、边框宽度

 @param position 边框位置
 @param cornerRadius 圆角大小
 @param color 边框颜色
 @param width 边框宽度
 */
- (void)setBorderWithPosition:(UIViewBorderPosition)position cornerRadius:(CGFloat)cornerRadius color:(UIColor *)color width:(CGFloat)width;

/**
 设置边框的：边框位置、边框颜色、边框宽度

 @param position 边框位置
 @param color 边框颜色
 @param width 边框宽度
 */
- (void)setBorderWithPosition:(UIViewBorderPosition)position color:(UIColor *)color width:(CGFloat)width;

/**
 设置边框的：边框位置、边框颜色

 @param position 边框位置
 @param color 边框颜色
 */
- (void)setBorderWithPosition:(UIViewBorderPosition)position color:(UIColor *)color;

/**
 设置边框的：边框位置
 
 @param position 边框位置
 */
- (void)setBorderWithPosition:(UIViewBorderPosition)position;

@end
