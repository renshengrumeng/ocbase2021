//
//  UIView+Border.m
//  ViewExtension
//
//  Created by 周保勇 on 2018/11/11.
//  Copyright © 2018年 周保勇. All rights reserved.
//

#import "UIView+Border.h"

@implementation UIView (Border)

- (void)setBorderWithPosition:(UIViewBorderPosition)position cornerRadius:(CGFloat)cornerRadius color:(UIColor *)color width:(CGFloat)width {
    [self layoutIfNeeded]; // 这句代码很重要，不能忘了
    UIBezierPath * framePath = [UIBezierPath bezierPathWithRoundedRect:self.bounds byRoundingCorners:UIRectCornerAllCorners cornerRadii:CGSizeMake(cornerRadius, cornerRadius)];
    CAShapeLayer * cornerLayer = [CAShapeLayer layer];
    cornerLayer.frame = self.bounds;
    cornerLayer.fillColor = [UIColor clearColor].CGColor;
    cornerLayer.strokeColor = color.CGColor;
    cornerLayer.lineWidth = width;
    UIBezierPath * cornerPath = [UIBezierPath bezierPath];
    if (position & UIViewBorderPositionTop) {
        UIBezierPath * topPath = [UIBezierPath bezierPath];
        [topPath moveToPoint:CGPointMake(cornerRadius, 0)];
        [topPath addLineToPoint:CGPointMake(self.bounds.size.width - cornerRadius, 0)];
        [cornerPath appendPath:topPath];
    }
    if (position & UIViewBorderPositionBottom) {
        UIBezierPath * bottomPath = [UIBezierPath bezierPath];
        [bottomPath moveToPoint:CGPointMake(cornerRadius, self.bounds.size.height)];
        [bottomPath addLineToPoint:CGPointMake(self.bounds.size.width - cornerRadius, self.bounds.size.height)];
        [cornerPath appendPath:bottomPath];
    }
    if (position & UIViewBorderPositionLeft) {
        UIBezierPath * leftPath = [UIBezierPath bezierPath];
        [leftPath moveToPoint:CGPointMake(0, cornerRadius)];
        [leftPath addLineToPoint:CGPointMake(0, self.bounds.size.height - cornerRadius)];
        [cornerPath appendPath:leftPath];
    }
    if (position & UIViewBorderPositionRight) {
        UIBezierPath * rightPath = [UIBezierPath bezierPath];
        [rightPath moveToPoint:CGPointMake(self.bounds.size.width, cornerRadius)];
        [rightPath addLineToPoint:CGPointMake(self.bounds.size.width, self.bounds.size.height - cornerRadius)];
        [cornerPath appendPath:rightPath];
    }
    if (position & UIViewBorderPositionAll) {
        cornerPath = framePath;
    }
    cornerLayer.path = cornerPath.CGPath;
    [self.layer insertSublayer:cornerLayer atIndex:0];
}

- (void)setBorderWithPosition:(UIViewBorderPosition)position color:(UIColor *)color width:(CGFloat)width {
    [self setBorderWithPosition:position cornerRadius:0.0 color:color width:width];
}

- (void)setBorderWithPosition:(UIViewBorderPosition)position color:(UIColor *)color {
    [self setBorderWithPosition:position cornerRadius:0.0 color:color width:1.0];
}

- (void)setBorderWithPosition:(UIViewBorderPosition)position {
    [self setBorderWithPosition:position cornerRadius:0.0 color:[UIColor blackColor] width:1.0];
}

@end
