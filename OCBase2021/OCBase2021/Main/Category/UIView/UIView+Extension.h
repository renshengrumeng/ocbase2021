//
//  UIView+Extension.h
//  ViewExtension
//
//  Created by 周保勇 on 2018/11/11.
//  Copyright © 2018年 周保勇. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Extension)

/** 视图的起点x坐标 */
@property (nonatomic, assign) CGFloat x;

/** 视图的终点x坐标 */
@property (nonatomic, assign, readonly) CGFloat maxX;

/** 视图的起点y坐标 */
@property (nonatomic, assign) CGFloat y;

/** 视图的终点y坐标 */
@property (nonatomic, assign, readonly) CGFloat maxY;

/** 视图的中心点x坐标 */
@property (nonatomic, assign) CGFloat centerX;

/** 视图的中心点y坐标 */
@property (nonatomic, assign) CGFloat centerY;

/** 视图的宽度 */
@property (nonatomic, assign) CGFloat width;

/** 视图的高度 */
@property (nonatomic, assign) CGFloat height;

/** 视图的尺寸大小 */
@property (nonatomic, assign) CGSize size;

/** 视图的起点坐标 */
@property (nonatomic, assign) CGPoint origin;

@end
