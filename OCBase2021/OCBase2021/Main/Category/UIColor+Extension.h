//
//  UIColor+Extension.h
//  OCBase2021
//
//  Created by 周保勇 on 2021/4/8.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIColor (Extension)

/// 随机颜色
@property(class, nonatomic, readonly) UIColor * random;

/// 颜色的16进制字符串
@property (nonatomic, copy, readonly) NSString * hex;

/// 16进制字符串颜色
/// @param hex 16进制字符串
+ (instancetype)colorWithHex:(NSString *)hex;

/// 获取修改透明度后的颜色
/// @param alpha 透明度
- (instancetype)colorWithAlpha:(CGFloat)alpha;

@end

NS_ASSUME_NONNULL_END
