//
//  UIViewController+Extension.m
//  OC-framework
//
//  Created by 周保勇 on 2021/4/7.
//  Copyright © 2021 周保勇. All rights reserved.
//

#import "UIViewController+Extension.h"
#import "NSObject+Extension.h"

@implementation UIViewController (Extension)

+ (void)load {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        [self swizzledOldMethod:@selector(viewWillAppear:) newMethod:@selector(zby_ViewWillAppear:)];
    });
}

- (void)zby_ViewWillAppear:(BOOL)animated {
    [self zby_ViewWillAppear:animated];
    if (![self isKindOfClass:UINavigationController.class]) {
        NSLog(@"进入了 %@", self.class);
    }
}

@end
