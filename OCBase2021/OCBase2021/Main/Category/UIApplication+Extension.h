//
//  UIApplication+Extension.h
//  OCBase2021
//
//  Created by 周保勇 on 2021/4/8.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIApplication (Extension)

/// App Store应用中的app唯一id
@property (nonatomic, strong, readonly) NSString * appId;

/// 当前显示的窗口
//@property (nonatomic, strong, readonly) UIWindow * currentWindow;

- (UIWindow *)currentWindow;

/// 是否已安装App或者可以打开App
/// @param scheme 打开App的URL Scheme字符串
- (BOOL)canOpenScheme:(NSString *)scheme;

/// 唤起三方app、唤起电话、唤起App Store
/// @param urlString 唤起的对应完整链接字符串
- (void)openUrlString:(NSString *)urlString;

/// 唤起系统电话弹窗
/// @param phone 电话号码字符串
- (void)openPhone:(NSString *)phone;

/// 唤起App对应App Store下载或更新界面
/// @param appId app在开发者对应的唯一id
- (void)openAppStore:(NSString *)appId;

/// 唤起该App对应App Store下载或更新界面
- (void)openAppStore;

@end

NS_ASSUME_NONNULL_END
