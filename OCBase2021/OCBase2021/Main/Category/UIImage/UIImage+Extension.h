//
//  UIImage+Extension.h
//  UIImageExtension
//
//  Created by 周保勇 on 2018/11/12.
//  Copyright © 2018年 周保勇. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Extension)

// MARK: - get image

/**
 获取可以任意拉伸的图片

 @param name 图片名称
 @return 图片
 */
+ (UIImage *)resizedImage:(NSString *)name;


/**
 获取纯色图片
 
 @param color 色值
 @param size 图片的尺寸
 @return 图片
 */
+ (UIImage *)imageFromColor:(UIColor *)color size:(CGSize)size;

/**
 获取1单位纯色图片
 
 @param color 色值
 @return 图片
 */
+ (UIImage *)imageFromColor:(UIColor *)color;

/**
 获取对应屏幕尺寸的LaumchImage
 
 @return 图片
 */
+ (UIImage *)getLaunchImage;

/**
 获取对应的AppIcon
 */
+ (UIImage *)getAppIcon;

/**
 文本生成二维码图片
 
 @param text 文本
 @param size 二维码尺寸
 @return 二维码
 */
+ (UIImage *)imageWithCodeText:(NSString *)text imageSize:(CGSize)size;

// MARK: - modifier image

/**
 获取指定尺寸的图片
 
 @param targetSize 原图片
 @return 图片
 */
- (UIImage *)scalingToSize:(CGSize)targetSize;

/**
 重绘图片：重绘颜色
 
 @param color 重绘颜色
 @return 图片
 */
- (UIImage *)refreshWithColor:(UIColor *)color;

/**
 旋转图片
 
 @param degree 顺时针旋转角度（0 - 360）
 @return 旋转后的图片
 */
- (UIImage *)refreshWithDegree:(CGFloat)degree;

/**
 重绘图片：重绘颜色、旋转角度

 @param color 重绘颜色
 @param degree 旋转角度
 @return 图片
 */
- (UIImage *)refreshWithColor:(UIColor *)color degree:(CGFloat)degree;

/**
 给图片添加斜体文字
 
 @param text 文字
 @return 返回图片
 */
- (UIImage *)imageWithText:(NSString *)text font:(UIFont *)font color:(UIColor *)color;

@end
