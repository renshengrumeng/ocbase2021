//
//  UIDevice+Extension.h
//  OCBase2021
//
//  Created by 周保勇 on 2021/4/8.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIDevice (Extension)

/// 设备是否是iPhone
@property (nonatomic, assign, readonly) BOOL isPhone;

/// 设备是否是刘海屏
@property (nonatomic, assign, readonly) BOOL isBang;

/// 设备是否是iPad
@property (nonatomic, assign, readonly) BOOL isPad;

/// 设备名称
@property (nonatomic, copy, readonly) NSString * deviceName;

@end

NS_ASSUME_NONNULL_END
