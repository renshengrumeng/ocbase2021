//
//  BaseModel.h
//  OC-framework
//
//  Created by 周保勇 on 2019/4/28.
//  Copyright © 2019 周保勇. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface BaseModel : NSObject

@end

NS_ASSUME_NONNULL_END
