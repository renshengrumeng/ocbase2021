//
//  Singleton.m
//  OC-framework
//
//  Created by 周保勇 on 2020/7/3.
//  Copyright © 2020 周保勇. All rights reserved.
//

#import "Singleton.h"

@implementation Singleton

// 用来保存唯一的单例对象
static id _instance;
//static dispatch_once_t onceToken;

+ (instancetype)allocWithZone:(struct _NSZone *)zone
{
//    dispatch_once(&onceToken, ^{
//        _instace = [super allocWithZone:zone];
//    });
    @synchronized(self) {
        if (_instance == nil) {
            _instance = [super allocWithZone:zone];
        }
    }
    return _instance;
}

+ (instancetype)sharedInstance {
    if (_instance == nil) {
        _instance = [[self alloc] init];
    }
    return _instance;
}

@end
