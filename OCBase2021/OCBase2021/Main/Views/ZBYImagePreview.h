//
//  ZBYImagePreview.h
//  OC-framework
//
//  Created by 周保勇 on 2021/3/16.
//  Copyright © 2021 周保勇. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ZBYImagePreview;
@protocol PhotoShowViewDelegate <NSObject>

@optional
-(void)photoShowViewTouchBegan:(nullable ZBYImagePreview *)psv;
-(void)photoShowViewTouchEnd:(nullable ZBYImagePreview *)psv;
-(void)photoShowViewPhotoReduction:(nullable ZBYImagePreview *)psv;//相片被还原时的回调

@end

NS_ASSUME_NONNULL_BEGIN

@interface ZBYImagePreview : UIView

@property(nonatomic,weak,nullable)id<PhotoShowViewDelegate> delegate;

@property(nonatomic,assign)BOOL enableTapTwo;       //是否允许双击,默认YES
@property(nonatomic,assign)BOOL enableRotation;     //是否允许旋转,默认YES
@property(nonatomic,assign)BOOL enablePinch;        //是否允许绽放,默认YES
@property(nonatomic,assign)BOOL enablePan;          //是否允许平移,默认YES
@property(nonatomic,assign)BOOL enableReset;        //是否自动复位,默认YES
@property(nonatomic,assign)NSInteger panFingerNum;  //平移时用的手指数,默认为2
@property(nonatomic,assign)CGFloat minScale;      //最小绽放比例,默认0.3
@property(nonatomic,assign)CGFloat maxScale;      //最大绽放比例,默认5.0
@property(nonatomic,assign)CGFloat doubleTapScale;//双击时的绽放比例,默认为2;
@property(nonatomic,strong,nullable)UIImageView* imageView; //要显示的图片
-(nonnull instancetype)initWithFrame:(CGRect)frame;
-(nullable instancetype)initWithCoder:(nonnull NSCoder *)aDecoder;
-(void)resetTransform;

@end

NS_ASSUME_NONNULL_END
