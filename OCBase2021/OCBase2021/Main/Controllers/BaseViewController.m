//
//  BaseViewController.m
//  OC-framework
//
//  Created by 周保勇 on 2019/4/28.
//  Copyright © 2019 周保勇. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseViewController ()

@end

@implementation BaseViewController


// MARK: - override

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)presentViewController:(UIViewController *)viewControllerToPresent animated:(BOOL)flag completion:(void (^)(void))completion {
    viewControllerToPresent.modalPresentationStyle = UIModalPresentationFullScreen;
    [self willPresentNext];
    [super presentViewController:viewControllerToPresent animated:flag completion: ^{
        [self didPresentNext];
        if (completion) {
            completion();
        }
    }];
}

- (void)dismissViewControllerAnimated:(BOOL)flag completion:(void (^)(void))completion {
    [self willDismissBack];
    [super dismissViewControllerAnimated:flag completion: ^{
        [self didDismissBack];
        if (completion) {
            completion();
        }
    }];
}

- (void)willMoveToParentViewController:(UIViewController *)parent {
    [super willMoveToParentViewController:parent];
    if (parent == nil) {
        [self willPopBack];
    }
    else {
        [self willPushNext];
    }
}

- (void)didMoveToParentViewController:(UIViewController *)parent {
    [super didMoveToParentViewController:parent];
    if (parent == nil) {
        [self didPopBack];
    }
    else {
        [self didPushNext];
    }
}

- (void)dealloc {
    NSLog(@"%@-dealloc", self.class);
}

// MARK: - public func

- (void)willPopBack {
    NSLog(@"导航将要返回上一页-包含侧滑返回");
}

- (void)didPopBack {
    NSLog(@"导航已经返回上一页-包含侧滑返回");
}

- (void)willPushNext {
    NSLog(@"导航将要跳转下一页");
}

- (void)didPushNext {
    NSLog(@"导航已经跳转下一页");
}

- (void)willPresentNext {
    NSLog(@"模态将要跳转下一页");
}

- (void)didPresentNext {
    NSLog(@"模态已经跳转下一页");
}

- (void)willDismissBack {
    NSLog(@"模态将要返回上一页");
}

- (void)didDismissBack {
    NSLog(@"模态已经返回上一页");
}

// MARK: - setter

- (void)setIsLeftBack:(BOOL)isLeftBack {
    _isLeftBack = isLeftBack;
    self.navigationController.interactivePopGestureRecognizer.enabled = isLeftBack;
}

// MARK: - getter

- (BOOL)isVisabled {
//    return self.isViewLoaded && (self.view.window != nil)
    return self.isViewLoaded && (self.view.window != nil);
}


@end
