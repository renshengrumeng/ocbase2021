//
//  BaseTabBarViewController.m
//  OC-framework
//
//  Created by 周保勇 on 2019/4/28.
//  Copyright © 2019 周保勇. All rights reserved.
//

#import "BaseTabBarViewController.h"
#import "HomeViewController.h"
#import "CourseViewController.h"
#import "ZBYLearningViewController.h"
#import "ZBYMineViewController.h"
#import "BaseNavigationViewController.h"

@interface BaseTabBarViewController ()

@end

@implementation BaseTabBarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSDictionary<NSAttributedStringKey,id> * normalAttrs = @{NSForegroundColorAttributeName : [UIColor colorWithHex:@"#999999"], NSFontAttributeName : [UIFont systemFontOfSize:10 weight:UIFontWeightMedium]};
    [UITabBarItem.appearance setTitleTextAttributes:normalAttrs forState:UIControlStateNormal];
    NSDictionary<NSAttributedStringKey,id> * selectedAttrs = @{NSForegroundColorAttributeName : [UIColor colorWithHex:@"#FF8C15"], NSFontAttributeName : [UIFont systemFontOfSize:10 weight:UIFontWeightMedium]};
    [UITabBarItem.appearance setTitleTextAttributes:selectedAttrs forState:UIControlStateSelected];
    [self setChildViewController:[HomeViewController new] title:@"首页" noamrlImage:@"home_normal_icon" selectedImage:@"home_selected_icon"];
    [self setChildViewController:[CourseViewController new] title:@"课程" noamrlImage:@"course_normal_icon" selectedImage:@"course_selected_icon"];
    [self setChildViewController:[ZBYLearningViewController new] title:@"学习" noamrlImage:@"learning_normal_icon" selectedImage:@"learning_selected_icon"];
    [self setChildViewController:[ZBYMineViewController new] title:@"我的" noamrlImage:@"mine_normal_icon" selectedImage:@"mine_selected_icon"];
}

- (void)setChildViewController:(UIViewController *)vc title:(NSString *)title noamrlImage:(NSString *)noamrlImage selectedImage:(NSString *)selectedImage {
    vc.title = title;
    vc.tabBarItem.image = [[UIImage imageNamed:noamrlImage] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    vc.tabBarItem.selectedImage = [[UIImage imageNamed:selectedImage] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    BaseNavigationViewController * nav = [[BaseNavigationViewController alloc] initWithRootViewController:vc];
    [self addChildViewController:nav];
}

@end
