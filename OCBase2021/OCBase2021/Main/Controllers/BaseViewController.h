//
//  BaseViewController.h
//  OC-framework
//
//  Created by 周保勇 on 2019/4/28.
//  Copyright © 2019 周保勇. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BaseViewController : UIViewController

/// 是否侧滑返回
@property (nonatomic, assign) BOOL isLeftBack;

/// 当前控制器是否可见
@property (nonatomic, assign, readonly, getter = isVisabled) BOOL visabled;

/// 导航将要跳转下一页
- (void)willPushNext;

/// 导航已经跳转下一页
- (void)didPushNext;

/// 导航将要返回上一页
- (void)willPopBack;

/// 导航已经返回上一页
- (void)didPopBack;

/// 模态将要跳转下一页
- (void)willPresentNext;

/// 模态已经跳转下一页
- (void)didPresentNext;

/// 模态将要返回上一页
- (void)willDismissBack;

/// 模态已经返回上一页
- (void)didDismissBack;

@end

NS_ASSUME_NONNULL_END
