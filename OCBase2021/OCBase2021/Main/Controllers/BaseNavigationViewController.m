//
//  BaseNavigationViewController.m
//  OC-framework
//
//  Created by 周保勇 on 2019/4/28.
//  Copyright © 2019 周保勇. All rights reserved.
//

#import "BaseNavigationViewController.h"

@interface BaseNavigationViewController () <UINavigationBarDelegate, UIGestureRecognizerDelegate>

@end

@implementation BaseNavigationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
//    self.navigationBar.opaque = NO;
//    self.automaticallyAdjustsScrollViewInsets = NO;
//    self.edgesForExtendedLayout = UIRectEdgeNone;
//    self.navigationBar.translucent = NO;
//    [self.navigationBar setHidden:YES];
    // 设置系统自带右侧popVC代理
//    self.interactivePopGestureRecognizer.delegate = self;
}

// MARK: - override

- (void)presentViewController:(UIViewController *)viewControllerToPresent animated:(BOOL)flag completion:(void (^)(void))completion {
    viewControllerToPresent.modalPresentationStyle = UIModalPresentationFullScreen;
    [super presentViewController:viewControllerToPresent animated:flag completion:completion];
}

- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated {
    if (self.childViewControllers.count > 0) {
        viewController.hidesBottomBarWhenPushed = YES;
    }
    [super pushViewController:viewController animated:animated];
}

// MARK: - UINavigationBarDelegate

/// 开始push动画：YES开始动画，NO不掉用didPush、shouldPop、didPop方法
- (BOOL)navigationBar:(UINavigationBar *)navigationBar shouldPushItem:(UINavigationItem *)item {
//    [self.tabBarController.tabBar setHidden:(self.viewControllers.count >= 2)];
    if (self.viewControllers.count >= 2) {  // 当导航控制器中控制器栈中大于2个时
        
    }
    return YES;
}

/// 在push的动画结束时调用，如果不是动画，则立即调用
- (void)navigationBar:(UINavigationBar *)navigationBar didPushItem:(UINavigationItem *)item {

}

/// 同shouldPush方法
//- (BOOL)navigationBar:(UINavigationBar *)navigationBar shouldPopItem:(UINavigationItem *)item {
//    if (self.viewControllers.count >= 2) {  // 当导航控制器中控制器栈中大于2个时
//
//    }
//    return [self navigationBar:navigationBar shouldPopItem:item];
//}

- (void)navigationBar:(UINavigationBar *)navigationBar didPopItem:(UINavigationItem *)item {
//    [self.tabBarController.tabBar setHidden:(self.viewControllers.count >= 2)];
    if (self.viewControllers.count < 2) {  // 当导航控制器中控制器栈中小于2个时

    }
}

// MARK: - UIGestureRecognizerDelegate

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer {
    return YES;
}

@end
