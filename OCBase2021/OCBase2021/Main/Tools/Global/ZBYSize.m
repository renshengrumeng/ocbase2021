//
//  ZBYSize.m
//  OCBase2021
//
//  Created by 周保勇 on 2021/4/8.
//

#import "ZBYSize.h"

@implementation ZBYSize

+ (CGRect)screenFrame {
    return [UIScreen mainScreen].bounds;
}

+ (CGSize)screenSize {
    return self.screenFrame.size;
}

+ (CGFloat)screenWidth {
    return self.screenSize.width;
}

+ (CGFloat)screenHeight {
    return self.screenSize.height;
}

+ (CGSize)statusSize {
    if (@available(iOS 13, *)) {
        UIWindow * window = [[UIApplication sharedApplication] currentWindow];
        return [[UIApplication sharedApplication] currentWindow].windowScene.statusBarManager.statusBarFrame.size;
    }
    else {
        return [UIApplication sharedApplication].statusBarFrame.size;
    }
}

+ (CGFloat)statusHeight {
    return self.statusSize.height;
}

+ (CGSize)navigationBarSize {
    return CGSizeMake(self.screenWidth, self.statusHeight + 44);
}

+ (CGFloat)navigationBarHeight {
    return self.navigationBarSize.height;
}

+ (CGFloat)bottomSafeHeight {
    if (@available(iOS 11, *)) {
        return [[UIApplication sharedApplication] currentWindow].safeAreaInsets.bottom;
    }
    else {
        return 0;
    }
}

+ (CGSize)tabBarSize {
    return CGSizeMake(self.screenWidth, self.bottomSafeHeight + 49);
}

+ (CGFloat)tabBarHeight {
    return self.tabBarSize.height;
}

+ (CGFloat)px:(CGFloat)value {
    return value / 750 * self.screenWidth;
}

@end
