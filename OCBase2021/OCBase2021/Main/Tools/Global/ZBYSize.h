//
//  ZBYSize.h
//  OCBase2021
//
//  Created by 周保勇 on 2021/4/8.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZBYSize : NSObject

// MARK: - 屏幕

/// 屏幕显示区域
@property(class, nonatomic, readonly) CGRect screenFrame;

/// 屏幕显示尺寸
@property(class, nonatomic, readonly) CGSize screenSize;

/// 屏幕显示宽度
@property(class, nonatomic, readonly) CGFloat screenWidth;

/// 屏幕显示高度
@property(class, nonatomic, readonly) CGFloat screenHeight;

/// 状态栏尺寸
@property(class, nonatomic, readonly) CGSize statusSize;

/// 状态栏高度
@property(class, nonatomic, readonly) CGFloat statusHeight;

/// 导航栏尺寸，包含状态栏的尺寸
@property(class, nonatomic, readonly) CGSize navigationBarSize;

/// 导航栏高度，包含状态栏的高度
@property(class, nonatomic, readonly) CGFloat navigationBarHeight;

/// 底部安全高度
@property(class, nonatomic, readonly) CGFloat bottomSafeHeight;

/// 标签栏尺寸，包含底部安全区尺寸
@property(class, nonatomic, readonly) CGSize tabBarSize;

/// 导航栏高度，包含底部安全区的高度
@property(class, nonatomic, readonly) CGFloat tabBarHeight;

/// pt值转换为px
/// @param value pt值
+ (CGFloat)px:(CGFloat)value;


@end

NS_ASSUME_NONNULL_END
