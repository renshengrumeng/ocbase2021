//
//  GlobalTool.m
//  OC-framework
//
//  Created by 周保勇 on 2020/7/3.
//  Copyright © 2020 周保勇. All rights reserved.
//

#import "GlobalTool.h"

@implementation GlobalTool


#ifdef DEBUG
bool const kIsDebug = true;
#else
bool const kIsDebug = false;
#endif

// MARK: - UserDefault save key

/// 本地app版本缓存关键字
NSString * const kLocationVersionKey = @"CFBundleVersion";

/// 是否加载广告缓存关键字：true加载，false不加载
NSString * const kAdvertisingKey = @"kAdvertisingKey";

// MARK: - notification key

/// 重新登录发送通知
NSString * const kReLoginNotification = @"kReLoginNotification";

@end
