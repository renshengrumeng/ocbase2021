//
//  GlobalTool.h
//  OC-framework
//
//  Created by 周保勇 on 2020/7/3.
//  Copyright © 2020 周保勇. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface GlobalTool : NSObject

/// 是否是开发模式
extern bool const kIsDebug;

/// 本地app版本缓存关键字
extern NSString * const kLocationVersionKey;

/// 是否加载广告缓存关键字：true加载，false不加载
extern NSString * const kAdvertisingKey;

extern NSString * const kReLoginNotification;

@end

NS_ASSUME_NONNULL_END
