//
//  SDWebImageTool.m
//  OCBase2021
//
//  Created by 周保勇 on 2021/4/8.
//

#import "SDWebImageTool.h"

@implementation SDWebImageTool

+ (void)webImageConfig {
    SDImageCache * defaultCache = [[SDImageCache alloc] init];
//    defaultCache.config = ;
    SDWebImageManager.defaultImageCache = defaultCache;
}

/**
 设置图片

 @param imageView 要设置的图片视图
 @param url 图片的url
 @param placeholder 占位图片
 */
+ (void)setImageView:(UIImageView *_Nonnull)imageView withImageWithURL:(NSString *)url placeholderImage:(NSString *)placeholder {
//    [url stringByAddingPercentEncodingWithAllowedCharacters:NSUTF8StringEncoding];
    NSString * encodeImagUrl = [url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    NSURL * imageUrl = [NSURL URLWithString:encodeImagUrl];
    UIImage * placeholderImage = [UIImage imageNamed:placeholder];
    [imageView sd_setImageWithURL:imageUrl placeholderImage:placeholderImage options:SDWebImageRetryFailed | SDWebImageProgressiveLoad | SDWebImageLowPriority];
}

//+ (void)sdfasdfa{
//    SDWebImageManager * manager = [SDWebImageManager sharedManager];
//    [manager.imageDownloader downloadImageWithURL:nil options:nil progress:nil completed:^(UIImage * _Nullable image, NSData * _Nullable data, NSError * _Nullable error, BOOL finished) {
//        <#code#>
//    }];
//}

/**
 内存警告时，取消下载，清除缓存
 */
+ (void)receiveMemoryWarning{
    SDWebImageManager * manager = [SDWebImageManager sharedManager];
    // 取消下载
    [manager cancelAll];
    // 清除缓存
    [manager.imageCache clearWithCacheType:SDImageCacheTypeAll completion:^{
        
    }];
}

@end
