//
//  SDWebImageTool.h
//  OCBase2021
//
//  Created by 周保勇 on 2021/4/8.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface SDWebImageTool : NSObject

+ (void)webImageConfig;

+ (void)setImageView:(UIImageView *_Nonnull)imageView withImageWithURL:(NSString *_Nonnull)url placeholderImage:(NSString *_Nullable)placeholder;
+ (void)receiveMemoryWarning;

@end

NS_ASSUME_NONNULL_END
