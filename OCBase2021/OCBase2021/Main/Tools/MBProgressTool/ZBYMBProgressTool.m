//
//  ZBYMBProgressTool.m
//  OCBase2021
//
//  Created by 周保勇 on 2021/4/27.
//

#import "ZBYMBProgressTool.h"

@implementation ZBYMBProgressTool

+ (BOOL)hadExist:(UIView *)superView {
    for (UIView * subview in superView.subviews) {
        if ([subview isKindOfClass:ZBYMBProgressView.class]) {
            return YES;
        }
    }
    return NO;
}

+ (ZBYMBProgressView *)progressView:(UIView *)superView {
    for (UIView * subview in superView.subviews) {
        if ([subview isKindOfClass:ZBYMBProgressView.class]) {
            return (ZBYMBProgressView *)subview;
        }
    }
    return nil;
}

+ (ZBYMBProgressView *)showText:(NSString *)text animated:(BOOL)animated {
    return [self showTextTo:[[UIApplication sharedApplication] currentWindow] text:text delay:2.0 animated:animated compelete:nil];
}

+ (ZBYMBProgressView *)showText:(NSString *)text animated:(BOOL)animated compelete:(void (^)(void))compelete {
    return [self showTextTo:[[UIApplication sharedApplication] currentWindow] text:text delay:2.0 animated:animated compelete:compelete];
}

+ (ZBYMBProgressView *)showTextTo:(UIView *)superView text:(NSString *)text animated:(BOOL)animated compelete:(void (^)(void))compelete {
    return [self showTextTo:superView text:text delay:2.0 animated:animated compelete:compelete];
}

+ (ZBYMBProgressView *)showTextTo:(UIView *)superView text:(NSString *)text animated:(BOOL)animated {
    return [self showTextTo:superView text:text delay:2.0 animated:animated compelete:nil];
}

+ (ZBYMBProgressView *)showTextTo:(UIView *)superView text:(NSString *)text delay:(NSTimeInterval)delay animated:(BOOL)animated {
    return [self showTextTo:superView text:text delay:delay animated:animated compelete:nil];
}

+ (ZBYMBProgressView *)showTextTo:(UIView *)superView text:(NSString *)text delay:(NSTimeInterval)delay animated:(BOOL)animated compelete:(void (^)(void))compelete {
    BOOL hadExist = [self hadExist:superView];
    ZBYMBProgressView * textView = [ZBYMBProgressView showHUDAddedTo:superView animated:animated];
    textView.mode = MBProgressHUDModeText;
    textView.bezelView.color = [UIColor colorWithHex:@"#666666"];
    textView.bezelView.style = MBProgressHUDBackgroundStyleSolidColor;
    textView.label.text = text;
    textView.label.font = [UIFont systemFontOfSize:14 weight:UIFontWeightRegular];
    textView.label.textColor = [UIColor whiteColor];
    if (hadExist) {
        ZBYMBProgressView * progressView = [self progressView:superView];
        [progressView hideAnimated:NO afterDelay:0];
    }
    [textView hideAnimated:animated afterDelay:delay];
    dispatch_after(delay, dispatch_get_main_queue(), ^{
        if (compelete) {
            compelete();
        }
    });
    return textView;
}

+ (ZBYMBProgressView *)showLoadingTo:(UIView *)superView text:(NSString *)text noRespondArea:(CGRect)rect animated:(BOOL)animated {
    BOOL hadExist = [self hadExist:superView];
    ZBYMBProgressView * loadingView = [ZBYMBProgressView showHUDAddedTo:superView animated:animated];
    loadingView.mode = MBProgressHUDModeIndeterminate;
    loadingView.bezelView.color = [UIColor colorWithHex:@"#666666"];
    loadingView.bezelView.style = MBProgressHUDBackgroundStyleSolidColor;
    loadingView.label.text = text;
    loadingView.label.font = [UIFont systemFontOfSize:14 weight:UIFontWeightRegular];
    loadingView.label.textColor = [UIColor whiteColor];
    loadingView.noRespondArea = rect;
    if (hadExist) {
        ZBYMBProgressView * progressView = [self progressView:superView];
        [progressView hideAnimated:NO afterDelay:0];
    }
    [loadingView hideAnimated:animated afterDelay:0];
    return loadingView;
}

+ (ZBYMBProgressView *)showCustomLoadingTo:(UIView *)superView text:(NSString *)text noRespondArea:(CGRect)rect perDuration:(NSTimeInterval)duration animated:(BOOL)animated {
    BOOL hadExist = [self hadExist:superView];
    NSMutableArray<UIImage *> * imageList = [[NSMutableArray alloc] initWithCapacity:10];
    for (NSInteger i = 1; i < 7; i++) {
        NSString * imageName = [NSString stringWithFormat:@"loading_icon_%ld", (long)i];
        UIImage * image = [UIImage imageNamed:imageName];
        [imageList addObject:image];
    }
    ZBYMBProgressView * loadingView = [ZBYMBProgressView showCustomLoadingTo:superView text:text imageList:imageList perDuration:duration animated:animated];
    loadingView.userInteractionEnabled = YES;
    loadingView.noRespondArea = rect;
    if (hadExist) {
        ZBYMBProgressView * progressView = [self progressView:superView];
        [progressView hideAnimated:NO afterDelay:0];
    }
    return loadingView;
}

@end
