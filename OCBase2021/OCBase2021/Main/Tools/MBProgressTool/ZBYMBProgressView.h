//
//  ZBYMBProgressView.h
//  OCBase2021
//
//  Created by 周保勇 on 2021/4/27.
//

#import <UIKit/UIKit.h>
#import <MBProgressHUD.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZBYMBProgressView : MBProgressHUD

/// 不响应事件区域：由父控件响应事件；例如加载中时，导航栏的返回事件
@property (nonatomic, assign) CGRect noRespondArea;

/// 自定义加载中视图
/// @param superView 父视图
/// @param text 显示文本
/// @param imageList 加载中动画图片列表
/// @param duration 每个图片动画的时间
/// @param animated 是否动画添加到父视图中
+ (ZBYMBProgressView *)showCustomLoadingTo:(UIView *)superView text:(NSString *)text imageList:(NSArray<UIImage *> *)imageList perDuration:(CGFloat)duration animated:(BOOL)animated;

@end

NS_ASSUME_NONNULL_END
