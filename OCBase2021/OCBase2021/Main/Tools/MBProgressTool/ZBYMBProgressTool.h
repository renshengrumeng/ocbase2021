//
//  ZBYMBProgressTool.h
//  OCBase2021
//
//  Created by 周保勇 on 2021/4/27.
//

#import <Foundation/Foundation.h>
#import "ZBYMBProgressView.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZBYMBProgressTool : NSObject

+ (ZBYMBProgressView *)showText:(NSString *)text animated:(BOOL)animated;

@end

NS_ASSUME_NONNULL_END
