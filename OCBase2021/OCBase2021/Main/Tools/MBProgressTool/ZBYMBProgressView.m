//
//  ZBYMBProgressView.m
//  OCBase2021
//
//  Created by 周保勇 on 2021/4/27.
//

#import "ZBYMBProgressView.h"

@interface ZBYMBProgressView ()



@end

@implementation ZBYMBProgressView

- (instancetype)init {
    self = [super init];
    if (self) {
        self.noRespondArea = CGRectZero;
    }
    return self;
}

- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event {
    if (CGRectContainsPoint(self.noRespondArea, point)) {
        return YES;
    }
    else {
        return NO;
    }
}

+ (ZBYMBProgressView *)showCustomLoadingTo:(UIView *)superView text:(NSString *)text imageList:(NSArray<UIImage *> *)imageList perDuration:(NSTimeInterval)duration animated:(BOOL)animated {
    ZBYMBProgressView * progressView = [ZBYMBProgressView showHUDAddedTo:superView animated:animated];
    UIImageView * imageView = [[UIImageView alloc] initWithImage:imageList.firstObject];
    imageView.animationImages = imageList;
    imageView.animationDuration = duration * imageList.count;
    [imageView startAnimating];
    progressView.label.text = text;
    progressView.customView = imageView;
    progressView.mode = MBProgressHUDModeCustomView;
    progressView.removeFromSuperViewOnHide = YES;
    progressView.animationType = MBProgressHUDAnimationFade;
    progressView.bezelView.color = [UIColor clearColor];
    progressView.bezelView.style = MBProgressHUDBackgroundStyleSolidColor;
    progressView.backgroundColor = [UIColor clearColor];
    return progressView;
}



@end
