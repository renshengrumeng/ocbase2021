//
//  MacroDefinition.h
//  OC-framework
//
//  Created by 周保勇 on 2020/7/3.
//  Copyright © 2020 周保勇. All rights reserved.
//

#ifndef MacroDefinition_h
#define MacroDefinition_h

// MARK: - 打印

#ifdef DEBUG    // 测试阶段
//#define NSLog(FORMAT, ...) fprintf(stderr,"%s:%d行 %s\n", __PRETTY_FUNCTION__, __LINE__, [[NSString stringWithFormat:FORMAT, ##__VA_ARGS__] UTF8String]);

#define NSLog(FORMAT, ...) fprintf(stderr,"[%s:%d行] %s\n",[[[NSString stringWithUTF8String:__FILE__] lastPathComponent] UTF8String], __LINE__, [[NSString stringWithFormat:FORMAT, ##__VA_ARGS__] UTF8String]);
#else           // 使用阶段

#endif

#endif /* MacroDefinition_h */
