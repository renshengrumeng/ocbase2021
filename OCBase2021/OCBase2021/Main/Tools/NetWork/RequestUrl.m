//
//  RequestUrl.m
//  OC-framework
//
//  Created by 周保勇 on 2020/7/3.
//  Copyright © 2020 周保勇. All rights reserved.
//

#import "RequestUrl.h"

@interface RequestUrl()

@end

@implementation RequestUrl

// MARK: - 服务器相关

/// 服务器
NSString * const kServer = @"https://www.baicu.com";
/// 接口域名后缀
NSString * const kSubPath = @"/api";
/// 接口版本号
NSString * const kVersion = @"/v1";

/// 请求的完整的url
/// @param server 服务器域名
/// @param subPath 子路径
/// @param version 版本号
/// @param suffixPath 路径后缀
+ (NSString *)fullUrlWithServer:(NSString *)server subPath:(NSString *)subPath version:(NSString *)version suffixPath:(NSString *)suffixPath {
    return [[NSString alloc] initWithFormat:@"%@%@%@%@", server, subPath, version, suffixPath];
}

/// 请求的完整的url
/// @param suffixPath 路径后缀
+ (NSString *)fullUrlWithSuffixPath:(NSString *)suffixPath {
    return [self fullUrlWithServer:kServer subPath:kSubPath version:kVersion suffixPath:suffixPath];
}


// MARK: - 启动模块

NSString * const kSystemInterfacePrefix = @"/System";

+ (NSString *)kSystemLaunchUrl {
    return [RequestUrl fullUrlWithSuffixPath:[[NSString alloc] initWithFormat:@"%@/launchUrl", kSystemInterfacePrefix]];
}

+ (NSString *)kSystemCodeUrl {
    return [RequestUrl fullUrlWithSuffixPath:[[NSString alloc] initWithFormat:@"%@/sendSms", kSystemInterfacePrefix]];
}

@end
