//
//  AFNetworkingTool.h
//  OC-framework
//
//  Created by 周保勇 on 2020/7/3.
//  Copyright © 2020 周保勇. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>

typedef enum : NSUInteger {
    AFNetworkingToolRequestTypeGet      = 0,
    AFNetworkingToolRequestTypeHead     = 1,
    AFNetworkingToolRequestTypePost     = 2,
    AFNetworkingToolRequestTypePut      = 3,
    AFNetworkingToolRequestTypePatch    = 4,
    AFNetworkingToolRequestTypeDelete   = 5,
} AFNetworkingToolRequestType;

typedef NS_ENUM(NSInteger, AFNetworkingToolNetworkType) {
    AFNetworkingToolNetworkTypeUnknown          = -1,   // 未知网络
    AFNetworkingToolNetworkTypeNotReachable     = 0,    // 无网络
    AFNetworkingToolNetworkTypeReachableViaWWAN = 1,    // 蜂窝网络
    AFNetworkingToolNetworkTypeReachableViaWiFi = 2,    // WiFi网络
};

typedef void(^AFNetworkingToolNetworkSuccess)(NSURLSessionDataTask * _Nullable task, id _Nullable responseObject);
typedef void(^AFNetworkingToolNetworkFailure)(NSURLSessionDataTask * _Nullable task, NSError * _Nullable error);
typedef void(^AFNetworkingToolNetworkProgress)(NSProgress * _Nullable uploadProgress);
typedef void(^AFNetworkingToolNetworkStatusChangedBlock)(AFNetworkingToolNetworkType status);

NS_ASSUME_NONNULL_BEGIN

@interface AFNetworkingTool : NSObject

/// 开启网络指示器，开始监听
+ (void)startMonitoring;

/// 网络指示器，关闭监听
+ (void)stopMonitoring;

/// 当前网络环境类型
+ (AFNetworkingToolNetworkType)getCurrentNetworkType;

/// 网络状态变动回调
/// @param changedBlock 回调
+ (void)networkStatusChanged:(AFNetworkingToolNetworkStatusChangedBlock _Nonnull )changedBlock;

/// 网络GET请求
+ (void)GET:(NSString *)URLString parameters:(id)parameters success:(void (^)(NSURLSessionDataTask * _Nullable task, id _Nullable responseObject))success failure:(void (^)(NSURLSessionDataTask * _Nullable task, NSError * _Nullable error))failure;

/// 网络带进度的POST请求
+ (void)POST:(NSString *)URLString parameters:(nullable id)parameters progress:(nullable void (^)(NSProgress *uploadProgress))progress success:(nullable void (^)(NSURLSessionDataTask *task, id _Nullable responseObject))success failure:(nullable void (^)(NSURLSessionDataTask * _Nullable task, NSError *error))failure;

/// 网络POST请求
+ (void)POST:(NSString *)URLString parameters:(nullable id)parameters success:(nullable void (^)(NSURLSessionDataTask *task, id _Nullable responseObject))success failure:(nullable void (^)(NSURLSessionDataTask * _Nullable task, NSError *error))failure;

/// 上传文件请求
+ (void)upload:(NSString *)URLString parameters:(nullable id)parameters data:(NSData *)data fileName:(NSString *)fileName mimeType:(NSString *)mimeType progress:(nullable void (^)(NSProgress *uploadProgress))progress success:(nullable void (^)(NSURLSessionDataTask *task, id _Nullable responseObject))success failure:(nullable void (^)(NSURLSessionDataTask * _Nullable task, NSError *error))failure;

/// 上传MP4文件请求
+ (void)uploadMP4:(NSString *)URLString parameters:(nullable id)parameters data:(NSData *)data progress:(nullable void (^)(NSProgress *uploadProgress))progress success:(nullable void (^)(NSURLSessionDataTask *task, id _Nullable responseObject))success failure:(nullable void (^)(NSURLSessionDataTask * _Nullable task, NSError *error))failure;

/// 上传jpeg图片文件请求
+ (void)uploadImage:(NSString *)URLString parameters:(nullable id)parameters data:(NSData *)data progress:(nullable void (^)(NSProgress *uploadProgress))progress success:(nullable void (^)(NSURLSessionDataTask *task, id _Nullable responseObject))success failure:(nullable void (^)(NSURLSessionDataTask * _Nullable task, NSError *error))failure;

@end

NS_ASSUME_NONNULL_END
