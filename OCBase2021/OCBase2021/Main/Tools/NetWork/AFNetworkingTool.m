//
//  AFNetworkingTool.m
//  OC-framework
//
//  Created by 周保勇 on 2020/7/3.
//  Copyright © 2020 周保勇. All rights reserved.
//

#import "AFNetworkingTool.h"
#import <AFNetworking.h>

@interface AFNetworkingTool()

@property (nonatomic, copy, class, readonly) NSDictionary * header;
@property (nonatomic, copy, class, readonly) NSDictionary * uploadHeader;
@property (nonatomic, copy, class, readonly) AFHTTPSessionManager * manager;

@end

@implementation AFNetworkingTool

// MARK: - class getter

+ (AFHTTPSessionManager *)manager {
    AFHTTPSessionManager * manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer willChangeValueForKey:@"timeoutInterval"];
    manager.requestSerializer.timeoutInterval = 15.0f;
    [manager.requestSerializer didChangeValueForKey:@"timeoutInterval"];
//  manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", @"text/plain", nil];
    return manager;
}

+ (NSDictionary *)header {
    return @{
        @"Authorization" : @"anniekids",    //
        @"Accept" : @"application/json",  // 接受数据类型
        @"Content-Type" : @"application/x-www-form-urlencoded", // 请求和响应中的媒体类型信息
        @"xsn" : UIDevice.currentDevice.identifierForVendor.UUIDString, // 设备id：UUID
        @"osmodel" : UIDevice.currentDevice.model, // 设备类型：iphone7，iphoneX
        @"osversion" : UIDevice.currentDevice.systemVersion, // 设备操作系统版本号
        @"xclient" : @"iOS",  // 大类设备类型：iOS和android
        @"xversion" : NSBundle.mainBundle.infoDictionary[@"CFBundleShortVersionString"],  // 设备安装app的版本号
        @"apiKey" : @"123"  // 服务器验证信息
    };
}

+ (NSDictionary *)uploadHeader {
    return @{@"content-type" : @"multipart/form-data"};
}

// MARK: - public methods

+ (void)GET:(NSString *)URLString parameters:(id)parameters success:(void (^)(NSURLSessionDataTask * _Nullable task, id _Nullable responseObject))success failure:(void (^)(NSURLSessionDataTask * _Nullable task, NSError * _Nullable error))failure {
    [self dataTaskWithHTTPMethod:AFNetworkingToolRequestTypeGet URLString:URLString parameters:parameters headers:self.header uploadProgress:nil downloadProgress:nil success:success failure:failure];
}

+ (void)POST:(NSString *)URLString parameters:(nullable id)parameters progress:(nullable void (^)(NSProgress *uploadProgress))progress success:(nullable void (^)(NSURLSessionDataTask *task, id _Nullable responseObject))success failure:(nullable void (^)(NSURLSessionDataTask * _Nullable task, NSError *error))failure {
    [self dataTaskWithHTTPMethod:AFNetworkingToolRequestTypePost URLString:URLString parameters:parameters headers:self.header uploadProgress:progress downloadProgress:nil success:success failure:failure];
}

+ (void)POST:(NSString *)URLString parameters:(id)parameters success:(void (^)(NSURLSessionDataTask * _Nullable task, id _Nullable responseObject))success failure:(void (^)(NSURLSessionDataTask * _Nullable task, NSError * _Nullable error))failure {
    [self dataTaskWithHTTPMethod:AFNetworkingToolRequestTypePost URLString:URLString parameters:parameters headers:self.header uploadProgress:nil downloadProgress:nil success:success failure:failure];
}

+ (void)uploadImage:(NSString *)URLString parameters:(nullable id)parameters data:(NSData *)data progress:(nullable void (^)(NSProgress *uploadProgress))progress success:(nullable void (^)(NSURLSessionDataTask *task, id _Nullable responseObject))success failure:(nullable void (^)(NSURLSessionDataTask * _Nullable task, NSError *error))failure {
    [self upload:URLString parameters:parameters data:data fileName:@"image.jpeg" mimeType:@"image/jpeg" progress:progress success:success failure:failure];
}

+ (void)uploadMP4:(NSString *)URLString parameters:(nullable id)parameters data:(NSData *)data progress:(nullable void (^)(NSProgress *uploadProgress))progress success:(nullable void (^)(NSURLSessionDataTask *task, id _Nullable responseObject))success failure:(nullable void (^)(NSURLSessionDataTask * _Nullable task, NSError *error))failure {
    [self upload:URLString parameters:parameters data:data fileName:@"video.mp4" mimeType:@"video/mp4" progress:progress success:success failure:failure];
}

+ (void)upload:(NSString *)URLString parameters:(nullable id)parameters data:(NSData *)data fileName:(NSString *)fileName mimeType:(NSString *)mimeType progress:(nullable void (^)(NSProgress *uploadProgress))progress success:(nullable void (^)(NSURLSessionDataTask *task, id _Nullable responseObject))success failure:(nullable void (^)(NSURLSessionDataTask * _Nullable task, NSError *error))failure {
    [self uploadData:URLString parameters:parameters headers:self.uploadHeader constructingBodyWithBlock:^(id<AFMultipartFormData> _Nonnull formData) {
        // 采用时间来防止名字重复
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        formatter.dateFormat = @"yyyy-MM-dd-HH:mm:ss";
        NSString * stampStr = [formatter stringFromDate:[NSDate date]];
        NSString * fileFullName = [NSString stringWithFormat:@"%@_%@", stampStr, fileName];
        /*
        1. name:指服务器获取该图片所用的“键”的名字
        2. fileName:文件在服务器上保存的名字
        3. fileName:文件的类型。例如：.png(image/png)、.jpg/.jpeg(image/jpeg)、.txt(text/plain) 、.mp3(audio/mpeg)、.mp4(audio/mp4, video/mp4)、.json(application/json)等
         其它文件可以参考：文件MIME_type对照表
        */
        [formData appendPartWithFileData:data name:@"file" fileName:fileFullName mimeType:mimeType];
    } progress:progress success:success failure:failure];
}

+ (AFNetworkingToolNetworkType)getCurrentNetworkType {
    AFNetworkReachabilityStatus status = [AFNetworkReachabilityManager sharedManager].networkReachabilityStatus;
    return (AFNetworkingToolNetworkType)status;
}

+ (void)networkStatusChanged:(AFNetworkingToolNetworkStatusChangedBlock _Nonnull )changedBlock {
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        changedBlock((AFNetworkingToolNetworkType)status);
    }];
}

/// 开启网络指示器，开始监听
+ (void)startMonitoring {
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
}

/// 网络指示器，关闭监听
+ (void)stopMonitoring {
    [[AFNetworkReachabilityManager sharedManager] stopMonitoring];
}

// MARK: - private methods

+ (NSURLSessionDataTask *_Nullable)dataTaskWithHTTPMethod:(AFNetworkingToolRequestType)type URLString:(NSString *)URLString parameters:(nullable id)parameters headers:(nullable NSDictionary <NSString *, NSString *> *)headers uploadProgress:(nullable void (^)(NSProgress *uploadProgress))uploadProgress downloadProgress:(nullable void (^)(NSProgress *downloadProgress))downloadProgress success:(nullable void (^)(NSURLSessionDataTask *task, id _Nullable responseObject))success failure:(nullable void (^)(NSURLSessionDataTask * _Nullable task, NSError *error))failure {
    NSString * methodType = @"GET";
    switch (type) {
        case AFNetworkingToolRequestTypeGet:
            methodType = @"GET";
            break;
        case AFNetworkingToolRequestTypeHead:
            methodType = @"Head";
            break;
        case AFNetworkingToolRequestTypePost:
            methodType = @"POST";
            break;
        case AFNetworkingToolRequestTypePut:
            methodType = @"PUT";
            break;
        case AFNetworkingToolRequestTypePatch:
            methodType = @"PATCH";
            break;
        case AFNetworkingToolRequestTypeDelete:
            methodType = @"DELETE";
            break;
        default:
            break;
    }
    NSURLSessionDataTask * dataTask = [self.manager dataTaskWithHTTPMethod:methodType URLString:URLString parameters:parameters headers:headers uploadProgress:uploadProgress downloadProgress:downloadProgress success:success failure:failure];
    return dataTask;
}

+ (void)uploadData:(NSString *)URLString parameters:(nullable id)parameters headers:(nullable NSDictionary<NSString *,NSString *> *)headers constructingBodyWithBlock:(nullable void (^)(id<AFMultipartFormData> _Nonnull))block progress:(nullable void (^)(NSProgress * _Nonnull))uploadProgress success:(nullable void (^)(NSURLSessionDataTask * _Nonnull, id _Nullable))success failure:(void (^)(NSURLSessionDataTask * _Nullable, NSError * _Nonnull))failure {
    [self.manager POST:URLString parameters:parameters headers:headers constructingBodyWithBlock:block progress:uploadProgress success:success failure:failure];
}

@end
