//
//  RequestUrl.h
//  OC-framework
//
//  Created by 周保勇 on 2020/7/3.
//  Copyright © 2020 周保勇. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface RequestUrl : NSObject

/// 请求的完整的url
/// @param server 服务器域名
/// @param subPath 子路径
/// @param version 版本号
/// @param suffixPath 路径后缀
+ (NSString *)fullUrlWithServer:(NSString *)server subPath:(NSString *)subPath version:(NSString *)version suffixPath:(NSString *)suffixPath;

/// 请求的完整的url
/// @param suffixPath 路径后缀
+ (NSString *)fullUrlWithSuffixPath:(NSString *)suffixPath;

/// 启动页接口
@property (nonatomic, copy, class, readonly) NSString * kSystemLaunchUrl;
/// 获取验证码接口
@property (nonatomic, copy, class, readonly) NSString * kSystemCodeUrl;

@end

NS_ASSUME_NONNULL_END
