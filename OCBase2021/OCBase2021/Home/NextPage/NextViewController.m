//
//  NextViewController.m
//  OCBase2021
//
//  Created by 周保勇 on 2021/4/7.
//

#import "NextViewController.h"
#import "UIView+Controller.h"

@interface NextViewController ()

@end

@implementation NextViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.isLeftBack = YES;
    
    UIView * subView = [UIView new];
    subView.frame = CGRectMake(100, 200, 100, 100);
    [self.view addSubview:subView];
    subView.backgroundColor = [[UIColor greenColor] colorWithAlpha:0.3];
//    subView.backgroundColor = [UIColor colorWithHex:@"#DC143C"];
    NSLog(@"%@", subView.backgroundColor.hex);
    NSLog(@"%@", subView.controller);
    NSLog(@"%@", subView.inputViewController);
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
