//
//  ZBYToolsViewController.m
//  OCBase2021
//
//  Created by anniekids good on 2023/9/28.
//

#import "ZBYToolsViewController.h"

@interface ZBYToolsViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, copy) NSArray * models;

@end

@implementation ZBYToolsViewController

- (NSArray *)models {
    if(_models == nil) {
        _models = @[
            @{@"title" : @"Mach-O分析工具", @"subTitle" : @"MachOView"},
            @{@"title" : @"OC崩溃分析工具", @"subTitle" : @"dSYMTools"},
            @{@"title" : @"iOS面试题", @"subTitle" : @"面试题"},
            @{@"title" : @"Hosts管理工具", @"subTitle" : @"SwitchHosts"},
            @{@"title" : @"Github加速神器", @"subTitle" : @"Github520"},
            @{@"title" : @"流程图绘制工具", @"subTitle" : @"draw.io"},
            @{@"title" : @"Markdown语法", @"subTitle" : @"Markdown demo"},
            @{@"title" : @"OpenCore Legacy Patcher", @"subTitle" : @"旧版Mac支持最新系统"},];
    }
    return _models;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITableView * tableView = [[UITableView alloc] initWithFrame:CGRectZero style:(UITableViewStylePlain)];
//    tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    tableView.delegate = self;
    tableView.dataSource = self;
    [self.view addSubview:tableView];
    
    tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(0);
    }];
    NSLog(@"%@", [BaseViewController subClassList]);
}

// MARK: - UITableViewDelegate, UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.models.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString * identifier = @"123456";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:identifier];
        cell.backgroundColor = [UIColor whiteColor];
        cell.textLabel.textColor = [UIColor blackColor];
        cell.detailTextLabel.textColor = [UIColor blackColor];
    }
    cell.textLabel.text = self.models[indexPath.row][@"title"];
    cell.detailTextLabel.text = self.models[indexPath.row][@"subTitle"];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSDictionary * info = self.models[indexPath.row];
    NSString * title = info[@"title"];
    if ([title containsString:@"Mach-O分析工具"]) {
        // 工具下载：https://github.com/gdbinit/MachOView
        [ZBYMBProgressTool showText:@"MachOView" animated:true];
    }
    if ([title containsString:@"OC崩溃分析工具"]) {
        // 工具下载：https://github.com/answer-huang/dSYMTools
        // 工具使用：https://www.jianshu.com/p/2493f6f3db6d
        [ZBYMBProgressTool showText:@"dSYMTools" animated:true];
    }
    if ([title containsString:@"iOS面试题"]) {
        // https://juejin.cn/post/6844904064937902094
    }
    if ([title containsString:@"Hosts管理工具"]) {
        // Github地址：https://github.com/oldj/SwitchHosts
        // 下载链接：https://swh.app/zh/
        [ZBYMBProgressTool showText:@"SwitchHosts" animated:true];
    }
    if ([title containsString:@"Github加速神器"]) {
        // Github地址：https://github.com/521xueweihan/GitHub520
    // 工具使用：https://www.cnblogs.com/xiaodongxier/p/tui-jian-yi-gegithub-guo-nei-fang-wen-jia-su-shen.html
        [ZBYMBProgressTool showText:@"Github520" animated:true];
    }
    if ([title containsString:@"流程图绘制工具"]) {
        /**
         在线编辑链接：
             https://app.diagrams.net
             https://draw.io
         下载链接：
             https://github.com/jgraph/drawio-desktop
             https://github.com/jgraph/drawio-desktop/releases
         介绍链接：
             https://zhuanlan.zhihu.com/p/220183321
         */
        [ZBYMBProgressTool showText:@"draw.io" animated:true];
    }
    if ([title containsString:@"Markdown语法"]) {
        // 例子地址：https://markdown-it.github.io
        [ZBYMBProgressTool showText:@"Markdown demo" animated:true];
    }
    if ([title containsString:@"OpenCore Legacy Patcher"]) {
        // 官网地址：https://sysin.org
        // https://sysin.org/blog/install-macos-13-on-unsupported-mac/
        // 介绍链接：https://zhuanlan.zhihu.com/p/577624296
        [ZBYMBProgressTool showText:@"Markdown demo" animated:true];
    }
}

@end
