//
//  HomeViewController.m
//  OCBase2021
//
//  Created by 周保勇 on 2021/4/7.
//

#import "HomeViewController.h"
#import "NextViewController.h"
#import "LoginViewController.h"
#import "ZBYToolsViewController.h"

@interface HomeViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, copy) NSArray * models;

@end

@implementation HomeViewController

- (NSArray *)models {
    if(_models == nil) {
        _models = @[
            @{@"title" : @"push跳转下一页", @"subTitle" : @"跳转下一页"},
            @{@"title" : @"present跳转下一页", @"subTitle" : @"跳转下一页"},
            @{@"title" : @"工具介绍", @"subTitle" : @"代码工具、开发工具、工具链接"}];
    }
    return _models;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // 添加占位视图
//    [self.view addSubview:[[UIView alloc] initWithFrame:CGRectMake(0, 0, 100, 100)]];
    
    UITableView * tableView = [[UITableView alloc] initWithFrame:CGRectZero style:(UITableViewStylePlain)];
    tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    tableView.delegate = self;
    tableView.dataSource = self;
    [self.view addSubview:tableView];
    
    tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(ZBYSize.navigationBarHeight);
        make.left.right.bottom.mas_equalTo(0);
    }];
    NSLog(@"%@", [BaseViewController subClassList]);
}

// MARK: - UITableViewDelegate, UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.models.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString * identifier = @"123456";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.backgroundColor = [UIColor whiteColor];
        cell.textLabel.textColor = [UIColor blackColor];
        cell.detailTextLabel.textColor = [UIColor blackColor];
    }
    cell.textLabel.text = self.models[indexPath.row][@"title"];
    cell.detailTextLabel.text = self.models[indexPath.row][@"subTitle"];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSDictionary * info = self.models[indexPath.row];
    NSString * title = info[@"title"];
    if ([title containsString:@"push跳转下一页"]) {
        [self.navigationController pushViewController:[NextViewController new] animated:YES];
    }
    if ([title containsString:@"present跳转下一页"]) {
        [self presentViewController:[LoginViewController new] animated:YES completion:nil];
    }
    if ([title containsString:@"工具介绍"]) {
        ZBYToolsViewController * toolsVC = [ZBYToolsViewController new];
        toolsVC.title = title;
        [self.navigationController pushViewController:toolsVC animated:true];
    }
}

@end
