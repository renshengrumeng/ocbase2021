//
//  LaunchViewController.m
//  OCBase2021
//
//  Created by 周保勇 on 2021/4/8.
//

#import "LaunchViewController.h"
#import "BaseTabBarViewController.h"

@interface LaunchViewController ()

@end

@implementation LaunchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    UIImageView * imageView = [UIImageView new];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    imageView.frame = self.view.bounds;
    imageView.image = [UIImage imageNamed:@"launch_icon"];
    [self.view addSubview:imageView];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [[UIApplication sharedApplication] currentWindow].rootViewController = [BaseTabBarViewController new];
}

@end
